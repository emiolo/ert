<?php

namespace Modules\CerebeloRepresentatives\Http\Controllers;

use Modules\CerebeloSettings\Http\Controllers\BaseController as Controller;

class CerebeloController extends Controller {

    const MODULE_VIEW = "cerebelorepresentatives::";

}
