<?php

namespace Modules\CerebeloRepresentatives\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\CerebeloRepresentatives\Http\Controllers\CerebeloController as Controller;
use Datatables;
use Modules\CerebeloRepresentatives\Entities\Representative;
use Modules\CerebeloRepresentatives\Http\Requests\RepresentativeRequest;
use Modules\CerebeloSettings\Entities\Address;
use JsValidator;

class RepresentativesController extends Controller
{

    const FOLDER = "representatives.";

    protected $_representative;
    protected $_address;

    public function __construct(Representative $representative, Address $address)
    {
        $this->_representative = $representative;
        $this->_address        = $address;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view(self::MODULE_VIEW . self::FOLDER . 'index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $validator = JsValidator::formRequest(new RepresentativeRequest, '#formCreateRepresentative');

        return view(self::MODULE_VIEW . self::FOLDER . 'create', compact('validator'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(RepresentativeRequest $request)
    {
        $attributes = $request->all();

        if ($attributes['person_type'] == 'PF')
        {
            $attributes['fantasy_name'] = $attributes['name'];
        }

        $create = $this->_representative->create($attributes);

        if ($create)
        {
            $address = new $this->_address();
            $address->fill($attributes);
            $create->address()->save($address);

            return $this->redirectBackAfterSuccess(trans('messages.success.create'));
        }

        return $this->redirectBackAfterError(trans('messages.error.create'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        //return view(self::MODULE_VIEW . self::FOLDER . 'show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $validator = JsValidator::formRequest(new RepresentativeRequest, '#formEditRepresentative');
        $model = $this->_representative->with('address')->find($id);

        return view(self::MODULE_VIEW . self::FOLDER . 'edit', compact('model', 'validator'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(RepresentativeRequest $request, $id)
    {
        $attributes = $request->all();

        $update = $this->_representative->find($id);

        if ($attributes['person_type'] == 'PF')
        {
            $attributes['fantasy_name'] = $attributes['name'];
        }

        $update->fill($attributes);

        if ($update->save())
        {
            if (isset($attributes['address_id']))
            {
                $address = $this->_address->find($attributes['address_id']);
            }
            else
            {
                $address = new $this->_address();
            }
            $address->fill($attributes);
            $update->address()->save($address);

            return $this->redirectAfterSuccess(trans('messages.success.update'), 'cerebelo.representatives.index');
        }

        return $this->redirectBackAfterError(trans('messages.error.update'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->_representative->destroy($id))
        {
            return $this->redirectBackAfterSuccess(trans('messages.success.destroy'));
        }

        return $this->redirectBackAfterError(trans('messages.error.destroy'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function datatable()
    {
        return Datatables::of($this->_representative->datatable())
                        ->addColumn('btnList', function ($object) {
                            $array = [
                                'edit'    => [
                                    'route' => route('cerebelo.representatives.edit', $object->id),
                                ],
                                'destroy' => [
                                    'route' => route('cerebelo.representatives.destroy', $object->id),
                                ],
                            ];
                            return iconsList($array);
                        })
                        ->escapeColumns([])
                        ->make(true);
    }

}
