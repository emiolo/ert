<?php

Route::group(['middleware' => ['web', 'cerebelo.authenticated'], 'prefix' => 'cerebelo', 'namespace' => 'Modules\CerebeloRepresentatives\Http\Controllers'], function() {

    Route::group(['middleware' => ['cerebelo.permission']], function() {

        Route::group(['prefix' => 'representatives'], function() {
            Route::get('/', 'RepresentativesController@index')->name('cerebelo.representatives.index');
            Route::get('/listing', 'RepresentativesController@datatable')->name('cerebelo.representatives.datatable');
            Route::get('/create', 'RepresentativesController@create')->name('cerebelo.representatives.create');
            Route::post('/', 'RepresentativesController@store')->name('cerebelo.representatives.store');
            //Route::get('/show/{id}', 'RepresentativesController@show')->name('cerebelo.representatives.show');
            Route::get('/edit/{id}', 'RepresentativesController@edit')->name('cerebelo.representatives.edit');
            Route::match(['put', 'patch'], '/update/{id}', 'RepresentativesController@update')->name('cerebelo.representatives.update');
            Route::delete('/delete/{id}', 'RepresentativesController@destroy')->name('cerebelo.representatives.destroy');
        });
    });
});
