<?php

namespace Modules\CerebeloRepresentatives\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RepresentativeRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'percentage' => 'required|digits_between:1,3|integer',
            'person_type' => 'required',
            'cpf_cnpj' => 'required|max:18|cpf_cnpj|unique:representatives,cpf_cnpj' . ((isset($this->id) && !empty($this->id)) ? ',' . $this->id : ''),
            //'state_registration' => 'required_if:person_type,PJ|max:17',
            'company_name' => 'required_if:person_type,PJ|max:150',
            'fantasy_name' => 'max:150',
            //'fantasy_name' => 'required_if:person_type,PJ|max:150',
            'name' => 'required_if:person_type,PF|max:150',
            'rg' => 'required_if:person_type,PF|max:18',
            'phone' => 'required|max:15|phone_with_ddd',
            'email' => 'required|email|max:150',
            'cep' => 'required|max:9',
            'street' => 'required|max:250',
            'number' => 'required|max:50',
            'neighborhood' => 'required|max:150',
            'city' => 'required|max:150',
            'state' => 'required|max:2',
            'prefix' => 'alpha|max:4',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

}
