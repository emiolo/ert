<?php

namespace Modules\CerebeloRepresentatives\Entities;

use Modules\CerebeloSettings\Entities\BaseModel as Model;
use Modules\CerebeloSettings\Entities\Address;

class Representative extends Model
{

    public $table       = 'representatives';
    protected $fillable = [
        'percentage',
        'cpf_cnpj',
        'state_registration',
        'prefix',
        'company_name',
        'fantasy_name',
        'rg',
        'phone',
        'phone2',
        'email',
    ];
    protected $appends  = ['name'];

    public static function boot()
    {
        parent::boot();
        static::deleting(function($model) {
            $model->address()->delete();
        });
    }

    /*
     * Attributes
     */
    /*
     * Gets/Sets
     */

    public function getNameAttribute()
    {
        return (empty($this->attributes['company_name']) ? $this->attributes['fantasy_name'] : $this->attributes['company_name']);
    }

    /*
     * Relationships
     */

    public function address()
    {
        return $this->hasOne(Address::class, 'table_id')->where('table', $this->table);
    }

    /*
     * Scopes
     */

    /*
     * Ohters
     */

    /*
     * Retorna todos os representantes
     * @return array
     */

    public static function formSelect()
    {
        $return = self::pluck('fantasy_name', 'id');
        return $return;
    }

    public static function datatable()
    {
        return self::select("*");
    }

}
