var correiosCep = document.getElementById("correios-cep").getAttribute("correios-cep");

$("input[name^=cep]").on('blur', function (e) {
    var val = this.value;
    if (val !== '') {
        $.blockUI({message: '<h4>buscando cep...</h4>'});
        $.ajax({
            dataType: "json",
            type: "GET",
            url: correiosCep + '/' + val
        }).done(function (response) {
            var street = response.street;
            var neighborhood = response.neighborhood;
            var city = response.city;
            var state = response.state;
            if (city !== '') {
                $("input[name=street]").val(street);
                $("input[name=neighborhood]").val(neighborhood);
                $("input[name=city]").val(city);
                $("input[name=state]").val(state);
            } else {
                $("input[name=cep]").val("");
                $("input[name=street]").val("");
                $("input[name=neighborhood]").val("");
                $("input[name=city]").val("");
                $("input[name=state]").val("");
            }
        }).error(function (response) {
            new PNotify({
                title: 'Atenção',
                text: 'Erro interno.',
                type: 'error',
                icon: 'fa fa-ban'
            });
        });
    }
});

var cpf_cnpj = $("input[name='cpf_cnpj']");
var cpf_cnpj_val = cpf_cnpj.val().replace(/\D/g, '').length;

var person_type = $("select[name='person_type']");

person_type.val((cpf_cnpj_val === 0 || cpf_cnpj_val > 11) ? 'PJ' : 'PF');

personType(person_type.val());

person_type.on('change', function (e) {
    var v = this.value;
    personType(v);
});

function personType(v) {
    var class_pj = $(".person_type_pj");
    var class_pf = $(".person_type_pf");

    if (v === "PJ")
    {
        class_pj.show(0);
        class_pf.hide(0);

        cpf_cnpj.closest('.form-group').find('label').html('CNPJ <i class="text-danger">*</i>');
        cpf_cnpj.attr('maxlength', '18');
        cpf_cnpj.attr('placeholder', 'CNPJ');
        cpf_cnpj.mask("99.999.999/9999-99");
    } else
    {
        class_pf.show(0);
        class_pj.hide(0);

        cpf_cnpj.closest('.form-group').find('label').html('CPF <i class="text-danger">*</i>');
        cpf_cnpj.attr('maxlength', '14');
        cpf_cnpj.attr('placeholder', 'CPF');
        cpf_cnpj.mask("999.999.999-99");
    }

    var cpf_cnpj_val = cpf_cnpj.val().replace(/\D/g, '').length;
    if (cpf_cnpj_val < 11) {
        cpf_cnpj.val('');
    }
}