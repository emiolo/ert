var route = document.getElementById("script-route").getAttribute("data-route");

var oTable = $('#datatable').DataTable({
    "pageLength": 10,
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "columnDefs": [{"targets": [], "orderable": false}],
    "ajax": {
        "url": route
    },
    "order": [[4, "asc"]],
    "columns": [
        {data: 'btnList', name: 'btnList', orderable: false, searchable: false},
        {data: 'id', name: 'id'},
        {data: 'percentage', name: 'percentage'},
        {data: 'cpf_cnpj', name: 'cpf_cnpj'},
        {data: 'fantasy_name', name: 'fantasy_name'},
        {data: 'phone', name: 'phone'},
        {data: 'email', name: 'email'},
        {data: 'created_at', name: 'created_at'},
    ]
});