var routes = document.getElementById("route-orderqueue");
var routeDatatable = routes.getAttribute("datatable");
var routeStore = routes.getAttribute("store");
var routeUpdate = routes.getAttribute("update");
var $formSearchOrders = $('#formSearchOrders');
var queue = ((typeof($('input:hidden[name="queue"]', $formSearchOrders).val()) != 'undefined') ? {'queue': true} : {});

var options = {
    "paging": false,
    "pageLength": 10,
    "processing": true,
    "serverSide": true,
    //"stateSave": true,
    "columnDefs": [
        //{"targets": [], "orderable": false},
        { "visible": false, "targets": [5, 6, 7] }
    ],
    "ajax": {
        "url": routeDatatable + '?' + $formSearchOrders.serialize(),
        "type": 'GET',
        "data": queue
    },
    "order": [[1, "asc"]],
    "createdRow": function (row, data, dataIndex) {
        var newIndex = dataIndex + 1;
        $(row).attr('id', data.id);
        $(row).find('td:eq(1)').html(newIndex);

        if(data.delivery_status == true) {
            $(row).find('td:eq(0)')
                                .removeClass('border-left-danger')
                                .addClass('border-left-xlg border-left-info')
                                .removeAttr('title')
            ;
        } else {
            $(row).find('td:eq(0)')
                                .removeClass('border-left-info')
                                .addClass('border-left-xlg border-left-danger')
                                .attr('title', 'Entrega em Atraso')
            ;
        }
    },
    "columns": [
        { data: 'btnCheck', name: 'btnCheck', orderable: false, searchable: false },
        { data: 'sorting', name: 'sorting', searchable: true },
        { data: 'infoOrder', name: 'client.fantasy_name', searchable: true },
        { data: 'group', name: 'group', orderable: false },
        { data: 'qty_piece', name: 'qty_piece', orderable: false, searchable: false },
        { data: 'order_number', name: 'order_number', searchable: true },
        { data: 'client.company_name', name: 'client.company_name', searchable: true, defaultContent: '-' },
        { data: 'client.cpf_cnpj', name: 'client.cpf_cnpj', searchable: true, defaultContent: '-' },
        { data: 'delivery_date', name: 'delivery_date', orderable: false, searchable: false },
        //{ data: 'status', name: 'status', orderable: false, searchable: false },
        { data: 'eventDate', name: 'event_date', orderable: false, searchable: false },
        { data: 'wait_action', name: 'wait_action', orderable: false, searchable: false },
        { data: 'actions', name: 'actions', orderable: false, searchable: false },
    ],
    //rowReorder: true,
};
var oTable = $('#datatable').DataTable(options);

$('button[type="button"]', $formSearchOrders).on('click', function () {
    document.getElementById("formSearchOrders").reset();
    $('select', $formSearchOrders).select2("val", 0); // Para forçar limpar campos select2 caso tem valor pré-definido
    oTable.ajax.url(routeDatatable).load();
});
$formSearchOrders.on('submit', function () {
    var $this = $(this);
    var $formData = $this.serialize();

    oTable.ajax.url(routeDatatable + '?' + $formData).load();

    return false;
});

if (findGetParameter('q') !== "" && findGetParameter('q') !== null) {
    oTable.search(findGetParameter('q')).draw();
}

$("#datatable tbody").sortable({
    placeholder: "sortable-placeholder",
    start: function (e, ui) {
        ui.placeholder.height(ui.item.outerHeight());
    },
    update: function (event, ui) {
        var ids = $(this).sortable('toArray');

        if (ids.length > 0) {
            $.ajax({
                url: routeStore,
                type: 'POST',
                dataType: 'json',
                data: {
                    ids: ids
                },
                success: function (response) {
                    if (!response.error) {
                        oTable.ajax.url(routeDatatable + '?' + $formSearchOrders.serialize()).load();
                    }
                },
                errors: function (response) {

                },
            });
        }
    }
}).disableSelection();

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName)
                result = decodeURIComponent(tmp[1]);
        });
    return result;
}

$("#check_order").on('click', function (e) {
    var $this = $(this);
    $(".check_order").prop('checked', $this.is(':checked'));
});

$(document).ajaxStop(function () {

    $(".event_date").off('keypress').on('keypress', function (e) {
        if (e.which === 13) { // Salvar ao pressionar ENTER
            var $this = $(this);
            var id = $this.attr('id');
            var event_date = $this.val();

            $.ajax({
                url: routeUpdate + '/' + id,
                type: 'PUT',
                dataType: 'json',
                data: {
                    event_date: event_date,
                },
                success: function (response) {
                    if (response.error === true) {
                        new PNotify({
                            title: 'Atenção',
                            text: response.message,
                            addclass: 'bg-warning alert-styled-right'
                        });
                    } else {
                        new PNotify({
                            title: 'Sucesso',
                            text: response.message,
                            addclass: 'bg-success alert-styled-right'
                        });
                    }
                },
                error: function (response) {

                }
            });
        }
    });

});

function btnPrintSelected(e) {
    var url = $(e).attr('href');

    var ids = $(".check_order").map(function () {
        var $this = $(this);
        if ($this.is(':checked')) {
            return $this.val();
        }
    }).get().join(',');

    if (ids == null || ids == '') {
        new PNotify({
            title: 'Atenção',
            text: 'Nenhum item foi selecionado',
            addclass: 'bg-warning alert-styled-right'
        });
    } else {
        popUpWin(url + '/' + ids, 900, 1000);
    }
}

function btnFormFilter() {
    $('#modal-form-filter').modal('show');
}