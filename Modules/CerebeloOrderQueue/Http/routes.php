<?php

Route::group(['middleware' => ['web', 'cerebelo.authenticated'], 'prefix' => 'cerebelo', 'namespace' => 'Modules\CerebeloOrderQueue\Http\Controllers'], function() {

    Route::group(['middleware' => ['cerebelo.permission']], function() {

        Route::group(['prefix' => 'order-queue'], function() {
            Route::get('/', 'OrderQueueController@index')->name('cerebelo.orderqueue.index');
            Route::get('/listing', 'OrderQueueController@datatable')->name('cerebelo.orderqueue.datatable');
            Route::get('/print/{ids?}', 'OrderQueueController@printer')->name('cerebelo.orderqueue.printer');
            //Route::get('/create', 'OrderQueueController@create')->name('cerebelo.orderqueue.create');
            Route::post('/', 'OrderQueueController@store')->name('cerebelo.orderqueue.store');
            //Route::get('/show/{id}', 'OrderQueueController@show')->name('cerebelo.orderqueue.show');
            //Route::get('/edit/{id}', 'OrderQueueController@edit')->name('cerebelo.orderqueue.edit');
            Route::match(['put', 'patch'], '/update/{id?}', 'OrderQueueController@update')->name('cerebelo.orderqueue.update');
            //Route::delete('/delete/{id}', 'OrderQueueController@destroy')->name('cerebelo.orderqueue.destroy');
        });
    });
});
