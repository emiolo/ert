<?php

namespace Modules\CerebeloOrderQueue\Http\Controllers;

use Carbon\Carbon;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\CerebeloOrderQueue\Http\Controllers\CerebeloController as Controller;
use Modules\CerebeloOrders\Entities\Order;
use Modules\CerebeloOrders\Entities\OrderStatus;
use Modules\CerebeloSettings\Entities\Sector;
use mPDF;
use Storage;

class OrderQueueController extends Controller
{

    protected $_order;
    protected $_sector;
    protected $_order_status;
    protected $_mpdf;

    const FOLDER = "order_queue.";

    public function __construct()
    {
        $this->_order = new Order();
        $this->_sector = new Sector();
        $this->_order_status = new OrderStatus();
        $this->_mpdf = new mPDF();
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $sectors = $this->_sector->formSelect();
        $orders_status = $this->_order_status->formSelect();
        $in_production = $this->_order_status->where('name', 'Em Produção')->first();

        $in_production_id = null;
        if (count($in_production) == 1) {
            $in_production_id = $in_production->id;
        }

        return view(self::MODULE_VIEW . self::FOLDER . 'index', compact('sectors', 'orders_status', 'in_production_id'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $sorting = $request->get('ids', false);

        if (count($sorting) > 0 && $sorting != false) {
            foreach ($sorting as $key => $value) {
                $this->_order->find($value)->update(['sorting' => $key]);
            }

            return response()->json([
                'error' => false,
            ]);
        }

        return response()->json([
            'error' => true,
        ]);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $attributes = $request->all();

        $model = $this->_order->find($id);
        $model->fill($attributes);

        if ($model->save()) {
            return response()->json([
                'error' => false,
                'message' => trans('messages.success.update'),
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => trans('messages.error.update'),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {

    }

    /**
     * Lista dos Setores
     * @return Response
     */
    public function datatable(Request $request)
    {
        $filters = $request->all();
        $filters['queue'] = true;

        return Datatables::of($this->_order->datatable($filters))
            ->addColumn('infoOrder', function ($object) {
                $urlEdit = "";
                if (!empty($object->have_join)) {
                    $explode = explode('-', $object->have_join);
                    $object_id = $explode[0];
                    $object_orders_id = $explode[1];
                    $urlEdit = route('cerebelo.orders.join_edit', $object_id) . '?orders_id=' . $object_orders_id;
                }
                $client_name = (isset($object->client->company_name)) ? (empty($object->client->company_name) ? $object->client->fantasy_name : $object->client->company_name) : "";
                $status_mark = ($object->urgent == 'Y') ? '<span title="Urgente" data-popup="tooltip" data-original-title="Urgente" class="status-mark border-danger position-left"></span>' : '<span class="status-mark border-blue position-left"></span>';
                $have_join = empty($object->have_join) ? '' : '<a onclick="popUpWin(\'' . $urlEdit . '\', \'900\', \'1000\');return false;" title="" data-popup="tooltip" data-original-title="Visualizar Juntados"><span class="status-mark border-violet-800 position-left"></span></a>';
                return '<div class="media-left">' .
                '  <div><a class="text-default text-semibold" href="' . route('cerebelo.orders.edit', $object->id) . '">' . $client_name . '</a></div>' .
                '  <div class="text-size-small">' . $have_join . '<a class="text-default text-semibold" href="' . route('cerebelo.orders.edit', $object->id) . '">' . $status_mark . $object->order_number . '</a></div>' .
                    '</div>';
            })
            ->addColumn('status', function ($object) {
                $latestSector = $object->status;
                if (!empty($latestSector)) {
                    return '<span class="text-muted">' . $latestSector->name . '</span>';
                } else {
                    return '<span class="text-muted">Sem orçamento</span>';
                }
            })
            ->addColumn('btnCheck', function ($object) {
                return \Form::checkbox('', $object->id, false, ['class' => 'check_order', 'id' => $object->id]);
            })
            ->addColumn('eventDate', function ($object) {
                return \Form::text('event_date', $object->event_date, ['data-popup' => 'tooltip', 'data-placement' => 'left', 'title' => 'Tecle ENTER para salvar', 'class' => 'form-control event_date date', 'id' => $object->id]);
            })
            ->addColumn('attendant', function ($object) {
                return (!isset($object->client->attendant->name)) ? "" : $object->client->attendant->name;
            })
            ->addColumn('updated_at', function ($object) {
                $updated_at = Carbon::createFromFormat('d/m/Y H:i:s', $object->updated_at);
                return '<span>' . $object->updated_at . '</span>' .
                '<div class="text-size-small"><span class="icon-watch2 position-left"></span>' . $updated_at->diffForHumans() . '</div>';
            })
            ->escapeColumns([])
            ->make(true);
    }

    /**
     * Impressão
     * @return Response
     */
    public function printer($ids)
    {
        $filters['ids'] = explode(',', $ids);
        $orders = $this->_order->datatable($filters)->get();

        $html = view(self::MODULE_VIEW . self::FOLDER . '_printer', compact('orders'))->render();

        try {
            $errorlevel = error_reporting();
            $errorlevel = error_reporting($errorlevel & ~(E_NOTICE | E_WARNING));
            $mpdf = new $this->_mpdf(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                0, // font size - default 0
                '', // default font family
                5, // margin_left
                5, // margin right
                10, // margin top
                5, // margin bottom
                0, // margin header
                0, // margin footer
                'P' // L - landscape, P - portrait
            );

            $stylesheet = '<link href="' . asset('css/cerebelo/application.css') . '" media="all" rel="stylesheet" type="text/css"/>';
            $mpdf->WriteHTML($stylesheet);
            $stylesheet = '<link href="' . asset('modules/cerebeloorders/css/orders/_order_print_area.css') . '" media="all" rel="stylesheet" type="text/css"/>';
            $mpdf->WriteHTML($stylesheet);
            $stylesheet = '<link href="' . asset('modules/cerebeloorders/css/orders/_order_print_pdf.css') . '" media="all" rel="stylesheet" type="text/css"/>';
            $mpdf->WriteHTML($stylesheet);

            $_html = '<!DOCTYPE html>';
            $_html .= '<html>';
            $_html .= ' <head>';
            $_html .= '  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>';
            $_html .= ' </head>';
            $_html .= ' <body>';
            $_html .= $html;
            $_html .= ' </body>';
            $_html .= '</html>';
            $mpdf->WriteHTML($_html, 2, false, true);

            $mpdf->Output();
        } catch (\Exception $ex) {
            \Log::error($ex);
            return;
        }
    }

}
