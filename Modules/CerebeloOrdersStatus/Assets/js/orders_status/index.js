var route = document.getElementById("route-orders-status");

var oTable = $('#datatable').DataTable({
//"pageLength": 10,
//"processing": true,
    "serverSide": true,
    //"stateSave": true,
    "paging": false,
    'createdRow': function (row, data, dataIndex) {
        $(row).attr('id', data.id);
    },
    "columnDefs": [
        {"targets": [], "orderable": false},
        {"visible": false, "targets": [3]}
    ],
    "ajax": {
        "url": route.getAttribute("route-datatable")
    },
    "order": [[3, "asc"]],
    "columns": [
        {data: 'btnList', name: 'btnList', orderable: false, searchable: false},
        {data: 'id', name: 'id'},
        {data: 'name', name: 'name'},
        {data: 'ordination', name: 'ordination', searchable: false},
    ]
});
$("#datatable tbody").sortable({
    placeholder: "sortable-placeholder",
    start: function (e, ui) {
        ui.placeholder.height(ui.item.outerHeight());
    },
    update: function (event, ui) {
        var productOrder = $(this).sortable('toArray');

        if (productOrder.length > 0) {
            $.ajax({
                url: route.getAttribute("ordination"),
                type: 'POST',
                dataType: 'json',
                data: {
                    ids: productOrder
                },
                success: function (response) {

                },
                errors: function (response) {

                },
            });
        }
    }
}).disableSelection();