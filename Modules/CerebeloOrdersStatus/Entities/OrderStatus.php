<?php

namespace Modules\CerebeloOrdersStatus\Entities;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{

    protected $table    = 'orders_status';
    protected $fillable = [
        'name',
        'ordination',
    ];
    public $timestamps  = false;

    /*
     * Relationships
     */

    /*
     * Attributes
     */

    /*
     * Scope
     */

    public function scopeOrdination($query, $value)
    {
        return $query->orderBy('ordination', $value);
    }

    /*
     * Ohters
     */

    /*
     * Retorna todos os status para select2
     * 
     * @return array
     */

    public static function formSelect()
    {
        $return = self::ordination('asc')->pluck('name', 'id');
        return $return;
    }

    public function datatable()
    {
        return $this->select("*");
    }

}
