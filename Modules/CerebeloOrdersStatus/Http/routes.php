<?php

Route::group(['middleware' => ['web', 'cerebelo.authenticated'], 'prefix' => 'cerebelo', 'namespace' => 'Modules\CerebeloOrdersStatus\Http\Controllers'], function() {

    Route::group(['middleware' => ['cerebelo.permission']], function() {

        Route::group(['prefix' => 'orders-status'], function() {

            Route::get('/', 'OrdersStatusController@index')->name('cerebelo.orders_status.index');
            Route::get('/listing', 'OrdersStatusController@datatable')->name('cerebelo.orders_status.datatable');
            Route::get('/create', 'OrdersStatusController@create')->name('cerebelo.orders_status.create');
            Route::post('/', 'OrdersStatusController@store')->name('cerebelo.orders_status.store');
            Route::get('/edit/{id}', 'OrdersStatusController@edit')->name('cerebelo.orders_status.edit');
            Route::match(['put', 'patch'], '/update/{id}', 'OrdersStatusController@update')->name('cerebelo.orders_status.update');
            Route::delete('/delete/{id}', 'OrdersStatusController@destroy')->name('cerebelo.orders_status.destroy');
            Route::post('/ordination', 'OrdersStatusController@ordination')->name('cerebelo.orders_status.ordination');
        });
    });
});
