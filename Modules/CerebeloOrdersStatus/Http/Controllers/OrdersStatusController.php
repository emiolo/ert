<?php

namespace Modules\CerebeloOrdersStatus\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\CerebeloOrdersStatus\Http\Controllers\CerebeloController as Controller;
use Datatables;
use Modules\CerebeloOrdersStatus\Entities\OrderStatus;
use Modules\CerebeloOrdersStatus\Http\Requests\OrdersStatusRequest;
use JsValidator;

class OrdersStatusController extends Controller
{

    const FOLDER = "orders_status.";

    protected $_status;

    public function __construct()
    {
        $this->_status = new OrderStatus();
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view(self::MODULE_VIEW . self::FOLDER . 'index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $validator = JsValidator::formRequest(new OrdersStatusRequest, '#formCreateOrderStatus');

        return view(self::MODULE_VIEW . self::FOLDER . 'create', compact('validator'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(OrdersStatusRequest $request)
    {
        $attributes = $request->all();

        $create = $this->_status->create($attributes);

        if ($create)
        {
            return $this->redirectBackAfterSuccess(trans('messages.success.create'));
        }

        return $this->redirectBackAfterError(trans('messages.error.create'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function ordination(Request $request)
    {
        $ids = $request->get('ids');

        foreach ($ids as $key => $value)
        {
            $this->_status->where('id', $value)->update(['ordination' => $key]);
        }

        return response()->json([
                    'error' => false
        ]);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view(self::MODULE_VIEW . self::FOLDER . 'show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $validator = JsValidator::formRequest(new OrdersStatusRequest, '#formEditOrderStatus');
        $model = $this->_status->find($id);

        return view(self::MODULE_VIEW . self::FOLDER . 'edit', compact('model', 'validator'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(OrdersStatusRequest $request, $id)
    {
        $attributes = $request->all();

        $update = $this->_status->find($id);
        $update->fill($attributes);

        if ($update->save())
        {
            return $this->redirectAfterSuccess(trans('messages.success.create'), 'cerebelo.' . self::FOLDER . 'index');
        }

        return $this->redirectBackAfterError(trans('messages.error.create'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->_status->destroy($id))
        {
            return $this->redirectBackAfterSuccess(trans('messages.success.destroy'));
        }

        return $this->redirectBackAfterError(trans('messages.error.destroy'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function datatable()
    {
        return Datatables::of($this->_status->datatable())
                        ->addColumn('btnList', function ($object) {
                            $array = [
                                'edit'    => [
                                    'route' => route('cerebelo.' . self::FOLDER . 'edit', $object->id),
                                ],
                                'destroy' => [
                                    'route' => route('cerebelo.' . self::FOLDER . 'destroy', $object->id),
                                ],
                            ];
                            return iconsList($array);
                        })
                        ->escapeColumns([])
                        ->make(true);
    }

}
