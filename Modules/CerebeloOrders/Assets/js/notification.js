var route_notification_menu = document.getElementById("correios-cep").getAttribute("notification-menu");

function showMenuOrdersNotification() {
    $.ajax({
        url: route_notification_menu + "/pedidos",
        type: "POST",
        dataType: "json",
        success: function (response) {
            if (!response.error) {
                $("ul.navigation-main li").each(function () {
                    var $this = $(this);
                    var li_class = $this.attr('class');

                    if (li_class != "navigation-header") {
                        var li = $(":contains('Pedidos')", $this);
                        var li_html = li.html();

                        if (typeof (li_html) != "undefined") {
                            var $style = "";
                            var $title = "Quatidade de Pedidos sem aprovação no seu Setor.";

                            if (response.message > 0) {
                                $style = "background-color: #ff7043; border-color: #ff7043; color: #fff;";
                            } else {
                                $style = "background-color: #2196F3; border-color: #2196F3; color: #fff;";
                            }

                            if ($("span.badge", $this).length > 0) {
                                $("span.badge", $this).replaceWith('<span data-placement="right" data-popup="tooltip" class="badge" style="' + $style + '" title="' + $title + '">' + response.message + '</span>');
                            } else {
                                $('a > span', $this).append('<span data-placement="right" data-popup="tooltip" class="badge" style="' + $style + '" title="' + $title + '">' + response.message + '</span>');
                            }
                        }
                    }
                });
            }
        },
        error: function (response) {
            console.log('error: ', response);
        },
    });
}

if (typeof (route_notification_menu) != "undefined" && route_notification_menu != "" && route_notification_menu !== null) {
    //showMenuOrdersNotification();

    var $interval = 60000; // 1 mn
    //setInterval(showMenuOrdersNotification, $interval);
}