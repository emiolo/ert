var $modal_remote = $("#modal_remote");

$(".btnClickLineUp").off('click').on('click', function (e) {
    e.preventDefault();
    var $this = $(this);
    btnClickLineUp($this);
});

$(document).ajaxStop(function () {

    $(".btnClickLineUp").off('click').on('click', function (e) {
        e.preventDefault();
        var $this = $(this);
        btnClickLineUp($this);
    });

});

function btnClickLineUp(e) {
    var $this = e;
    var href = $this.attr('href');
    var type = $this.attr('type');

    if (type == "modal") {
        $.ajax({
            url: href,
            type: 'GET',
            dataType: 'html',
            success: function (response) {
                $modal_remote.modal('show').html(response);
            },
            error: function (response) {
                console.log(response);
            },
        });
    } else {
        var messages = {
            accept: 'Deseja realmente confirmar o recebimento?',
            next: 'Deseja realmente enviar para esse setor?'
        };
        var message = ((typeof (messages[type]) == 'undefined') || (messages[type] == '')) ? 'Deseja realmente enviar para esse setor?' : messages[type];
        var buttons = {
            cancel: {
                label: "Não",
                className: "btn-default",
                callback: function () {

                }
            },
            success: {
                label: "Sim",
                className: "btn-success",
                callback: function () {
                    //bootbox.alert("Hello . You've chosen <b></b>");
                    $.ajax({
                        url: href,
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            obs: document.getElementById('swal-obs').value
                        },
                        success: function (response) {
                            new PNotify({
                                title: (!response.error) ? 'Sucesso' : 'Aviso',
                                text: response.message,
                                addclass: (!response.error) ? 'bg-info alert-styled-right' : 'bg-warning alert-styled-right'
                            });

                            if (!response.error) {
                                bootbox.hideAll();

                                if (typeof oTable !== 'undefined') {
                                    oTable.ajax.reload();
                                }
                                if (type == "next") {
                                    $modal_remote.modal('hide').html("");
                                }
                                //showMenuOrdersNotification();
                            }
                        },
                        error: function (response) {

                        },
                    });

                    return false;
                }
            },
        };

        if (type == "accept") {
            var reject_button = {
                label: "Recusar",
                className: "btn-warning pull-left title-reject",
                callback: function () {
                    //bootbox.alert("Hello . You've chosen <b></b>");
                    if(document.getElementById('swal-obs').value != "") {
                        $.ajax({
                            url: href,
                            type: 'GET',
                            dataType: 'json',
                            data: {
                                obs: document.getElementById('swal-obs').value,
                                reject: true,
                            },
                            success: function (response) {
                                new PNotify({
                                    title: (!response.error) ? 'Sucesso' : 'Aviso',
                                    text: response.message,
                                    addclass: (!response.error) ? 'bg-info alert-styled-right' : 'bg-warning alert-styled-right'
                                });

                                if (!response.error) {
                                    bootbox.hideAll();

                                    if (typeof oTable !== 'undefined') {
                                        oTable.ajax.reload();
                                    }
                                    if (type == "next") {
                                        $modal_remote.modal('hide').html("");
                                    }
                                    //showMenuOrdersNotification();
                                }
                            },
                            error: function (response) {

                            },
                        });
                    } else {
                        new PNotify({
                            title: 'Aviso',
                            text: 'O campo de Observação é obrigatório.',
                            addclass: 'bg-warning alert-styled-right'
                        });
                    }

                    return false;
                }
            };
            buttons.reject = reject_button;
        }

        bootbox.dialog({
            title: "Atenção",
            message: message + "<br/><br/><div class='form-group'><textarea id='swal-obs' class='form-control' placeholder='Digite algo'></textarea></div>",
            buttons: buttons
        });
        var title_reject = 'Ao recusar um pedido, o mesmo será reenviado para o responsável.';
        $("button.title-reject").attr('title', title_reject).attr('data-popup', 'tooltip').attr('data-original-title', title_reject).tooltip();
        $('<span class="help-block text-warning text-left">' + title_reject + '</span>').insertAfter($("button.title-reject"));
    }
}

function btnClickChangeActionOrder(obj) {
    var $this = $(obj);
    var id = $this.val();
    var href = $this.attr('href');

    swal({
        title: 'Atenção',
        text: 'Deseja realmente alterar a ação do pedido?',
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Não",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                url: href,
                type: 'PATCH',
                dataType: 'json',
                data: {
                    action_id: id
                },
                success: function (response) {
                    swal.close();
                    new PNotify({
                        title: (!response.error) ? 'Sucesso' : 'Aviso',
                        text: response.message,
                        addclass: (!response.error) ? 'bg-info alert-styled-right' : 'bg-warning alert-styled-right'
                    });

                    if (!response.error && typeof oTable !== 'undefined') {
                        oTable.ajax.reload();
                        //showMenuOrdersNotification();
                    }
                },
                error: function (response) {
                    swal.close();
                    console.log(response);
                },
            });
        }
    });
}

function orderSendMailToFiscal(confirm, obj) {
    var $this = $(obj);
    var orderSendMail = $this.attr('url');
    var modal_email = $("#modal_email");

    if (typeof (confirm) === "undefined" || confirm !== true) {
        $("#email_body").summernote('destroy');
        $("#email_body").summernote({
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ],
            //height: 200,
            lang: 'pt-BR'
        });
        modal_email.modal('show');
        $(".modal-footer button.btn-primary", modal_email).attr('onclick', 'orderSendMailToFiscal(true, this);').attr('url', orderSendMail);
        $(".modal-header h5.modal-title", modal_email).html('Enviar orçamento/pedido para o Financeiro');
        $("input[name='email_to']", modal_email).val("O e-mail será enviado para os usuários do setor Fiscal");
    } else {
        var email_type = $("select[name='email_type']");
        var email_recipients = $("textarea[name='email_recipients']");
        var email_body = $("textarea[name='email_body']");

        $.blockUI({ message: '<h4>aguarde... enviando e-mail</h4>' });
        email_body.html($("#email_body").summernote('code'));

        //var outerHTML = document.getElementById("section-to-print").outerHTML;

        $.ajax({
            dataType: "json",
            type: "POST",
            url: orderSendMail,
            data: {
                email_type: email_type.val(),
                email_recipients: email_recipients.val(),
                email_body: email_body.val(),
                to_fiscal: true,
                //html: outerHTML,
            }
        }).done(function (response) {
            $.unblockUI();
            if (!response.error) {
                modal_email.modal('hide');
                email_type.val("");
                email_recipients.val("");
                email_body.val("");
                email_body.html("");
                $("#email_body").summernote('destroy')

                new PNotify({
                    title: 'Sucesso',
                    text: response.message,
                    addclass: 'bg-success alert-styled-right'
                });
            } else {
                new PNotify({
                    title: 'Atenção',
                    text: response.message,
                    addclass: 'bg-warning alert-styled-right'
                });
            }
        }).error(function (response) {
            $.unblockUI();
            new PNotify({
                title: 'Atenção',
                text: response.message,
                addclass: 'bg-warning alert-styled-right'
            });
        });
    }
}