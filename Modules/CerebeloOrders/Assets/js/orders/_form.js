var idleTime = 0;

var $formOrderForClient = $('#formOrderForClient');
$('input', $formOrderForClient).keydown(function (event) {
    var keypressed = event.keyCode || event.which;
    if (keypressed === 13) {
        $.ajax({
            url: $formOrderForClient.attr('action'),
            type: 'PATCH',
            dataType: 'json',
            data: $formOrderForClient.serialize(),
            success: function (response) {
                if (response.error) {
                    new PNotify({
                        title: 'Atenção',
                        text: response.message,
                        addclass: 'bg-warning alert-styled-right'
                    });
                }
            },
            error: function (response) {

            },
        });
    }
});

$('input:text[name="production_date"]').keydown(function (event) {
    var keypressed = event.keyCode || event.which;
    if (keypressed === 13) {
        var val = this.value;
        $.ajax({
            url: $formOrderForClient.attr('action'),
            type: 'PATCH',
            dataType: 'json',
            data: {
                production_date: val
            },
            success: function (response) {
                if (response.error) {
                    new PNotify({
                        title: 'Atenção',
                        text: response.message,
                        addclass: 'bg-warning alert-styled-right'
                    });
                }
            },
            error: function (response) {

            },
        });
    }
});

$("a#sendToCustomer").on('click', function () {
    $("#formOrder").prepend('<input type="hidden" name="btnSendTo" value="sendToCustomer"/>').submit();
});

$("a#sendToDesign").on('click', function () {
    var messages = {
        next: 'Deseja realmente enviar para esse setor?'
    };
    var message = messages['next'];

    swal({
        title: 'Atenção',
        text: message + "<br/><br/><textarea id='swal-obs' class='form-control' placeholder='Digite algo'></textarea>",
        html: true,
        showCancelButton: true,
        cancelButtonText: "Não",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function (isConfirm) {
        if (isConfirm) {
            $("#formOrder").prepend('<input type="hidden" name="btnSendTo" value="sendToDesign"/><textarea style="display: none;" name="obs">' + document.getElementById('swal-obs').value + '</textarea>').submit();
        }
    });
});

$("select[name='printer_option']").on('change', function () {
    var $this = $(this);
    var url = $this.attr('url');
    var value = $this.val();
    $.ajax({
        url: url,
        type: 'PATCH',
        dataType: 'json',
        data: {
            printer: value
        },
        success: function (response) {
            if (response.error) {
                new PNotify({
                    title: 'Atenção',
                    text: response.message,
                    addclass: 'bg-warning alert-styled-right'
                });
            }
        },
        error: function (response) {

        },
    });
})/*.select2({
    minimumResultsForSearch: Infinity,
    width: "100%",
    dropdownAutoWidth: true,
    containerCssClass: 'select-xs'
})*/;
$("select[name='prepress']").on('change', function () {
    var $this = $(this);
    var url = $this.attr('url');
    var value = $this.val();
    $.ajax({
        url: url,
        type: 'PATCH',
        dataType: 'json',
        data: {
            prepress: value
        },
        success: function (response) {
            if (response.error) {
                new PNotify({
                    title: 'Atenção',
                    text: response.message,
                    addclass: 'bg-warning alert-styled-right'
                });
            }
        },
        error: function (response) {

        },
    });
})/*.select2({
    minimumResultsForSearch: Infinity,
    width: "100%",
    dropdownAutoWidth: true,
    containerCssClass: 'select-xs'
})*/;

//Zero the idle timer on mouse movement.
$(this).mousemove(function (e) {
    idleTime = 0;
});

$(this).keypress(function (e) {
    idleTime = 0;
});

$("a.btnClientFiles").on('click', function (e) {
    e.preventDefault();
    var $this = $(this);
    var href = $this.attr('href');
    popUpWin(href + "#datatable_wrapper", 1000, 600);
});

var hostname = document.location.hostname;

$("ul.nav-tabs-highlight li a").on('click', function () {
    var $this = $(this);
    var href = $this.attr('href');
    window.location.hash = href;
    if (hostname != 'ert.local' && hostname != 'ert.dev' && hostname != 'localhost' && hostname != '10.0.0.78') {
        autoSaveTab(href, true);
    }
    if (ordersWeight() > 0 && $("select[name='transports_id']").val() != "" && href === "#justified-badges-tab2") {
        //freightTransportsChange($("select[name='transports_id']"));
    }

    //window.location.href = (window.location.href).replace('?act=print', '');
});

if (hostname != 'ert.local' && hostname != 'ert.dev' && hostname != 'localhost' && hostname != '10.0.0.78' && $("ul.nav-tabs-highlight li a").length > 0) {
    var $max_minutes = 60000;
    setInterval(autoSaveTab, $max_minutes);
}

/*
 * Salvar abas automaticamente
 */
function autoSaveTab($href, tab_click) {
    console.log('antes: ', idleTime);
    idleTime++;
    console.log('depois: ', idleTime);
    if ((idleTime % 3 === 0 && idleTime > 0) || tab_click === true) {
        var $this = $("ul.nav-tabs-highlight li.active a");
        var href = $this.attr('href');

        if (href !== "#justified-badges-tab5" && href !== "#justified-badges-tab6") {
            if (typeof ($href) !== "undefined") {
                var action = $(href).find('form').attr('action');
                action = action.split('#');
                $(href).find('form').attr('action', action[0] + $href);
            }
            $(href).find('form').submit();
        }
    }
}

/*
 * Calcular Badges para todas as abas
 */
function badgeJustifiedBadgesTab(fields, tab) {
    var aSpanTab1 = $("a[href='" + tab + "'] span");
    var total = 0;
    $.each(fields, function (key, value) {
        var ctrl = $(tab + ' [name^="' + value + '"]');
        switch (ctrl.prop('type')) {
            case "radio":
            case "checkbox":
                ctrl.on('click', function (e) {
                    badgeJustifiedBadgesTab(fields, tab);
                });
                if (!ctrl.is(':checked')) {
                    total++;
                }
                break;
            case "select":
            case "select-one":
            case "select-multiple":
                ctrl.on('change', function (e) {
                    badgeJustifiedBadgesTab(fields, tab);
                });
                if (ctrl.val() === "" || ctrl.val() === null) {
                    total++;
                }
                break;
            default:
                ctrl.on('blur', function (e) {
                    badgeJustifiedBadgesTab(fields, tab);
                });
                if (ctrl.val() === "" || ctrl.val() === null) {
                    total++;
                }
        }
        if (typeof (ctrl.prop('type')) === "undefined") {
            total++;
        }
    });
    aSpanTab1.html(total);
    aSpanTab1.removeClass('badge-primary').addClass('badge-danger');
    if (total === 0) {
        aSpanTab1.removeClass('badge-danger').addClass('badge-primary');
    }
}

if(findGetParameter('act') === 'print') {
    setTimeout(function() {
        //var originalTitle = document.title;
        console.log('document.readyState: ', document.readyState);
        if(document.readyState == "interactive") {
            document.title = $("h5.text-uppercase", $("div#justified-badges-tab6")).html();
            orderPrintArea();
        }
        //document.title = originalTitle;
    }, 1000);
}

function orderPrintArea() {
    window.print();
}

function orderSendMail(confirm) {
    var modal_email = $("#modal_email");
    if (typeof (confirm) === "undefined" || confirm !== true) {
        $("#email_body").summernote('destroy');
        $("#email_body").summernote({
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ],
            //height: 200,
            lang: 'pt-BR'
        });
        modal_email.modal('show');
        $(".modal-footer button.btn-primary", modal_email).attr('onclick', 'orderSendMail(true);');
        $(".modal-header h5.modal-title", modal_email).html('Enviar orçamento/pedido para o Cliente');
        var email_to = $("input[name='email_to']", modal_email);
        email_to.val($("input[name='email']", $("#justified-badges-tab1")).val());
    } else {
        var email_to = $("input[name='email_to']");
        var email_type = $("select[name='email_type']");
        var email_recipients = $("textarea[name='email_recipients']");
        var email_body = $("textarea[name='email_body']");
        var orderSendMail = document.getElementById("orders-routes").getAttribute("order-send-mail");
        $.blockUI({ message: '<h4>aguarde... enviando e-mail</h4>' });
        email_body.html($("#email_body").summernote('code'));

        $.ajax({
            dataType: "json",
            type: "POST",
            url: orderSendMail,
            data: {
                email_to: email_to.val(),
                email_type: email_type.val(),
                email_recipients: email_recipients.val(),
                email_body: email_body.val(),
                //html: document.getElementById("section-to-print").outerHTML,
            }
        }).done(function (response) {
            $.unblockUI();
            if (!response.error) {
                modal_email.modal('hide');
                email_type.val("");
                email_recipients.val("");
                email_body.val("");
                email_body.html("");
                $("#email_body").summernote('destroy');
                $(".note-editor", modal_email).remove();

                new PNotify({
                    title: 'Sucesso',
                    text: response.message,
                    addclass: 'bg-success alert-styled-right'
                });
            } else {
                new PNotify({
                    title: 'Atenção',
                    text: response.message,
                    addclass: 'bg-warning alert-styled-right'
                });
            }
        }).error(function (response) {
            $.unblockUI();
            new PNotify({
                title: 'Atenção',
                text: response.message,
                addclass: 'bg-warning alert-styled-right'
            });
        });
    }
}

function sendOrderToClient(confirm) {
    if (countSizesValues()) {
        new PNotify({
            title: 'Atenção',
            text: 'A quatidade total dos produtos não bate com a distribuição dos tamanhos.',
            addclass: 'bg-warning alert-styled-right'
        });
    } else {
        var modal_email = $("#modal_email");
        if (typeof (confirm) === "undefined" || confirm !== true) {
            $("#email_body").summernote('destroy');
            $("#email_body").summernote({
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ],
                //height: 200,
                lang: 'pt-BR'
            });
            modal_email.modal('show');
            $(".modal-footer button.btn-primary", modal_email).attr('onclick', 'sendOrderToClient(true);');
            $(".modal-header h5.modal-title", modal_email).html('Enviar orçamento/pedido para o Cliente');
        } else {
            var email_to = $("input[name='email_to']");
            var email_type = $("select[name='email_type']");
            var email_recipients = $("textarea[name='email_recipients']");
            var email_body = $("textarea[name='email_body']");
            var orderSendMail = document.getElementById("orders-routes").getAttribute("order-send-mail");
            $.blockUI({ message: '<h4>aguarde... enviando e-mail</h4>' });
            email_body.html($("#email_body").summernote('code'));

            $.ajax({
                dataType: "json",
                type: "PATCH",
                url: orderSendMail,
                data: {
                    email_to: email_to.val(),
                    email_type: email_type.val(),
                    email_recipients: email_recipients.val(),
                    email_body: email_body.val(),
                    html: $("#section-to-print", $("#justified-badges-tab5")).parent().html(),
                }
            }).done(function (response) {
                $.unblockUI();
                if (!response.error) {
                    modal_email.modal('hide');
                    email_type.val("");
                    email_recipients.val("");
                    email_body.val("");
                    email_body.html("");
                    $("#email_body").summernote('destroy');
                    $(".note-editor", modal_email).remove();

                    new PNotify({
                        title: 'Sucesso',
                        text: response.message,
                        addclass: 'bg-success alert-styled-right'
                    });
                } else {
                    new PNotify({
                        title: 'Atenção',
                        text: response.message,
                        addclass: 'bg-warning alert-styled-right'
                    });
                }
            }).error(function (response) {
                $.unblockUI();
                new PNotify({
                    title: 'Atenção',
                    text: response.message,
                    addclass: 'bg-warning alert-styled-right'
                });
            });
        }
    }
}

function orderDownload() {
    var orderSendMail = document.getElementById("orders-routes").getAttribute("order-send-mail");
    $.blockUI({ message: '<h4>aguarde... preparando o PDF</h4>' });
    $.ajax({
        dataType: "html",
        type: "POST",
        url: orderSendMail,
        data: {
            download: true,
            //html: document.getElementById("section-to-print").outerHTML
        }
    }).done(function (response) {
        $.unblockUI();
        if(response != "")
        {
            window.open(response);
        }
        else
        {
            new PNotify({
                title: 'Atenção',
                text: 'Não foi possível gerar o PDF, tente novamente.',
                addclass: 'bg-warning alert-styled-right'
            });
        }
    }).error(function (response) {
        $.unblockUI();
        new PNotify({
            title: 'Atenção',
            text: response.message,
            addclass: 'bg-warning alert-styled-right'
        });
    });
}

function orderPrintAndSendToSector(obj) {
    var $this = $(obj);

    if (countSizesValues()) {
        new PNotify({
            title: 'Atenção',
            text: 'A quatidade total dos produtos não bate com a distribuição dos tamanhos.',
            addclass: 'bg-warning alert-styled-right'
        });
    } else {
        var originalTitle = document.title;
        document.title = $("h5.text-uppercase", $("div#justified-badges-tab6")).html();
        //hidePagebreak();
        window.print();
        btnClickLineUp($(".btnClickLineUp"));
        document.title = originalTitle;
        //showPagebreak();
    }
}

function orderPrintProdutionOrder(obj) {
    var $this = $(obj);

    if (countSizesValues()) {
        new PNotify({
            title: 'Atenção',
            text: 'A quatidade total dos produtos não bate com a distribuição dos tamanhos.',
            addclass: 'bg-warning alert-styled-right'
        });
    } else {
        var originalTitle = document.title;
        document.title = $("h5.text-uppercase", $("div#justified-badges-tab6")).html();
        //hidePagebreak();
        window.print();
        document.title = originalTitle;
        //showPagebreak();
    }
}

function showPagebreak() {
    $("center.document-title").remove();
    $("#selected-client-files").closest('div.pagebreak').css({
        'display': 'block'
    });
}

function hidePagebreak() {
    $("div#justified-badges-tab6 .pagebreak").prepend("<center class='document-title'>" + document.title + "</center>");
    var selected_client_files = $.trim($("#selected-client-files").html());
    if(selected_client_files == "")
    {
        $("#selected-client-files").closest('div.pagebreak').css({
            'display': 'none'
        });
    }
}

function attachClientFilesOP(obj) {
    var $this = $(obj);

    if (countSizesValues()) {
        new PNotify({
            title: 'Atenção',
            text: 'A quatidade total dos produtos não bate com a distribuição dos tamanhos.',
            addclass: 'bg-warning alert-styled-right'
        });
    } else {
        var url = $this.attr('url');
        var routeAttachFiles = $this.attr('routeAttachFiles');

        var $html = "";
        $html += '<div class="modal-dialog modal-lg">';
        $html += '    <div class="modal-content">';
        $html += '        <div class="modal-header">';
        $html += '            <button class="close" type="button" data-dismiss="modal"> &times;</button>';
        $html += '            <h5 class="modal-title"> Arquivos do Cliente</h5>';
        $html += '        </div>';
        $html += '        <div class="modal-body">';
        $html += '            <div class="alert alert-info alert-styled-left text-blue-800 content-group">';
        $html += '                É permitido anexar somente arquivo de imagem [jpg, jpeg, png, bmp].';
        $html += '            </div>';
        $html += '            <div class="table-responsive">';
        $html += '                <table class="table table-hover" id="datatableClientFiles">';
        $html += '                    <thead>';
        $html += '                        <tr>';
        $html += '                            <th class="text-center" style="width: 20px;">#</th>';
        $html += '                            <th>ID</th>';
        $html += '                            <th>Titulo</th>';
        $html += '                            <th>Arquivo</th>';
        $html += '                            <th>Tipo</th>';
        $html += '                            <th>Criado em</th>';
        $html += '                        </tr>';
        $html += '                    </thead>';
        $html += '                    <tbody></tbody>';
        $html += '                </table>';
        $html += '            </div';
        $html += '        </div>';
        $html += '        <div class="modal-footer">';
        $html += '            <button type="button" class="btn btn-link" data-dismiss="modal">Fechar</button>';
        $html += '            <button type="button" class="btn btn-primary" id="btnAddSelectedFiles">Concluir</button>';
        $html += '        </div>';
        $html += '    </div>';
        $html += '</div>';

        var modal_remote = $("#modal_remote");
        modal_remote.modal('show').html($html);

        $('#datatableClientFiles').DataTable({
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            "stateSave": true,
            "columnDefs": [{ "targets": [], "orderable": false }],
            "ajax": {
                "url": url
            },
            "order": [[2, "asc"]],
            createdRow: function (row, data, dataIndex) {
                $(row).find('td:eq(0)').find('input').attr('contentFile', data.contentFile).attr('contentDescription', data.contentDescription);
            },
            "columns": [
                { data: 'inputCheckbox', name: 'inputCheckbox', orderable: false, searchable: false },
                { data: 'id', name: 'id' },
                { data: 'title', name: 'title' },
                { data: 'file', name: 'file', orderable: false, searchable: false },
                { data: 'file_extension', name: 'file_extension', orderable: false, searchable: false },
                { data: 'created_at', name: 'created_at' },
            ],
            "initComplete": function (settings, json) {
                $("#btnAddSelectedFiles").on('click', function () {
                    var $selected_client_files = $("#selected-client-files");
                    var $client_files = $(".client-files");

                    $selected_client_files.html("");
                    var ids = [];
                    $client_files.map(function (k, v) {
                        var input = $(v);
                        if (input.is(':checked')) {
                            ids.push(input.val());

                            var contentFile = input.attr('contentFile');
                            var contentDescription = input.attr('contentDescription');
                            //var table = '<div class="pagebreak">';
                            var table = '';
                            table += '   <div class="table-responsive">';
                            table += '       <table class="table table-borderless table-xs">';
                            table += '           <tbody>';
                            table += '               <tr>';
                            table += '                   <td class="text-center">';
                            table += '                       <img class="img-product-order" src="' + contentFile + '" alt="Anexo do Cliente" style=" margin: 10px; 0px;"/>';
                            table += '                   </td>';
                            table += '               </tr>';
                            table += '               <tr>';
                            table += '                   <td class="text-center">';
                            table += '                       <span class="text-semibold">' + contentDescription + '</span>';
                            table += '                   </td>';
                            table += '               </tr>';
                            table += '           </tbody>';
                            table += '       </table>';
                            table += '   </div>';
                            //table += '</div>';
                            $selected_client_files.append(table);
                        }
                    });

                    $.blockUI({ message: '<h4>aguarde... abrindo a tela de impressão</h4>' });

                    // Salvar anexos
                    $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: routeAttachFiles,
                        data: {
                            ids: ids
                        }
                    });

                    modal_remote.modal('hide').html("");
                    setTimeout(function () {
                        $.unblockUI();
                        window.print();
                    }, 4000);
                });
            }
        });
    }
}

function countSizesValues() {
    var total_quantity = parseInt(0);

    $("input[name^='quantity[']").each(function (index, element) {
        var $this = $(this);
        var card = $this.closest('div.panel');
        var have_op_name = $(".op-with-name-sizes", card).length;
        if (have_op_name != 0) {
            var total_sizes = parseInt(0);
            $("input[name^='sizes[']", card).each(function (index, element) {
                var $this = $(this);
                var card = $this.closest('div.panel');
                var have_sizes = $("input[name^='have_sizes[']", card).is(':checked');
                if (!have_sizes) {
                    total_sizes += ($(element).val() === "") ? parseFloat(0) : parseInt($(element).val());
                }
            });

            var have_sizes = $("input[name^='have_sizes[']", card).is(':checked');
            if (!have_sizes) {
                total_quantity += ($(element).val() === "") ? parseFloat(0) : parseInt($(element).val());
            }

            return (total_quantity != total_sizes);
        }
    });

    return false;
}

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName)
                result = decodeURIComponent(tmp[1]);
        });
    return result;
}