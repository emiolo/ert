var route = document.getElementById("route-orders").getAttribute("datatable");
var $modal_remote = $("#modal_remote");
var dashboard = ((typeof($('#formSearchOrders input:hidden[name="dashboard"]').val()) != 'undefined') ? {'dashboard': ''} : {});

var options = {
    "pageLength": 10,
    "processing": true,
    "serverSide": true,
    //"stateSave": true,
    "columnDefs": [
        //{"targets": [], "orderable": false},
        { "visible": false, "targets": [4, 5, 6, 7] },
    ],
    "ajax": {
        "url": route,
        "type": 'GET',
        "data": dashboard
    },
    "order": [[13, 'desc'], [7, 'asc']],
    //    createdRow: function (row, data, dataIndex) {
    //        $(row).find('td:eq(3)').attr('class', 'btn-group-justified');
    //    },
    "columns": [
        { data: 'input_checkbox', name: 'input_checkbox', orderable: false, searchable: false },
        //{ data: 'sorting', name: 'sorting', searchable: true },
        { data: 'btnList', name: 'btnList', orderable: false, searchable: false },
        { data: 'infoOrder', name: 'client.fantasy_name', searchable: true },
        { data: 'group', name: 'group' },
        { data: 'order_number', name: 'order_number', searchable: true },
        { data: 'client.company_name', name: 'client.company_name', searchable: true, defaultContent: '-' },
        { data: 'client.cpf_cnpj', name: 'client.cpf_cnpj', searchable: true, defaultContent: '-' },
        { data: 'sectors_actions_id', name: 'sectors_actions_id', searchable: false, defaultContent: '-' },
        //{ data: 'status', name: 'status', orderable: false, searchable: false },
        { data: 'wait_action', name: 'wait_action', orderable: false, searchable: false },
        { data: 'actions', name: 'actions', orderable: false, searchable: false },
        { data: 'delivery_date', name: 'delivery_date' },
        { data: 'attendant', name: 'client.attendant.name' },
        { data: 'designer', name: 'client.designer.name' },
        { data: 'updated_at', name: 'updated_at', searchable: false },
    ]
};
var oTable = $('#datatable').DataTable(options);

//if(Object.keys(dashboard).length > 0) {
    var logged_user_sector = document.getElementById("route-orders").getAttribute("logged-user-sector");
    if(logged_user_sector == 'atendente') {
        var column = oTable.column(11);
        column.visible(!column.visible());
    } else if(logged_user_sector == 'designer') {
        var column = oTable.column(12);
        column.visible(!column.visible());
    }
/*} else {
    var column = oTable.column(12);
    column.visible(!column.visible());
}*/

var $formSearchOrders = $('#formSearchOrders');

$('button[type="button"]', $formSearchOrders).on('click', function () {
    clearFilter();
});
$formSearchOrders.on('submit', function () {
    var $this = $(this);
    var $formData = $this.serialize();

    oTable.ajax.url(route + '?' + $formData).load();

    $('#modal-form-filter').modal('toggle');
    return false;
});

if (findGetParameter('q') !== "" && findGetParameter('q') !== null) {
    oTable.search(findGetParameter('q')).draw();
}

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName)
                result = decodeURIComponent(tmp[1]);
        });
    return result;
}

function filterOrdersByActions(e) {
    var $this = $(e);
    var $id = $this.attr('id');

    var f = document.createElement("form");

    var i = document.createElement("input");
    i.setAttribute('name', "sectors_actions_id[]");
    i.value = $id;

    f.appendChild(i);

    var $formData = $(f).serialize();

    oTable.ajax.url(route + '?' + $formData).load();
    return false;
}

function clearFilter() {
    document.getElementById("formSearchOrders").reset();
    oTable.ajax.url(route).load();
}

function btnPrintOPOrBudget(obj) {
    var $this = $(obj);
    var href1 = $this.attr('href1');
    var href2 = $this.attr('href2');

    swal({
        title: 'O que deseja fazer?',
        text: "<a class='btn btn-primary btn-xs' href='" + href1 + "'>Imprimir Orçamento</a>" + "    " + "<a class='btn btn-info btn-xs' href='" + href2 + "'>Imprimir OP</a>",
        type: "warning",
        html: true,
        showCloseButton: true,
        showCancelButton: true,
        cancelButtonText: "Cancelar",
        showConfirmButton: false,
    });
}

function btnFormFilter() {
    $('#modal-form-filter').modal('show');
}

function btnXMLFinancial(e) {
    var $this = $(e);
    var href = $this.attr('href');

    var ids = $(".check_order").map(function () {
        var $this = $(this);
        if ($this.is(':checked')) {
            return $this.val();
        }
    }).get().join(',');

    if (ids == null || ids == '') {
        new PNotify({
            title: 'Atenção',
            text: 'Nenhum item foi selecionado',
            addclass: 'bg-warning alert-styled-right'
        });
    } else {
        $.ajax({
            url: href + '/' + ids,
            type: 'GET',
            dataType: 'html',
            beforeSend: function () {
                $.blockUI({ message: '<p><h4>aguarde...</h4></p> <p><h4>gerando XML</h4></p>' });
            },
            success: function (response) {
                $.unblockUI();
                $modal_remote.modal('show').html(response);
            },
            error: function (response) {
                $.unblockUI();
                console.log(response);
            },
        });
    }
}

$("#check_order").on('click', function (e) {
    var $this = $(this);
    $(".check_order").prop('checked', $this.is(':checked'));
});
