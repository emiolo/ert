var hash = window.location.hash;
if (hash) {
    // remover todos de active
    $('a[href^="#justified-badges-tab"]').parent('li').removeClass('active');
    $("div[id^='justified-badges-tab']").removeClass('active');

    $('a[href="' + hash + '"]').parent('li').addClass('active');
    $(hash).addClass('active');
}

/* Inicio
 * Verifica campos obrigatórios da aba 1 (Dados do Cliente) e mostrar o 'badges'
 */
var client_data_fields = [
    'attendant_id', 'representative_id', 'designer_id', 'cpf_cnpj',
    'state_registration', 'company_name', 'fantasy_name', 'phone',
    'email', 'tables_id', 'cep', 'street', 'number',
    'neighborhood', 'city', 'state', 'address_id',
];
$("a[href='#justified-badges-tab1'] span").html(client_data_fields.length);
//badgeJustifiedBadgesTab(client_data_fields, '#justified-badges-tab1');
//loadSavedTables();
//loadSavedAddresses();
/* Fim
 */

var select_tables_parse = JSON.parse($("input[name='clients_id']").attr('select_tables'));
var selectedIds = [];
$.each(select_tables_parse, function (key, value) {
    selectedIds.push(value);
});
$("select[name^='tables_id']").select2({
    minimumResultsForSearch: Infinity
}).val(selectedIds);

/*
 * Buscar cliente pelo CPF/CNPJ
 */
var clients_id = $("input[name='clients_id']");
//clientSearchByCpfcnpj(clients_id);

$("#cpfcnpj-select-remote-data").on('blur', function (e) {
    clientSearchByCpfcnpj($(this));
});

var $clients_contacts_id = $("input:checkbox[name='clients_contacts_id']");
$clients_contacts_id.on('click', function (e) {
    var $this = $(this);
    var index = $clients_contacts_id.index($this);
    var unchecked = $clients_contacts_id.not(":eq(" + index + ")");
    unchecked.prop('checked', false);
    unchecked.uniform();
});

var $address_id = $("input:checkbox[name='address_id']");
$address_id.on('click', function (e) {
    var $this = $(this);
    var index = $address_id.index($this);
    var unchecked = $address_id.not(":eq(" + index + ")");
    unchecked.prop('checked', false);
    unchecked.uniform();
});

var $billing_address_id = $("input:checkbox[name='billing_address_id']");
$billing_address_id.on('click', function (e) {
    var $this = $(this);
    var panel = $this.closest('div.panel');
    var index = $billing_address_id.index($this);
    var unchecked = $billing_address_id.not(":eq(" + index + ")");
    unchecked.prop('checked', false);
    unchecked.uniform();

    $("input:checkbox[name='clients_contacts_id']").prop('checked', false).uniform();
    $("input:checkbox[name='clients_contacts_id']", panel).prop('checked', $this.is(':checked')).uniform();
});

function clientSearchByCpfcnpj(obj) {
    var val = obj.val();
    if (val !== '' && document.getElementById("orders-routes")) {
        var clientSearchByCpfcnpj = document.getElementById("orders-routes").getAttribute("client-search-by-cpfcnpj");
        $.blockUI({ message: '<h4>verificando cliente...</h4>' });
        $.ajax({
            dataType: "json",
            type: "POST",
            url: clientSearchByCpfcnpj,
            data: {
                cpf_cnpj: val
            }
        }).done(function (response) {
            formSetClientData(response);
        });
    }
}

function formSetClientData(jsonData) {
    populate($('#formOrder'), jsonData);
    formSetClientAddresses(jsonData);
}

/*
 * Percorre os dados
 * 
 * @param {type} frm
 * @param {type} jsonData
 * @returns {undefined}
 */
function populate(frm, jsonData) {
    resetForm(frm);
    if (!$.isEmptyObject(jsonData)) {
        jsonData.clients_id = jsonData.id;
        delete jsonData.id;
        $.each(jsonData, function (key, value) {
            setFields(key, value, frm);
        });
        if (!$.isEmptyObject(jsonData.address)) {
            $.each(jsonData.address, function (key, value) {
                setFields(key, value, frm);
            });
        }
        if (!$.isEmptyObject(jsonData.price_tables)) {
            var selectedItems = [];
            var selectedIds = [];
            $.each(jsonData.price_tables, function (key, value) {
                var object = {};
                var id = value.id;
                var name = value.name;
                object = { id: id, text: name };
                selectedItems.push(object);
                selectedIds.push(id);
            });

            $("select[name^='tables_id']").select2({
                minimumResultsForSearch: Infinity,
                data: selectedItems
            }).val(selectedIds).on('change', function () {
                setSelectedTablesBudget($(this));
            });
        }
    }
}

/*
 * Preenche na aba 1 de orçamento as tabelas selecionadas na aba 1
 * 
 * @returns {undefined}
 */
function setSelectedTablesBudget(obj) {
    var texts = $("option:selected", obj).map(function () {
        return $(this).text();
    }).get();
    var values = obj.val();
    var budget_tables = [];

    if (values != '' && values != null) {
        budget_tables = values.map(function (k, v) {
            return { id: k, text: texts[v] };
        });
    }

    var old_price_table_id = $("select[name='price_table_id']").attr('old_price_table_id');
    $("select[name='price_table_id']").html('').select2({
        minimumResultsForSearch: Infinity,
        width: "100%",
        data: budget_tables,
        dropdownAutoWidth: true
    }).val(old_price_table_id);
    //badgeJustifiedBadgesTab(budget_fields, '#justified-badges-tab2');
}

/*
 * Preenche os campos
 * 
 * @param {type} key
 * @param {type} value
 * @param {type} frm
 * @returns {undefined}
 */
function setFields(key, value, frm) {
    var ctrl = $('[name=' + key + ']', frm);
    switch (ctrl.prop("type")) {
        case "radio":
        case "checkbox":
            ctrl.each(function () {
                if ($(this).attr('value') == value)
                    $(this).attr("checked", value);
            });
            break;
        default:
            ctrl.val(value);
    }
}

/*
 * Limpa os campos referente ao cliente
 * 
 * @param {type} $form
 * @returns {undefined}
 */
function resetForm($form) {
    $("#listAddresses").html('');
    $form.find('input[name="clients_id"], input[name="id"], input[name="state_registration"], input[name="company_name"], input[name="fantasy_name"], input[name="phone"], input[name="email"]').val('');
    $form.find('select[name^="tables_id"], select[name="attendant_id"], select[name="representative_id"], select[name="designer_id"]').val('');
    $form.find('input[name="cep[0]"], input[name="street[0]"], input[name="number[0]"], input[name="complement[0]"], input[name="neighborhood[0]"], input[name="city[0]"], input[name="state[0]"], input[name="country[0]"]').val('');
}

/*
 * Monta o 'html' dos endereços salvos
 * 
 * @param {type} jsonData
 * @returns {undefined}
 */
function formSetClientAddresses(jsonData, select_address) {
    var listAddresses = $("#listAddresses");
    var html = '';
    if (!$.isEmptyObject(jsonData)) {
        var billing_address_id = $("input[name='billing_address_id']").val();
        var delivery_address_id = $("input[name='delivery_address_id']").val();
        var hasError = true;
        var featured = false;

        if ((jsonData.last_used_address).length > 0 && (delivery_address_id === '' || delivery_address_id === null || typeof (delivery_address_id) === 'undefined')) {
            delivery_address_id = jsonData.last_used_address[0].delivery_address_id;
            featured = parseInt(delivery_address_id);
        }

        $.each(jsonData.addresses, function (key, value) {
            var address_id = (select_address === '' || select_address === null || typeof (select_address) === 'undefined') ? '' : select_address;
            address_id = (delivery_address_id === '' || delivery_address_id === null || typeof (delivery_address_id) === 'undefined') ? address_id : delivery_address_id;
            if (address_id == value.id) {
                hasError = false;
            }
        });

        $.each(jsonData.addresses, function (key, value) {
            var address_id = (select_address === '' || select_address === null || typeof (select_address) === 'undefined') ? '' : select_address;
            address_id = (delivery_address_id === '' || delivery_address_id === null || typeof (delivery_address_id) === 'undefined') ? address_id : delivery_address_id;

            html += '<div class="col-md-4 ' + ((hasError) ? "has-error" : "") + '">';
            html += '  <div class="panel' + ((featured && featured === value.id) ? " alert alert-primary" : "") + '">';
            html += '    <div class="panel-body">';
            html += '      <div class="checkbox">';
            html += '        <label>';
            html += '          <input cep="' + value.cep + '" class="" type="radio" name="address_id" value="' + value.id + '" ' + ((address_id == value.id) ? '' : '') + '> Usar este endereço (' + value.local + ')';
            html += '        </label>';
            html += '      </div>';
            html += '      <p>CEP: ' + value.cep + '</p>';
            html += '      <p>' + value.street + ', ' + value.number + ', ' + value.complement + '</p>';
            html += '      <p>' + value.neighborhood + ' - ' + value.city + '/' + value.state + '</p>';
            html += '    </div>';
            html += '  </div>';
            if (hasError) {
                html += '  <span class="help-block text-danger"><i class="icon-cancel-circle2 position-left"></i> O campo Endereço deve ser preenchido.</span>';
            }
            html += '</div>';
        });
    }
    listAddresses.html(html);
    //badgeJustifiedBadgesTab(client_data_fields, '#justified-badges-tab1');
}

function loadSavedAddresses() {
    var val = $("input[name='clients_id']");
    if (val.val() !== '' && document.getElementById("orders-routes")) {
        var select_address = val.attr('select_address');
        var loadAddresses = document.getElementById("orders-routes").getAttribute("load-addresses");
        $.ajax({
            dataType: "json",
            type: "POST",
            url: loadAddresses,
            data: {
                table_id: val.val(),
                table: 'clients'
            }
        }).done(function (response) {
            formSetClientAddresses(response, select_address);

            /*
             * Calcular frete após a validação do formulário falhar
             */
            var transports_id = $("select[name='transports_id']");
            correiosTable(transports_id);
        });
    }
}

function loadSavedTables() {
    var val = $("input[name='clients_id']");
    if (val.val() !== '' && document.getElementById("orders-routes")) {
        var select_tables = val.attr('select_tables');
        var loadTables = document.getElementById("orders-routes").getAttribute("load-tables");
        $.ajax({
            dataType: "json",
            type: "POST",
            url: loadTables,
            data: {
                client_id: val.val()
            }
        }).done(function (response) {
            var selectedItems = [];
            var selectedIds = [];
            $.each(response, function (key, value) {
                var object = {};
                object = { id: key, text: value };
                selectedItems.push(object);
                selectedIds.push(key);
            });
            var pattern = /[0-9a-zA-Z]+/g;
            var matches = select_tables.match(pattern);

            $("select[name^='tables_id']").select2({
                minimumResultsForSearch: Infinity,
                data: selectedItems
            }).val(selectedIds).on('change', function () {
                setSelectedTablesBudget($(this));
            });
            setSelectedTablesBudget($("select[name^='tables_id']"));
        });
    }
}

/*
 * Buscar cep Novo endereço
 */
$("input[name='cep[0]']").on('blur', function (e) {
    var val = this.value;
    var cep = $(this);
    var street = $("input[name='street[0]']");
    var neighborhood = $("input[name='neighborhood[0]']");
    var city = $("input[name='city[0]']");
    var state = $("input[name='state[0]']");
    if (val !== '' && document.getElementById("correios-cep")) {
        var correiosCep = document.getElementById("correios-cep").getAttribute("correios-cep");
        $.blockUI({ message: '<h4>buscando cep...</h4>' });
        $.ajax({
            dataType: "json",
            type: "GET",
            url: correiosCep + '/' + val
        }).done(function (response) {
            if (response.city !== '') {
                street.val(response.street);
                neighborhood.val(response.neighborhood);
                city.val(response.city);
                state.val(response.state);
            } else {
                cep.val("");
                street.val("");
                neighborhood.val("");
                city.val("");
                state.val("");
            }
        });
    } else {
        street.val("");
        neighborhood.val("");
        city.val("");
        state.val("");
    }
});