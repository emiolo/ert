var route = document.getElementById("route-orders").getAttribute("datatable");
var production = {'production': true};

var options = {
    "paging": false,
    "pageLength": 10,
    "processing": true,
    "serverSide": true,
    //"stateSave": true,
    "columnDefs": [
        //{"targets": [], "orderable": false},
        { "visible": false, "targets": [0, 4, 5, 6] },
    ],
    "ajax": {
        "url": route,
        "type": 'GET',
        "data": production
    },
    "order": [[0, 'asc']],
    //    createdRow: function (row, data, dataIndex) {
    //        $(row).find('td:eq(3)').attr('class', 'btn-group-justified');
    //    },
    "columns": [
        { data: 'sorting', name: 'sorting', searchable: true },
        { data: 'btnProduction', name: 'btnProduction', orderable: false, searchable: false },
        { data: 'orderInfo', name: 'client.fantasy_name', searchable: true },
        { data: 'group', name: 'group' },
        { data: 'order_number', name: 'order_number', searchable: true },
        { data: 'client.company_name', name: 'client.company_name', searchable: true, defaultContent: '-' },
        { data: 'client.cpf_cnpj', name: 'client.cpf_cnpj', searchable: true, defaultContent: '-' },
        { data: 'delivery_date', name: 'delivery_date' },
        { data: 'event_date', name: 'event_date' },
    ]
};
var oTable = $('#datatable').DataTable(options);

if (findGetParameter('q') !== "" && findGetParameter('q') !== null) {
    oTable.search(findGetParameter('q')).draw();
}

var $max_minutes = 60000;
setInterval(function() {
    oTable.ajax.url(route).load();
}, $max_minutes);

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName)
                result = decodeURIComponent(tmp[1]);
        });
    return result;
}
