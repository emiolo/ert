$(document).ready(function () {

    order_comments = $("#order_comments");
    files_table = $("#files_table");
    addComment();
    rmComment();
    addFile();

});

function addComment() {
    $("button#btn_add_comment").on('click', function (e) {
        var text_comment = $("textarea#text_comment");

        if (text_comment.val() !== "") {
            var html = '<dt>agora mesmo - Você</dt>';
            html += '<dd><input type="hidden" name="comments[]" value="' + text_comment.val() + '"/><a class="rmComment">' + text_comment.val() + '</a></dd>';

            order_comments.find("dl").prepend(html);
            text_comment.val("");
            rmComment();
        }
    });
}

function addFile() {
    $("button#btn_add_file").on('click', function (e) {
        var file_name = $("input#file_name");
        var customer_text = $("#customer_text");
        var $clone = $('input#file').clone();
        $clone
                .removeClass('file-styled')
                .removeAttr('id')
                .attr('name', 'files[]');

        var file = document.querySelector('input#file').files[0];
        if (document.querySelector('input#file').value !== "" && file_name.val() !== "") {
            if (/\.(jpe?g|png|gif|doc|docx|xls|xlsx|cdr|psd|pdf)$/i.test(file.name)) {
                var html = '<tr>';
                html += '  <td>';
                html += '    <div class="media-left">';
                html += '      <div>';
                html += '        <input type="hidden" name="file_name[]" value="' + file_name.val() + '"/>';
                html += '        <input type="hidden" name="customer_text[]" value="' + customer_text.val() + '"/>';
                html += '        <div class="hidden_file"></div>';
                html += '        <text-default class="text-semibold">' + file_name.val() + '</text-default>';
                html += '      </div>';
                html += '    </div>';
                html += '  </td>';
                html += '  <td><span> ' + $("input#file_sector").val() + '</span></td>';
                html += '  <td><span> agora mesmo</span></td>';
                html += '  <td class="text-center">';
                html += '    <ul class="icons-list">';
                html += '      <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown"><i class="icon-menu7"></i></a>';
                html += '        <ul class="dropdown-menu dropdown-menu-right">';
                html += '          <li><a class="rmFile"><i class="icon-trash"></i> Excluir</a></li>';
                html += '        </ul>';
                html += '      </li>';
                html += '    </ul>';
                html += '  </td>';
                html += '</tr>';
                files_table.find("tbody").append(html);
                $clone.appendTo(files_table.find('tr').last().find('.hidden_file').hide());
                // Limpar campos
                file_name.val("");
                customer_text.val("");
                $('input#file').val("");

                rmFile();
            } else {
                new PNotify({
                    title: 'Atenção',
                    text: 'Arquivo inválido.',
                    addclass: 'bg-warning alert-styled-right'
                });
            }
        } else {
            new PNotify({
                title: 'Atenção',
                text: 'Os campos Nome do Arquivo e Arquivo são obrigatórios.',
                addclass: 'bg-warning alert-styled-right'
            });
        }
    });
}

function rmComment() {
    order_comments.find("a.rmComment").off('dblclick').on('dblclick', function (e) {
        e.preventDefault();
        var id = $(this).attr('id');
        var index = order_comments.find("a.rmComment").index(this);
        if (typeof id !== "undefined" && id !== "") {
            swal({
                title: 'Atenção',
                text: 'Deseja realmente excluir esse comentário?',
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Não",
                confirmButtonColor: "#03A0E8",
                confirmButtonText: "Sim, excluir!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
            }, function () {
                var url = document.getElementById("orders-routes").getAttribute("remove-order-comment");
                $.ajax({
                    url: url,
                    data: {
                        id: id
                    },
                    type: 'DELETE',
                    dataType: 'json',
                    success: function (data, textStatus, jqXHR) {
                        if (!data.error) {
                            order_comments.find("dt:eq(" + index + "), dd:eq(" + index + ")").remove();
                            swal("Excluído!", data.message, "success");
                        } else {
                            swal("Erro", data.message, "error");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        swal.close();
                    }
                });
            });
        } else {
            order_comments.find("dt:eq(" + index + "), dd:eq(" + index + ")").remove();
        }
    });
}

function rmFile() {
    files_table.find("a.rmFile").off('click').on('click', function (e) {
        e.preventDefault();
        var id = $(this).attr('id');
        var tr = $(this).closest('tr');

        if (typeof id !== "undefined" && id !== "") {
            swal({
                title: 'Atenção',
                text: 'Deseja realmente excluir esse comentário?',
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Não",
                confirmButtonColor: "#03A0E8",
                confirmButtonText: "Sim, excluir!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
            }, function () {
                var url = document.getElementById("orders-routes").getAttribute("remove-order-files");
                $.ajax({
                    url: url,
                    data: {
                        id: id
                    },
                    type: 'DELETE',
                    dataType: 'json',
                    success: function (data, textStatus, jqXHR) {
                        if (!data.error) {
                            tr.remove();
                            swal("Excluído!", data.message, "success");
                        } else {
                            swal("Erro", data.message, "error");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        swal.close();
                    }
                });
            });
        } else {
            tr.remove();
        }
    });
}

function getBase64(file) {
    var base64_file = "";

    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        base64_file = reader.result;
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };

    return base64_file;
}