var $total_order = $("input[name='total_order']");
var $final_total_order = $("input[name='final_total_order']");

if ($total_order.length > 0) {
    var $session_count = 0;

    var credit_value = $("input[name='credit_value']");
    var use_credit_value = $("input[name='use_credit_value']");
    var clients_id = $("input[name='clients_id']");
    var $total_order_span = $("span#total-order");
    var $final_total_order_span = $("span#final-total-order");
    var $volumes = $("input[name='volumes']");
    var $transports = $("select[name='transports_id']");
    var $cost_freight = $("input[name='cost_freight']");
    var $transport_infos = $("div#transport_infos");
    var $boxes_infos = $("div#boxes_infos");
    var $payments_form = $("select[name='payments_form_id']");
    var $discount_type = $("select[name='discount_type']");
    var $discount_value = $("input[name='discount_value']");
    var $transport_type = $("input[name='transport_type']");
    var $transport_deadline = $("input[name='transport_deadline']");
    var $price_table_id = $("select[name='price_table_id']");
    var $budget_tables_selected = $("div#budget-tables-selected");
    var $to_charge = $("input[name='to_charge']");
    var $portion_list = $("#list-value-plots-payments-form");

    var $count_portion = $("#portion");
    var $portion = $("select[name^='portion']");

    var listProducts = $('#listProducts');
    var session_old_input = JSON.parse(listProducts.attr('session-old-input'));

    /* ############################################################################
     * Início
     * Inicializando metodos
     */

    use_credit_value.on('click', function () {
        if ($(this).is(':checked')) {
            credit_value.removeAttr('disabled');
        } else {
            credit_value.attr('disabled', 'disabled');
        }
        calcTotalOrder();
    });
    if (use_credit_value.is(':checked')) {
        credit_value.removeAttr('disabled');
    } else {
        credit_value.attr('disabled', 'disabled');
    }

    if (ordersWeight() > 0 && $transports.val() != "") {
        var $this = $("ul.nav-tabs-highlight li.active a");
        var href = $this.attr('href');
        if (href === "#justified-badges-tab2") {
            //freightTransportsChange($transports);
            layoutCalcBoxes($("#delivery-boxes-load").attr("load"));

            if($('option:selected', $transports).attr('type') != 'correios')
            {
                $transport_infos.html('');
            }
        }
    }

    if ($to_charge.is(':checked') === true) {
        $portion.closest('div.col-lg-1').show();
        $portion_list.show();
    } else {
        $portion.closest('div.col-lg-1').hide();
        $portion_list.hide();
    }
    $to_charge.on('click', function () {
        optionsPaymentsFormToCharge($(this));
        if ($(this).is(':checked') === true) {
            $portion.closest('div.col-lg-1').show();
            $portion_list.show();
        } else {
            $portion.closest('div.col-lg-1').hide();
            $portion_list.hide();
        }

        $payments_form.val("");
    });
    optionsPaymentsFormToCharge($to_charge);

    function optionsPaymentsFormToCharge($to_charge)
    {
        if ($to_charge.is(':checked') === true) {
            $payments_form.children('option[value="4"],option[value="5"],option[value="6"],option[value="7"]').css('display','none');
            $payments_form.children('option[value="1"],option[value="2"],option[value="3"]').css('display','block');
        } else {
            $payments_form.children('option[value="4"],option[value="5"],option[value="6"],option[value="7"]').css('display','block');
            $payments_form.children('option[value="1"],option[value="2"],option[value="3"]').css('display','none');
        }
    }

    plotsPaymentForm();
    function plotsPaymentForm()
    {
        $("select[name^='plots_payment_form[']").off('change').on('change', function() {
            var $this = $(this);
            var div_parent = $this.closest('div.row');
            var val = $this.val();
            var array = new Array('BLT', 'CHQ');
            var indexOf = array.indexOf(val);
            div_parent = $("input[name^='plots_expiration_date[']", div_parent).closest('div.col-lg-2');

            if(indexOf >= 0)
            {
                div_parent.css({
                    display: 'block'
                });
            }
            else
            {
                div_parent.css({
                    display: 'none'
                });
            }
        });
    }

    plotsFile();

    /* 
     * Inicializando metodos
     * Fim
     */

    // Salvar Observação da OP
    textAreaAdjust(document.querySelector('textarea[name="op_observation"]'));
    $('textarea[name="op_observation"]').on('blur', function () {
        var $this = this;
        var val = $this.value;
        var url = $($this).attr('url');
        $.ajax({
            url: url,
            type: "PATCH",
            dataType: "json",
            data: {
                op_observation: val
            },
            success: function (response) {
                textAreaAdjust($this);
                if (response.error) {
                    new PNotify({
                        title: 'Atenção',
                        text: response.message,
                        addclass: 'bg-warning alert-styled-right'
                    });
                }
            },
            error: function () {

            }
        });
    });

    function textAreaAdjust(o) {
        if (typeof (o) != 'undefined' && o != null) {
            o.style.height = "1px";
            o.style.height = (25 + o.scrollHeight) + "px";
        }
    }

    // Setar o id do endereço selecionado
    $("input[name='address_id']").on('click', function () {
        $("input[name='delivery_address_id']").val($(this).val());
    });

    // Adicionar Mais Produtos
    $("button#btnAddProduct").on('click', function () {
        addCardProduct([]);
    });

    // Setar a tabela para todos os cards que não tem uma tabela selecionada
    $price_table_id.on('change', function () {
        var $this = $(this);
        var val = $this.val();
        $('select[name^="custom_price_table_id["]').each(function () {
            var $_this = $(this);
            var _val = $_this.val();
            if (_val == "" || _val == 0) {
                $_this.val(val);
                $_this.select2({
                    minimumResultsForSearch: Infinity,
                    width: "100%",
                    dropdownAutoWidth: true
                });
            }
        });
    });

    // Alterar forma de envio (FRETE)
    $transports.on('change', function () {
        $cost_freight.val("0,00");
        freightTransportsChange($(this));
        $session_count++;
        calcTotalOrder();
    });

    // Atualizar valor do FRETE
    $cost_freight.on('blur', function () {
        $session_count++;
        calcTotalOrder();
    });

    // Atualizar valor Total com desconto
    $discount_type.on('change', function () {
        $session_count++;
        calcTotalOrder();
    });
    $discount_value.on('blur', function () {
        $session_count++;
        calcTotalOrder();
    });

    function plotsFile() {
        $('select[name^="plots_status["]').off('change').on('change', function () {
            var val = this.value;
            var name = this.name;
            var index = name.match(/\d+/g);
            var $this = $(this);
            var href = $this.attr('href');
            var row = $this.closest('div.row').find('.plots_file');

            if (val == "PG") {
                row.html('<input type="file" name="plots_file[' + index + ']" class="file-styled" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-default">');

                /*$(".file-styled", row).uniform('destroy').uniform({
                    radioClass: 'choice',
                    fileDefaultHtml: "Nenhum arquivo selecionado",
                    fileButtonHtml: '<i class="icon-plus2"></i>'
                });*/
                $('.file-styled').fileinput({
                    browseLabel: '',
                    browseIcon: '<i class="icon-images2"></i>',
                    showPreview: false,
                    showCaption: false,
                    showRemove: false,
                    showUpload: false,
                });
                /*$('input[name="plots_file[' + index + ']"]', row).off('change').on('change', function () {
                    var file = this.files[0];
                    var imagefile = file.type;
                    var match = ["image/jpeg", "image/png", "image/jpg"];

                    if (match.indexOf(imagefile) >= 0) {
                        swal({
                            title: 'Atenção',
                            text: "Salvar o comprovante?",
                            html: true,
                            showCancelButton: true,
                            cancelButtonText: "Não",
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Sim",
                            closeOnConfirm: false,
                            showLoaderOnConfirm: true,
                        }, function (isConfirm) {
                            if (isConfirm) {
                                var url = document.getElementById("orders-routes").getAttribute("upload-checking-copy");
                                var plots_id = $('input[name="plots_id[' + index + ']"]', $this.closest('div.row')).val();
                                var data = new FormData();
                                data.append('id', plots_id);
                                data.append('image[]', file);

                                $.ajax({
                                    url: url,
                                    type: "POST",
                                    dataType: "json",
                                    data: data,
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    success: function (response) {
                                        if (!response.error) {
                                            swal("Sucesso", response.message, "success");
                                            row.html('<button class="btn bg-success" title="" data-original-title="Comprovante" data-popup="tooltip" data-placement="top" type="button" href="' + href + '" onclick="editCheckingCopy($(this));"><i class="icon-image3"></i></button>');
                                        } else {
                                            swal("Atenção", response.message, "error");
                                        }
                                    },
                                    error: function () {
                                        swal.close();
                                    }
                                });
                            }
                        });
                    }
                    else
                    {
                        new PNotify({
                            title: 'Atenção',
                            text: "O arquivo selecionado não é uma imagem válida.",
                            addclass: 'bg-warning alert-styled-right'
                        });
                    }
                });*/
            }
            else {
                row.html('');
            }
        });
    }

    /* ############################################################################
     * Início
     * Metodos por CARD de produto
     */

    loadCardMethods();

    // Duplica CARD de produto
    function duplicateProductCard(e) {
        var $this = $(e);
        var card = $this.closest('div.panel');
        var products_index = card.find("input[name^='products_index']").val();
        var products_id = $("select[name='products_id[" + products_index + "]']").val();
        if (products_id != "") {
            var newCard = card.clone();
            $form = $("<form></form>");

            var result = { };
            $.each($form.append(newCard).serializeArray(), function() {
                result[this.name] = this.value;
            });
            result['products_id[' + products_index + ']'] = products_id;

            var variations_id = $("select[name^='variations_id[" + products_index + "][']");
            if(variations_id) {
                variations_id.each(function(k, v) {
                    var val = this.value;
                    var name = $(this).attr('name');
                    result[name] = val;
                });
            }

            var custom_price_table_id = $("select[name='custom_price_table_id[" + products_index + "]']").val();
            result['custom_price_table_id[' + products_index + ']'] = custom_price_table_id;

            var presents_partnerships_id = $("select[name='presents_partnerships_id[" + products_index + "]']").val();
            result['presents_partnerships_id[' + products_index + ']'] = presents_partnerships_id;

            var have_sizes = $("imput:checkbox[name='have_sizes[" + products_index + "]']").is(':checked');
            result['have_sizes[' + products_index + ']'] = (have_sizes === true) ? 'N' : 'Y';

            var session_old_input = decodeURIComponent($.param(result));

            addCardProduct(session_old_input, card);
        } else {
            new PNotify({
                title: 'Atenção',
                text: "Voce deve preencher os campos antes de duplicar.",
                addclass: 'bg-warning alert-styled-right'
            });
        }
    }

    // Carrega os metodos necessários para calculo e outras ações de cada CARD
    function loadCardMethods() {
        $(".row-sortable").sortable({
            connectWith: '.row-sortable',
            items: '.panel',
            helper: 'original',
            cursor: 'move',
            handle: '[data-action=move]',
            revert: 100,
            containment: '.content-wrapper',
            forceHelperSize: true,
            placeholder: 'sortable-placeholder',
            forcePlaceholderSize: true,
            tolerance: 'pointer',
            start: function (e, ui) {
                ui.placeholder.height(ui.item.outerHeight());
            }
        });

        $('.select').select2({
            minimumResultsForSearch: Infinity,
            width: "100%",
            dropdownAutoWidth: true
        });

        // Selecionar um Produto no card
        $("select[name^='products_id[']").off('change').on('change', function () {
            var $this = $(this);
            var card = $this.closest('div.panel');

            getPercentageProductTable(card);

            clearFreightData();

            // setProductDataByCard(card);

            // calcVolumesByCard(card);
            // calcSubtotalCard(card);
        });

        // Validar se os tamanhos digitados não ultrapassem a quantidade total
        $("input[name^='sizes[']")
            .off('blur').on('blur', function (e) {
                var $this = $(this);
                var card = $this.closest('div.panel');
                var quantity_piece = parseInt(0);//($("input[name^='quantity_piece']", card).val() == "") ? parseInt(0) : parseInt($("input[name^='quantity_piece']", card).val());
                var quantity = ($("input[name^='quantity[']", card).val() == "") ? parseInt(0) : parseInt($("input[name^='quantity[']", card).val());
                //quantity = quantity + quantity_piece;

                validationOfSizesByCards(card, quantity);
            });

        // Digitar a quantidade de cada CARD
        $("input[name^='quantity[']")
            .off('change').on('change', function (e) {
                var $this = $(this);
                var card = $this.closest('div.panel');
                $("input[name^='have_sizes[']", card).focus();
            })
            .off('blur').on('blur', function () {
                var $this = $(this);
                var card = $this.closest('div.panel');

                var quantity = ($this.val() == "") ? parseInt(0) : parseInt($this.val());

                validationOfSizesByCards(card, quantity);

                getPercentageProductTable(card);

                clearFreightData();

                // calcVolumesByCard(card);
                // calcSubtotalCard(card);
            });

        // Recalcular o valor unitário de cada CARD baseado na tabela personalizada
        $("select[name^='custom_price_table_id[']").off('change').on('change', function () {
            var $this = $(this);
            var card = $this.closest('div.panel');

            getPercentageProductTable(card);

            // calcVolumesByCard(card);
            // calcSubtotalCard(card);
        });//.val($price_table_id.val());

        // Definir CARD como Brinde/Parceria (o CARD não será calculado no total do pedido/orçamento)
        $("select[name^='presents_partnerships_id']").off('change').on('change', function () {
            var $this = $(this);
            var card = $this.closest('div.panel');

            calcVolumesByCard(card);
            calcSubtotalCard(card);
        });

        // Adicionar variação ao CARD/produto
        $(".addVariationToProduct").off('click').on('click', function () {
            var html = '';
            var card = $(this).closest('div.panel');
            var id_select = $(this).attr('id');
            var products_index = card.find("input[name^='products_index']").val();
            var clone = card.find(".clone-variation").clone();

            clone.find("option").removeAttr("selected");
            clone.find("option[value='" + id_select + "']").attr("selected", "selected");

            html += '<div class="col-lg-6">';
            html += '  <div class="form-group">';
            html += '    <div class="media-body">';
            html += '       <select name="variations_id[' + products_index + '][' + id_select + ']" class="form-control">' + clone.html() + '</select>';
            html += '    </div>';
            html += '    <div class="media-right media-middle">';
            html += '      <ul class="icons-list">';
            html += '        <li><a class="closeVariation"><i class="icon-cross2"></i></a></li>';
            html += '      </ul>';
            html += '    </div>';
            html += '  </div>';
            html += '</div>';

            card.find(".listVariations").append(html);

            cardLoadVariationMethods(card);
            calcSubtotalCard(card);
        });

        // Remover/Excluir um CARD
        $(".closeBlockProduct").off('click').on('click', function (e) {
            e.preventDefault();
            var $this = $(this);
            var card = $this.closest('div.panel');
            var href = $this.attr('href');

            if (typeof (href) !== 'undefined' && href !== "") {
                new PNotify({
                    title: 'Atenção',
                    text: 'Deseja realmente excluir o produto do pedido?',
                    addclass: 'bg-warning',
                    icon: 'glyphicon glyphicon-question-sign',
                    hide: false,
                    confirm: {
                        confirm: true,
                        buttons: [
                            {
                                text: 'Sim',
                                addClass: 'btn-primary',
                                click: function (notice) {
                                    notice.remove();

                                    $.ajax({
                                        url: href,
                                        type: "DELETE",
                                        dataType: "json",
                                        success: function (response) {
                                            if (!response.error) {
                                                card.remove();

                                                calcVolumesTotal();
                                                clearFreightData();
                                                calcTotalOrder();
                                            } else {
                                                new PNotify({
                                                    title: 'Atenção',
                                                    text: response.message,
                                                    addclass: 'bg-warning alert-styled-right'
                                                });
                                            }
                                        },
                                        error: function () {

                                        }
                                    });
                                }
                            },
                            {
                                text: 'Não',
                                click: function (notice) {
                                    notice.remove();
                                }
                            }
                        ]
                    },
                    buttons: {
                        closer: false,
                        sticker: false
                    },
                    history: {
                        history: false
                    }
                });
            } else {
                card.remove();

                calcVolumesTotal();
                clearFreightData();
                calcTotalOrder();
            }

        });

        deleteVariation();
        changeVariation();

        calcVolumesTotal();
        calcTotalOrder();
    }

    // Validar os tamanhos por card
    function validationOfSizesByCards(card, quantity) {
        var total = parseInt(0);
        $("input[name^='sizes[']", card).each(function (index, element) {
            total += ($(element).val() === "") ? parseFloat(0) : parseInt($(element).val());
        });

        if (total > quantity) {
            new PNotify({
                title: 'Atenção',
                text: "A soma dos tamanhos (" + total + ") não deve ultrapassar a quantidade total (" + quantity + ").",
                addclass: 'bg-warning alert-styled-right'
            });
            card.removeClass('panel-black').addClass('panel-warning');
        } else if (total < quantity) {
            new PNotify({
                title: 'Aviso',
                text: "Faltam " + (quantity - total) + " para definir os tamanhos.",
                addclass: 'bg-info alert-styled-right'
            });
            card.removeClass('panel-black').addClass('panel-warning');
        } else {
            card.removeClass('panel-warning').addClass('panel-black');
        }
    }

    // Buscar porcentagem dos valores das tabelas
    function getPercentageProductTable(card) {
        $session_count++;
        var products_id = $("select[name^='products_id[']", card);
        var price_table_id = $("select[name='price_table_id']");
        var custom_price_table_id = $("select[name^='custom_price_table_id[']", card);

        if (custom_price_table_id.val() == "") {
            custom_price_table_id.val(price_table_id.val());
        }

        if (products_id.val() != "" && custom_price_table_id.val() != "") {
            var route = document.getElementById("orders-routes-budget").getAttribute("percentage-product-table");
            $.blockUI({ message: '<h4>aguarde... buscando valor da tabela personalizada</h4>' });
            $.ajax({
                url: route,
                type: "POST",
                dataType: "json",
                data: {
                    products_id: products_id.val(),
                    price_table_id: custom_price_table_id.val()
                },
                success: function (response) {
                    var percentage = response.message;
                    var custom_price_table_option = $("option:selected", custom_price_table_id);
                    custom_price_table_option.attr('percentage', percentage);

                    setProductDataByCard(card);
                    calcVolumesByCard(card);
                    calcSubtotalCard(card);

                    if (percentage !== null) {
                    }
                },
                error: function () {

                }
            });
        } else {

        }
    }

    // Operação com Nome do produto
    function productOpWithName($this) {
        var card = $this.closest('div.panel');
        var orders_products_id = card.find("input[name^='orders_products_id']");
        var route = document.getElementById("orders-routes-budget").getAttribute("product-op-name");

        $.ajax({
            url: route + "/" + orders_products_id.val(),
            type: "GET",
            dataType: "html",
            success: function (html) {
                var modal_remote = $("#modal_remote");
                modal_remote.modal('show').html(html);

                var tableOpNameSaveData = $("#tableOpNameSaveData");

                // Para organizar os tamanhos
                tooltipOpNameOrdination(tableOpNameSaveData);

                rmvOpName(route);
                calculateOpName(card);

                $(".file-styled").uniform({
                    radioClass: 'choice',
                    fileDefaultHtml: "Nenhum arquivo selecionado",
                    fileButtonHtml: '<i class="icon-plus2"></i>'
                });
                $("form#formOpNameExcel").ajaxForm({
                    dataType: "json",
                    beforeSubmit: function (formData, jqForm, options) {
                    },
                    success: function (responseText, statusText, xhr, $form) {
                        if (!responseText.error) {
                            var html = '';
                            var sizes = JSON.parse(tableOpNameSaveData.attr('sizes'));

                            $.each(responseText, function (key, value) {
                                $.each(value, function (k, v) {
                                    html += '<tr class="size_' + v[2] + '">';
                                    html += '   <td><i class="icon-arrow-resize8" title="" data-popup="tooltip" data-original-title="Segure e arraste para organizar"></i></td>';
                                    html += '   <td><a class="text-danger rmvOpName" title="Remover" data-popup="tooltip"><i class="icon-close2"></i></a></td>';
                                    html += '   <td><input type="text" name="name[]" value="' + v[0] + '" class="form-control input-xs" maxlength="100" /></td>';
                                    html += '   <td><input type="number" name="quantity[]" value="' + v[1] + '" class="form-control input-xs integer" maxlength="10" sizes_id="' + v[3] + '" /></td>';
                                    html += '   <td><input type="text" name="size[]" value="' + v[2] + '" class="form-control input-xs ' + sizes[v[2]] + '" maxlength="4" /></td>';
                                    html += '</tr>';
                                });
                            });

                            $("tbody", tableOpNameSaveData).html(html);

                            // Para organizar os tamanhos
                            tooltipOpNameOrdination(tableOpNameSaveData);

                            rmvOpName(route);
                            calculateOpName(card);
                        } else {
                            new PNotify({
                                title: 'Atenção',
                                text: responseText.message,
                                addclass: 'bg-warning alert-styled-right'
                            });
                        }
                    }
                });
                $("form#formOpNameSaveData").ajaxForm({
                    dataType: "json",
                    beforeSubmit: function (formData, jqForm, options) {
                        return validateOnSubmitOpName(card);
                    },
                    success: function (responseText, statusText, xhr, $form) {
                        if (!responseText.error) {
                            productOpWithNameSizes(card, responseText);
                            modal_remote.modal('hide').html("");
                            new PNotify({
                                title: 'Sucesso',
                                text: responseText.message,
                                addclass: 'bg-success alert-styled-right'
                            });
                        } else {
                            new PNotify({
                                title: 'Atenção',
                                text: responseText.message,
                                addclass: 'bg-warning alert-styled-right'
                            });
                        }
                    }
                });
            },
            error: function () {

            }
        });
    }

    function tooltipOpNameOrdination(tableOpNameSaveData) {
        // Para organizar os tamanhos
        $('tbody tr td:eq(0) i', tableOpNameSaveData)
            .attr('title', '')
            .attr('data-popup', 'tooltip')
            .attr('data-original-title', 'Segure e arraste para organizar')
            .tooltip()
            ;
        $('tbody', tableOpNameSaveData).sortable();
        $('tbody', tableOpNameSaveData).disableSelection();
    }

    function productOpWithNameSizes(card, response) {
        var sizes_name_with_op = card.find(".sizes-name-with-op");
        var op_with_name_sizes = card.find(".op-with-name-sizes");
        op_with_name_sizes.html(response.html);
        sizes_name_with_op.hide(0);
    }

    function totalBySizeCardOpName($formOpNameSaveData, id) {
        var sum = parseInt(0);
        $("input[name='quantity[]']", $formOpNameSaveData).each(function (k, v) {
            sum += ($(this).val() == "") ? parseInt(0) : parseInt($(this).val());
        });
        return sum;
    }

    function validateOnSubmitOpName(card) {
        var products_index = $("input[name^='products_index[']", card).val();
        var $formOpNameSaveData = $("form#formOpNameSaveData");
        var quantity_total = ($("input[name='quantity[" + products_index + "]']", card).val() == "") ? parseInt(0) : parseInt($("input[name='quantity[" + products_index + "]']", card).val());
        var flag = true;

        $("input[name='quantity[]']", $formOpNameSaveData).each(function () {
            var $this = $(this);
            var sum_group = totalBySizeCardOpName($formOpNameSaveData);

            if (quantity_total > sum_group) {
                var sub = Math.abs(quantity_total - sum_group);

                flag = false;
            } else if (quantity_total < sum_group) {
                var sub = Math.abs(sum_group - quantity_total);
                $this.val("");

                flag = false;
            }
        });

        if (flag === false) {
            new PNotify({
                title: 'Atenção',
                text: "Soma dos tamanhos não bate com a quantidade total do card.",
                addclass: 'bg-warning alert-styled-right'
            });
        }

        return flag;
    }

    function calculateOpName(card) {
        var products_index = $("input[name^='products_index[']", card).val();
        var $formOpNameSaveData = $("form#formOpNameSaveData");
        var quantity_total = ($("input[name='quantity[" + products_index + "]']", card).val() == "") ? parseInt(0) : parseInt($("input[name='quantity[" + products_index + "]']", card).val());

        $("input[name='quantity[]']", $formOpNameSaveData).off('blur').on('blur', function () {
            var $this = $(this);
            var sum_group = totalBySizeCardOpName($formOpNameSaveData);

            if (quantity_total > sum_group) {
                var sub = Math.abs(quantity_total - sum_group);
                new PNotify({
                    title: 'Atenção',
                    text: "Faltam " + sub + " para completar os tamanhos.",
                    addclass: 'bg-warning alert-styled-right'
                });
            } else if (quantity_total < sum_group) {
                var sub = Math.abs(sum_group - quantity_total);
                $this.val("");
                new PNotify({
                    title: 'Atenção',
                    text: "Soma dos tamanhos ultrapassou por " + sub + " a quantidade total.",
                    addclass: 'bg-warning alert-styled-right'
                });
            }
        });
    }

    function rmvOpName(route) {
        $("a.rmvOpName").on('click', function (e) {
            e.preventDefault();
            var $this = $(this);
            var tr = $this.closest('tr');
            var id_op_name = $('input[name^="id_op_name"]', tr).val();

            if (typeof (id_op_name) == "undefined") {
                tr.remove();
            } else {
                $.ajax({
                    url: route + "/" + id_op_name,
                    type: "DELETE",
                    dataType: "json",
                    success: function (response) {
                        if (!response.error) {
                            tr.remove();
                        } else {
                            new PNotify({
                                title: 'Atenção',
                                text: responseText.message,
                                addclass: 'bg-warning alert-styled-right'
                            });
                        }
                    },
                    error: function (response) {

                    }
                });
            }
        });
    }

    // Upload da arte do produto
    function uploadProductArt($this) {
        var card = $this.closest('div.panel');
        var orders_products_id = card.find("input[name^='orders_products_id']");

        if (typeof (orders_products_id) !== "undefined" && orders_products_id.val() !== "" && orders_products_id.val() > 0) {
            var route = document.getElementById("orders-routes-budget").getAttribute("product-upload-art");
            var routeDestroyArt = document.getElementById("orders-routes-budget").getAttribute("product-destroy-art");

            $.ajax({
                url: route + "/" + orders_products_id.val(),
                type: "GET",
                dataType: "html",
                success: function (html) {
                    var modal_remote = $("#modal_remote");

                    modal_remote.modal('show').html(html);
                    $(".file-styled").uniform({
                        radioClass: 'choice',
                        fileDefaultHtml: "Nenhum arquivo selecionado",
                        fileButtonHtml: '<i class="icon-plus2"></i>'
                    });
                    $("form.formUploadArt").ajaxForm({
                        dataType: "json",
                        beforeSubmit: function (formData, jqForm, options) {

                        },
                        success: function (responseText, statusText, xhr, $form) {
                            if (!responseText.error) {
                                // Inicio
                                // Alterar status da Action para "Arte Aprovada" (4)
                                var sectors_actions_id = $("select[name='sectors_actions_id'] option:selected").val();
                                var action_id = 4;
                                if(typeof(sectors_actions_id) != "undefined" && sectors_actions_id != action_id)
                                {
                                    $.ajax({
                                        url: $("select[name='sectors_actions_id']").attr('href'),
                                        type: 'PATCH',
                                        dataType: 'json',
                                        data: {
                                            action_id: action_id
                                        },
                                        success: function (response) {
                                            $("select[name='sectors_actions_id']")
                                            .val(action_id)
                                            .select2({
                                                minimumResultsForSearch: Infinity,
                                                width: "100%",
                                                dropdownAutoWidth: true
                                            });
                                        },
                                        error: function (response) {
                                        },
                                    });
                                }
                                // Alterar status da Action para "Arte Aprovada" (4)
                                // Fim

                                modal_remote.modal('hide').html("");
                                new PNotify({
                                    title: 'Sucesso',
                                    text: responseText.message,
                                    addclass: 'bg-success alert-styled-right'
                                });
                            } else {
                                new PNotify({
                                    title: 'Atenção',
                                    text: responseText.message,
                                    addclass: 'bg-warning alert-styled-right'
                                });
                            }
                        }
                    });
                    $("#destroy-art").on('click', function () {
                        $.ajax({
                            url: routeDestroyArt + "/" + orders_products_id.val(),
                            type: "DELETE",
                            dataType: "json",
                            success: function (response) {
                                if (!response.error) {
                                    modal_remote.modal('hide').html("");
                                    new PNotify({
                                        title: 'Sucesso',
                                        text: response.message,
                                        addclass: 'bg-success alert-styled-right'
                                    });
                                } else {
                                    new PNotify({
                                        title: 'Atenção',
                                        text: response.message,
                                        addclass: 'bg-warning alert-styled-right'
                                    });
                                }
                            },
                            error: function () {

                            }
                        });
                    });
                },
                error: function () {

                }
            });
        } else {

        }
    }

    // Setar os dados (peso, preço unitário) do produto em inputs
    function setProductDataByCard(card) {
        var totalVariation = calcTotalPriceVariationByCard(card);
        var products_this = $("select[name^='products_id['] option:selected", card);
        var price = toFloat(products_this.attr('price'));
        //var unit_price = parseFloat(price) + parseFloat(totalVariation);
        var unit_price = parseFloat(price);

        $("input[name^='unit_price[']", card).val(convertMoney(getProductUnitPriceTablesByCard(card, unit_price), true));
    }

    // Calcular total das variações por CARD/produto
    function calcTotalPriceVariationByCard(card) {
        var total = parseFloat(0);
        $("select[name^='variations_id[']", card).each(function () {
            var $this = $(this);
            var price = $this.find('option:selected').attr('price');
            var price = (price === "") ? parseFloat(0) : parseFloat(toFloat(price));
            total += price;
        });
        return total;
    }

    // Calcular volume de cada CARD
    function calcVolumesByCard(card) {
        var weight = $("select[name^='products_id['] option:selected", card).attr('weight');
        weight = (weight === "") ? parseFloat(0) : parseFloat(toFloat(weight));

        var quantity = $("input[name^='quantity[']", card).val();
        quantity = (quantity === "") ? parseInt(0) : parseInt(quantity);

        var total = weight * quantity;

        $("input[name^='volume[']", card).val(total);

        calcVolumesTotal();
    }

    // Calcular volume total
    function calcVolumesTotal() {
        var total = parseFloat(0);
        $volumes.val(convertVolume(total));
        $("input[name^='volume']").each(function () {
            var $this = $(this);
            var vol = ($this.val() === "") ? parseFloat(0) : parseFloat(toFloat($this.val()));
            total += vol;
        });
        $volumes.val(convertVolume(total));
    }

    // Calcular subtotal de cada CARD
    function calcSubtotalCard(card) {
        var presents_partnerships_id = $("select[name^='presents_partnerships_id[']", card);

        var total = parseFloat(0);

        if (presents_partnerships_id.val() === "" || presents_partnerships_id.val() == 3 || $('option:selected', presents_partnerships_id).text() == "Sem Nome") {
            setProductDataByCard(card);
            var quantity = getQuantityByCard(card);
            var unit_price = getProductUnitPriceByCard(card);

            total = unit_price * quantity;
        } else {
            $("input[name^='unit_price[']", card).val(convertMoney(0, true));
        }

        $("input[name^='subtotal[']", card).val(convertMoney(total, true));

        //clearFreightData();
        calcTotalOrder();
    }

    // Retorna o valor unitário baseado no percentual das tabelas padrão/personalizadas por CARD
    function getProductUnitPriceTablesByCard(card, price) {
        var totalVariation = calcTotalPriceVariationByCard(card);

        // Tabela padrão
        var price_table_id = $("select[name='price_table_id'] option:selected");
        var price_table_percentage = parseFloat(price_table_id.attr('percentage'));

        // Tabela personalizada
        var custom_price_table_id = $("select[name^='custom_price_table_id[']", card);
        var custom_price_table_option = $("option:selected", custom_price_table_id);
        var custom_price_table_percentage = custom_price_table_option.attr('percentage');

        var custom_or_default_percent = (custom_price_table_percentage <= 0 || custom_price_table_percentage <= "0" || custom_price_table_percentage === "" || typeof (custom_price_table_percentage) === "undefined") ? price : parseFloat(custom_price_table_percentage);
        //var custom_or_default_percent = (custom_price_table_percentage === "" || typeof (custom_price_table_percentage) === "undefined") ? price_table_percentage : parseFloat(custom_price_table_percentage);

        $("input[name^='price_table_value[']", card).val(custom_or_default_percent);

        var unit_price = parseFloat(custom_or_default_percent + parseFloat(totalVariation)).toFixed(2);
        //var unit_price = parseFloat(price - (price * (custom_or_default_percent / 100))).toFixed(2);

        return unit_price;
    }

    // Retorna a quantidade digitada por CARD
    function getQuantityByCard(card) {
        var quantity = $("input[name^='quantity[']", card).val();
        return (quantity === "") ? parseInt(0) : parseInt(quantity);
    }

    // Retorna o preço unitário do produto selecionado no card
    function getProductUnitPriceByCard(card) {
        var unit_price = $("input[name^='unit_price[']", card).val();
        return (unit_price === "") ? parseFloat(0) : parseFloat(toFloat(unit_price));
    }

    function cardLoadVariationMethods(card) {
        // Carregar plugin de dropdown
        /*$('.select').select2({
            minimumResultsForSearch: Infinity,
            width: "100%",
            dropdownAutoWidth: true
        });*/

        deleteVariation();
        changeVariation();
    }

    // Alterar variação
    function changeVariation() {
        $("select[name^='variations_id[']").off('change').on('change', function () {
            var $this = $(this);

            var val = $this.val();
            var name = $this.attr('name');
            name = name.replace(/\]\[(\d+)\]/, '][' + val + ']');
            $this.attr('name', name);

            var card = $this.closest('div.panel');
            calcSubtotalCard(card);
        });
    }

    // Remover/Excluir variações
    function deleteVariation() {
        $("a.closeVariation").off('click').on('click', function (e) {
            e.preventDefault();
            var $this = $(this);
            var id = $this.attr('id');
            var card = $this.closest('div.panel');

            if (id > 0) {
                $.ajax({
                    url: "/",
                    type: "DELETE",
                    dataType: "json",
                    data: {
                        id: id
                    },
                    success: function (response) {
                        $this.closest("div.col-lg-6").remove();
                        calcSubtotalCard(card);
                    },
                    error: function () {

                    },
                });
            } else {
                $this.closest("div.col-lg-6").remove();
                calcSubtotalCard(card);
            }
        });
    }

    /* 
     * Metodos por CARD de produto
     * Fim
     */

    function btnAddSizeName(e) {
        var $this = $(e);
        var row = $this.closest('div.row');
        var name = row.find('input[name="name"]').val();
        var quantity = row.find('input[name="quantity"]').val();
        var size_value = row.find('select[name="size"]').val();
        var size_text = row.find('select[name="size"] option:selected').text();

        var tableOpNameSaveData = $('table#tableOpNameSaveData');

        var tr = '';
        if (name != '' && quantity != '' && size_value != '' && size_text != '') {
            var route = document.getElementById("orders-routes-budget").getAttribute("product-op-name");

            tr += '<tr class="size_' + size_text + '">';
            tr += '   <td><i class="icon-arrow-resize8"></i></td>';
            tr += '   <td><a class="text-danger rmvOpName" title="Remover" data-popup="tooltip"><i class="icon-close2"></i></a></td>';
            tr += '   <td><input type="text" name="name[]" value="' + name + '" class="form-control input-xs" maxlength="100" /></td>';
            tr += '   <td><input type="number" name="quantity[]" value="' + quantity + '" class="form-control input-xs integer" maxlength="10" sizes_id="' + size_value + '" /></td>';
            tr += '   <td><input type="text" name="size[]" value="' + size_text + '" class="form-control input-xs color-' + size_text.toLowerCase() + '" maxlength="4" /></td>';
            tr += '</tr>';

            var sizeHasClass = $('tr', tableOpNameSaveData).hasClass("size_" + size_text);
            if(sizeHasClass === true)
            {
                $(tr).insertAfter($('tr.size_' + size_text + ':last', tableOpNameSaveData));
            }
            else
            {
                if($('input[name="quantity[]"]', tableOpNameSaveData).length == 0)
                {
                    $('tbody', tableOpNameSaveData).html(tr);
                }
                else
                {
                    var sizes    = Object.values(Object.keys(JSON.parse(tableOpNameSaveData.attr('sizes'))));
                    var size_val = "size_" + size_text;
                    var bigger   = false;
                    var length   = $('input[name="quantity[]"]', tableOpNameSaveData).length;

                    for(var i = 0; i < length; i++)
                    {
                        var $this       = $('input[name="quantity[]"]:eq('+i+')', tableOpNameSaveData);
                        var line        = $this.closest('tr');
                        var cl_sz       = line.attr('class');
                        var cl_sz_split = cl_sz.split('_')[1];

                        var indexOf1 = sizes.indexOf(size_text);
                        var indexOf2 = sizes.indexOf(cl_sz_split);

                        if(indexOf1 < indexOf2)
                        {
                            size_text = cl_sz_split;
                            bigger    = true;
                            break;
                        }
                    }

                    if(bigger === true)
                    {
                        $(tr).insertBefore($('tr.size_' + size_text + ':first', tableOpNameSaveData));
                    }
                    else
                    {
                        $(tr).insertAfter($('tr:last', tableOpNameSaveData));
                    }
                }
            }

            // Para organizar os tamanhos
            tooltipOpNameOrdination(tableOpNameSaveData);

            rmvOpName(route);

            $('input, select', row).val('');
        } else {
            new PNotify({
                title: 'Atenção',
                text: 'Os campos Nome, Quantidade e Tamanho são obrigatórios.',
                addclass: 'bg-warning alert-styled-right'
            });
        }
    }

    function onlyUnique(value, index, self) { 
        return self.indexOf(value) === index;
    }

    function addCardProduct(session_old_input, below = false) {
        if (document.getElementById("orders-routes")) {
            var addBlockProduct = document.getElementById("orders-routes").getAttribute("add-block-product");
            var budget_tables_selected = $budget_tables_selected.attr('budget_tables_selected');
            var price_table_id = $price_table_id.val();
            var price_table_value = $("option:selected", $price_table_id).attr('percentage');
            $.blockUI({ message: '<h4>adicionando produto...</h4>' });
            $.ajax({
                dataType: "html",
                type: "GET",
                url: addBlockProduct,
                data: {
                    clients_id: clients_id.val(),
                    session_old_input: session_old_input,
                    budget_tables_selected: budget_tables_selected,
                    price_table_id: price_table_id,
                    price_table_value: price_table_value,
                }
            }).done(function (response) {
                if(below === false) {
                    listProducts.append(response);
                } else {
                    $(response).insertAfter(below);
                }

                loadCardMethods();
            });
        }
    }

    function freightTransportsChange($this) {
        var option = $('option:selected', $this);
        var type = option.attr('type');

        if (type === "correios") {
            var context = $this.context.nodeName;
            if (context != '#document') {
                $transport_type.val('');
            }

            $transport_infos.html('');
            correiosCostFreight();
            //$transport_infos.html('<div class="row"><div class="col-lg-8"><button type="button" class="btn border-slate text-slate-800 btn-flat btn-xs"><i class="icon-truck position-left"></i> Caclcular Frete</button></div></div>');
        } else if (type === "transportadora") {
            var link = option.attr('link');
            var phone = option.attr('phone');
            var comments = option.attr('comments');

            $transport_infos.html('<i class="icon-truck position-left"></i> <a target="_blank" href="' + link + '">Link para a transportadora<br/>\n\
                                            Tel: ' + phone + '<br/>\n\
                                            Obs: ' + comments + '</a>'
            );
        } else {

        }

        layoutCalcBoxes('A');

        //$cost_freight.val("0,00");
    }

    /* INICIO
     * Calcular Caixas
     */
    function layoutCalcBoxes(load = 'A') {
        var url = document.getElementById("orders-routes").getAttribute("consult-boxes");

        if (ordersWeight() > 0) {
            $boxes_infos.html("");
            $boxes_infos
            .css('min-height', '65px')
            .css({ margin: '10px 0px' })
            .block({
                message: '<h5>aguarde, calculando as caixas...</h5>',
                css: { width: '100%' }
            });
            $.ajax({
                dataType: "json",
                type: "POST",
                url: url,
                //async: false,
                data: {
                    load: load,
                    order_id: $("input:hidden[name='order_id']").val(),
                    products_weight: ordersWeight()
                },
                success: function (response) {
                    $boxes_infos.unblock();
                    var error = response.error;
                    var message = response.message;

                    if (error === false) {
                        if (Object.keys(message).length > 0) {
                            var transports_option = $('option:selected', $transports);
                            var transports_type = transports_option.attr('type');
                            var colspan = (transports_type == "correios") ? 5 : 4;

                            var html = '<div class="col-lg-8">';
                            html += '<table class="table table-xs">';
                            html += '   <thead>';
                            html += '       <tr>';
                            html += '           <th colspan="' + colspan + '" class="text-center">Informações das Caixas</th>';
                            html += '       </tr>';
                            html += '       <tr>';
                            html += '           <th>#</th>';
                            html += '           <th>Quantidade (caixas)</th>';
                            html += '           <th>Pesso Total (kg)</th>';
                            html += '           <th>Dimensão</th>';
                            if (transports_type == "correios") {
                                html += '       <th></th>';
                            }
                            html += '       </tr>';
                            html += '   </thead>';
                            html += '   <tbody>';
                            $.each(message, function (key, value) {
                                html += deliveryBox_TR(value);
                            });
                            html += '   </tbody>';
                            html += '   <tfoot>';
                            html += '       <tr>';
                            html += '           <td colspan="' + colspan + '" class="text-center"><button type="button" class="btn btn-black btn-xs" onclick="deliveryBox_Add()"><i class="icon-plus3 position-left"></i> Acrescentar caixa</button></td>';
                            html += '       </tr>';
                            html += '   </tfoot>';
                            html += '</table>';
                            html += '</div>';

                            $boxes_infos.html(html);

                            $('.weight').mask("#0.000", { reverse: true });
                        }
                    } else {
                        new PNotify({
                            title: 'Atenção',
                            text: message,
                            addclass: 'bg-warning alert-styled-right'
                        });
                    }
                },
                error: function () {
                    $boxes_infos.unblock();
                }
            });
        } else {
            $boxes_infos.html("");
            new PNotify({
                title: 'Atenção',
                text: 'É necessário peso mínimo dos produto para o cálculo do frete.',
                addclass: 'bg-warning alert-styled-right'
            });
        }
    }

    function deliveryBox_Remove($this)
    {
        $this.closest('tr').remove();
    }

    function deliveryBox_Add()
    {
        $('tbody', $boxes_infos).append(deliveryBox_TR());
    }

    function deliveryBox_TR(value)
    {
        var print_correios_label_url = document.getElementById("orders-routes-budget").getAttribute("print-correios-label");
        var transports_option = $('option:selected', $transports);
        var transports_type = transports_option.attr('type');
        var total_box = total_weight = total_length = total_width = total_height = 0;
        if(value != "" && value != null)
        {
            total_box = (typeof(value.quantity) != 'undefined') ? value.quantity : value.weight.length;
            total_weight = convertVolume(value.weight.reduce(summing, 0));
            total_length = value.length;
            total_width = value.width;
            total_height = value.height;
        }

        var tr = '<tr>';
        tr += '<td><a class="text-danger" onclick="deliveryBox_Remove($(this))"><i class="icon-close2"></i></a></td>';
        tr += '<td><input type="number" min="1" name="box_quantity[]" class="form-control" value="' + total_box + '" onblur="defineParamsLabel($(this));" /></td>';
        tr += '<td><input type="text" name="box_weight[]" class="form-control weight" value="' + total_weight + '" onblur="defineParamsLabel($(this));" /></td>';
        var dimension = "";
        if(value != "" && value != null)
        {
            if(value.hasOwnProperty('dimension')){
                dimension = value.dimension;
            }
        }
        tr += '<td>' + formSelect(dimension) + '</td>';
        if (transports_type == "correios") {
            tr += '   <td><button total_height="' + total_height + '" total_width="' + total_width + '" total_length="' + total_length + '" total_weight="' + total_weight + '" total_box="' + total_box + '" print_correios_label_url="' + print_correios_label_url + '" onclick="windowOpen($(this));" title="" data-popup="tooltip" data-title="Gerar Etiqueta" data-placement="right" type="button" class="btn border-slate text-slate-600 btn-flat btn-icon btn-xs"><i class="icon-price-tags"></i></button></td>';
        }
        tr += '</tr>';

        return tr;
    }

    function defineParamsLabel($this)
    {
        var tr = $this.closest('tr');
        var option = $('select[name="box_dimension[]"] option:selected', tr);
        var height = option.attr('height');
        var width = option.attr('width');
        var length = option.attr('length');
        var weight = option.attr('weight');

        var button = $('button[type="button"]', tr);
        button.attr('total_height', height);
        button.attr('total_width', width);
        button.attr('total_length', length);
        button.attr('total_weight', weight);
    }

    function formSelect(id = "")
    {
        var boxes = JSON.parse(document.getElementById("orders-routes-budget").getAttribute("delivery-boxes"));

        var select = '<select name="box_dimension[]" class="form-control" onchange="defineParamsLabel($(this));">';
        $.each(boxes, function(index, item) {
            var split  = item.split('|');
            var length = split[0];
            var width  = split[1];
            var height = split[2];
            var weight = split[3];

            var selected = (id == index) ? 'selected' : '';
            select += '<option height="' + height + '" width="' + width + '" length="' + length + '" weight="' + weight + '" value="' + index + '" ' + selected + '>' + "{0}x{1}x{2}cm {3}kg".format(length, width, height, weight) + '</option>';
        });
        select += '</select>';

        return select;
    }
    /* FIM
     * Calcular Caixas
     */

    function windowOpen($this) {
        var print_correios_label_url = $this.attr('print_correios_label_url');
        var total_box = $this.attr('total_box');
        var total_weight = $this.attr('total_weight');
        var total_length = $this.attr('total_length');
        var total_width = $this.attr('total_width');
        var total_height = $this.attr('total_height');
        var transport_freight = $("input[name='transport_freight']:checked").val();

        if (typeof (transport_freight) == "undefined") {
            new PNotify({
                title: 'Atenção',
                text: 'Selecione um tipo de frete.',
                addclass: 'bg-warning alert-styled-right'
            });
        } else {
            window.open(print_correios_label_url + '?transport_freight=' + transport_freight + '&total_box=' + total_box + '&weight=' + total_weight + '&length=' + total_length + '&width=' + total_width + '&height=' + total_height, "newwindow", "width=550,height=700");
        }
    }

    function summing(a, b) {
        return parseFloat(a) + parseFloat(b);
    }

    function calcTotalOrder() {
        var total = parseFloat(0);
        $("input[name^='subtotal']").each(function () {
            var $this = $(this);
            var price = ($this.val() === "") ? parseFloat(0) : parseFloat(toFloat($this.val()));
            total += price;
        });
        incrementFreightValueToTotalOrder(total);
    }

    function incrementFreightValueToTotalOrder(subtotal) {
        //var total = parseFloat(0);
        var freight_value = $cost_freight.val();
        var freight = (freight_value === "") ? parseFloat(0) : parseFloat(toFloat(freight_value));
        //total = subtotal + freight;

        totalFinalOrder(subtotal, freight);

        $total_order_span.html(convertMoney(subtotal, true));
        $total_order.val(convertMoney(subtotal));
    }

    function totalFinalOrder(subtotal, freight) {
        var discount = $discount_value.val();
        var discount_type = $discount_type.val();
        var v = (discount == "") ? parseFloat(0) : parseFloat(toFloat(discount));

        var total = parseFloat(0);

        if (discount_type == "P") {
            total = (subtotal - (subtotal * (v / 100))) + freight;
        } else {
            total = (subtotal - v) + freight;
        }

        if (use_credit_value.is(':checked')) {
            total = total - parseFloat(toFloat(credit_value.val()));
        }

        $final_total_order_span.html(convertMoney(total, true));
        $final_total_order.val(convertMoney(total));

        var max_plots = $('option:selected', $payments_form).attr('max_plots');
        if (checkIfAnyPayPlots() === false) {
            generatePortions(max_plots);
        }

        if (typeof ($("option:selected", $portion)) != "undefined") {
            portionPlotsPaymentsForm($("option:selected", $portion));
        }
    }

    function checkIfAnyPayPlots() {
        var pay = false;
        $("select[name^='plots_status']").each(function () {
            var val = this.value;
            if (val == "PG") {
                pay = true;
            }
        });

        return pay;
    }

    function toFloat(value) {
        if (typeof (value) !== "undefined") {
            var index = value.indexOf(",");
            if (index >= 0) {
                return parseFloat(value.replace('.', '').replace(',', '.'));
            }
            return parseFloat(value);
        } else {
            return 0;
        }
    }

    function convertMoney(value, formatted) {
        if (formatted === true) {
            return $.number(value, 2, ',', '.');
        }
        return value;
    }

    function convertVolume(value, formatted) {
        if (formatted === true) {
            return $.number(value, 3, ',', '');
        }
        return $.number(value, 3, '.', '');
    }

    /*
     * Volume do pedido
     */
    function ordersWeight() {
        return ($volumes.val() === "") ? 0 : $volumes.val();
    }

    /* ############################################################################
     * Início
     * TRATAMENTOS FRETE
     */

    // Calcula o frete dos Correios, busca os dados da Transportadora e calcula o total do pedido
    function correiosCostFreight() {
        var url = document.getElementById("orders-routes").getAttribute("consult-freight");

        if (ordersWeight() > 0) {
            var cep = $("input:hidden[name='delivery_cep']").val();

            if (typeof (cep) !== "undefined" && cep !== "") {
                var transport_deadline = $transport_deadline.val();
                var cost_freight = $cost_freight.val();

                if (transport_deadline == "" && cost_freight == "") {
                    $.blockUI({ message: '<h4>aguarde...</h4><h5>buscando opções de frete dos correios!</h5>' });
                }
                $transport_infos.css({ height: '60px', margin: '10px 0px 10px 0px' }).block({
                    message: '<h5>carregando as opções de frete...</h5>',
                    css: { width: '100%' }
                });
                /*new PNotify({
                    title: 'Correios',
                    text: '<h5>carregando as opções de frete...</h5>',
                    addclass: 'stack-bottom-left bg-info',
                    hide: false
                });*/
                $.ajax({
                    dataType: "json",
                    type: "POST",
                    url: url,
                    //async: false,
                    data: {
                        cep: cep,
                        freight_type: 'correios',
                        type: 'pac,sedex',
                        products_weight: ordersWeight()
                    },
                    success: function (response) {
                        $.unblockUI();
                        $transport_infos.unblock();
                        /*PNotify.removeAll();
                        new PNotify({
                            text: '<h5>as opções de frete foram carregadas com sucesso.</h5>',
                            addclass: 'stack-bottom-left bg-success',
                        });*/
                        var error = response.error;
                        var message = response.message;

                        if (error === false) {
                            var transport_type = $transport_type.val();
                            var html = '';

                            $.each(message, function (key, value) {
                                var total = parseFloat(0);
                                var deadline = parseInt(0);
                                var service_name = value[0].nome;
                                var service_code = value[0].codigo;

                                $.each(value, function (k, v) {
                                    deadline = v.deadline;
                                    var val = v.value;
                                    total += (val === "") ? parseFloat(0) : parseFloat(val);
                                });

                                html += '<div class="col-xs-6 col-sm-3">';
                                html += '   <div class="list-group no-border">';
                                html += '       <a class="list-group-item"><input type="radio" name="transport_freight" class="styled" value="' + service_code + '" ' + ((transport_type == service_code && transport_type != '') ? "checked" : "") + ' /> Tipo: ' + service_name + '</a>';
                                html += '       <a class="list-group-item"><i class="icon-cash"></i> Valor: R$ ' + convertMoney(total, true) + '<input type="hidden" name="transport_price_' + service_code + '" value="' + $.number(total, 2, '.', '') + '"/></a>';
                                html += '       <a class="list-group-item"><i class="icon-home"></i> Entrega em até: ' + deadline + ' dias úteis. <input type="hidden" name="transport_deadline_' + service_code + '" value="' + deadline + '"/></a>';
                                html += '   </div>';
                                html += '</div>';
                            });

                            $transport_infos.html(html);

                            loadFreightMethods();
                        } else {
                            new PNotify({
                                title: 'Atenção',
                                text: message,
                                addclass: 'bg-warning alert-styled-right'
                            });
                        }
                    },
                    error: function () {
                        $.unblockUI();
                        $transport_infos.css({ height: '60px', margin: '10px 0px 10px 0px' }).block({
                            message: '<h5>Não foi possível carregar as oções de frete, tente novamente.</h5>',
                            css: { width: '100%', backgroundColor: '#E1B6B6', color: '#000' },
                            overlayCSS: { backgroundColor: 'transparent' }
                        });
                        /*PNotify.removeAll();
                        new PNotify({
                            title: 'Correios',
                            text: '<h5>Não foi possível carregar as oções de frete, tente novamente.</h5>',
                            addclass: 'bg-warning alert-styled-right',
                        });*/
                    }
                });
            } else {
                new PNotify({
                    title: 'Correios',
                    text: '<h5>Não foi possível carregar as oções de frete, tente novamente.</h5>',
                    addclass: 'bg-warning alert-styled-right'
                });
            }
        } else {
            new PNotify({
                title: 'Atenção',
                text: 'É necessário peso mínimo dos produto para o cálculo do frete.',
                addclass: 'bg-warning alert-styled-right'
            });
        }
    }

    // Limpar dados do frete
    function clearFreightData() {
        $transports.val("").select2({
            minimumResultsForSearch: Infinity,
            width: "100%",
            dropdownAutoWidth: true,
        });
        $transport_infos.html("");
        $boxes_infos.html("");
        $cost_freight.val(convertMoney(0, true));
    }

    // Carregar funcções/metodos
    function loadFreightMethods() {
        $("input[name='transport_freight']").on('click', function () {
            var $this = $(this);
            var value = $this.val();
            var price = $("input[name='transport_price_" + value + "']").val();
            var deadline = $("input[name='transport_deadline_" + value + "']").val();

            $cost_freight.val(convertMoney(Math.ceil(price), true));
            $transport_deadline.val(deadline);

            $session_count++;
            calcTotalOrder();
        });
        $.uniform.update("input[name='transport_freight']");
    }

    /*
     * TRATAMENTOS FRETE
     * Fim
     */


    /* ############################################################################
     * Início
     * TRATAMENTOS PAGAMENTO
     */

    generatePortions($('option:selected', $payments_form).attr('max_plots'));

    $payments_form.on('change', function () {
        $session_count++;
        var $this = $(this);
        var option = $('option:selected', $this);
        var max_plots = option.attr('max_plots');
        plotsPaymentsForm(max_plots);
    });

    $discount_type.on('change', function () {
        var $this = $(this);
        var value = $this.val();
    });

    /*
     * Parcelas da forma de pagamento
     *
     * @param obj (var payments_form_id)
     */
    function generatePortions(max_plots) {
        var selectedItems = [];
        if (max_plots != 2) {
            for (var i = 1, max = max_plots; i <= max; i++) {
                //var object = { id: i, text: i };
                var object = '<option value="' + i + '">' + i + '</option>';
                selectedItems.push(object);
            }
        } else {
            //var object = { id: 2, text: 2 };
            var object = '<option value="2">2</option>';
            selectedItems.push(object);
        }

        var portion = null;
        if ($portion.val() == null) {
            if ($count_portion.val() != "" && $count_portion.val() > 0) {
                portion = $count_portion.val();
            } else {
                portion = $("option:first", $portion).val();
            }
        } else {
            portion = $portion.val();
        }

        $portion
            .html("")
            .append(selectedItems.join(''))
            .val(portion)
            .on('change', function () {
                $session_count++;
                portionPlotsPaymentsForm($(this));
            });

        if ($count_portion.val() == "" || $count_portion.val() <= 0) {
            portionPlotsPaymentsForm($portion);
        }
    }

    function plotsPaymentsForm(max_plots) {
        generatePortions(max_plots);
        $portion.val($("option:first", $portion).val());
        portionPlotsPaymentsForm($portion);
    }

    /*
     * Valor das parcelas da forma de pagamento
     *
     * @param obj (var plots)
     */
    function portionPlotsPaymentsForm($this) {
        var total_order = parseFloat($final_total_order.val());
        var max = parseInt($this.val());
        var $plots_input = $("input[name^='plots[']");
        var plots_length = $plots_input.length;
        var html = "";

        var paymentsForms = JSON.parse(document.getElementById("orders-routes-budget").getAttribute("attributes-payments-forms"));
        var sum = parseFloat(0);

        var selectedItems = [];
        $.each(paymentsForms, function (k, v) {
            //var object = { id: k, text: v };
            var object = "<option value='" + k + "'>" + v + "</option>";
            selectedItems.push(object);
        });

        if (plots_length > 0) {
            if ($session_count == 0)
                return;

            // Somar parcelas em uso
            $plots_input.each(function () {
                var $this2 = $(this);
                var val = toFloat($this2.val());

                sum += parseFloat(val);
            });

            var plots_unpaid = $("select[name^='plots_status['] option[value!='PG']:selected").length; // AP
            var plots_paid = $("select[name^='plots_status['] option[value='PG']:selected").length;
            var total_paid = totalPaidOrUnpaid('paid');

            if(sum == parseFloat(total_order).toFixed(2) && plots_unpaid <= 0)
            {
                return;
            }

            /*console.group("Todas Variaveis:");
            console.log("plots_unpaid: ", plots_unpaid)
            console.log("total_paid: ", total_paid)
            console.log("plots_paid: ", plots_paid);
            console.log("total_order: ", total_order);
            console.log("sum: ", sum);
            console.log("max: ", max);
            console.log("plots_length: ", plots_length);
            console.log("session_count: ", $session_count);
            console.groupEnd();*/

            if (total_order != total_paid)
            {
                if (max > plots_length) { // Acrescenta mais parcelas
                    //console.log('## A ##');
                    var bg_color = " bg-warning ";
                    var next_i = plots_paid + 1;
                    var value = (total_order - total_paid) / (max - plots_paid);

                    // Remover parcelas maiores do que a selecionada
                    $("select[name^='plots_status['] option[value!='PG']:selected").closest('div.port-line').remove(); // AP

                    for (var i = next_i; i <= max; i++) {
                        $portion_list.append(portionHtmlLine(i, bg_color, value));
                    }

                    var ap = $("select[name^='plots_status['] option[value!='PG']:selected"); // AP
                    for(var i = 0; i < ap.length; i++)
                    {
                        $('.plots_payment_form', ap[i].closest('div.port-line')).html(selectedItems.join(''));
                    }
                } else if (max < plots_length) { // Remove parcelas
                    //console.log('## B ##');
                    // Remover parcelas maiores do que a selecionada
                    var next_i = max + 1;
                    for (var i = next_i; i <= plots_length; i++) {
                        var div_portion = $("select[name='plots_status[" + i + "]']");
                        if (div_portion.val() != 'PG') {
                            div_portion.closest('div.port-line').remove();
                        }
                    }

                    // Recalcular o valor das parcelas Não Pagas
                    for (var i = 1; i <= max; i++) {
                        var portion_val = $("select[name='plots_status[" + i + "]']").val();
                        if (portion_val != 'PG') {
                            var value = (total_order - total_paid) / (max - plots_paid);
                            $("input[name='plots[" + i + "]']").val(convertMoney(value, true));
                        }
                    }
                } else {
                    //console.log('## C ##');
                    // Recalcular o valor das parcelas Não Pagas
                    for (var i = 1; i <= max; i++) {
                        var portion_val = $("select[name='plots_status[" + i + "]']").val();
                        if (portion_val != 'PG') {
                            var value = (total_order - total_paid) / (max - plots_paid);
                            $("input[name='plots[" + i + "]']").val(convertMoney(value, true));
                        }
                    }
                }
            }
            else
            {
                // Remover parcelas maiores do que a selecionada
                $("select[name^='plots_status['] option[value!='PG']:selected").closest('div.port-line').remove(); // AP
                $("input[id='portion']").val(plots_paid);
            }
        }
        else
        { // Cria todas as parcelas
            var value = total_order / max;
            var bg_color = " bg-warning ";

            for (var i = 1; i <= max; i++) {
                html += portionHtmlLine(i, bg_color, value);
            }

            $portion_list.html(html);

            $('.plots_payment_form').html(selectedItems.join(''));
        }

        /*
         * Esse trecho faz o reajuste dos centavos arredondados e coloca na primeira parcela
         */
        var total_unpaid = totalPaidOrUnpaid('unpaid');
        var paid_unpaid = total_paid + total_unpaid;

        if(paid_unpaid > 0 && paid_unpaid != total_order)
        {
            var round_total = paid_unpaid - total_order;
            if(round_total != 0)
            {
                var into = 0;
                for (var i = 1; i <= max; i++) {
                    var portion_val = $("select[name='plots_status[" + i + "]']").val();
                    if (portion_val != 'PG') {
                        var portion1_value = $("input[name='plots[" + i + "]']");
                        if(typeof(portion1_value) != 'undefined' && into === 0)
                        {
                            var v = ((total_order - total_paid) / (max - plots_paid)) - round_total;
                            portion1_value.val(convertMoney(v, true));
                            into++;
                            break;
                        }
                    }
                }
            }
        }

        $('.date').formatter({
            pattern: '{{99}}/{{99}}/{{9999}}'
        });
        $('.datepicker').datepicker("destroy").datepicker({
            format: 'dd/mm/yyyy',
            todayHighlight: true,
        });
        $('.select22').select2({
            minimumResultsForSearch: Infinity,
            width: "100%",
            dropdownAutoWidth: true
        });

        plotsPaymentForm();
        plotsFile();
    }

    function totalPaidOrUnpaid(type) {
        var total = parseFloat(0);
        $("input[name^='plots[']").each(function () {
            var $this = $(this);
            var index = parseInt($this.attr('index'));
            var val = $this.val();
            var plots_status = $('select[name="plots_status[' + index + ']"]').val();

            if (plots_status != 'PG' && type == 'unpaid') {
                total += parseFloat(toFloat(val));
            } else if (plots_status == 'PG' && type == 'paid') {
                total += parseFloat(toFloat(val));
            }
        });
        return parseFloat(total);
    }

    function portionHtmlLine(i, bg_color, value) {
        var html = '';
        html += '<div class="form-group text-left port-line">';
        html += '<div class="row">';
        html += '<div class="col-lg-1 col-md-1 col-sm-1">';
        html += '   <div class="form-group">';
        html += '       <a title="Remover" onclick="$(this).closest(\'.port-line\').remove();"><i class="icon-cross2 text-danger"></i></a>';
        html += '       ' + i + "ª Parcela";
        html += '   </div>';
        html += '</div>';
        html += '<div class="col-lg-2 col-md-2 col-sm-2" style="width: 120px;">';
        html += '   <div class="form-group">';
        html += '       <input index="' + i + '" onkeyup="recalculatePortions($(this));" class="form-control decimal' + bg_color + '" type="text" name="plots[' + i + ']" placeholder="Parcela ' + i + '" value="' + convertMoney(value, true) + '">';
        html += '   </div>';
        html += '</div>';
        html += '<div class="col-lg-2 col-md-2 col-sm-2" style="width: 120px; display: none;">';
        html += '   <div class="form-group">';
        html += '       <input class="form-control' + bg_color + '" type="text" name="plots_expiration_date[' + i + ']" placeholder="Prazo" value="">';
        html += '   </div>';
        html += '</div>';
        html += '<div class="col-lg-2 col-md-2 col-sm-2" style="width: 120px;">';
        html += '   <div class="form-group">';
        html += '       <input class="form-control date datepicker' + bg_color + '" type="text" name="plots_date[' + i + ']" placeholder="Data Pgto." value="">';
        html += '   </div>';
        html += '</div>';
        html += '<div class="col-lg-2 col-md-2 col-sm-2" style="width: 150px;">';
        html += '   <div class="form-group">';
        html += '       <select class="plots_payment_form' + bg_color + ' form-control" name="plots_payment_form[' + i + ']"></select>';
        html += '   </div>';
        html += '</div>';
        html += '<div class="col-lg-2 col-md-2 col-sm-2">';
        html += '   <div class="form-group">';
        html += '       <select class="' + bg_color + ' form-control" name="plots_status[' + i + ']">';
        html += generateSelectPlotsStatus();
        html += '       </select>';
        html += '   </div>';
        html += '</div>';
        html += '<div class="col-lg-1 col-md-1 col-sm-1 plots_file">';
        html += '   <div class="form-group">&nbsp;';
        html += '   </div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';

        return html;
    }

    function generateSelectPlotsStatus(type = 'html')
    {
        var paymentsForms = JSON.parse(document.getElementById("orders-routes-budget").getAttribute("attributes-payment-status"));
        var selectedItems = [];
        $.each(paymentsForms, function (k, v) {
            var object = null;

            if(type == 'html')
            {
                object = "<option value='" + k + "'>" + v + "</option>";
            }
            else
            {
                object = { id: k, text: v };
            }

            if(object !== null)
                selectedItems.push(object);
        });

        return selectedItems;
    }

    function recalculatePortions($this) {
        var plots = $("input[name^='plots[']");
        var plots_length = plots.length;
        var index = parseInt($this.attr('index'));
        var div = parseInt(plots_length) - index;

        if (div > 0) {
            var total_order = $final_total_order.val();
            var accumulate = parseFloat(0);
            // Somando os valores até o campo atual.
            for (var i = 1, max = index; i <= max; i++) {
                var _plots = $("input[name='plots[" + i + "]']");
                accumulate += parseFloat(toFloat(_plots.val()));
            }
            // Dividindo o resto para o restante dos campos.
            for (var i = (index + 1), max = plots_length; i <= max; i++) {
                var _plots = $("input[name='plots[" + i + "]']");
                var value = (parseFloat(total_order) - parseFloat(accumulate)) / parseInt(div);
                _plots.val(convertMoney(value, true));
            }
            accumulate = 0;
        }
    }

    function editCheckingCopy($this) {
        var route = $this.attr('href');

        $.ajax({
            url: route,
            type: "GET",
            dataType: "html",
            success: function (html) {
                var modal_remote = $("#modal_remote");

                modal_remote.modal('show').html(html);
                $(".file-styled").uniform({
                    radioClass: 'choice',
                    fileDefaultHtml: "Nenhum arquivo selecionado",
                    fileButtonHtml: '<i class="icon-plus2"></i>'
                });
                $("form.formUploadCheckingCopy").ajaxForm({
                    dataType: "json",
                    beforeSubmit: function (formData, jqForm, options) {
                        $.blockUI({ message: '<h4>aguarde...</h4>' });
                    },
                    success: function (responseText, statusText, xhr, $form) {
                        $.unblockUI();
                        if (!responseText.error) {
                            modal_remote.modal('hide').html("");
                            new PNotify({
                                title: 'Sucesso',
                                text: responseText.message,
                                addclass: 'bg-success alert-styled-right'
                            });
                        } else {
                            new PNotify({
                                title: 'Atenção',
                                text: responseText.message,
                                addclass: 'bg-warning alert-styled-right'
                            });
                        }
                    }
                });
                $(".destroy-checking-copy").on('click', function (e) {
                    e.preventDefault();

                    var $this = $(this);
                    var div = $this.closest('div.col-lg-3');
                    var url = $this.attr('href');

                    swal({
                        type: 'warning',
                        title: 'Atenção',
                        text: "Deseja realmente excluir o comprovante?",
                        html: true,
                        showCancelButton: true,
                        cancelButtonText: "Não",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Sim, excluir!",
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true,
                    }, function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: url,
                                type: "DELETE",
                                dataType: "json",
                                success: function (response) {
                                    swal.close();
                                    if (!response.error) {
                                        div.remove();
                                        new PNotify({
                                            title: 'Sucesso',
                                            text: response.message,
                                            addclass: 'bg-success alert-styled-right'
                                        });
                                    } else {
                                        new PNotify({
                                            title: 'Atenção',
                                            text: response.message,
                                            addclass: 'bg-warning alert-styled-right'
                                        });
                                    }
                                },
                                error: function () {
                                    swal.close();
                                }
                            });
                        }
                    });
                });
            },
            error: function () {

            }
        });
    }

    /* 
     * TRATAMENTOS PAGAMENTO
     * Fim
     */

    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() > $(document).height() - 40) {
            $('.fab-menu-bottom-right').addClass('reached-bottom');
        } else {
            $('.fab-menu-bottom-right').removeClass('reached-bottom');
        }
    });
}