<?php

/** Sotnas
 * Formata as moedas de 1000.00 para 1.000,00
 * 
 * @return int
 */
if (!function_exists('moneyUnFormat'))
{

    function moneyUnFormat($number)
    {
        return str_replace(",", ".", str_replace(".", "", $number));
    }

}

/** Sotnas
 * Formata as moedas de 1.000,00 para 1000.00
 * 
 * @return int
 */
if (!function_exists('moneyFormat'))
{

    function moneyFormat($number)
    {
        return number_format($number, 2, ",", ".");
    }

}

/** Sotnas
 * Formata as moedas de 1.000,00 para 1000.00
 * 
 * @return int
 */
if (!function_exists('weightFormat'))
{

    function weightFormat($number)
    {
        return number_format($number, 3, ".", "");
    }

}

/** Sotnas
 * Retorna todos os tipos de frete disponíveis (ou específico) dos Correios (sedex,sedex_a_cobrar,sedex_10,sedex_hoje,pac,pac_contrato,sedex_contrato,esedex)
 * 
 * @return int
 */
if (!function_exists('correiosTypesFreight'))
{

    function correiosTypesFreight()
    {
        $assoc_arr = array_reduce(array_keys(\Correios::getTipos()), function ($result, $item) {
            $result[$item] = str_replace("_", " ", title_case($item));
            return $result;
        }, array());

        return $assoc_arr;
    }

}

/** Sotnas
 * Gerar index automatico para o box de produtos em pedidos
 * 
 * @return int
 */
if (!function_exists('autoIndex'))
{

    function autoIndex()
    {
        $t     = microtime(true);
        $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
        $d     = new DateTime(date('Y-m-d H:i:s.' . $micro, $t));
        return $d->format("ymdHisu");
    }

}

/** Sotnas
 * Consultar Frete nos Correios
 * 
 * Observe a variável $data no método para entender os paramêtros de consulta
 * 
 * @return int
 */
if (!function_exists('correiosConsultFreight'))
{

    function correiosConsultFreight(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
                    //'tipo' => 'required|in:sedex,sedex_a_cobrar,sedex_10,sedex_hoje,pac,pac_contrato,sedex_contrato,esedex',
                    'tipo'        => 'required',
                    'formato'     => 'required|in:caixa,rolo,envelope',
                    'cep_destino' => 'required|min:8|max:9',
                    'cep_origem'  => 'min:8|max:9',
                    'peso'        => 'required|numeric',
                    'comprimento' => 'required|numeric',
                    'altura'      => 'required|numeric',
                    'largura'     => 'required|numeric',
        ]);
        if ($validator->fails())
        {
            $messages = $validator->messages();
            $message  = "";
            foreach ($messages->all(':message<br/>') as $value)
            {
                $message .= $value;
            }
            $result = [
                'error'   => true,
                'message' => $message
            ];
            return collect($result);
        }

        $client_data = \Modules\CerebeloSettings\Entities\ClientsData::first();
        if (empty($request->get('cep_origem')) && (empty($client_data) || !isset($client_data->cep)))
        {
            $result = [
                'error'   => true,
                'message' => 'Cep de origem não encontrado no sistema. (Cerebelo > Dados do Cliente)'
            ];
            return collect($result);
        }

        $cep_origem        = $request->get('cep_origem', $client_data->cep);
        $tipo              = $request->get('tipo', 'pac');
        $formato           = $request->get('formato', 'caixa');
        $cep_destino       = $request->get('cep_destino', '');
        $peso              = $request->get('peso', 1);
        $comprimento       = $request->get('comprimento', 16);
        $altura            = $request->get('altura', 11);
        $largura           = $request->get('largura', 11);
        $diametro          = $request->get('diametro', 0);
        $empresa           = $request->get('empresa', '13414690');
        $senha             = $request->get('senha', '10845338'); // 10845338 | dc6e9y
        $mao_propria       = $request->get('mao_propria', 0);
        $valor_declarado   = $request->get('valor_declarado', 0);
        $aviso_recebimento = $request->get('aviso_recebimento', 0);

        $data = [
            'tipo'              => $tipo, // opções: `sedex`, `sedex_a_cobrar`, `sedex_10`, `sedex_hoje`, `pac`, 'pac_contrato', 'sedex_contrato' , 'esedex'
            'formato'           => $formato, // opções: `caixa`, `rolo`, `envelope`
            'cep_destino'       => $cep_destino, // Obrigatório
            'cep_origem'        => $cep_origem, // Obrigatorio
            'empresa'           => $empresa, // Código da empresa junto aos correios, não obrigatório.
            'senha'             => $senha, // Senha da empresa junto aos correios, não obrigatório.
            'peso'              => $peso, // Peso em kilos
            'comprimento'       => $comprimento, // Em centímetros
            'altura'            => $altura, // Em centímetros
            'largura'           => $largura, // Em centímetros
            'diametro'          => $diametro, // Em centímetros, no caso de rolo
            'mao_propria'       => $mao_propria, // Não obrigatórios
            'valor_declarado'   => $valor_declarado, // Não obrigatórios
            'aviso_recebimento' => $aviso_recebimento, // Não obrigatórios
        ];

        try
        {
            //$result = \Correios::frete($data);
            $sigep  = new \Modules\CerebeloOrders\Support\CorreiosSigep();
            $result = $sigep->calculationShipping($data);

            if (is_array($result) && !empty($result))
            {
                if (count($result) > 1)
                {
                    $tipos = array_flip(\Correios::getTipos());
                    foreach ($result as $key => $value)
                    {
                        $codigo = $value['codigo'];
                        array_map(function($k, $v) use(&$codigo) {
                            if ((int) $k === $codigo)
                            {
                                return $codigo = $v;
                            }
                        }, array_keys($tipos), array_values($tipos));
                        $result[$key]['tipo'] = $codigo;
                    }
                }
                else
                {
                    $result['tipo'] = $tipo;
                }
            }
            else
            {
                $result = [
                    'error'   => true,
                    'message' => "Frete não encontrado."
                ];
            }
        }
        catch (\Exception $ex)
        {
            $result = [
                'error'   => true,
                'message' => $ex->getMessage()
            ];
        }

        return collect($result);
    }

}

/** Sotnas
 * Status do pedido
 * 
 * @var $id
 * @return array or string
 */
if (!function_exists('orderStatus'))
{

    function orderStatus($id = 0)
    {
        $status = array(
            1 => "Em Negociação",
            2 => "Aprovado",
            3 => "Arte Aprovada",
            4 => "Em Produção",
            5 => "Enviado",
            6 => "Finalizado",
        );

        if (array_key_exists($id, $status))
        {
            return $status[$id];
        }

        return $status;
    }

}

/** Sotnas
 * Calcular Total do pedido - Desconto
 * 
 * @var $subtotal
 * @var $discount
 * @var $discount_type
 * 
 * @return float
 */
if (!function_exists('calculateFinalOrder'))
{

    function calculateFinalOrder($subtotal = 0, $discount = 0, $discount_type = "M")
    {
        if ($discount_type == "P")
        {
            return $subtotal - ($subtotal * ($discount / 100));
        }
        else
        {
            return $subtotal - $discount;
        }
    }

}

/** Sotnas
 * Retorna se o Desconto é Dinheiro ou Percentual
 * 
 * @var $subtotal
 * @var $discount
 * @var $discount_type
 * 
 * @return float
 */
if (!function_exists('moneyOrPercentage'))
{

    function moneyOrPercentage($discount = 0, $discount_type = "M")
    {
        if ($discount_type == "P")
        {
            return "% $discount";
        }
        else
        {
            return "R$ $discount";
        }
    }

}

/** Sotnas
 * Calcula e retorna um periodo que um pedido demora em um determinado setor
 * 
 * @var $portion
 * 
 * @return array
 */
if (!function_exists('calculatePeriodPerSector'))
{

    function calculatePeriodPerSector($period_option, $period, $confirmed = 'N')
    {
        $carbon_now      = \Carbon\Carbon::now();
        $start_datetime  = $carbon_now->format('Y-m-d H:i:s');
        $carbon_now_copy = $carbon_now->copy();
        $end_datetime    = null;
        if ($period_option == "M") // Minutos
        {
            $end_datetime = $carbon_now_copy->addMinutes($period)->format('Y-m-d H:i:s');
        }
        elseif ($period_option == "H") // Horas
        {
            $end_datetime = $carbon_now_copy->addHours($period)->format('Y-m-d H:i:s');
        }
        elseif ($period_option == "D") // Dias
        {
            $end_datetime = $carbon_now_copy->addDays($period)->format('Y-m-d H:i:s');
        }
        elseif ($period_option == "W") // Semanas
        {
            $end_datetime = $carbon_now_copy->addWeeks($period)->format('Y-m-d H:i:s');
        }

        $attrs = [
            'start_datetime' => $start_datetime,
            'end_datetime'   => $end_datetime,
            'confirmed'      => $confirmed,
        ];

        return $attrs;
    }
}

/** Sotnas
 * Retorna a estrutura em <tr> das parcelas de pagamento (Orçamento/Pedidos)
 * 
 * @var $portion
 * 
 * @return html
 */
if (!function_exists('paymentsInstallmentsHtml'))
{

    function paymentsInstallmentsHtml($portion)
    {
        $html            = '';
        $status          = $portion->status;
        $num_portion     = $portion->portion;
        $value           = $portion->value;
        $date            = trim($portion->date);
        $expiration_date = trim($portion->expiration_date);
        $payment_form    = trim($portion->payment_form);

        $array_font_color = [
            'AP' => 'color: #F5542F !important;', // Vermelho
            'PG' => 'color: #6BBE64 !important;', // Verde
            'PT' => '',
            'RS' => '',
            'CD' => 'color: #4691F1 !important;' // Azul
        ];
        $style = 'style="font-weight: 900 !important; ' . (!array_key_exists($status, $array_font_color) ? "" : $array_font_color[$status]) . '"';

        $html .= '<tr>';
        $html .= '  <td>';
        $html .= '      <span class="display-block" ' . $style . '>' . $num_portion . 'ª Parcela</span>';
        $html .= '  </td>';
        $html .= '  <td>';
        $html .= '      <span class="display-block" ' . $style . '>R$ ' . $value . '</span>';
        $html .= '  </td>';
        if(!empty($expiration_date)) {
            $html .= '  <td>';
            $html .= '      <span class="display-block" ' . $style . '>Prazo: ' . $expiration_date . '</span>';
            $html .= '  </td>';
        }
        if(!empty($date)) {
            $html .= '  <td>';
            $html .= '      <span class="display-block" ' . $style . '>Quitada: ' . toDate($date) . '</span>';
            $html .= '  </td>';
        }
        $html .= '  <td>';
        $html .= '      <span class="display-block" ' . $style . '>Forma Pgto.: ' . ((empty($payment_form) || $payment_form == 'Forma de Pgto.') ? "Não Definido" : trans('attributes.payments_forms.' . $payment_form)) . '</span>';
        $html .= '  </td>';
        $html .= '  <td>';
        $html .= '      <span class="display-block" ' . $style . '>Status: ' . trans('attributes.payment_status.' . $status) . '</span>';
        $html .= '  </td>';
        $html .= '</tr>';

        echo $html;
    }

}

/** Sotnas
 * Formata um endereço
 *
 * @var $address Modules\CerebeloSettings\Entities\Address
 *
 * @return string
 */
if (!function_exists('formatAddress'))
{
    function formatAddress($address) {
        $string = "";

        try {
            $string = sprintf("CEP: %s | Logradouro: %s | Número: %s | Complemento: %s | Bairro: %s | Cidade: %s | Estado: %s", $address->cep, $address->street, $address->number, $address->complement, $address->neighborhood, $address->city, $address->state);
        } catch(Exception $ex) {

        }

        return $string;
    }
}

/** Sotnas
 * Remove todos os caracteres especiais de uma string
 *
 * @var $string
 *
 * @return string
 */
if (!function_exists('cleanSpecialChars'))
{
    function cleanSpecialChars($string) {
        $string = str_replace(' ', '', $string);
        $string = str_replace('-', '', $string);
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    }
}

/** Sotnas
 * Retorna o nome do serviço do correio
 *
 * @var $param string
 *
 * @return string
 */
if (!function_exists('correiosFreightName'))
{
    function correiosFreightName($param = "") {
        if(empty($param) || !is_numeric($param)) return;

        $cr = new \Modules\CerebeloOrders\Support\CorreiosServicoDePostagem($param);
        return $cr->getNome();
    }
}

/** Sotnas
 * XML de Integração Financeiro
 *
 * @var $ids string
 * @var $type Modules\CerebeloOrders\Entities\Order, Modules\CerebeloOrders\Entities\OrderJoin
 *
 * @return string
 */
if (!function_exists('generatorXmlFinancial'))
{
    function generatorXmlFinancial($ids = "", $model = 'Order') {
        if(empty($ids)) return;

        $explode = explode(',', trim($ids));

        if(count($explode) == 0) return;

        if($model == 'Order') {
            $results = \Modules\CerebeloOrders\Entities\Order::with([
                'client', 'ordersProducts'
            ])
            ->whereIn('id', $explode)
            ->get();
        } elseif($model == 'OrderJoin') {
            $results = \Modules\CerebeloOrders\Entities\OrderJoin::with([
                'client'
            ])
            ->whereIn('id', $explode)
            ->get();
        } else {
            return;
        }


        if($model == 'OrderJoin') {
            foreach ($results as $key => $value) {
                $orders_id = trim($value->orders_id);
                $explode = explode(',', $orders_id);

                $ordersProducts = \Modules\CerebeloOrders\Entities\Order::with(['ordersProducts'])
                                        ->whereIn('id', $explode)
                                        ->get()
                                        ->map(function($v) {
                                            return $v->ordersProducts;
                                        });
                $mergedOrdersProducts = [];
                foreach ($ordersProducts as $val) {
                    foreach ($val as $v) {
                        $mergedOrdersProducts[] = $v;
                    }
                }

                $results[$key]->ordersProducts = $mergedOrdersProducts;
            }
        }

        $freight_cif_fob_r = ['CIF' => 0, 'FOB' => 1];
        $xml = new \SimpleXMLElement('<IntegracaoFinanceiro/>');
        $orders = $xml->addChild('Pedidos');
        foreach ($results as $value) {
            $order_number    = substr(cleanSpecialChars($value->order_number), -6);
            $cpf_cnpj        = cleanSpecialChars($value->client->cpf_cnpj);
            $freight_cif_fob = trim($value->freight_cif_fob);

            $order = $orders->addChild('PedidoItem');
            $order->addChild('NumeroPedido', $order_number);
            $order->addChild('CnpjCpfCliente', $cpf_cnpj);
            $order->addChild('DataFaturamento', date('d/m/Y'));
            $order->addChild('Observacao', '');
            $order->addChild('TipoFrete', (!array_key_exists($freight_cif_fob, $freight_cif_fob_r)) ? '' : $freight_cif_fob_r[$freight_cif_fob]);
            $order->addChild('CondicaoPagamento', '');
            $order->addChild('ValorDesconto', $value->discount_value);
            $order->addChild('ValorFrete', $value->cost_freight);
            $products = $order->addChild('Produtos');
            foreach ($value->ordersProducts as $v) {
                $product = $products->addChild('ProdutoItem');
                $product->addChild('NumeroPedido', $order_number);
                $product->addChild('CodigoProduto', $v->id);
                $product->addChild('QuantidadeProduto', $v->pivot->quantity);
                $product->addChild('ValorUnitario', moneyFormat($v->pivot->unit_price));
            }
        }

        $domxml = new \DOMDocument('1.0');
        $domxml->preserveWhiteSpace = false;
        $domxml->formatOutput = true;
        /* @var $xml SimpleXMLElement */
        $domxml->loadXML($xml->asXML());
        $user_logged = auth()->user();
        $file_name = "pedidos_xml-financeiro-integracao_$user_logged->id.xml";
        $file_path = storage_path("app/$file_name");
        $domxml->save($file_path);

        return $file_name;
    }
}

/** Sotnas
 * file_get_contents_curl
 *
 * @var $url string
 *
 * @return string
 */
if (!function_exists('file_get_contents_curl'))
{
    function file_get_contents_curl($url) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }
}

/** Sotnas
 * actionsOptionsSelect
 *
 * @var $object Modules\CerebeloOrders\Entities\Order()
 * @var $waitActions
 * @var $select2
 *
 * @return string
 */
if (!function_exists('actionsOptionsSelect'))
{
    function actionsOptionsSelect($object, $select2) {
        $user_data = auth()->user();
        $waitActions = $user_data->sector->waitActions->pluck('name', 'id');

        if(empty($waitActions) || count($waitActions) == 0) {
            return (isset($object->sectorAction->name) ? $object->sectorAction->name : "");
        }

        $search = collect($select2)->where('id', $object->sectors_actions_id)->first();
        if(!empty(array_get($search, 'id')) && !empty(array_get($search, 'name')))
        {
            $waitActions->put(array_get($search, 'id'), array_get($search, 'name'));
        }

        $waitActions = $waitActions->toArray();

        return \Form::select('sectors_actions_id', ['' => 'Selecione'] + $waitActions, $object->sectors_actions_id, ['href' => route('cerebelo.orders.sector_actions', $object->id), 'class' => 'select-search', 'onchange' => 'btnClickChangeActionOrder(this);']);
    }
}

/** Sotnas
 * waitAction
 *
 * @var $object Modules\CerebeloOrders\Entities\Order()
 *
 * @return string
 */
if (!function_exists('waitAction'))
{
    function waitAction($object) {
        $sectors_actions_id = $object->sectors_actions_id;
        $latestSector = $object->latestSector()->first();

        $now = \Carbon\Carbon::now();

        $idLatestSector               = $latestSector->sectors_id;
        $confirmedLatestSector        = $latestSector->pivot->confirmed;
        $pivotIdLatestSector          = $latestSector->pivot->id;
        $pivotEndDatetimeLatestSector = $latestSector->pivot->end_datetime;
        $diffInDays                   = -1;

        if (!empty($pivotEndDatetimeLatestSector)) {
            $dt = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $pivotEndDatetimeLatestSector);

            $diffInDays = $now->diffInDays($dt, false);
        }

        $currentSector = $latestSector->sector()->first();

        if (!empty($idLatestSector)) {
            if ($confirmedLatestSector != "Y") {
                return '<button class="btn btn-yellow btn-xs btn-group-justified btnClickLineUp text-size-small" type="accept" href="' . route('cerebelo.orders.line_up', ['type' => 'accept', 'id' => $pivotIdLatestSector, 'order_id' => $object->id]) . '">' . $currentSector->name . ' Conf. Rec.</button>';
            } else {
                if ($diffInDays >= 0) {
                    return '<button class="btn btn-success btn-xs btn-group-justified btnClickLineUp text-size-small" type="modal" href="' . route('cerebelo.orders.line_up', ['type' => 'modal', 'order_id' => $object->id, 'sector_id' => $idLatestSector]) . '">' . $currentSector->name . ' > Sel. Setor</button>';
                } else {
                    if($sectors_actions_id != 1)
                    {
                        return '<button class="btn btn-danger btn-xs btn-group-justified btnClickLineUp text-size-small" type="modal" href="' . route('cerebelo.orders.line_up', ['type' => 'modal', 'order_id' => $object->id, 'sector_id' => $idLatestSector]) . '">' . $currentSector->name . ' > Sel. Setor</button>';
                    }
                    else {
                        return '<button class="btn btn-success btn-xs btn-group-justified btnClickLineUp text-size-small" type="modal" href="' . route('cerebelo.orders.line_up', ['type' => 'modal', 'order_id' => $object->id, 'sector_id' => $idLatestSector]) . '">' . $currentSector->name . ' > Sel. Setor</button>';
                    }
                }
            }
        } else {
            return '<a class="btn btn-default btn-xs btn-group-justified text-size-small" href="' . route('cerebelo.orders.edit', $object->id) . '"> Completar Cadastro</a>';
        }
    }
}

/** Sotnas
 * waitAction para tela (Dashboard) de Produção
 *
 * @var $object Modules\CerebeloOrders\Entities\Order()
 *
 * @return string
 */
if (!function_exists('waitActionProduction'))
{
    function waitActionProduction($object) {
        $sectors_actions_id = $object->sectors_actions_id;
        $latestSector = $object->latestSector()->first();

        $now = \Carbon\Carbon::now();

        $idLatestSector               = $latestSector->sectors_id;
        $confirmedLatestSector        = $latestSector->pivot->confirmed;
        $pivotIdLatestSector          = $latestSector->pivot->id;
        $start_datetime               = $latestSector->pivot->start_datetime;
        $pivotEndDatetimeLatestSector = $latestSector->pivot->end_datetime;
        $diffInDays                   = -1;

        if (!empty($pivotEndDatetimeLatestSector)) {
            $dt = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $pivotEndDatetimeLatestSector);

            $diffInDays = $now->diffInSeconds($dt, false);
        }

        $currentSector = $latestSector->sector()->first();

        if (!empty($idLatestSector)) {
            if ($confirmedLatestSector != "Y") {
                return  \Form::button('<i class="icon-touch"></i> Conf. Rec.' . spanBadgeByTime($start_datetime, true), ['style' => 'width: 152px; height: 95px;', 'class' => 'btn btn-yellow btn-float btn-float-lg btnClickLineUp', 'type' => 'accept', 'href' => route('cerebelo.orders.line_up', ['type' => 'accept', 'id' => $pivotIdLatestSector, 'order_id' => $object->id])]);
            } else {
                $next_sector_id = auth()->user()->nextSectorId();
                if ($diffInDays >= 0) {
                    return \Form::button('<i class="icon-touch"></i> Próximo Setor' . spanBadgeByTime($pivotEndDatetimeLatestSector), ['style' => 'width: 152px; height: 95px;', 'class' => 'btn btn-success btn-float btn-float-lg btnClickLineUp', 'type' => 'next', 'href' => route('cerebelo.orders.line_up', ['type' => 'next', 'order_id' => $object->id, 'sector_id' => $idLatestSector, 'next_sector_id' => $next_sector_id])]);
                } else {
                    if($sectors_actions_id != 1)
                    {
                        return \Form::button('<i class="icon-touch"></i> Próximo Setor' . spanBadgeByTime($pivotEndDatetimeLatestSector), ['style' => 'width: 152px; height: 95px;', 'class' => 'btn btn-danger btn-float btn-float-lg btnClickLineUp', 'type' => 'next', 'href' => route('cerebelo.orders.line_up', ['type' => 'next', 'order_id' => $object->id, 'sector_id' => $idLatestSector, 'next_sector_id' => $next_sector_id])]);
                    }
                    else
                    {
                        return \Form::button('<i class="icon-touch"></i> Próximo Setor' . spanBadgeByTime($pivotEndDatetimeLatestSector), ['style' => 'width: 152px; height: 95px;', 'class' => 'btn btn-success btn-float btn-float-lg btnClickLineUp', 'type' => 'next', 'href' => route('cerebelo.orders.line_up', ['type' => 'next', 'order_id' => $object->id, 'sector_id' => $idLatestSector, 'next_sector_id' => $next_sector_id])]);
                    }
                }
            }
        } else {

        }
    }
}


/** Sotnas
 * Badge de tempo Positivo/Negativo do pedido no setor
 *
 * @var $datetime string Y-m-d H:i:s
 * @var $is_confirm boolean
 *
 * @return html
 */
if (!function_exists('spanBadgeByTime'))
{
    function spanBadgeByTime($datetime, $is_confirm = false) {
        $now = \Carbon\Carbon::now();
        if($is_confirm == true) {
            $datetime = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $datetime)->addHours(12);
        } else {
            $datetime = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $datetime);
        }
        $diffForHumans = $now->diffForHumans($datetime, true);

        $span_badge = $now->lte($datetime) ? '<span style="padding-top: 0px;" class="badge bg-success-400 text-size-large">Falta ' . $diffForHumans . '</span>' : '<span style="padding-top: 0px;" class="badge bg-warning-400 text-size-large">Ultrapassou ' . $diffForHumans . '</span>';

        return $span_badge;
    }
}


/** Sotnas
 * Envia email para o Cliente se o campo de mensagem do setor for preenchido
 *
 * @var $object Modules\CerebeloOrders\Entities\Order()
 * @var $sector Modules\CerebeloSettings\Entities\Sector()
 * @var $end_datetime string
 *
 * uso em Modules\CerebeloSettings\Http\Controllers\SectorsController
 * $suggest_collect = ["cliente.nome", "cliente.cpf_cnpj", "pedido.numero", "designer.nome", "atendente.nome", "representante.nome", "data.entrega",];
 *
 * @return string
 */
if (!function_exists('sendMailOnProductionSteps'))
{
    function sendMailOnProductionSteps($object, $sector, $end_datetime) {
        $message_status = $sector->message_status_to_client;

        if(!empty($message_status)) {
            if(!isset($object->client))
                return;

            $client_name = str_contains($message_status, '@cliente.nome');
            $client_cpf_cnpj = str_contains($message_status, '@cliente.cpf_cnpj');
            $order_number = str_contains($message_status, '@pedido.numero');
            $designer_name = str_contains($message_status, '@designer.nome');
            $atendent_name = str_contains($message_status, '@atendente.nome');
            $representative_name = str_contains($message_status, '@representante.nome');
            $delivery_date = str_contains($message_status, '@data.entrega');

            if($client_name == true) {
                $client_name = $object->client->name;
                $message_status = str_replace('@cliente.nome', $client_name, $message_status);
            }
            if($client_cpf_cnpj == true) {
                $client_cpf_cnpj = $object->client->cpf_cnpj;
                $message_status = str_replace('@cliente.cpf_cnpj', $client_name, $message_status);
            }
            if($order_number == true) {
                $order_number = $object->order_number;
                $message_status = str_replace('@pedido.numero', $order_number, $message_status);
            }
            if($designer_name == true) {
                $designer_name = $object->client->designer->name;
                $message_status = str_replace('@designer.nome', $designer_name, $message_status);
            }
            if($atendent_name == true) {
                $atendent_name = $object->client->attendant->name;
                $message_status = str_replace('@atendente.nome', $atendent_name, $message_status);
            }
            if($representative_name == true) {
                $representative_name = $object->client->representative->name;
                $message_status = str_replace('@representante.nome', $representative_name, $message_status);
            }
            if($delivery_date == true) {
                $delivery_date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $end_datetime)->addDay()->diffForHumans();
                $message_status = str_replace('@data.entrega', $delivery_date, $message_status);
            }

            $data['message_status'] = $message_status;

            Mail::send(['html' => 'cerebeloorders::emails.status'], $data, function ($message) use ($object) {
                $message->from('nao-responder@ertuniformes.com.br', 'ERT UNIFORMES | Uniformes Personalizados para Ciclismo')
                        ->to($object->client->email, $object->client->name)
                        ->to($object->client->attendant->email, $object->client->attendant->name)
                        ->to($object->client->representative->email, $object->client->representative->name)
                        //->bcc('sotnas.lony@emiolo.com', 'Sotnas Leunam - eMiolo.com')
                        ->subject('Status do Pedido nº ' . $object->order_number);
            });
            if (count(Mail::failures()) == 0) {
                return true;
            } else {
                return false;
            }
        }

        return;
    }
}


/** Sotnas
 * Envia email para o Cliente se o campo de mensagem do setor for preenchido
 *
 * @var $param string
 *
 * @return string
 */
if (!function_exists('installmentsBGStatus'))
{
    function installmentsBGStatus($param = 'AP') {
        if($param == 'PG')
        {return ' bg-success ';}
        elseif($param == 'CD')
        {return ' bg-info ';}
        else
        {return ' bg-warning ';}
    }
}


/** Sotnas
 * Calculo de caixas para entrega
 *
 * @return int
 */
if (!function_exists('estimateBox'))
{

    function estimateBox($products_weight, $total = false)
    {
        $freights_grouping = [];

        $products_weight_calc = empty($products_weight) ? 0 : $products_weight;

        do
        {
            $_delivery_box = new Modules\CerebeloOrders\Entities\DeliveryBox();
            $delivery_box = $_delivery_box->dimension($products_weight_calc);

            $min_weight = $delivery_box->min_weight;
            $max_weight = $delivery_box->max_weight;
            $length     = $delivery_box->length;
            $width      = $delivery_box->width;
            $height     = $delivery_box->height;
            $id         = $delivery_box->id;

            $index  = $length . $width . $height;
            $weight = ($products_weight_calc > $max_weight) ? $max_weight : $products_weight_calc;

            $freights_grouping[$index]['format']   = 'caixa';
            $freights_grouping[$index]['tolerate'] = $max_weight;
            $freights_grouping[$index]['weight'][] = $weight;
            $freights_grouping[$index]['length']   = $length;
            $freights_grouping[$index]['height']   = $height;
            $freights_grouping[$index]['width']    = $width;
            $freights_grouping[$index]['dimension']= $id;

            $products_weight_calc -= $max_weight;
        }
        while ($products_weight_calc > 0);

        if($total !== false)
        {
            $weight = 0;
            foreach($freights_grouping as $value)
            {
                $weight += count($value['weight']);
            }

            return $weight;
        }

        return $freights_grouping;
    }

}


/** Sotnas
 *
 *
 * @return array
 */
if (!function_exists('array_reverse_recursive'))
{

    function array_reverse_recursive($arr) {
        foreach ($arr as $key => $val) {
            if (is_array($val))
                $arr[$key] = array_reverse_recursive($val);
        }
        return array_reverse($arr, true);
    }

}


/**
 * Determines if an array is associative.
 *
 * An array is "associative" if it doesn't have sequential numerical keys beginning with zero.
 *
 * @param  array  $array
 * @return bool
 */
if (!function_exists('isAssoc'))
{

    function isAssoc(array $array)
    {
        $keys = array_keys($array);

        return array_keys($keys) !== $keys;
    }

}

/**
 * Recursively sort an array by keys and values.
 *
 * @param  array  $array
 * @return array
 */
if (!function_exists('sortRecursive'))
{

    function sortRecursive($array)
    {
        foreach ($array as &$value) {
            if (is_array($value)) {
                $value = sortRecursive($value);
            }
        }

        if (isAssoc($array)) {
            arsort($array);
        } else {
            natcasesort($array);
        }

        return $array;
    }

}

/**
 * Brinde/Parceria ou Patrocínio na impressão do orçamento Não Cobrar
 * @param  array  $array
 * @return array
 */
if (!function_exists('not_to_charge'))
{

    function not_to_charge($id_pay = 4)
    {
        $items = [4 => 'Brinde', 5 => 'Patrocínio', 6 => 'Parceria', 7 => 'Reposição'];

        if(array_key_exists($id_pay, $items))
            return sprintf("%s/%s", mb_strtoupper($items[$id_pay]), "NÃO COBRAR");

        return sprintf("%s/%s", mb_strtoupper(current($items)), "NÃO COBRAR");
    }

}

/**
 * Brinde/Parceria ou Patrocínio na impressão do orçamento Não Cobrar
 * @param  array  $array
 * @return array
 */
if (!function_exists('selectDeliveryBox'))
{

    function selectDeliveryBox()
    {
        return Modules\CerebeloOrders\Entities\DeliveryBox::formSelect()->toArray();
    }

}

/**
 * Extrair nome do produto da variavel $products_selected [productSelected()]
 * @param  array  $array
 * @return string
 */
if (!function_exists('extractProductName'))
{

    function extractProductName($id, $products_id, $products_name)
    {
        $products_id = array_flip($products_id);
        $id = array_get($products_id, $id);
        return array_get($products_name, $id);
    }

}

/**
 * Soma os valores de um array exceto index
 * @param  array  $array
 * @param  string|boolean $index
 * @return int
 */
if (!function_exists('arraySum'))
{

    function arraySum($array, $index = false) : int
    {
        if($index !== false && array_key_exists($index, $array)) {
            unset($array[$index]);
        }

        return array_sum($array);
    }

}
