<?php

namespace Modules\CerebeloOrders\Support;

use PhpSigep\Config;
use PhpSigep\Bootstrap;
use PhpSigep\Services\SoapClient\Real;
use PhpSigep\Model\Dimensao;
use PhpSigep\Model\Destinatario;
use PhpSigep\Model\DestinoNacional;
//use PhpSigep\Model\ServicoAdicional;
use Modules\CerebeloOrders\Support\CorreiosServicoDePostagem as ServicoDePostagem;
use PhpSigep\Model\Remetente;
use PhpSigep\Model\ObjetoPostal;
use PhpSigep\Model\PreListaDePostagem;
//use PhpSigep\Model\AccessDataHomologacao;
use PhpSigep\Pdf\CartaoDePostagem;
use PhpSigep\Pdf\CartaoDePostagem2016;
use PhpSigep\Model\AccessData;
use PhpSigep\Model\SolicitaEtiquetas;
use PhpSigep\Model\CalcPrecoPrazo;
use Modules\CerebeloSettings\Entities\ClientsData;
use Modules\CerebeloOrders\Entities\CorreiosService;
use Exception;
use PhpSigep\Model\Diretoria;

/**
 * Description of CorreiosSigep
 *
 * @author sotnas.lony
 */
class CorreiosSigep extends Real
{

    private $accessData;
    private $transportService;
    private $qtdEtiquetas;

    public function __construct()
    {
        // Teste
        //$accessData = new AccessDataHomologacao();
        // Produção
        $accessData = new AccessData();
        $accessData->setUsuario('10845338000106');
        $accessData->setSenha('dc6e9y');
        $accessData->setCodAdministrativo('13414690');
        $accessData->setNumeroContrato('9912336067');
        $accessData->setCartaoPostagem('0067928188');
        $accessData->setCnpjEmpresa('10845338000106');
        $accessData->setDiretoria(new Diretoria(Diretoria::DIRETORIA_DR_MINAS_GERAIS));

        $this->accessData = $accessData;

        $this->setConfig();
    }

    private function setConfig()
    {
        $config = new Config();
        $config->setAccessData($this->accessData);
        $config->setEnv(Config::ENV_PRODUCTION);
        $config->setCacheOptions(
                array(
                    'storageOptions' => array(
                        // Qualquer valor setado neste atributo será mesclado ao atributos das classes 
                        // "\PhpSigep\Cache\Storage\Adapter\AdapterOptions" e "\PhpSigep\Cache\Storage\Adapter\FileSystemOptions".
                        // Por tanto as chaves devem ser o nome de um dos atributos dessas classes.
                        'enabled'  => false,
                        'ttl'      => 10, // "time to live" de 10 segundos
                        'cacheDir' => sys_get_temp_dir(), // Opcional. Quando não inforado é usado o valor retornado de "sys_get_temp_dir()"
                    ),
                )
        );
        Bootstrap::start($config);
    }

    public function checkPostingService($param)
    {
        try
        {
            $obj = new ServicoDePostagem($param);

            return $obj->getIdServico();
        }
        catch (Exception $ex)
        {
            return false;
        }
    }

    private function requestTags()
    {
        $params = new SolicitaEtiquetas();
        $params->setQtdEtiquetas((int) $this->qtdEtiquetas);
        $params->setServicoDePostagem(new ServicoDePostagem($this->transportService));
        $params->setAccessData($this->accessData);

        return $this->solicitaEtiquetas($params);
    }

    private function searchClient()
    {
        $result = $this->buscaCliente($this->accessData);

        if (!$result->hasError())
        {
            /** @var $buscaClienteResult \PhpSigep\Model\BuscaClienteResult */
            $buscaClienteResult = $result->getResult();

            // Anula as chancelas antes de imprimir o resultado, porque as chancelas não estão é liguagem humana
            $servicos = $buscaClienteResult->getContratos()->cartoesPostagem->servicos;
            foreach ($servicos as &$servico)
            {
                $servico->servicoSigep->chancela->chancela = 'Chancelas anulada via código.';
            }
        }
    }

    private function setReceiver($param = [])
    {
        $destinatario = new Destinatario();

        try
        {
            $destinatario->setNome($param['name']);
            $destinatario->setLogradouro($param['street']);
            $destinatario->setNumero($param['number']);
            $destinatario->setComplemento($param['complement']);
        }
        catch (\Exception $ex)
        {
            
        }

        return $destinatario;
    }

    private function setDimension($param = [])
    {
        $dimensao = new Dimensao();

        try
        {
            $dimensao->setAltura($param['height']);
            $dimensao->setLargura($param['width']);
            $dimensao->setComprimento($param['length']);
            $dimensao->setDiametro($param['diameter']);
            $dimensao->setTipo(Dimensao::TIPO_PACOTE_CAIXA);
        }
        catch (\Exception $ex)
        {
            
        }

        return $dimensao;
    }

    private function setNationalDestination($param = [])
    {
        $destino = new DestinoNacional();

        try
        {
            $destino->setBairro($param['neighborhood']);
            $destino->setCep($param['cep']);
            $destino->setCidade($param['city']);
            $destino->setUf($param['state']);
        }
        catch (\Exception $ex)
        {
            
        }

        return $destino;
    }

    private function setSender($param = [])
    {
        $remetente = new Remetente();

        try
        {
            $remetente->setNome($param['name']);
            $remetente->setLogradouro($param['street']);
            $remetente->setNumero($param['number']);
            $remetente->setComplemento($param['complement']);
            $remetente->setBairro($param['neighborhood']);
            $remetente->setCep($param['cep']);
            $remetente->setUf($param['state']);
            $remetente->setCidade($param['city']);
        }
        catch (\Exception $ex)
        {
            
        }

        return $remetente;
    }

    private function setLabel()
    {
        $EtiquetaSemDv = $this->requestTags();

        return $EtiquetaSemDv->getResult();
    }

    public function printLabel($peso, $dimension = [], $receiver = [], $national_destination = [], $sender = [], $transport_type, $total_box)
    {
        $this->transportService = $transport_type;
        $this->qtdEtiquetas     = $total_box;

        // Estamos criando uma etique falsa, mas em um ambiente real voçê deve usar o método
        // {@link Real::solicitaEtiquetas() } para gerar o número das etiquetas
        $etiqueta = $this->setLabel();

        $this->searchClient();

        // ***  DADOS DA ENCOMENDA QUE SERÁ DESPACHADA *** //
        $dimensao = $this->setDimension($dimension);

        $destinatario = $this->setReceiver($receiver);

        $destino = $this->setNationalDestination($national_destination);

        //$servicoAdicional = new ServicoAdicional();
        //$servicoAdicional->setCodigoServicoAdicional(ServicoAdicional::SERVICE_REGISTRO);
        // Se não tiver valor declarado informar 0 (zero)
        //$servicoAdicional->setValorDeclarado(0);

        $encomendas = array();

        for ($index = 0; $index < $total_box; $index++)
        {
            $encomenda = new ObjetoPostal();
            $encomenda->setServicosAdicionais(array());
            $encomenda->setDestinatario($destinatario);
            $encomenda->setDestino($destino);
            $encomenda->setDimensao($dimensao);
            $encomenda->setEtiqueta($etiqueta[$index]);
            $encomenda->setPeso((float) $peso); // 500 gramas
            $encomenda->setServicoDePostagem(new ServicoDePostagem($this->transportService));

            $encomendas[] = $encomenda;
        }
        // ***  FIM DOS DADOS DA ENCOMENDA QUE SERÁ DESPACHADA *** //
        // 
        // *** DADOS DO REMETENTE *** //
        $remetente = $this->setSender($sender);
        // *** FIM DOS DADOS DO REMETENTE *** //

        $plp = new PreListaDePostagem();
        $plp->setAccessData($this->accessData);
        $plp->setEncomendas($encomendas);
        $plp->setRemetente($remetente);

        $client_data = ClientsData::first();

        $layoutChancela = [$this->layoutChancela()];

        $pdf = new CartaoDePostagem($plp, time(), route('storage.files', ['filename' => $client_data->img]), $layoutChancela);
        $pdf->render('I', 'etiqueta.pdf');
    }

    private function layoutChancela()
    {
        $ServicoDePostagem = new ServicoDePostagem($this->transportService);

        $str_contains = stristr($ServicoDePostagem->getNome(), 'pac');

        if ($str_contains)
        {
            return "pac";
        }
        else
        {
            $str_contains = stristr($ServicoDePostagem->getNome(), 'sedex');

            if ($str_contains)
            {
                return "sedex";
            }
        }

        return "";
    }

    public function calculationShipping($param)
    {
        $dimensao = new Dimensao();
        $dimensao->setTipo(Dimensao::TIPO_PACOTE_CAIXA);
        $dimensao->setAltura($param['altura']); // em centímetros
        $dimensao->setComprimento($param['comprimento']); // em centímetros
        $dimensao->setLargura($param['largura']); // em centímetros

        $correios_types = CorreiosService::pluck('code');
        $services       = [];
        foreach ($correios_types as $value)
        {
            $servico_postagem = new ServicoDePostagem($value);

            $services[] = $servico_postagem;
        }

        $params = new CalcPrecoPrazo();
        $params->setAccessData($this->accessData);
        $params->setCepOrigem($param['cep_origem']);
        $params->setCepDestino($param['cep_destino']);
        $params->setServicosPostagem($services);
        $params->setAjustarDimensaoMinima(true);
        $params->setDimensao($dimensao);
        $params->setPeso($param['peso']); // 150 gramas

        $calcPrecoPrazo = $this->calcPrecoPrazo($params);

        $result = $calcPrecoPrazo->getResult();

        $data = array();
        foreach ($result as $value)
        {
            $erroCodigo = $value->getErroCodigo();
            $erroMsg    = $value->getErroMsg();

            if ($erroCodigo === 0)
            {
                $valor                 = $value->getValor();
                $prazoEntrega          = $value->getPrazoEntrega();
                $valorMaoPropria       = $value->getValorMaoPropria();
                $valorAvisoRecebimento = $value->getValorAvisoRecebimento();
                $valorValorDeclarado   = $value->getValorValorDeclarado();
                $entregaDomiciliar     = false; //$value->getEntregaDomiciliar();
                $entregaSabado         = false; //$value->getEntregaSabado();
                $servico               = $value->getServico();
                $servicoCodigo         = $servico->getCodigo();
                $servicoNome           = $servico->getNome();
                $servicoIdServico      = $servico->getIdServico();

                $data[] = array(
                    'codigo'             => $servicoCodigo,
                    'nome'               => $servicoNome,
                    'id_servico'         => $servicoIdServico,
                    'valor'              => (float) str_replace(',', '.', $valor),
                    'prazo'              => (int) str_replace(',', '.', $prazoEntrega),
                    'mao_propria'        => (float) str_replace(',', '.', $valorMaoPropria),
                    'aviso_recebimento'  => (float) str_replace(',', '.', $valorAvisoRecebimento),
                    'valor_declarado'    => (float) str_replace(',', '.', $valorValorDeclarado),
                    'entrega_domiciliar' => $entregaDomiciliar,
                    'entrega_sabado'     => $entregaSabado,
                    'erro'               => array('codigo' => (real) $erroCodigo, 'mensagem' => $erroMsg),
                );
            }
        }

        return $data;
    }

}
