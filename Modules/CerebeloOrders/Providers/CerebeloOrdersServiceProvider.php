<?php

namespace Modules\CerebeloOrders\Providers;

use Illuminate\Support\ServiceProvider;
use Storage;
use Symfony\Component\Finder\Finder;

class CerebeloOrdersServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerArtisanCommands('Modules\\CerebeloOrders\\Console');

        $mpdf_tmp        = 'mpdf/tmp/';
        $mpdf_ttfontdata = 'mpdf/ttfontdata/';

        Storage::makeDirectory($mpdf_tmp, 0777);
        Storage::makeDirectory($mpdf_ttfontdata, 0777);

        if (!defined('_MPDF_TEMP_PATH'))
        {
            define("_MPDF_TEMP_PATH", storage_path("app/$mpdf_tmp"));
        }
        if (!defined('_MPDF_TTFONTDATAPATH'))
        {
            define("_MPDF_TTFONTDATAPATH", storage_path("app/$mpdf_ttfontdata"));
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        foreach (glob(app_path() . '/../Modules/CerebeloOrders/Support/*.php') as $filename)
        {
            require_once($filename);
        }
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('cerebeloorders.php'),
                ], 'config');
        $this->mergeConfigFrom(
                __DIR__ . '/../Config/config.php', 'cerebeloorders'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/cerebeloorders');

        $sourcePath = __DIR__ . '/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
                            return $path . '/modules/cerebeloorders';
                        }, \Config::get('view.paths')), [$sourcePath]), 'cerebeloorders');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/cerebeloorders');

        if (is_dir($langPath))
        {
            $this->loadTranslationsFrom($langPath, 'cerebeloorders');
        }
        else
        {
            $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'cerebeloorders');
        }
    }

    /**
     * Register artisan commands.
     *
     * @param string $namespace
     * @return void
     */
    public function registerArtisanCommands($namespace = '')
    {
        $finder = new Finder(); // from Symfony\Component\Finder;
        $finder->files()->name('*Command.php')->in(__DIR__ . '/../Console');

        $classes = [];
        foreach ($finder as $file) {
            $class = $namespace.'\\'.$file->getBasename('.php');
            array_push($classes, $class);
        }

        $this->commands($classes);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

}
