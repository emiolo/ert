<?php

namespace Modules\CerebeloOrders\Entities;

use Illuminate\Database\Eloquent\Model;

class OrderActionHistory extends Model
{

    protected $table    = 'orders_actions_history';
    protected $fillable = [
        'orders_id',
        'sectors_actions_id',
        'orders__sectors_id',
    ];

    /*
     * Relationships
     */

    /*
     * Attributes
     */

    /*
     * Scope
     */

    /*
     * Ohters
     */

}
