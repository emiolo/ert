<?php

namespace Modules\CerebeloOrders\Entities;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PaymentInstallment extends Model
{

    protected $table    = 'orders_payments_installments';
    protected $fillable = [
        'orders_id',
        'portion',
        'value',
        'date',
        'expiration_date',
        'status',
        'file',
        'payment_form',
    ];

    /*
     * Relationships
     */

    public function receipts()
    {
        return $this->hasMany(PaymentInstallmentReceipt::class, 'orders_payments_installments_id');
    }

    /*
     * get e set do campo 'value'
     */

    public function getValueAttribute()
    {
        return moneyFormat($this->attributes['value']);
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = moneyUnFormat($value);
    }

    /*
     * get e set do campo 'date'
     */

    public function setDateAttribute($value)
    {
        if (!empty($value))
            $this->attributes['date'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }

    public function getDateAttribute()
    {
        if (empty($this->attributes['date']))
            return;

        return Carbon::createFromFormat('Y-m-d', $this->attributes['date'])->format('d/m/Y');
    }

    /*
     * Scope
     */

    public function scopeWherePortion($query, $value)
    {
        return $query->where('portion', $value);
    }

    public function scopeWhereStatus($query, $operator = '=', $value)
    {
        return $query->where('status', $operator, $value);
    }

    public function scopeFindByPortion($value)
    {
        return $this->wherePortion($value)->first();
    }

    public function scopeWhereId($query, $order_id)
    {
        return $query->where('orders_id', $order_id);
    }

    /*
     * Others
     */

    public function destroyAll($order_id, $portion)
    {
        return $this->whereId($order_id)->where('portion', $portion)
                    //->whereStatus('<>', 'PG')
                    ->delete();
    }

}
