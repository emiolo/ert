<?php

namespace Modules\CerebeloOrders\Entities;

use Illuminate\Database\Eloquent\Model;

class CorreiosService extends Model
{

    protected $table    = 'correios_services';
    protected $fillable = [
        'id',
        'name',
        'code',
        'directory_code',
    ];

}
