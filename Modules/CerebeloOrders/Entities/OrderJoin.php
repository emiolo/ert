<?php

namespace Modules\CerebeloOrders\Entities;

use DB;
use Modules\CerebeloClient\Entities\Client;
use Modules\CerebeloSettings\Entities\Address;
use Modules\CerebeloSettings\Entities\BaseModel as Model;
use Modules\CerebeloSettings\Entities\User;
use Modules\CerebeloTransports\Entities\Transport;

class OrderJoin extends Model
{
    protected $table = 'orders_join';
    protected $fillable = [
        'clients_id',
        'transports_id',
        'payments_form_id',
        'order_number',
        'orders_id',
        'delivery_address_id',
        'billing_address_id',
        'cost_freight',
        'discount_type',
        'discount_value',
        'freight_cif_fob',
        'transport_type',
        'transport_deadline',
        'delivery_boxes',
        'to_charge',
        'created_by',
    ];
    protected $appends = ['order_id_to_num'];

    public static function boot()
    {
        parent::boot();
        static::created(function ($model) {
            if (empty($model->order_number)) {
                $client = Client::find($model->clients_id);

                $representative_id = $client->representative_id;
                $attendant_id = $client->attendant_id;
                $client_id = $model->clients_id;
                $now_date = date('dmy');
                $id = $model->id;
                $order_number = "$representative_id.$attendant_id.$client_id.$now_date.$id";

                $model->order_number = $order_number;
                $model->save();
            }
        });
    }

    /*
     * Attributes
     */

    public function getOrderIdToNumAttribute()
    {
        $orders_id = explode(',', $this->attributes['orders_id']);

        if (count($orders_id) > 1) {
            $result = DB::table('orders')->whereIn('id', $orders_id)->select(DB::raw('GROUP_CONCAT(orders.order_number SEPARATOR " | ") AS num'))->get();
            return (!isset($result[0]->num)) ?: $result[0]->num;
        }

        return;
    }

    /*
     * Get e Set
     */

    public function getCostFreightAttribute()
    {
        try {
            return number_format($this->attributes['cost_freight'], 2, ",", ".");
        } catch (\Exception $e) {
            return null;
        }
    }

    public function setCostFreightAttribute($value)
    {
        try {
            $this->attributes['cost_freight'] = str_replace(",", ".", str_replace(".", "", $value));
        } catch (\Exception $e) {
            $this->attributes['cost_freight'] = null;
        }
    }

    public function getDiscountValueAttribute()
    {
        try {
            return moneyFormat($this->attributes['discount_value']);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function setDiscountValueAttribute($value)
    {
        if (empty($value)) {
            return;
        }

        try {
            $this->attributes['discount_value'] = moneyUnFormat($value);
        } catch (\Exception $e) {
            $this->attributes['discount_value'] = null;
        }
    }

    public function getVolumesAttribute()
    {
        try {
            return str_replace(",", ".", $this->attributes['volumes']);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function setVolumesAttribute($value)
    {
        try {
            $this->attributes['volumes'] = str_replace(",", ".", $value);
        } catch (\Exception $e) {
            $this->attributes['volumes'] = null;
        }
    }

    /*
     * get e set do campo 'delivery_boxes'
     */

    public function setDeliveryBoxesAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['delivery_boxes'] = json_encode($value, true);
        }
    }

    public function getDeliveryBoxesAttribute()
    {
        if (!empty($this->attributes['delivery_boxes'])) {
            return json_decode($this->attributes['delivery_boxes']);
        }
    }

    /*
     * Relationships
     */

    public function deliveryAddress()
    {
        return $this->belongsTo(Address::class, 'delivery_address_id');
    }

    public function billingAddress()
    {
        return $this->belongsTo(Address::class, 'billing_address_id');
    }

    public function transport()
    {
        return $this->belongsTo(Transport::class, 'transports_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'clients_id');
    }

    public function paymentsInstallments()
    {
        return $this->hasMany(OrderJoinPaymentInstallment::class, 'orders_join_id');
    }

    /*
     * Others
     */

    public static function findByClientAndOrdersId($client_id, $order_ids)
    {
        return (new self)->where('clients_id', $client_id)->where('orders_id', trim($order_ids))->first();
    }

    public function datatable($clients_id, $filters = [])
    {
        $return = $this->where('clients_id', $clients_id)
            ->select("orders_join.*");

        return $return;
    }
}
