<?php

namespace Modules\CerebeloOrders\Entities;

use Modules\CerebeloSettings\Entities\BaseModel as Model;
use Modules\CerebeloSettings\Entities\User;
use Carbon\Carbon;

class File extends Model
{

    protected $table = 'orders_files';
    protected $fillable = [
        'orders_id',
        'users_id',
        'name',
        'description',
        'file',
    ];

    /*
     * Relationships
     */

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    /*
     * Attributes
     */
}
