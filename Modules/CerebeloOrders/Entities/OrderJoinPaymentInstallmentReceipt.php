<?php

namespace Modules\CerebeloOrders\Entities;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Storage;

class OrderJoinPaymentInstallmentReceipt extends Model
{

    protected $table    = 'orders_join_payments_installments_receipt';
    protected $fillable = [
        'orders_join_payments_installments_id',
        'file',
        'description',
    ];

    public static function boot()
    {
        parent::boot();
        static::deleted(function($model) {
            $storage = Storage::disk('local');
            $file    = $model->file;
            if ($storage->exists($file))
            {
                $storage->delete($file);
            }
        });
    }

    /*
     * Scope
     */

    public function scopeWhereInstallment($query, $value)
    {
        return $query->where('orders_join_payments_installments_id', $value);
    }

    /*
     * Others
     */

    public static function allByInstallment($id)
    {
        return (new self)->whereInstallment($id)->get();
    }

}
