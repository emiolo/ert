<?php

namespace Modules\CerebeloOrders\Entities;

use Carbon\Carbon;
//use Modules\CerebeloProducts\Entities\Product;
use DB;
use Modules\CerebeloClient\Entities\Client;
use Modules\CerebeloClient\Entities\ClientContact;
use Modules\CerebeloClient\Entities\File as ClientFile;
use Modules\CerebeloCompanies\Entities\Company;
use Modules\CerebeloFinancial\Entities\PaymentsForm;
use Modules\CerebeloSettings\Entities\Address;
use Modules\CerebeloSettings\Entities\BaseModel as Model;
use Modules\CerebeloSettings\Entities\Sector;
use Modules\CerebeloSettings\Entities\SectorAction;
use Modules\CerebeloSettings\Entities\User;
use Modules\CerebeloTransports\Entities\Transport;

class Order extends Model
{

    protected $table = 'orders';
    protected $fillable = [
        'sorting',
        'order_number',
        'clients_id',
        'clients_contacts_id',
        'price_table_id',
        'transports_id',
        'payments_methods_id',
        'payments_form_id',
        'status_id',
        'companies_id',
        'sectors_actions_id',
        'event_date',
        'billing_address_id',
        'production_date',
        'delivery_date',
        'delivery_boxes',
        'delivery_address_id',
        'customer_remarks',
        'volumes',
        'cost_freight',
        'discount_type',
        'discount_value',
        'credit_value',
        'freight_cif_fob',
        'group',
        'transport_type',
        'transport_deadline',
        'payment_notes',
        'printer_option',
        'prepress',
        'to_charge',
        'urgent',
        'pre_shipment_name',
        'pre_shipment_date',
        'pre_shipment_hour',
        'pre_shipment_form',
        'op_observation',
    ];
    protected $appends = ['qty_piece', 'full_order_number'];

    public static function boot()
    {
        parent::boot();
        static::created(function ($model) {
            (new self)->defineDeliveryDate($model);
        });
        static::updating(function ($model) {
            $new = $model->getAttribute('delivery_address_id');
            $old = $model->getOriginal('delivery_address_id');

            if ($new != $old) {
                $model->transport_type = null;
                $model->transport_deadline = null;
            }
        });
        static::updated(function ($model) {
            (new self)->defineDeliveryDate($model);
        });
    }

    public function transformAudit(array $data): array
    {
        $getOriginal = $this->getOriginal();
        $getAttributes = $this->getAttributes();

        // billing_address_id
        $data = transform_audit_for_humans(['data' => $data, 'model' => new Address, 'compare' => 'billing_address_id', 'get_original' => $getOriginal, 'get_attributes' => $getAttributes, 'attribute' => 'cep']);

        // sectors_actions_id
        $data = transform_audit_for_humans(['data' => $data, 'model' => new SectorAction, 'compare' => 'sectors_actions_id', 'get_original' => $getOriginal, 'get_attributes' => $getAttributes, 'attribute' => 'name']);

        // delivery_address_id
        $data = transform_audit_for_humans(['data' => $data, 'model' => new Address, 'compare' => 'delivery_address_id', 'get_original' => $getOriginal, 'get_attributes' => $getAttributes, 'attribute' => 'cep']);

        // payments_form_id
        $data = transform_audit_for_humans(['data' => $data, 'model' => new PaymentsForm, 'compare' => 'payments_form_id', 'get_original' => $getOriginal, 'get_attributes' => $getAttributes, 'attribute' => 'name']);

        // clients_contacts_id
        $data = transform_audit_for_humans(['data' => $data, 'model' => new ClientContact, 'compare' => 'clients_contacts_id', 'get_original' => $getOriginal, 'get_attributes' => $getAttributes, 'attribute' => 'name']);

        // price_table_id
        $data = transform_audit_for_humans(['data' => $data, 'model' => new PriceTable, 'compare' => 'price_table_id', 'get_original' => $getOriginal, 'get_attributes' => $getAttributes, 'attribute' => 'name']);

        // companies_id
        $data = transform_audit_for_humans(['data' => $data, 'model' => new Company, 'compare' => 'companies_id', 'get_original' => $getOriginal, 'get_attributes' => $getAttributes, 'attribute' => 'name']);

        // status_id
        $data = transform_audit_for_humans(['data' => $data, 'model' => new OrderStatus, 'compare' => 'status_id', 'get_original' => $getOriginal, 'get_attributes' => $getAttributes, 'attribute' => 'name']);

        // transports_id
        $data = transform_audit_for_humans(['data' => $data, 'model' => new Transport, 'compare' => 'transports_id', 'get_original' => $getOriginal, 'get_attributes' => $getAttributes, 'attribute' => 'name']);

        // clients_id
        $data = transform_audit_for_humans(['data' => $data, 'model' => new Client, 'compare' => 'clients_id', 'get_original' => $getOriginal, 'get_attributes' => $getAttributes, 'attribute' => 'name']);

        if (!array_has($data, 'old_values.order_number')) {
            $data['old_values']['order_number'] = $this->order_number;
        }
        if (!array_has($data, 'new_values.order_number')) {
            $data['new_values']['order_number'] = $this->order_number;
        }

        return $data;
    }

    private function defineDeliveryDate($model)
    {
        $status_id = $model->status_id;
        $delivery_date = $model->delivery_date;
        // Status Aprovado (pelo cliente)
        if (empty($delivery_date) && $status_id == 8) { // $status_id = Pré-Produção
            $created_at = Carbon::createFromFormat('d/m/Y H:i:s', $model->updated_at)->addDays(30)->format('d/m/Y');

            $model->delivery_date = $created_at;
            $model->save();
        }
    }

    /*
     * Relationships
     */

    public function company()
    {
        return $this->belongsTo(Company::class, 'companies_id');
    }

    public function sectorAction()
    {
        return $this->belongsTo(SectorAction::class, 'sectors_actions_id');
    }

    public function transport()
    {
        return $this->belongsTo(Transport::class, 'transports_id');
    }

    public function deliveryAddress()
    {
        return $this->belongsTo(Address::class, 'delivery_address_id');
    }

    public function billingAddress()
    {
        return $this->belongsTo(Address::class, 'billing_address_id');
    }

    public function status()
    {
        return $this->belongsTo(OrderStatus::class, 'status_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'clients_id');
    }

    public function briefings()
    {
        return $this->belongsToMany(Briefing::class, 'orders__briefings', 'orders_id', 'briefings_id')->withPivot('value');
    }

    public function auditing()
    {
        return $this->belongsToMany(User::class, 'orders_audit', 'orders_id', 'users_id')->whereNull('orders_audit.deleted_at')->withPivot('id', 'message', 'created_at');
    }

    public function files()
    {
        return $this->belongsToMany(User::class, 'orders_files', 'orders_id', 'users_id')->withPivot('name', 'description', 'file')->withTimestamps();
    }

    public function manyFiles()
    {
        return $this->hasMany(File::class, 'orders_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'orders_id');
    }

    public function ordersProducts()
    {
        return $this->belongsToMany(Product::class, 'orders_products', 'orders_id', 'products_id', 'id')->orderBy('ordination', 'asc')->withPivot('id', 'quantity', 'have_sizes', 'quantity_piece', 'size_piece', 'name_piece', 'unit_price', 'extra_variation', 'price_table_id', 'price_table_value', 'presents_partnerships_id', 'image_art');
    }

    public function latestSector()
    {
        return $this->sectors()->orderBy('orders__sectors.id', 'desc');
    }

    public function sectors()
    {
        return $this->belongsToMany(User::class, 'orders__sectors', 'orders_id', 'users_id')->withPivot('id', 'start_datetime', 'end_datetime', 'confirmed')->withTimestamps();
    }

    public function attachedFiles()
    {
        return $this->belongsToMany(ClientFile::class, 'orders_attached_files', 'orders_id', 'clients_files_id');
    }

    public function paymentsInstallments()
    {
        return $this->hasMany(PaymentInstallment::class, 'orders_id');
    }

    public function actionsHistory()
    {
        return $this->hasMany(OrderActionHistory::class, 'orders_id');
    }

    public function billingClient()
    {
        return $this->belongsTo(ClientContact::class, 'clients_contacts_id');
    }

    /*
     * Attributes
     */

    public function getOrderNumberAttribute()
    {
        $prefix = "";
        try
        {
            if(!empty($this->client->representative->prefix) && !empty($this->attributes['order_number']))
                $prefix = $this->client->representative->prefix . "";
        }
        catch(\Exception $ex)
        {}

        return $prefix . '.' . last(explode('.', $this->attributes['order_number']));
    }

    public function getFullOrderNumberAttribute()
    {
        $prefix = "";
        try
        {
            if(!empty($this->client->representative->prefix) && !empty($this->attributes['order_number']))
                $prefix = $this->client->representative->prefix . "";
        }
        catch(\Exception $ex)
        {}

        return $prefix . $this->attributes['order_number'];
    }

    public function getQtyPieceAttribute()
    {
        try
        {
            return $this->ordersProducts()->sum(DB::raw('orders_products.quantity + orders_products.quantity_piece'));
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function getSortingAttribute()
    {
        $sorting = $this->attributes['sorting'];

        return ($sorting == 0 ? 1 : $sorting);
    }

    public function getToChargeAttribute()
    {
        $to_charge = $this->attributes['to_charge'];

        return ($to_charge == "Y" ? true : false);
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d/m/Y');
    }

    public function getPreShipmentDateAttribute()
    {
        $value = $this->attributes['pre_shipment_date'];
        if (!empty($value)) {
            return toDate($value);
        }
    }

    public function setPreShipmentDateAttribute($value)
    {
        $this->attributes['pre_shipment_date'] = !empty($value) ? toSql($value) : null;
    }

    public function getPreShipmentHourAttribute()
    {
        $value = $this->attributes['pre_shipment_hour'];
        return !empty($value) ? Carbon::createFromFormat('H:i:s', $value)->format('H:i') : "";
    }

    public function setPreShipmentHourAttribute($value)
    {
        $this->attributes['pre_shipment_hour'] = !empty($value) ? Carbon::createFromFormat('H:i', $value)->format('H:i:s') : null;
    }

    /*
     * get e set do campo 'event_date'
     */

    public function setEventDateAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['event_date'] = @Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        } else {
            $this->attributes['event_date'] = null;
        }
    }

    public function getEventDateAttribute()
    {
        if (empty($this->attributes['event_date'])) {
            return;
        }

        return Carbon::createFromFormat('Y-m-d', $this->attributes['event_date'])->format('d/m/Y');
    }

    /*
     * get e set do campo 'delivery_date'
     */

    public function setDeliveryDateAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['delivery_date'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        } else {
            $this->attributes['delivery_date'] = null;
        }
    }

    public function getDeliveryDateAttribute()
    {
        if (!empty($this->attributes['delivery_date'])) {
            return Carbon::createFromFormat('Y-m-d', $this->attributes['delivery_date'])->format('d/m/Y');
        }
    }

    /*
     * get e set do campo 'delivery_boxes'
     */

    public function setDeliveryBoxesAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['delivery_boxes'] = json_encode($value, true);
        }
    }

    public function getDeliveryBoxesAttribute()
    {
        if (!empty($this->attributes['delivery_boxes'])) {
            return json_decode($this->attributes['delivery_boxes']);
        }
    }

    /*
     * get e set do campo 'production_date'
     */

    public function setProductionDateAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['production_date'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        } else {
            $this->attributes['production_date'] = null;
        }

    }

    public function getProductionDateAttribute()
    {
        if (!empty($this->attributes['production_date'])) {
            return Carbon::createFromFormat('Y-m-d', $this->attributes['production_date'])->format('d/m/Y');
        }
    }

    /*
     * get e set do campo 'cost_freight'
     */

    public function getCostFreightAttribute()
    {
        try {
            return moneyFormat($this->attributes['cost_freight']);
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function setCostFreightAttribute($value)
    {
        try {
            $this->attributes['cost_freight'] = moneyUnFormat($value);
        } catch (\Exception $e) {
            return 0;
        }
    }

    /*
     * get e set do campo 'credit_value'
     */

    public function getCreditValueAttribute()
    {
        try {
            return number_format($this->attributes['credit_value'], 2, ",", ".");
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function setCreditValueAttribute($value)
    {
        try {
            $this->attributes['credit_value'] = moneyUnFormat($value);
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function getDiscountValueAttribute()
    {
        try {
            return moneyFormat($this->attributes['discount_value']);
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function setDiscountValueAttribute($value)
    {
        try {
            $this->attributes['discount_value'] = moneyUnFormat($value);
        } catch (\Exception $e) {
            return 0;
        }
    }

    /*
     * get e set do campo 'volumes'
     */

    public function getVolumesAttribute()
    {
        try {
            return str_replace(",", ".", $this->attributes['volumes']);
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function setVolumesAttribute($value)
    {
        try {
            $this->attributes['volumes'] = str_replace(",", ".", $value);
        } catch (\Exception $e) {
            return 0;
        }
    }

    /*
     * Scopes
     */

    /*
     * Others
     */

    public function filesPath()
    {
        return $this->table . "/";
    }

    public function datatable($filters = [])
    {
        $isDashboard = isset($filters['dashboard']);
        $isProduction = isset($filters['production']);
        $auth_user = auth()->user();
        $sector = $auth_user->sector->alias;
        $users_id = $auth_user->id;
        $getAllMain = Sector::getAllMain();

        $subQuery = "(SELECT MAX(orders__sectors.id) FROM orders__sectors WHERE orders__sectors.orders_id = orders.id AND orders__sectors.deleted_at IS NULL GROUP BY orders__sectors.orders_id)";
        $selectHaveJoin = "(SELECT CONCAT(orders_join.id, '-', orders_join.orders_id) AS have_join FROM orders_join WHERE orders_join.deleted_at IS NULL AND FIND_IN_SET(orders.id, orders_join.orders_id) LIMIT 1) AS have_join";

        $return = $this->with(['status', 'client.attendant', 'client.designer', 'client' => function ($query) use ($users_id, $sector, $isDashboard) {
            if ($isDashboard == true) {
                if ($sector == Sector::ATTENDANCE) {
                    $query->where('attendant_id', $users_id);
                }
                if ($sector == Sector::DESIGNER) {
                    $query->where('designer_id', $users_id);
                }
            }
        }]);
        if ($isDashboard == true) {
            $return->whereHas('client', function ($query) use ($users_id, $sector) {
                if ($sector == Sector::ATTENDANCE) {
                    $query->where('attendant_id', $users_id);
                }
                if ($sector == Sector::DESIGNER) {
                    $query->where('designer_id', $users_id);
                }
            });
        }
        $return->leftJoin('sectors_actions', 'sectors_actions.id', '=', 'orders.sectors_actions_id')
            ->join('orders__sectors', 'orders__sectors.orders_id', '=', 'orders.id');
        if (isset($filters['sectors']) && count($filters['sectors']) > 0) {
            $return->join('users', 'users.id', '=', 'orders__sectors.users_id')
                ->whereIn('users.sectors_id', $filters['sectors']);
        }
        if (isset($filters['queue']) && !empty($filters['queue']) && $filters['queue'] == true) {
            if (!isset($filters['sectors']) && count($filters['sectors']) == 0) {
                $return->join('users', 'users.id', '=', 'orders__sectors.users_id');
            }
            $return
                ->join('sectors', 'sectors.id', '=', 'users.sectors_id')
                ->where('sectors.group', Sector::PRODUCTION)
                ->whereNotNull('orders.delivery_date');
        }
        $return->where(function ($query) use ($sector, $users_id, $getAllMain, $isDashboard) {
            if ($isDashboard == true) {
                if (!in_array($sector, $getAllMain)) {
                    $query->where("orders__sectors.users_id", $users_id);
                }

                $query->where(function ($query) {
                    $query->orWhere("sectors_actions.alias", "<>", "completed")->orWhereNull("sectors_actions.alias");
                });
            }
        })
            ->where(function ($query) use ($sector, $users_id, $getAllMain, $isProduction) {
                if ($isProduction == true) {
                    if (!in_array($sector, $getAllMain)) {
                        $query->where("orders__sectors.users_id", $users_id);
                    }

                    $query->where(function ($query) {
                        $query->orWhere("sectors_actions.alias", "<>", "completed")->orWhereNull("sectors_actions.alias");
                    });
                }
            })
            ->where(function ($query) use ($filters) {
                if (isset($filters['status_id']) && count($filters['status_id']) > 0) {
                    $query->whereIn('status_id', $filters['status_id']);
                }
                if (isset($filters['client_id_join']) && !empty($filters['client_id_join'])) {
                    $query->where('clients_id', $filters['client_id_join']);
                }
                if (isset($filters['ids']) && !empty($filters['ids']) && is_array($filters['ids'])) {
                    $query->whereIn('orders.id', $filters['ids']);
                }
                if (isset($filters['sectors_actions_id']) && !empty($filters['sectors_actions_id']) && is_array($filters['sectors_actions_id'])) {
                    if (in_array(0, $filters['sectors_actions_id'])) {
                        array_push($filters['sectors_actions_id'], '');
                    }
                    $query->whereIn('orders.sectors_actions_id', $filters['sectors_actions_id'])->orWhereNull('orders.sectors_actions_id');
                }
            })
            ->where(function ($query) use ($filters) {
                if (empty($filters['search']['value']) && !isset($filters['sectors_actions_id'])) {
                    $query->orWhere("sectors_actions.alias", "<>", "completed")->orWhereNull("sectors_actions.alias");
                }
            })
            ->whereNull("orders__sectors.deleted_at")
            ->whereRaw("orders__sectors.id = $subQuery")
        //->orderBy('sectors_actions.id', 'asc')
        //->orderBy('orders.sorting', 'desc')
            ->orderBy('orders.updated_at', 'desc')
            ->select("orders.*", DB::raw($selectHaveJoin))
        ;

        return $return;
    }

    public function totalOrdersInSector()
    {
        $auth_user = auth()->user();
        $users_id = $auth_user->id;

        $subQuery = "(SELECT MAX(orders__sectors.id) FROM orders__sectors WHERE orders__sectors.orders_id = orders.id AND orders__sectors.deleted_at IS NULL AND orders__sectors.confirmed = 'N' GROUP BY orders__sectors.orders_id)";

        $return = DB::table('orders')
            ->join('orders__sectors', 'orders__sectors.orders_id', '=', 'orders.id')
            ->where("orders.sectors_actions_id", "<>", 8)
            ->whereNull("orders.deleted_at")
            ->where("orders__sectors.users_id", $users_id)
            ->whereRaw("orders__sectors.id = $subQuery")
            ->whereNull("orders__sectors.deleted_at")
            ->count();

        return $return;
    }

    /*
     * Todos os pedidos/orçamentos que estrapolaram a data limite no setor
     */
    public static function expiredInSectors()
    {
        $subQueryLastSectorConfirmed = "(SELECT MAX(orders__sectors.id) FROM orders__sectors
                                        WHERE orders__sectors.orders_id = orders.id
                                        AND orders__sectors.deleted_at IS NULL
                                        AND orders__sectors.end_datetime IS NOT NULL
                                        AND DATE_FORMAT(orders__sectors.end_datetime, '%Y-%m-%d') < CURRENT_DATE()
                                        AND orders__sectors.confirmed = 'Y' GROUP BY orders__sectors.orders_id)";

        $result = DB::table('orders')
            ->join('orders__sectors', 'orders__sectors.orders_id', '=', 'orders.id')
            ->join('users', 'users.id', '=', 'orders__sectors.users_id')
            ->join('sectors', 'sectors.id', '=', 'users.sectors_id')
            ->whereNull('orders.deleted_at')
            ->whereNotIn('orders.status_id', [6]) // não incluir status: Finalizado (6)
            ->whereRaw("orders__sectors.id = $subQueryLastSectorConfirmed")
            ->select('orders.id', 'orders.order_number', 'users.name', 'sectors.name AS sector_name', DB::raw('DATE_FORMAT(orders__sectors.end_datetime, "%d/%m/%Y") AS end_datetime'))
            ->get();

        return $result;
    }

    /*
     * Todos os pedidos/orçamentos que estrapolaram a data limite de aceite no setor
     */
    public static function orderAcceptExpired()
    {
        //DB::enableQueryLog();
        $subQueryLastSectorConfirmed = "(SELECT MAX(orders__sectors.id) FROM orders__sectors
                                        WHERE orders__sectors.orders_id = orders.id
                                        AND orders__sectors.deleted_at IS NULL
                                        AND orders__sectors.created_at IS NOT NULL
                                        AND DATE_ADD(orders__sectors.created_at, INTERVAL 12 HOUR) > NOW()
                                        AND orders__sectors.confirmed = 'N' GROUP BY orders__sectors.orders_id)";

        $result = DB::table('orders')
            ->join('orders__sectors', 'orders__sectors.orders_id', '=', 'orders.id')
            ->join('users', 'users.id', '=', 'orders__sectors.users_id')
            ->join('sectors', 'sectors.id', '=', 'users.sectors_id')
            ->whereNull('orders.deleted_at')
            ->whereNotIn('orders.status_id', [6]) // não incluir status: Finalizado (6)
            ->whereRaw("orders__sectors.id = $subQueryLastSectorConfirmed")
            ->select('orders.id', 'orders.order_number', 'users.name', 'sectors.name AS sector_name', 'sectors.responsible_id')
            ->get();
        //dd(DB::getQueryLog());

        return $result;
    }

    /*
     * Verifica se o usuário logado é do mesmo setor que o setor atual do pedido
     *
     * @var $orders_id int
     * @var $logged_sector string
     *
     * @return boolean
     */
    public function isUserAtualSector($orders_id, $logged_sector)
    {
        try {
            $auth_user = auth()->user();
            $alias = $auth_user->sector->alias;
            if (trim($alias) == 'admin') {
                return true;
            }

            $result = $this->find($orders_id)->latestSector()->first();
            $sector = $result->sector->alias;

            if (trim($sector) == trim($logged_sector)) {
                return true;
            }

            return false;
        } catch (\Exception $ex) {
            return false;
        }
    }

    public function statusFilter($from = 'dashboard')
    {
        try {
            $getAllMain = Sector::getAllMain();
            $auth_user = auth()->user();
            $sector = $auth_user->sector->alias;
            $users_id = $auth_user->id;
            $sectors_id = $auth_user->sectors_id;

            $client_where = $sectors_where = "";
            if($from == 'dashboard')
            {
                if ($sector == Sector::ATTENDANCE) {
                    $client_where = " AND clients.attendant_id = $users_id ";
                }
                if ($sector == Sector::DESIGNER) {
                    $client_where = " AND clients.designer_id = $users_id ";
                }
                if (!in_array($sector, $getAllMain)) {
                    $sectors_where = " orders__sectors.users_id = $users_id ";
                }
            }

            // Pega o último setor por pedido
            $subQuery = "(SELECT MAX(orders__sectors.id) 
                        FROM orders__sectors 
                        WHERE orders__sectors.orders_id = orders.id AND orders__sectors.deleted_at IS NULL 
                        GROUP BY orders__sectors.orders_id)";

            // Pega o total de pedidos por ações
            $subQuery = "(SELECT COUNT(orders.id) 
                        FROM orders 
                        INNER JOIN clients ON clients.id = orders.clients_id AND clients.deleted_at IS NULL $client_where 
                        INNER JOIN orders__sectors ON orders__sectors.orders_id = orders.id AND orders__sectors.deleted_at IS NULL $sectors_where 
                        WHERE orders.sectors_actions_id = sectors_actions.id 
                        AND orders.deleted_at IS NULL 
                        AND orders__sectors.id = $subQuery 
                        GROUP BY orders.sectors_actions_id 
                        HAVING COUNT(orders.id) > 0) AS total_orders";

            // Lista todas as ações invuladas ao setor
            $return = DB::table('sectors_actions')
                        ->join('sectors_has_sectors_actions', 'sectors_has_sectors_actions.sectors_actions_id', '=', 'sectors_actions.id')
                        //->where("sectors_has_sectors_actions.sectors_id", $sectors_id)
                        ->where(function($where) use($from) {
                            if($from == 'dashboard')
                            {
                                $where->orWhere("sectors_actions.alias", "<>", "completed")->orWhereNull("sectors_actions.alias");
                            }
                        })
                        ->groupBy(DB::raw('sectors_actions.id, sectors_actions.name'))
                        ->get(['sectors_actions.id', 'sectors_actions.name', DB::raw($subQuery)]);

            return $return;
        } catch (\Exception $ex) {
            return $ex;
        }
    }

}
