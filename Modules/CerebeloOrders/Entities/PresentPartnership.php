<?php

namespace Modules\CerebeloOrders\Entities;

use Illuminate\Database\Eloquent\Model;

class PresentPartnership extends Model
{

    protected $table    = 'presents_partnerships';
    protected $fillable = [
        'name',
    ];
    public $timestamps  = false;

    /*
     * Relationships
     */

    /*
     * Scopes
     */

    /*
     * Ohters
     */

    /*
     * Retorna todos para select2
     * 
     * @return array
     */

    public static function formSelect()
    {
        $return = self::pluck('name', 'id')->prepend('Selecione', '');
        return $return;
    }

}
