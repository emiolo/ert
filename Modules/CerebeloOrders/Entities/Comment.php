<?php

namespace Modules\CerebeloOrders\Entities;

use Modules\CerebeloSettings\Entities\BaseModel as Model;

class Comment extends Model
{

    protected $table = 'orders_audit';
    protected $fillable = [
        'orders_id',
        'users_id',
        'message',
    ];

    /*
     * Relationships
     */
}
