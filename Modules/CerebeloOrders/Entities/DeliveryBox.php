<?php

namespace Modules\CerebeloOrders\Entities;

use DB;
use Modules\CerebeloSettings\Entities\BaseModel as Model;

class DeliveryBox extends Model
{

    protected $table = 'delivery_box';
    protected $fillable = [
        'min_weight',
        'max_weight',
        'width',
        'length',
        'height',
    ];

    /*
     * Scopes
     */

    public function scopeName()
    {
        return $this->select(DB::raw('CONCAT(length, "|", width, "|", height, "|", max_weight) as name, id'));
    }

    public function scopeOrWhereMaxWeight($query, $param)
    {
        return $query->orWhere('max_weight', '>=', $param);
    }

    public function scopeOrWhereWeightMax($query)
    {
        return $query->orWhere('max_weight', '=', DB::table($this->table)->max('max_weight'));
    }

    public function scopeWhereMaxWeight($query, $param)
    {
        return $query->orWhereMaxWeight($param)->orWhereWeightMax();
    }

    public function scopeOrderByMaxWeight($query, $param)
    {
        return $query->orderBy('max_weight', $param);
    }

    /*
     * Others
     */

    public function dimension($weight)
    {
        return $this->whereMaxWeight($weight)->first();
    }

    public static function formSelect()
    {
        $return = self::name()->pluck('name', 'id');
        return $return;
    }

}
