<?php

namespace Modules\CerebeloOrders\Entities;

use Illuminate\Database\Eloquent\Model;

class OrderProductOpWithName extends Model
{

    protected $table = 'orders_products_op_with_name';
    protected $fillable = [
        'ordination',
        'orders_products_id',
        'name',
        'quantity',
        'size',
    ];

    /*
     * Relationships
     */

    /*
     * Attributes
     */

    /*
     * Scope
     */

    public function scopeOrderByOrdination($query, $param = "asc")
    {
        $query->orderBy('ordination', $param);
    }

    /*
     * Ohters
     */

    public function allByProduct($id)
    {
        return $this->where('orders_products_id', $id)->orderByOrdination()->get();
    }

}
