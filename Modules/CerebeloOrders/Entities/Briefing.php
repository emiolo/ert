<?php

namespace Modules\CerebeloOrders\Entities;

use Modules\CerebeloSettings\Entities\BaseModel as Model;

class Briefing extends Model {

    protected $table = 'briefings';
    protected $fillable = [
        'parent_id',
        'title',
        'field_type', //text;textarea;checkbox;radio
        'relationship',
        'description',
        'class_content',
        'ordination',
    ];

    /*
     * Relationships
     */

    public function answers() {
        return $this->hasMany(Briefing::class, 'parent_id');
    }
    
    public function orders() {
        return $this->belongsToMany(Order::class, 'orders__briefings', 'orders_id', 'briefings_id')->withPivot('value');
    }

    /*
     * Scopes
     */

    public function scopeWhereQuestion($q) {
        return $q->whereNull('parent_id')->orWhere('parent_id', 0)->orWhere('parent_id', '');
    }

    public function scopeOrdinationAsc($q) {
        return $q->orderBy('ordination', 'asc');
    }

    /*
     * Others
     */

    public function allOrdinationAsc() {
        return $this->with(['answers.answers.answers'])->whereQuestion()->ordinationAsc()->get();
    }

}
