<?php

namespace Modules\CerebeloOrders\Entities;

use Event;
use Modules\CerebeloSettings\Entities\BaseModel as Model;
use Modules\CerebeloSettings\Events\AlertSysEvent;
use Modules\CerebeloSizes\Entities\Size;
use Modules\CerebeloVariations\Entities\Variation;
use Storage;

class OrderProduct extends Model
{

    protected $table = 'orders_products';
    protected $fillable = [
        'orders_id',
        'products_id',
        'price_table_id',
        'price_table_value',
        'presents_partnerships_id',
        'quantity',
        'have_sizes',
        'quantity_piece',
        'size_piece',
        'name_piece',
        'unit_price',
        'extra_variation',
        'image_art',
        'ordination',
    ];
    protected $appends = ['name'];

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($model) {
            $old_file_name = $model->image_art;
            $storage = Storage::disk('local');

            if (!empty($old_file_name)) {
                if ($storage->exists($old_file_name)) {
                    $storage->delete($old_file_name);
                }
            }
        });
        static::deleted(function ($model) {

        });
        static::creating(function ($model) {
            (new self)->checkIfNewProductAdd($model);
        });
        static::updating(function ($model) {
            (new self)->checkIfNameProductUpdated($model);
        });
    }

    public function transformAudit(array $data): array
    {
        $getOriginal = $this->getOriginal();
        $getAttributes = $this->getAttributes();
        // orders_id
        $data = transform_audit_for_humans(['data' => $data, 'model' => new Order, 'compare' => 'orders_id', 'get_original' => $getOriginal, 'get_attributes' => $getAttributes, 'attribute' => 'order_number']);
        // products_id
        $data = transform_audit_for_humans(['data' => $data, 'model' => new Product, 'compare' => 'products_id', 'get_original' => $getOriginal, 'get_attributes' => $getAttributes, 'attribute' => 'name']);
        // price_table_id
        $data = transform_audit_for_humans(['data' => $data, 'model' => new PriceTable, 'compare' => 'price_table_id', 'get_original' => $getOriginal, 'get_attributes' => $getAttributes, 'attribute' => 'name']);
        // presents_partnerships_id
        $data = transform_audit_for_humans(['data' => $data, 'model' => new PresentPartnership, 'compare' => 'presents_partnerships_id', 'get_original' => $getOriginal, 'get_attributes' => $getAttributes, 'attribute' => 'name']);

        $data['old_values']['order_number'] = $this->order->order_number;
        $data['new_values']['order_number'] = $this->order->order_number;

        $data['old_values']['product_name'] = $this->product->name;
        $data['new_values']['product_name'] = $this->product->name;

        return $data;
    }

    /* DESIGNER
     * Verifica se um novo produto foi adicionado e envia a notificação/alerta para o designer
     *
     * @var $model Modules\CerebeloOrders\Entities\OrderProduct
     */
    private function checkIfNewProductAdd($model)
    {
        $attributes = $model->getAttributes();
        $original = $model->getOriginal();

        if (empty($original) && count($original) == 0) {
            $auth_user = auth()->user();

            $order = $model->order()->first();
            $order_number = $order->getAttribute('order_number');
            $client = $model->order()->first()->client()->first();
            $designer_id = $client->getAttribute('designer_id');

            $product = $model->product()->first();
            $product_name = $product->getAttribute('name');

            $content = "$auth_user->name criou um novo produto ($product_name) no pedido nº <span class='text-bold'>$order_number</span>, <a href='" . route('cerebelo.orders.edit', $model->orders_id) . "#justified-badges-tab2'>ver pedido</a>.";
            Event::fire(new AlertSysEvent($designer_id, $content));
        }
    }

    /* DESIGNER
     * Verifica se o produto foi alterado e envia a notificação/alerta para o designer
     *
     * @var $model Modules\CerebeloOrders\Entities\OrderProduct
     */
    private function checkIfNameProductUpdated($model)
    {
        $attributes = $model->getAttributes();
        $original = $model->getOriginal();

        if ($original['products_id'] != $attributes['products_id']) {
            $auth_user = auth()->user();

            $order = $model->order()->first();
            $order_number = $order->getAttribute('order_number');
            $client = $model->order()->first()->client()->first();
            $designer_id = $client->getAttribute('designer_id');

            $product = $model->product()->first();
            $product_name = $product->getAttribute('name');

            $content = "$auth_user->name alterou o produto para <em>$product_name</em> do pedido nº <span class='text-bold'>$order_number</span>, <a href='" . route('cerebelo.orders.edit', $model->orders_id) . "#justified-badges-tab2'>ver pedido</a>.";
            Event::fire(new AlertSysEvent($designer_id, $content));
        }
    }

    /* DESIGNER
     * Verifica se as variações o produto foram alteradas e envia a notificação/alerta para o designer
     *
     * @var $model Modules\CerebeloOrders\Entities\OrderProduct
     * @var $new_variations array(2 => "2" 3 => "3" 4 => "1")
     */
    public function checkIfVariationProductUpdated($model, $new_variations)
    {
        $auth_user = auth()->user();
        $order = $model->order()->first();
        $order_number = $order->getAttribute('order_number');

        $client = $model->order()->first()->client()->first();
        $designer_id = $client->getAttribute('designer_id');

        $product = $model->product()->first();
        $product_name = $product->getAttribute('name');

        $old_variations = $model->variations()->get()->toArray();

        $flag = false;

        foreach ($old_variations as $key => $value) {
            if (isset($value['id']) && !in_array($value['id'], array_values($new_variations))) {
                $flag = true;
            }
        }

        if ($flag === true) {
            $content = "$auth_user->name alterou uma ou mais variações do produto <em>$product_name</em> do pedido nº <span class='text-bold'>$order_number</span>, <a href='" . route('cerebelo.orders.edit', $model->orders_id) . "#justified-badges-tab2'>ver pedido</a>.";
            Event::fire(new AlertSysEvent($designer_id, $content));
        }
    }

    /*
     * Relationships
     */

    public function order()
    {
        return $this->belongsTo(Order::class, 'orders_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'products_id');
    }

    public function sizes()
    {
        return $this->belongsToMany(Size::class, 'orders_products_sizes', 'orders_products_id', 'sizes_id')->withPivot('quantity');
    }

    public function variations()
    {
        return $this->belongsToMany(Variation::class, 'orders_products_variations', 'orders_products_id', 'variations_id')->withPivot('id', 'position');
    }

    public function opWithName()
    {
        return $this->hasMany(OrderProductOpWithName::class, 'orders_products_id')
            ->selectRaw("orders_products_op_with_name.*, FIELD(size, 'PP', 'P', 'M', 'G', 'GG', '3G', '4G', '5G') as _index")
            ->orderByRaw('_index asc')
        ;
    }

    /*
     * Attributes
     */

    public function getNameAttribute()
    {
        return '"' . $this->order->order_number . '" - ' . $this->product->name;
    }

    public function getUnitPriceAttribute()
    {
        return moneyFormat($this->attributes['unit_price']);
    }

    public function setUnitPriceAttribute($value)
    {
        $this->attributes['unit_price'] = moneyUnFormat($value);
    }

}
