<?php

namespace Modules\CerebeloOrders\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class HttpLoggerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $http_logger_env = env('HTTP_LOGGER', "");

        if(auth()->check() && $http_logger_env === true)
        {
            $auth      = auth()->user();
            $user_id   = $auth->id;
            $user_name = $auth->name;

            $now = date("d-m-Y");
            $file_name = "/logs/http-$user_id-logger-$now.log";

            try
            {
                $log = new Logger('HttpLogger');
                $log->pushHandler(new StreamHandler(storage_path() . $file_name, Logger::DEBUG));
                $log->debug("DADOS DE REQUEST ($user_name): ", $request->all());
            }
            catch(\Exception $ex)
            {}
        }

        return $next($request);
    }
}
