<?php

Route::get('/test-sendgrid', function () {
    $data = ['content_body' => 'Testando envio de E-mail!!'];
    \Mail::to('sotnas.lony@emiolo.com')->send(new \App\Mail\TestEmail($data));
    dd('E-mail Enviado.');
});

Route::group(['middleware' => ['web', 'cerebelo.authenticated'], 'prefix' => 'cerebelo', 'namespace' => 'Modules\CerebeloOrders\Http\Controllers'], function () {

    Route::get('/correios-consulta-frete', function (Illuminate\Http\Request $request) {
        return correiosConsultFreight($request);
    })->name('cerebelo.correios.frete');

    Route::group(['middleware' => ['cerebelo.permission']], function () {

        Route::group(['prefix' => 'orders'], function () {

            Route::get('/', 'OrdersController@index')->name('cerebelo.orders.index');
            Route::get('/listing', 'OrdersController@datatable')->name('cerebelo.orders.datatable');
            Route::get('/comments/{client_id}', 'OrdersController@comments')->name('cerebelo.orders.comments');
            Route::get('/create', 'OrdersController@create')->name('cerebelo.orders.create');
            Route::get('/add-block-product', 'OrdersController@addBlockProduct')->name('cerebelo.orders.add_block_product');
            Route::delete('/remove-comment', 'OrdersController@removeComment')->name('cerebelo.orders.remove_comment');
            Route::delete('/remove-file', 'OrdersController@removeFile')->name('cerebelo.orders.remove_file');
            Route::post('/consult-freight', 'OrdersController@traitConsultFreight')->name('cerebelo.orders.consult_freight');
            Route::post('/consult-boxes/{join?}', 'OrdersController@traitEstimateBox')->name('cerebelo.orders.consult_boxes');
            Route::post('/', 'OrdersController@store')->name('cerebelo.orders.store');
            Route::post('/send-mail/{id}', 'OrdersController@sendMail')->name('cerebelo.orders.send_mail');
            Route::patch('/send-mail/{id}', 'OrdersController@traitSendOrderToClient')->name('cerebelo.orders.send_mail');
            Route::get('/print/{id}', 'OrdersController@show')->name('cerebelo.orders.print');
            //Route::get('/show/{id}', 'OrdersController@show')->name('cerebelo.orders.show');
            Route::get('/edit/{id}', 'OrdersController@edit')->name('cerebelo.orders.edit');
            Route::get('/view-art/{id}', 'OrdersController@viewArt')->name('cerebelo.orders.view_art');
            Route::get('/upload-art/{id}', 'OrdersController@uploadArt')->name('cerebelo.orders.upload_art');
            Route::post('/upload-art', 'OrdersController@saveArt')->name('cerebelo.orders.upload_art');
            Route::delete('/delete-art/{id?}', 'OrdersController@deleteArt')->name('cerebelo.orders.destroy_art');
            Route::match(['put', 'patch'], '/update/{id}', 'OrdersController@update')->name('cerebelo.orders.update');
            Route::delete('/delete/{id}', 'OrdersController@destroy')->name('cerebelo.orders.destroy');
            Route::delete('/delete-product/{id}', 'OrdersController@destroyProduct')->name('cerebelo.orders.destroy_product');
            Route::post('/upload-checking-copy', 'OrdersController@traitSaveCheckingCopy')->name('cerebelo.orders.upload_checking_copy');
            Route::get('/edit-checking-copy/{id}', 'OrdersController@traitEditCheckingCopy')->name('cerebelo.orders.edit_checking_copy');
            Route::delete('/delete-checking-copy/{id}', 'OrdersController@traitDestroyCheckingCopy')->name('cerebelo.orders.destroy_checking_copy');
            Route::match(['post', 'get'], '/line-up', 'OrdersController@lineUp')->name('cerebelo.orders.line_up');
            Route::match(['put', 'patch'], '/sector-action/{id}', 'OrdersController@sectorAction')->name('cerebelo.orders.sector_actions');
            Route::match(['post', 'get'], '/notification-menu/{type?}', 'OrdersController@notificationMenu')->name('cerebelo.orders.notification_menu');
            // Aba 1 - Dados do Cliente
            Route::post('/store-client-data', 'OrdersController@traitStoreClientData')->name('cerebelo.orders.store_client_data');
            Route::match(['put', 'patch'], '/update-client-data/{id}', 'OrdersController@traitUpdateClientData')->name('cerebelo.orders.update_client_data');
            // Aba 2 - Orçamento
            Route::post('/percentage', 'OrdersController@percentage')->name('cerebelo.orders.percentage');
            Route::match(['put', 'patch'], '/update-budget/{id}', 'OrdersController@traitUpdateBudget')->middleware(Modules\CerebeloOrders\Http\Middleware\HttpLoggerMiddleware::class)->name('cerebelo.orders.update_budget');
            Route::get('/op-name/{id?}', 'OrdersController@indexOpWithName')->name('cerebelo.orders.op_name');
            Route::post('/op-name/{id?}', 'OrdersController@saveOpWithName')->name('cerebelo.orders.op_name');
            Route::delete('/op-name/{id?}', 'OrdersController@traitDestroyOpWithName')->name('cerebelo.orders.op_name');
            Route::get('/print-op-name/{id}', 'OrdersController@traitPrintOpWithName')->name('cerebelo.orders.print_op_name');
            // Aba 3 - Briefing
            Route::match(['put', 'patch'], '/update-briefing/{id}', 'OrdersController@traitUpdateBriefing')->name('cerebelo.orders.update_briefing');
            // Aba 6 - Ordem de Produção
            Route::patch('/update-printer/{id}', 'OrdersController@updatePrinter')->name('cerebelo.orders.update_printer');
            Route::get('/print-correios-label/{id}', 'OrdersController@printCorreiosLabel')->name('cerebelo.orders.print_correios_label');
            // Juntar orçamentos/pedidos do Cliente
            Route::get('/join/{ids?}/{unified_num?}', 'OrdersController@traitJoinOrdersOfClient')->name('cerebelo.orders.join');
            // Orçamentos Unificados
            Route::get('/join-datatable/{client_id}', 'OrdersController@traitDatatableOrdersOfClient')->name('cerebelo.orders.datatable_unified');
            Route::delete('/join/{id}', 'OrdersController@traitDestroyJoinOrdersOfClient')->name('cerebelo.orders.join_destroy');
            Route::get('/join-edit/{id}', 'OrdersController@traitEditJoinOrdersOfClient')->name('cerebelo.orders.join_edit');
            Route::match(['put', 'patch'], '/update-budget-unified/{id}', 'OrdersController@traitUpdateJoinOrdersOfClient')->name('cerebelo.orders.update_budget_unified');
            Route::get('/edit-join-checking-copy/{id}', 'OrdersController@traitEditJoinCheckingCopy')->name('cerebelo.orders.edit_join_checking_copy');
            Route::post('/upload-join-checking-copy', 'OrdersController@traitSaveJoinCheckingCopy')->name('cerebelo.orders.upload_join_checking_copy');
            Route::delete('/delete-join-checking-copy/{id}', 'OrdersController@traitDestroyJoinCheckingCopy')->name('cerebelo.orders.destroy_join_checking_copy');
            // Listagem dos Arquivos do Cliente
            Route::get('/datatable-client-files/{client_id}', 'OrdersController@traitDatatableClientFiles')->name('cerebelo.orders.datatable_client_files');
            // Salvar anexos
            Route::post('/attach-files/{id}', 'OrdersController@traitAttachFiles')->name('cerebelo.orders.attach_files');
            // Gerar XML para Financeiro
            Route::get('/xml/{ids?}', 'OrdersController@xmlFinancial')->name('cerebelo.orders.xml');
        });
    });
});
