<?php

namespace Modules\CerebeloOrders\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'order_number' => 'required|max:20|unique:orders,order_number' . ((isset($this->id) && !empty($this->id)) ? ',' . $this->id : ''),
            'address_id' => 'required',
            'price_table_id' => 'required',
            'attendant_id' => 'required',
            'representative_id' => 'required',
            'designer_id' => 'required',
            'tables_id' => 'required',
            'cpf_cnpj' => 'required|max:18|cpf_cnpj|unique:clients,cpf_cnpj' . ((isset($this->clients_id) && !empty($this->clients_id)) ? ',' . $this->clients_id : ''),
            'state_registration' => 'required|max:17',
            'company_name' => 'required|max:150',
            'fantasy_name' => 'required|max:150',
            'phone' => 'required|max:15|phone_with_ddd',
            'email' => 'required|email|max:150',
            'cep' => 'required|max:9',
            'street' => 'required|max:250',
            'number' => 'required|max:50',
            'neighborhood' => 'required|max:150',
            'city' => 'required|max:150',
            'state' => 'required|max:2',
            //'address_id' => 'required_if:_cep,""',
            '_cep' => 'required_without:address_id',
            '_street' => 'required_without:address_id',
            '_number' => 'required_without:address_id',
            '_neighborhood' => 'required_without:address_id',
            '_city' => 'required_without:address_id',
            '_state' => 'required_without:address_id',
            'files.*' => 'max:10240|mimes:jpeg,bmp,png,jpg,gif,pdf,xls,xlsx,doc,docx,cdr,psd',
            'transports_id' => 'required',
            'volumes' => 'required',
            'payments_form_id' => 'required',
            'portion' => 'required',
            'plots' => 'required',
            'total_order' => 'required',
            'cost_freight' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

}
