<?php

namespace Modules\CerebeloOrders\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class BudgetRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price_table_id' => 'required',
            'transports_id' => 'required',
            'volumes' => 'required',
            'payments_form_id' => 'required_with:to_charge',
            'portion' => 'required_with:to_charge',
            'plots' => 'required_with:to_charge|plots_zero_without:to_charge|total_plots:to_charge,final_total_order,plots_status',
            'total_order' => 'required',
            'cost_freight' => 'required|gt:0,to_charge',
            'transport_freight' => 'required_if:transports_id,1',
            'products_id.*' => 'required',
            'quantity.*' => 'required|sum_different:sizes',
            'unit_price.*' => 'required|regex:/^\d*(\,\d{2})?$/',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'products_id.*.required' => 'O campo Produto é obrigatório.',
            'quantity.*.required' => 'O campo Quantidade é obrigatório.',
            'quantity.*.sum_different' => 'O valor da Quantidade não bate com o total dos tamanhos.',
            'unit_price.*.required' => 'O campo Preço Unitário é obrigatório.',
            'unit_price.*.regex' => 'O formato de Preço Unitário é inválido.',
            'transport_freight.required_if' => 'O campo Frete é obrigatório quando a opção Correios for selecionada.',
            'payments_form_id.required_with' => 'O campo Forma de Pagamento é obrigatório.',
            'portion.required_with' => 'O campo Parcela é obrigatório quando Cobrar for selecionado.',
            'plots.required_with' => 'O campo Parcelas é obrigatório quando Cobrar for selecionado.',
            'plots.total_plots' => 'O valor total do pedido é diferente do total das parcelas.',
            'cost_freight.gt' => 'O Valor do Frete não pode ser zero (0).',
        ];
    }

    public function response(array $errors)
    {
        if ($this->expectsJson()) {
            return new JsonResponse($errors, 422);
        }

        $route = $this->get('order_id', false) === false ? route('cerebelo.orders.create', ["#justified-badges-tab2"]) : route('cerebelo.orders.edit', ['id' => $this->get('order_id'), "#justified-badges-tab2"]);

        return redirect()->to($route)
            ->withInput($this->except($this->dontFlash))
            ->withErrors($errors, $this->errorBag);
    }

}
