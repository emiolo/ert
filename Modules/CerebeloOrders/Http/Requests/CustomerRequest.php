<?php

namespace Modules\CerebeloOrders\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class CustomerRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = '|unique:clients,cpf_cnpj,NULL,id,deleted_at,NULL';

        if(isset($this->clients_id) && !empty($this->clients_id)) {
            $unique = '|unique:clients,cpf_cnpj,' . $this->clients_id . ',id,deleted_at,NULL';
        }

        return [
            'attendant_id'        => 'required',
            'representative_id'   => 'required',
            'designer_id'         => 'required',
            //'tables_id'           => 'required',
            'person_type'         => 'required',
            'cpf_cnpj'            => 'required|max:18' . $unique, //'|cpf_cnpj|unique:clients,cpf_cnpj,NULL' . ((isset($this->clients_id) && !empty($this->clients_id)) ? ',' . $this->clients_id : ',id') . ',deleted_at,NULL',
            'state_registration' => 'max:20',
            'company_name'        => 'max:150',
            'fantasy_name'        => 'max:150',
            //'fantasy_name'        => 'required_if:person_type,PJ|max:150',
            'name'                => 'max:150',
            //'rg'                  => 'required_if:person_type,PF|max:18',
            'phone'               => 'required|max:15|phone_with_ddd_if:international,Y',
            'email'               => 'required|email|max:150',
            'contact_name'        => 'max:150',
            'contact_phone'       => 'max:15',
            'contact_email'       => 'email|max:150',
            'contact_description' => 'max:150',
            'address_id'          => 'required_without:cep',
//            'type.*'             => 'required_without:address_id',
            'cep.*'               => 'required_without:address_id|max:9',
            'street.*'            => 'required_without:address_id|max:250',
            'number.*'            => 'required_without:address_id|max:50',
            'neighborhood.*'      => 'required_without:address_id|max:150',
            'city.*'              => 'required_without:address_id|max:150',
            //'state.*'             => 'required_without:address_id|max:100',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
//            'type.*.required'         => 'O campo Tipo é obrigatório',
            'cep.*.required_without'          => 'O campo CEP é obrigatório quando Endereço Salvo não está presente.',
            'street.*.required_without'       => 'O campo Logradouro é obrigatório quando Endereço Salvo não está presente',
            'number.*.required_without'       => 'O campo Número é obrigatório quando Endereço Salvo não está presente',
            'neighborhood.*.required_without' => 'O campo Bairro é obrigatório quando Endereço Salvo não está presente',
            'city.*.required_without'         => 'O campo Cidade é obrigatório quando Endereço Salvo não está presente',
            'state.*.required_without'        => 'O campo Estado é obrigatório quando Endereço Salvo não está presente',
            'phone.phone_with_ddd_if'         => 'O campo Telefone deve ser preenchido corretamente se o Cliente não for internacional.',
        ];
    }

    public function response(array $errors)
    {
        if ($this->expectsJson())
        {
            return new JsonResponse($errors, 422);
        }

        $route = $this->get('order_id', false) === false ? route('cerebelo.orders.create', ["#justified-badges-tab1"]) : route('cerebelo.orders.edit', ['id' => $this->get('order_id'), "#justified-badges-tab1"]);

        return redirect()->to($route)
                        ->withInput($this->except($this->dontFlash))
                        ->withErrors($errors, $this->errorBag);
    }

}
