<?php

namespace Modules\CerebeloOrders\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\CerebeloOrders\Http\Controllers\CerebeloController as Controller;
use Datatables;
use Storage;
use Carbon\Carbon;
use Modules\CerebeloOrders\Http\Controllers\Traits\OrdersClientDataTrait;
use Modules\CerebeloOrders\Http\Controllers\Traits\OrdersBudgetTrait;
use Modules\CerebeloOrders\Http\Controllers\Traits\OrdersBriefingTrait;
use Modules\CerebeloOrders\Http\Controllers\Traits\JoinOrdersOfClientTrait;
use Modules\CerebeloOrders\Http\Controllers\Traits\OrdersProductionTrait;
use Modules\CerebeloOrders\Http\Controllers\Traits\PaymentsCheckingCopyTrait;
use Mail;
use Image;
use DB;
use Validator;
// __construct Entities ####################################################### start
use Modules\CerebeloOrders\Entities\Order;
use Modules\CerebeloOrders\Entities\OrderJoin;
use Modules\CerebeloSettings\Entities\User;
use Modules\CerebeloProducts\Entities\Product;
use Modules\CerebeloRepresentatives\Entities\Representative;
use Modules\CerebeloVariations\Entities\Variation;
use Modules\CerebeloSizes\Entities\Size;
use Modules\CerebeloTransports\Entities\Transport;
use Modules\CerebeloOrders\Entities\Briefing;
use Modules\CerebeloOrders\Entities\DeliveryBox;
use Modules\CerebeloFinancial\Entities\PaymentsForm;
use Modules\CerebeloSettings\Entities\Address;
use Modules\CerebeloSettings\Entities\Sector;
use Modules\CerebeloSettings\Entities\SectorAction;
use Modules\CerebeloOrders\Entities\OrderProduct;
use Modules\CerebeloOrders\Entities\PaymentInstallment;
use Modules\CerebeloOrders\Entities\OrderJoinPaymentInstallment;
use Modules\CerebeloOrders\Entities\File as FileModel;
use Modules\CerebeloOrders\Entities\Comment as Comment;
use Modules\CerebeloOrders\Entities\OrderStatus;
use Modules\CerebeloOrders\Entities\PresentPartnership;
use Modules\CerebeloTable\Entities\PriceTable;
use Modules\CerebeloClient\Entities\Client;
use Modules\CerebeloClient\Entities\File as ClientFile;
use Modules\CerebeloCompanies\Entities\Company;
use Modules\CerebeloOrders\Entities\OrderProductOpWithName;
use mPDF;
use Modules\CerebeloOrders\Support\CorreiosSigep;
use Swap;

// __construct Entities ####################################################### end

use Event;
use Modules\CerebeloSettings\Events\AlertSysEvent;

class OrdersController extends Controller
{

    use OrdersClientDataTrait,
        OrdersBudgetTrait,
        OrdersBriefingTrait,
        JoinOrdersOfClientTrait,
        OrdersProductionTrait,
        PaymentsCheckingCopyTrait;

    protected $_order;
    protected $_user;
    protected $_representative;
    protected $_product;
    protected $_variation;
    protected $_transport;
    protected $_briefing;
    protected $_delivery_box;
    protected $_payments_form;
    protected $_address;
    protected $_order_product;
    protected $_client;
    protected $_payment_installment;
    protected $_join_payment_instal;
    protected $_file_model;
    protected $_comment;
    protected $_sector;
    protected $_order_status;
    protected $_present_partnership;
    protected $_company;
    protected $_mpdf;
    protected $_op_with_name;
    protected $_order_join;
    protected $_file;
    protected $_sector_action;

    const FOLDER = "orders.";

    public function __construct()
    {
        $this->_order               = new Order();
        $this->_user                = new User();
        $this->_representative      = new Representative();
        $this->_price_table         = new PriceTable();
        $this->_product             = new Product();
        $this->_variation           = new Variation();
        $this->_size                = new Size();
        $this->_transport           = new Transport();
        $this->_briefing            = new Briefing();
        $this->_delivery_box        = new DeliveryBox();
        $this->_payments_form       = new PaymentsForm();
        $this->_address             = new Address();
        $this->_order_product       = new OrderProduct();
        $this->_client              = new Client();
        $this->_payment_installment = new PaymentInstallment();
        $this->_join_payment_instal = new OrderJoinPaymentInstallment();
        $this->_file_model          = new FileModel();
        $this->_comment             = new Comment();
        $this->_sector              = new Sector();
        $this->_order_status        = new OrderStatus();
        $this->_present_partnership = new PresentPartnership();
        $this->_company             = new Company();
        $this->_mpdf                = new mPDF();
        $this->_op_with_name        = new OrderProductOpWithName();
        $this->_order_join          = new OrderJoin();
        $this->_file                = new ClientFile();
        $this->_sector_action       = new SectorAction();
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $sectors         = $this->_sector->formSelect();
        $orders_status   = $this->_order_status->formSelect();
        $sectors_actions = $this->_sector_action->formSelect();
        $sectors_actions->prepend('Não Definido', 0);
        $status_filter = $this->_order->statusFilter('orders');

        return view(self::MODULE_VIEW . self::FOLDER . 'index', compact('sectors', 'orders_status', 'sectors_actions', 'status_filter'));
    }

    public function printCorreiosLabel(Request $request, $id)
    {
        $total_box      = $request->get('total_box');
        $weight         = $request->get('weight') / $total_box;
        $length         = $request->get('length');
        $width          = $request->get('width');
        $height         = $request->get('height');
        $transport_type = $request->get('transport_freight');

        $model = $this->_order->with(['client', 'company.address', 'ordersProducts', 'deliveryAddress'])->find($id);

        //$transport_type = $model->transport_type;
        $client  = $model->client;
        $company = $model->company;

        if (empty($company)) {
            return view("errors.popup", ["message" => "Nenhuma empresa foi cadastrada."]);
        }

        $company_name    = $company->fantasy_name;
        $company_address = $model->company->address;

        if (empty($company_address)) {
            return view("errors.popup", ["message" => "A empresa $company_name não tem um endereço cadastrado."]);
        }

        $deliveryAddress = $model->deliveryAddress;

        $client_name = $client->fantasy_name;

        $company_address_Cep          = $company_address->cep;
        $company_address_Street       = $company_address->street;
        $company_address_Number       = $company_address->number;
        $company_address_Complement   = $company_address->complement;
        $company_address_Neighborhood = $company_address->neighborhood;
        $company_address_City         = $company_address->city;
        $company_address_State        = $company_address->state;

        $deliveryAddress_Cep          = $deliveryAddress->cep;
        $deliveryAddress_Street       = $deliveryAddress->street;
        $deliveryAddress_Number       = $deliveryAddress->number;
        $deliveryAddress_Complement   = $deliveryAddress->complement;
        $deliveryAddress_Neighborhood = $deliveryAddress->neighborhood;
        $deliveryAddress_City         = $deliveryAddress->city;
        $deliveryAddress_State        = $deliveryAddress->state;

        $volumes              = $weight;
        $dimension            = [
            'height'   => $height,
            'width'    => $width,
            'length'   => $length,
            'diameter' => 0,
        ];
        $receiver             = [
            'name'       => $client_name,
            'street'     => $deliveryAddress_Street,
            'number'     => $deliveryAddress_Number,
            'complement' => $deliveryAddress_Complement,
        ];
        $national_destination = [
            'neighborhood' => $deliveryAddress_Neighborhood,
            'cep'          => $deliveryAddress_Cep,
            'city'         => $deliveryAddress_City,
            'state'        => $deliveryAddress_State,
        ];
        $sender               = [
            'name'         => $company_name,
            'street'       => $company_address_Street,
            'number'       => $company_address_Number,
            'complement'   => $company_address_Complement,
            'neighborhood' => $company_address_Neighborhood,
            'cep'          => $company_address_Cep,
            'city'         => $company_address_City,
            'state'        => $company_address_State,
        ];

        //$buscaCliente = $soap_client->buscaCliente();
        //$correios_types = CorreiosService::all();
        //dd($transport_type, $correios_types);

        $CorreiosSigep = new CorreiosSigep();

        if ($CorreiosSigep->checkPostingService($transport_type) === false) {
            return view("errors.popup", ['message' => 'Tipo de frete não encontrado, selecione um tipo de frete.']);
        }

        $CorreiosSigep->printLabel($volumes, $dimension, $receiver, $national_destination, $sender, $transport_type, $total_box);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        $client_query_param = (int) $request->get('c');

        $attendant_users = $this->_user->formSelectAttendant();
        $designer_users  = $this->_user->formSelectDesigner();
        $representatives = $this->_representative->formSelect();
        $tables          = $this->_price_table->formSelect();

        $client_id = old('clients_id');

        $model_client = null;
        if ($client_query_param > 0) {
            $model_client = $this->_client->with('lastUsedAddress', 'addresses')->find($client_query_param);
            $client_id    = $model_client->id;
        }

        $model_addresses  = [];
        $model_contacts  = [];
        $featured_address = null;
        if (!empty($client_id)) {
            $client = $this->_client->with('lastUsedAddress', 'addresses', 'contacts')->find($client_id);
            if (isset($client->addresses)) {
                $model_addresses = $client->addresses;
            }

            if (isset($client->lastUsedAddress[0])) {
                $featured_address = [$client->lastUsedAddress[0]->delivery_address_id, $client->lastUsedAddress[0]->billing_address_id];
            }

            if (isset($client->contacts)) {
                $model_contacts = $client->contacts;
            }
        }

        $tables_id       = old('tables_id');
        $tables_selected = [];
        if (!empty($tables_id)) {
            $tables_selected = $this->_price_table->whereIn('id', $tables_id)->pluck('id')->toArray();
        }

        return view(self::MODULE_VIEW . self::FOLDER . 'create', compact('featured_address', 'model_client', 'attendant_users', 'designer_users', 'representatives', 'tables', 'model_addresses', 'model_contacts', 'tables_selected'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //return $this->redirectBackAfterError(trans('messages.error.create'));
    }

    /**
     * Layout da OP com Nome
     * @return Response
     */
    public function indexOpWithName($id)
    {
        $_sizes   = $this->_size->getAll();
        $sizes    = $this->colorsSizes($_sizes);
        $sizes_id = $this->colorsSizes($_sizes, 'name', 'id');
        $op_names = $this->_order_product->with('opWithName')->find($id)->opWithName;

        return view(self::MODULE_VIEW . self::FOLDER . '_product_op_name', compact('id', 'sizes', 'sizes_id', 'op_names'))->render();
    }

    private function colorsSizes($_sizes, $field_i = 'name', $field_v = 'hex_color')
    {
        $sizes = [];
        foreach ($_sizes as $value) {
            $sizes[$value->{$field_i}] = $value->{$field_v};
        }

        return $sizes;
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view(self::MODULE_VIEW . self::FOLDER . 'show');
    }

    /**
     *
     * @return Response
     */
    public function deleteArt($id)
    {
        $storage = Storage::disk('local');

        $order_product = $this->_order_product->find($id);
        $old_file_name = $order_product->image_art;

        if (!empty($old_file_name)) {
            if ($storage->exists($old_file_name)) {
                if ($storage->delete($old_file_name)) {
                    $order_product->image_art = "";
                    $order_product->save();

                    return response()->json([
                                'error'   => false,
                                'message' => "Arte excluída com sucesso."
                    ]);
                }
            } else {
                return response()->json([
                            'error'   => true,
                            'message' => "Nenhuma arte foi encontrada."
                ]);
            }
        }

        return response()->json([
                    'error'   => true,
                    'message' => "Não foi possível excluir a arte."
        ]);
    }

    /**
     *
     * @return Response
     */
    public function saveArt(Request $request)
    {
        $id   = $request->get('id');
        $file = $request->file('image');

        $validator = Validator::make(['image' => $file], ['image' => 'mimes:jpeg,png,jpg,bmp']);
        if ($validator->fails())
        {
            return response()->json(['error' => true, 'message' => $validator->errors()->first()]);
        }

        $storage = Storage::disk('local');

        $extension     = $file->getClientOriginalExtension();
        $new_file_name = "products_arts/" . md5($file->getFilename()) . '.' . $extension;

        $image = Image::make($file->getRealPath());

        $image->rotate(270)->resize(null, 1500, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        if ($storage->put($new_file_name, (string) $image->encode())) {
            $order_product = $this->_order_product->find($id);
            $old_file_name = $order_product->image_art;

            $order_product->image_art = $new_file_name;
            $order_product->save();

            if (!empty($old_file_name)) {
                if ($storage->exists($old_file_name)) {
                    $storage->delete($old_file_name);
                }
            }

            return response()->json([
                        'error'   => false,
                        'message' => "Upload efetuado com sucesso."
            ]);
        } else {
            return response()->json([
                        'error'   => true,
                        'message' => "Não foi possível efetuar o upload."
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function uploadArt($id)
    {
        $file_exists   = false;
        $model         = $this->_order_product->find($id);
        $old_file_name = $model->image_art;

        $storage = Storage::disk('local');

        if (!empty($old_file_name)) {
            $file_exists = $storage->exists($old_file_name);
        }

        return view(self::MODULE_VIEW . self::FOLDER . '_upload_product_art', compact('model', 'file_exists'))->render();
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function viewArt($id)
    {
        $order_product = $this->_order_product->with(['product', 'sizes', 'variations'])->find($id);
        $variations    = [];

        foreach ($order_product->variations as $value) {
            $variations[] = $value->name;
        }

        return view(self::MODULE_VIEW . self::FOLDER . '_print_production_order_art', compact('order_product', 'sizes', 'variations'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $featured_address = null;
        $order_id         = $id;
        $model            = $this->_order->with(['transport', 'client', 'billingClient', 'briefings', 'ordersProducts', 'paymentsInstallments', 'auditing', 'deliveryAddress', 'billingAddress', 'attachedFiles'])->find($id);

        if(empty($model)) return $this->redirectAfterError('Orçamento não encontrado.', 'cerebelo.orders.index');

        $attendant_users = $this->_user->formSelectAttendant();
        $designer_users  = $this->_user->formSelectDesigner();
        $prepress_users  = $this->_user->formSelectPrepress();
        $admin_users     = $this->_user->formSelectAdmin();
        $representatives = $this->_representative->formSelect();
        $tables          = $this->_price_table->formSelect();
        $orders_status   = $this->_order_status->formSelect();
        $companies       = $this->_company->formSelect2();

        $clients_id = $model->client->id;
        $categories = $this->clientCategories($clients_id);

        $products      = $this->_product->formSelect2($categories);
        $variations    = $this->_variation->formSelect2();
        $sizes         = $this->_size->getAll();
        $sizes_select2 = $this->_size->formSelect();

        $transports = $this->_transport->formSelect2();

        $briefings              = $this->_briefing->allOrdinationAsc();
        $correios_types_freight = correiosTypesFreight();

        $payments_forms = $this->_payments_form->formSelect2();

        $presents_partnerships = $this->_present_partnership->formSelect();

        $tables_id = old('tables_id');
        if (!empty($tables_id)) {
            $tables_selected = $this->_price_table->whereIn('id', $tables_id)->pluck('id')->toArray();
        } else {
            $tables_selected = (isset($model->client->priceTables) && !empty($model->client->priceTables)) ? $model->client->priceTables->pluck('id')->toArray() : [];
        }

        $budget_tables_selected = $this->removeDuplicateItems();

        $price_table_id    = "";
        $price_table_value = "";
        $products_index    = "";

        $colors_sizes      = $this->colorsSizes($sizes);
        $products_selected = $this->productSelected($model, $budget_tables_selected, $presents_partnerships, $colors_sizes);
        //dd($products_selected);
        // COTAÇÃO DOLAR
        try {
            $dolar_rate  = Swap::latest('USD/BRL'); // Dolar
            $dolar_value = $dolar_rate->getValue();
            $dolar_date  = $dolar_rate->getDate()->format('d/m/Y');
        } catch (\Exception $ex) {
            $dolar_value = "Não foi possível obter a cotação, tente novamente.";
            $dolar_date  = $ex->getMessage();
        }

        $user_data = auth()->user();
        $waitActions = $user_data->sector->waitActions->pluck('name', 'id');
        $select2 = $this->_sector_action->formSelect2()->toArray();
        //$select2 = $this->actionsForSelect($waitActions);

        $client_id = old('clients_id');
        if (!empty($client_id)) {
            $addresses = $this->_client->with('lastUsedAddress', 'addresses')->find($client_id);
            $model->client = $addresses;
        }

        $script_attendant = "";
        $auth = auth()->user();
        $sector_alias = $auth->sector->alias;
        if ($sector_alias == Sector::ATTENDANCE) {
            $user_id = $auth->id;
            if ($model->attendant_id == $user_id) {
                $script_attendant = '<script type="text/javascript">$("select[name=attendant_id]").val(' . $user_id . ');</script>';
            }
        }

        //dd($products_selected);

        return view(self::MODULE_VIEW . self::FOLDER . 'edit', compact('script_attendant', 'select2', 'waitActions', 'dolar_value', 'dolar_date', 'featured_address', 'companies', 'products_index', 'price_table_value', 'price_table_id', 'orders_status', 'budget_tables_selected', 'model_client', 'order_id', 'model', 'attendant_users', 'designer_users', 'prepress_users', 'admin_users', 'representatives', 'tables', 'products', 'variations', 'sizes', 'sizes_select2', 'transports', 'briefings', 'correios_types_freight', 'payments_forms', 'tables_selected', 'products_selected', 'presents_partnerships'));
    }

    private function removeDuplicateItems()
    {
        $remove_duplicate_items = function ($tables, $tables_selected) {
            $collection = [];
            foreach ($tables as $value) {
                $key = $value['id'];
                foreach ($tables_selected as $value2) {
                    if ($key == $value2['id']) {
                        $collection[] = $value;
                    }
                }
            }

            return collect($collection);
        };
        $tables_select_extra = $this->_price_table->formSelectExtra();

        return $remove_duplicate_items($tables_select_extra, $tables_select_extra);
    }

    private function productSelected($model, $budget_tables_selected, $presents_partnerships, $colors_sizes = [])
    {
        $products_selected = [];

        $price_table_id    = "";
        $price_table_value = "";
        $products_index    = "";

        $check_product = [];

        $sizes = $this->_size->getAll();

        foreach ($model->ordersProducts as $key => $value) {
            $autoIndex = autoIndex();

            $weight                   = moneyUnFormat($value->weight);
            $sizes_code               = $value->sizes_code;
            $quantity                 = $value->pivot->quantity;
            $have_sizes               = $value->pivot->have_sizes;
            $quantity_piece           = $value->pivot->quantity_piece;
            $size_piece               = $value->pivot->size_piece;
            $name_piece               = $value->pivot->name_piece;
            $unit_price               = $value->pivot->unit_price;
            $image_art                = $value->pivot->image_art;
            $extra_variation          = $value->pivot->extra_variation;
            $price_table_id           = $value->pivot->price_table_id;
            $price_table_value        = $value->pivot->price_table_value;
            $presents_partnerships_id = $value->pivot->presents_partnerships_id;
            $orders_products_id       = $value->pivot->id;

            $order_product = OrderProduct::with(['sizes', 'variations', 'opWithName'])->find($orders_products_id);

            if(empty($order_product))
            {
                continue;
            }

            $product                 = $this->_product->find($value->id)->productsPrices()->where('price_table_id', $price_table_id)->first();
            $_budget_tables_selected = $budget_tables_selected->map(function ($item, $key) use ($product, $price_table_id) {
                $id         = $item['id'];
                $percentage = (isset($product->pivot->value) && !empty($product->pivot->value)) ? $product->pivot->value : null;
                if ($price_table_id == $id && !empty($percentage)) {
                    $item['percentage'] = $percentage;
                    return $item;
                }
                return $item;
            });

            $products_selected['orders_products_id'][$autoIndex]       = $orders_products_id;
            $products_selected['products_name'][$autoIndex]            = $value->name;
            $products_selected['products_index'][$autoIndex]           = $autoIndex;
            $products_selected['products_id'][$autoIndex]              = $value->id;
            $products_selected['weight'][$autoIndex]                   = $weight;
            $products_selected['sizes_code'][$autoIndex]               = $sizes_code;
            $products_selected['quantity'][$autoIndex]                 = $quantity;
            $products_selected['have_sizes'][$autoIndex]               = $have_sizes;
            $products_selected['quantity_piece'][$autoIndex]           = $quantity_piece;
            $products_selected['size_piece'][$autoIndex]               = $size_piece;
            $products_selected['name_piece'][$autoIndex]               = $name_piece;
            $products_selected['unit_price'][$autoIndex]               = $unit_price;
            $products_selected['image_art'][$autoIndex]                = $image_art;
            $products_selected['extra_variation'][$autoIndex]          = $extra_variation;
            $products_selected['custom_price_table_id'][$autoIndex]    = $price_table_id;
            $products_selected['price_table_value'][$autoIndex]        = $price_table_value;
            $products_selected['presents_partnerships_id'][$autoIndex] = $presents_partnerships_id;
            $products_selected['present_partnership'][$autoIndex]      = $presents_partnerships[$presents_partnerships_id];
            $products_selected['volume'][$autoIndex]                   = $weight * $quantity;
            $products_selected['_budget_tables_selected'][$autoIndex]  = $_budget_tables_selected;

            //$subtotal_card = 0;
            // montar variações
            foreach ($order_product->variations as $variation) {
                $products_selected['variations_pivot_id'][$autoIndex][$variation->id]       = $variation->pivot->id;
                $products_selected['variations_pivot_position'][$autoIndex][$variation->id] = $variation->pivot->position;
                $products_selected['variations_id'][$autoIndex][$variation->id]             = $variation->id;
                $products_selected['variations_total'][$autoIndex][$variation->id]          = $variation->value;
                $products_selected['variations_name'][$autoIndex][$variation->id]           = $variation->name;

                //$subtotal_card += moneyUnFormat($variation->value);
            }

            // calcular subtotal do card
            if ($presents_partnerships_id > 0 && $presents_partnerships_id != 3) {
                $products_selected['subtotal'][$autoIndex] = moneyFormat(0);
            } else {
                $products_selected['subtotal'][$autoIndex] = moneyFormat($unit_price * $quantity);
            }

            $order_product_sizes = $order_product->sizes;

            // montar tamanhos
            foreach ($order_product_sizes as $size) {
                $products_selected['sizes'][$autoIndex][$size->id]      = $size->pivot->quantity;
                $products_selected['sizes_name'][$autoIndex][$size->id] = $size->name;
            }
            // completar tamanhos não existentes
            foreach($sizes as $size):
                if(!isset($products_selected['sizes'][$autoIndex][$size->id])):
                    $products_selected['sizes'][$autoIndex][$size->id]      = 0;
                    $products_selected['sizes_name'][$autoIndex][$size->id] = $size->name;
                endif;
            endforeach;

            if(isset($order_product->opWithName) && count($order_product->opWithName) > 0)
            {
                // Zerar as quantidades
                foreach ($order_product_sizes as $size) {
                    $products_selected['sizes'][$autoIndex][$size->id] = 0;
                }

                foreach ($order_product->opWithName as $size) {
                    // OP COM NOME - Para a Aba de Orçamento Cards de produtos
                    $products_selected['op_name'][$autoIndex][$size->size][]     = $size->name;
                    $products_selected['op_quantity'][$autoIndex][$size->size][] = $size->quantity;
                    $products_selected['op_size'][$autoIndex][$size->size][]     = $size->size;
                    $products_selected['op_color'][$autoIndex][$size->size]      = $colors_sizes[$size->size];
                    // OP COM NOME - Para a Aba de Ordem de Produção
                    $products_selected['op_name_tab6'][$autoIndex][$size->size][]     = $size->name;
                    $products_selected['op_quantity_tab6'][$autoIndex][$size->size][] = $size->quantity;
                    $products_selected['op_size_tab6'][$autoIndex][$size->size][]     = $size->size;
                    $products_selected['op_color_tab6'][$autoIndex][$size->size]      = $colors_sizes[$size->size];

                    // Redefinir as quantidades dos tamanhos da OP com Nome
                    $flipped = array_flip($products_selected['sizes_name'][$autoIndex]);
                    $array_get = array_get($flipped, $size->size);
                    $products_selected['sizes'][$autoIndex][$array_get] += $size->quantity;
                }
            }

            if(!empty($size_piece) && $size_piece > 0 && !empty($quantity_piece) && $quantity_piece > 0)
            {
                $find_str_size = $sizes->where('id', $size_piece)->first()->name;
                $index = count($products_selected['op_name'][$autoIndex][$find_str_size]);
                $products_selected['op_client_name'][$autoIndex][$find_str_size][$index]     = $name_piece;
                $products_selected['op_client_quantity'][$autoIndex][$find_str_size][$index] = $quantity_piece;
                $products_selected['op_client_size'][$autoIndex][$find_str_size][$index]     = $find_str_size;
                $products_selected['op_client_color'][$autoIndex][$find_str_size]            = $colors_sizes[$find_str_size];
            }

            $extra_variation = strtolower(trim($extra_variation)); // Infantil | Modelo 1.1 | Modelo 2.1 | ...
            if ((str_contains($extra_variation, 'infantil') || ($presents_partnerships_id > 0 && !empty($presents_partnerships_id))) && end($check_product) == $value->id) {
                $products_selected['join_op_art'][$value->id][str_slug($extra_variation)][] = $autoIndex;
            }
            array_push($check_product, $value->id);
        }

        if (isset($products_selected['join_op_art'])) {
            //$sizes = $this->_size->getAll();
            $products_id = $products_selected['products_id'];
            $__array = $products_selected['products_index'];

            foreach ($products_selected['join_op_art'] as $key => $value) {
                foreach ($value as $ky => $val) {
                    foreach ($val as $k => $v) {
                        // Inicio - Pega o indice anterior que é o produto principal
                        $__extra_variation = $this->groupingExtraVariation($products_selected['extra_variation'], $v); // Pega o produto pai da variação extra agrupada (variação igual)
                        if($k == 0) {
                            $__keys = array_flip(array_keys($__array));
                            $__values = array_values($__array);
                            $__prev_products_index = $__values[$__keys[$v]-1];
                        }

                        // Pega o produto pai da variação extra agrupada (variação igual)
                        if($__extra_variation > 0)
                        {
                            $__prev_products_index = $__extra_variation;
                        }
                        // Fim - Pega o indice anterior que é o produto principal

                        $extra_variation          = $products_selected['extra_variation'][$v]; // Se é Infanil
                        $presents_partnerships_id = $products_selected['presents_partnerships_id'][$v]; // Se é Brinde, Parceria, Sem Nome
                        $quantity_piece           = $products_selected['quantity_piece'][$v];
                        $quantity                 = $products_selected['quantity'][$v];
                        $size_piece               = $products_selected['size_piece'][$v];
                        $name_piece               = $products_selected['name_piece'][$v];
                        $products_name            = $products_selected['products_name'][$v];
                        $have_sizes               = $products_selected['have_sizes'][$v];

                        $extra_variation = strtolower(trim($extra_variation)); // Infantil | Modelo 1.1 | Modelo 2.1 | ...
                        if (str_contains($extra_variation, 'infantil')) {
                            $products_selected['infantil'][$key][$__prev_products_index]['original_index'] = $v;
                            $products_selected['infantil'][$key][$__prev_products_index]['product'] = $products_name;
                            $products_selected['infantil'][$key][$__prev_products_index]['quantity'] += $quantity;
                            foreach ($sizes as $size) {
                                $_size = $products_selected['sizes'][$v][$size->id];
                                $products_selected['infantil'][$key][$__prev_products_index]['size'][$size->id] += $_size + (($size_piece == $size->id) ? $quantity_piece : 0);
                                $products_selected['infantil'][$key][$__prev_products_index]['ert_qtd_piece'][$size->id] += (($size_piece == $size->id) ? $quantity_piece : 0);
                            }
                        } elseif ($presents_partnerships_id == 3) { // Sem Nome
                            $products_selected['sem_nome'][$key][$__prev_products_index]['original_index'] = $v;
                            $products_selected['sem_nome'][$key][$__prev_products_index]['product'] = $products_name;
                            $products_selected['sem_nome'][$key][$__prev_products_index]['quantity'] += $quantity;
                            foreach ($sizes as $size) {
                                $_size = $products_selected['sizes'][$v][$size->id];
                                $_with_size = $_size + (($size_piece == $size->id) ? $quantity_piece : 0);

                                $products_selected['sem_nome'][$key][$__prev_products_index]['size'][$size->id] += $_with_size;
                                $products_selected['sem_nome'][$key][$__prev_products_index]['ert_qtd_piece'][$size->id] += (($size_piece == $size->id) ? $quantity_piece : 0);

                                if($_with_size > 0)
                                //if($_with_size > 0 && isset($products_selected['op_name_tab6'][$__prev_products_index][$size->name]))
                                {
                                    $index = count($products_selected['op_name_tab6'][$__prev_products_index][$size->name]);
                                    $products_selected['op_name_tab6'][$__prev_products_index][$size->name][$index]     = "Sem Nome";
                                    $products_selected['op_quantity_tab6'][$__prev_products_index][$size->name][$index] = $_with_size;
                                    $products_selected['op_size_tab6'][$__prev_products_index][$size->name][$index]     = $size->name;
                                    $products_selected['op_color_tab6'][$__prev_products_index][$size->name]            = $colors_sizes[$size->name];
                                }
                            }
                        } elseif ($presents_partnerships_id == 1) { // Brinde
                            $products_selected['brinde'][$key][$__prev_products_index]['original_index'] = $v;
                            $products_selected['brinde'][$key][$__prev_products_index]['product'] = $products_name;
                            $products_selected['brinde'][$key][$__prev_products_index]['quantity'] += $quantity;
                            $brinde_have_name = false;
                            foreach ($sizes as $size) {
                                $_size = $products_selected['sizes'][$v][$size->id];
                                $_with_size = $_size + (($size_piece == $size->id) ? $quantity_piece : 0);

                                $products_selected['brinde'][$key][$__prev_products_index]['size'][$size->id] += $_with_size;
                                $products_selected['brinde'][$key][$__prev_products_index]['ert_qtd_piece'][$size->id] += (($size_piece == $size->id) ? $quantity_piece : 0);

                                if($_with_size > 0 && isset($products_selected['op_name_tab6'][$v][$size->name]))
                                {
                                    $brinde_have_name = true;
                                    $this->opNameExtras($products_selected, $__prev_products_index, $v, $size);
                                }
                            }

                            if($brinde_have_name === true)
                            {
                                //unset($products_selected['brinde'][$key][$__prev_products_index]);
                            }
                        } elseif ($presents_partnerships_id == 2) { // Parceria
                            $products_selected['parceria'][$key][$__prev_products_index]['original_index'] = $v;
                            $products_selected['parceria'][$key][$__prev_products_index]['product'] = $products_name;
                            $products_selected['parceria'][$key][$__prev_products_index]['quantity'] += $quantity;
                            $parceria_have_name = false;
                            foreach ($sizes as $size) {
                                $_size = $products_selected['sizes'][$v][$size->id];
                                $_with_size = $_size + (($size_piece == $size->id) ? $quantity_piece : 0);

                                $products_selected['parceria'][$key][$__prev_products_index]['size'][$size->id] += $_with_size;
                                $products_selected['parceria'][$key][$__prev_products_index]['ert_qtd_piece'][$size->id] += (($size_piece == $size->id) ? $quantity_piece : 0);

                                if($_with_size > 0 && isset($products_selected['op_name_tab6'][$v][$size->name]))
                                {
                                    $parceria_have_name = true;
                                    $this->opNameExtras($products_selected, $__prev_products_index, $v, $size);
                                }
                            }

                            if($parceria_have_name === true)
                            {
                                //unset($products_selected['parceria'][$key][$__prev_products_index]);
                            }
                        }
                    }
                }
            }
        }

        $this->traitMountLayout($products_selected);

        if(auth()->user()->id == 1 && request()->has('emi-debug'))
        {
            dd($products_selected);
        }

        return $products_selected;
    }

    private function opNameExtras(&$products_selected, $__prev_products_index, $v, $size) {
        $prev_op_name_tab6 = $products_selected['op_name_tab6'][$__prev_products_index][$size->name];
        $orig_op_name_tab6 = $products_selected['op_name_tab6'][$v][$size->name];
        $prev_op_quantity_tab6 = $products_selected['op_quantity_tab6'][$__prev_products_index][$size->name];
        $orig_op_quantity_tab6 = $products_selected['op_quantity_tab6'][$v][$size->name];
        $prev_op_size_tab6 = $products_selected['op_size_tab6'][$__prev_products_index][$size->name];
        $orig_op_size_tab6 = $products_selected['op_size_tab6'][$v][$size->name];
        $prev_op_color_tab6 = $products_selected['op_color_tab6'][$__prev_products_index][$size->name];
        $orig_op_color_tab6 = $products_selected['op_color_tab6'][$v][$size->name];

        if(is_array($prev_op_name_tab6) && is_array($orig_op_name_tab6))
            $products_selected['op_name_tab6'][$__prev_products_index][$size->name] = array_merge($prev_op_name_tab6, $orig_op_name_tab6);

        if(is_array($prev_op_quantity_tab6) && is_array($orig_op_quantity_tab6))
            $products_selected['op_quantity_tab6'][$__prev_products_index][$size->name] = array_merge($prev_op_quantity_tab6, $orig_op_quantity_tab6);

        if(is_array($prev_op_size_tab6) && is_array($orig_op_size_tab6))
            $products_selected['op_size_tab6'][$__prev_products_index][$size->name] = array_merge($prev_op_size_tab6, $orig_op_size_tab6);

        if(is_array($prev_op_color_tab6) && is_array($orig_op_color_tab6))
            $products_selected['op_color_tab6'][$__prev_products_index][$size->name] = array_merge($prev_op_color_tab6, $orig_op_color_tab6);
    }

    private function groupingExtraVariation($extra_variation, $index) {
        if(!array_key_exists($index, $extra_variation))
        {
            return 0;
        }

        $keys = array_flip(array_keys($extra_variation));
        $values = array_values($extra_variation);
        $index = ($keys[$index] - 1);
        $keys = array_flip($keys);

        return $keys[$index];

        /*$value = $extra_variation[$index];
        $collection = array_filter($extra_variation);

        foreach($collection as $key => $val)
        {
            if(strtolower(trim($val)) == strtolower(trim($value)))
            {
                $value = $key;
                break;
            }
        }

        return $value;*/
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $attributes = $request->all();

        $order = $this->_order->find($id);

        $order->fill($attributes);

        if ($order->save()) {
            return response()->json([
                        'error'   => false,
                        'message' => trans('messages.success.update')
            ]);
        } else {
            return response()->json([
                        'error'   => true,
                        'message' => trans('messages.error.update')
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->_order->destroy($id)) {
            return $this->redirectBackAfterSuccess(trans('messages.success.destroy'));
        }

        return $this->redirectBackAfterError(trans('messages.error.destroy'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroyProduct($id)
    {
        if ($this->_order_product->destroy($id)) {
            return response()->json([
                        'error'   => false,
                        'message' => trans('messages.success.destroy')
            ]);
        }

        return response()->json([
                    'error'   => true,
                    'message' => trans('messages.error.destroy')
        ]);
    }

    /**
     * Ação Esperada
     * @return Response
     */
    public function sectorAction(Request $request, $id)
    {
        $action_id = $request->get('action_id');

        $action = $this->_sector_action->find($action_id);
        $alias = $action->alias;
        $type = $action->type;

        $model = $this->_order->find($id);

        if($type == 'send_to') { // Enviar para usuário/setor
            if($alias == 'admin') { // usuário do setor Administrador
                //$admin_user = 28; // Usuário do Fabricio (28)
                $sector_admin = $this->_sector->withAlias(Sector::ADMIN)->first();
                if(empty($sector_admin->responsible_id))
                {
                    return response()->json([
                        'error'   => true,
                        'message' => 'Não foi definido um responsável para o setor Administrador.'
                    ]);
                }

                $admin_user = $sector_admin->responsible_id;

                $user = $this->_user->with('sector')->find($admin_user);
                if (isset($user->sector->period))
                {
                    $period        = $user->sector->period;
                    $period_option = $user->sector->period_option;

                    $model->sectors()->attach([$admin_user => calculatePeriodPerSector($period_option, $period)]);

                    // Criar Alerta
                    $users_name = auth()->user()->name;
                    $content = "AGENDAMENTO: $users_name enviou o pedido <span class='text-bold'>$model->order_number</span> para sua análise, clique <a href='" . route('cerebelo.orders.edit', $id) . '#justified-badges-tab2' . "'>aqui</a>.";
                    Event::fire(new AlertSysEvent($admin_user, $content));
                }
            } else if(in_array($alias, ['atendente', 'designer'])) { // usuário dos setores Atendimento/Designer
                if($alias == 'atendente')
                {
                    $user = $model->client->attendant;
                }
                else
                {
                    $user = $model->client->designer;
                }

                if (isset($user->sector->period))
                {
                    $user_id       = $user->id;
                    $period        = $user->sector->period;
                    $period_option = $user->sector->period_option;

                    $model->sectors()->attach([$user_id => calculatePeriodPerSector($period_option, $period)]);

                    // Criar Alerta
                    $users_name = auth()->user()->name;
                    $client_name = $model->client->name;
                    $material = empty($model->group) ? "" : ", material " . $model->group;
                    $content = "$users_name enviou o pedido nº <span class='text-bold'>$model->order_number</span>$material do cliente <span class='text-bold'>$client_name</span> com a arte aprovada, clique <a href='" . route('cerebelo.orders.edit', $id) . '#justified-badges-tab2' . "'>aqui</a> para ver o pedido.";
                    Event::fire(new AlertSysEvent($user_id, $content));
                }
            }
        }

        $model->sectors_actions_id = $action_id;

        if($model->save()) {
            $latestSector = $model->latestSector()->first();
            $pivotIdLatestSector = $latestSector->pivot->id;
            $model->actionsHistory()->create(['sectors_actions_id' => $action_id, 'orders__sectors_id' => $pivotIdLatestSector]);

            return response()->json([
                'error'   => false,
                'message' => trans('messages.success.update')
            ]);
        }

        return response()->json([
            'error'   => true,
            'message' => trans('messages.error.update')
        ]);
    }

    /**
     * Enviar para Setor
     * @return Response
     */
    public function lineUp(Request $request)
    {
        $type      = $request->get('type');
        $order_id  = $request->get('order_id');
        $sector_id = $request->get('sector_id');
        $obs       = $request->get('obs');
        $reject    = $request->get('reject', false);

        $logged_user_sector = auth()->user()->sector->alias;

        if ($type == "modal") {
            $sectors = $this->_sector->allLineUp();

            return view(self::MODULE_VIEW . self::FOLDER . '_wait_action', compact('sectors', 'sector_id', 'order_id'))->render();
        } elseif ($reject == true) {
            $isUserAtualSector = $this->_order->isUserAtualSector($order_id, $logged_user_sector);

            if($isUserAtualSector === true) {
                try {
                    $id = $request->get('id');

                    $users_name = auth()->user()->name;
                    $result = current(DB::select(
                        DB::raw("SELECT IF(c.company_name = '', c.fantasy_name, c.company_name) AS client_name, o.group, o.order_number, r.prefix, (SELECT os2.users_id FROM orders__sectors os2 WHERE os2.orders_id = os.orders_id ORDER BY os2.id DESC LIMIT 1, 1) AS previous_user_id
                                FROM orders__sectors os
                                JOIN orders o ON o.id = os.orders_id
                                JOIN clients c ON c.id = o.clients_id
                                JOIN representatives r ON r.id = c.representative_id
                                WHERE os.id = $id")
                    ));
                    $prefix = "";
                    try
                    {
                        if(!empty($result->prefix) && !empty($result->order_number))
                            $prefix = $result->prefix . "";
                    }
                    catch(\Exception $ex)
                    {}

                    $order_number = $prefix . '.' . last(explode('.', $result->order_number));
                    $material = empty($result->group) ? "" : ", material " . $result->group;
                    $client_name = $result->client_name;
                    $users_id = $result->previous_user_id;

                    $user = $this->_user->with('sector')->find($users_id);
                    if (isset($user->sector->period))
                    {
                        $period        = $user->sector->period;
                        $period_option = $user->sector->period_option;

                        $model = $this->_order->find($order_id);

                        $model->sectors()->attach([$users_id => calculatePeriodPerSector($period_option, $period)]);
                    }

                    $content = "<i class='icon-warning22 text-warning-300'></i> $users_name recusou o seu pedido nº <span class='text-bold'><a href='" . route('cerebelo.orders.edit', $order_id) . '#justified-badges-tab2' . "'>$order_number</a></span>$material do cliente <span class='text-bold'>$client_name</span>.";
                    if(!empty($obs)) {
                        $content .= "<br/>$obs";
                    }
                    Event::fire(new AlertSysEvent($users_id, $content));

                    // Excluir atual usuário
                    //DB::table('orders__sectors')->where('id', $id)->delete();
                    // Alterando o Status do atual setor/usuário para Rejeitado (R)
                    DB::table('orders__sectors')->where('id', $id)->update(['confirmed' => 'R', 'updated_at' => date('Y-m-d H:i:s')]);
                } catch(Exception $ex) {
                    \Log::error("Erros - Alert Sys: lineUp() > type == 'reject' :: " . $ex);
                }

                return response()->json([
                    'error'   => false,
                    'message' => "Pedido recusado com sucesso e reenviado para o responsável."
                ]);

            } else {
                return response()->json([
                            'error'   => true,
                            'message' => "Pemissão negada, somente usuários do mesmo setor podem recusar o pedido."
                ]);
            }
        } elseif ($type == "accept" && $reject == false) {
            $isUserAtualSector = $this->_order->isUserAtualSector($order_id, $logged_user_sector);

            if($isUserAtualSector === true) {
                $id = $request->get('id');

                $result = DB::table('orders__sectors')->where('id', $id)->update(['confirmed' => 'Y', 'updated_at' => date('Y-m-d H:i:s')]);

                //if ($result) {
                    try {
                        $users_name = auth()->user()->name;
                        $result = current(DB::select(
                            DB::raw("SELECT IF(c.company_name = '', c.fantasy_name, c.company_name) AS client_name, o.group, o.order_number, r.prefix, (SELECT os2.users_id FROM orders__sectors os2 WHERE os2.orders_id = os.orders_id ORDER BY os2.id DESC LIMIT 1, 1) AS designer_id
                                    FROM orders__sectors os
                                    JOIN orders o ON o.id = os.orders_id
                                    JOIN clients c ON c.id = o.clients_id
                                    JOIN representatives r ON r.id = c.representative_id
                                    WHERE os.id = $id")
                        ));

                        $prefix = "";
                        try
                        {
                            if(!empty($result->prefix) && !empty($result->order_number))
                                $prefix = $result->prefix . "";
                        }
                        catch(\Exception $ex)
                        {}
                        $order_number = $prefix . '.' . last(explode('.', $result->order_number));
                        $material = empty($result->group) ? "" : ", material " . $result->group;
                        $client_name = $result->client_name;
                        $users_id = $result->designer_id;
                        $content = "$users_name confirmou o recebimento do pedido nº <span class='text-bold'>$order_number</span>$material do cliente <span class='text-bold'>$client_name</span>.";
                        if(!empty($obs)) {
                            $content .= "<br/>$obs";
                        }
                        Event::fire(new AlertSysEvent($users_id, $content));
                    } catch(Exception $ex) {
                        \Log::error("Erros - Alert Sys: lineUp() > type == 'accept' :: " . $ex);
                    }

                    return response()->json([
                                'error'   => false,
                                'message' => "Você confirmou o recebimento do pedido com sucesso."
                    ]);
                /*} else {
                    return response()->json([
                                'error'   => true,
                                'message' => "Não foi possível confirmar o recebimento do pedido."
                    ]);
                }*/
            } else {
                return response()->json([
                            'error'   => true,
                            'message' => "Pemissão negada, somente usuários do mesmo setor podem aceitar o pedido."
                ]);
            }
        } elseif ($type == "next") {
            $isUserAtualSector = $this->_order->isUserAtualSector($order_id, $logged_user_sector);

            if($isUserAtualSector === true) {
                // Próximo Setor
                $next_sector_id     = $request->get('next_sector_id');
                $next_sector        = $this->_sector->with('users')->find($next_sector_id);

                $next_period_option = $next_sector->period_option;
                $next_period        = $next_sector->period;
                $next_sector_alias  = $next_sector->alias;
                $next_sector_responsible = $next_sector->responsible_id;

                if (empty($next_period_option) || empty($next_period)) {
                    return response()->json([
                                'error'   => true,
                                'message' => "Você precisa definir o tempo do pedido em cada setor no cadastro de Setores."
                    ]);
                }

                // Setor atual
                $sector               = $this->_sector->find($sector_id);
                $current_sector_alias = $sector->alias;

                $model = $this->_order->with('client', 'client.attendant', 'client.designer', 'ordersProducts', 'paymentsInstallments')->find($order_id);

                $rulesToSendNextSector = $this->rulesToSendNextSector($model, $current_sector_alias, $next_sector_alias);
                if ($rulesToSendNextSector !== false) {
                    return response()->json([
                                'error'   => true,
                                'message' => $rulesToSendNextSector
                    ]);
                }

                $client_attendant_id         = $model->client->attendant->id;
                $client_attendant_sectors_id = $model->client->attendant->sectors_id;

                $client_designer_id         = $model->client->designer->id;
                $client_designer_sectors_id = $model->client->designer->sectors_id;

                if ($next_sector_id == $client_attendant_sectors_id) {
                    $users_id = $client_attendant_id;
                } elseif ($next_sector_id == $client_designer_sectors_id) {
                    $users_id = $client_designer_id;
                } else {
                    if(empty($next_sector_responsible))
                    {
                        return response()->json([
                            'error'   => true,
                            'message' => "Não foi definido um responsável para o setor (em Cerebelo > Setores)."
                        ]);
                    }

                    //$sector_unique_users_id = $next_sector->users->first();
                    //$users_id = $sector_unique_users_id->id;

                    $users_id = $next_sector_responsible;
                }

                if (empty($users_id)) {
                    return response()->json([
                                'error'   => true,
                                'message' => "Não identificamos usuário nesse setor, favor cadastrar um usuário para prosseguir."
                    ]);
                }

                $carbon_now      = Carbon::now();
                $start_datetime  = $carbon_now->format('Y-m-d H:i:s');
                $carbon_now_copy = $carbon_now->copy();

                // 'M' => 'Minutos', 'H' => 'Horas', 'D' => 'Dias', 'W' => 'Semanas'
                if ($next_period_option == "M") {
                    $end_datetime = $carbon_now_copy->addMinutes($next_period)->format('Y-m-d H:i:s');
                } elseif ($next_period_option == "H") {
                    $end_datetime = $carbon_now_copy->addHours($next_period)->format('Y-m-d H:i:s');
                } elseif ($next_period_option == "D") {
                    $end_datetime = $carbon_now_copy->addDays($next_period)->format('Y-m-d H:i:s');
                } elseif ($next_period_option == "W") {
                    $end_datetime = $carbon_now_copy->addWeeks($next_period)->format('Y-m-d H:i:s');
                }

                $attrs = [
                    'start_datetime' => $start_datetime,
                    'end_datetime'   => $end_datetime,
                    'confirmed'      => 'N',
                ];

                $model->sectors()->attach([$users_id => $attrs]);

                // Criar Alerta
                try {
                    $user_auth = auth()->user();
                    $users_name = $user_auth->name;
                    $client_name = $model->client->name;
                    $material = empty($model->group) ? "" : ", material " . $model->group;

                    $content = "$users_name enviou o pedido nº <span class='text-bold'>$model->order_number</span>$material do cliente <span class='text-bold'>$client_name</span> para você.";
                    if(!empty($obs)) {
                        $content .= "<br/>$obs<br/>";
                    }

                    $latestSector = $model->latestSector()->first();
                    $pivotIdLatestSector = $latestSector->pivot->id;
                    $content .= " <a q='" . $model->order_number . "' onclick='btnReceiveOrderFromAlert(this);return false;' href='" . route('cerebelo.' . self::FOLDER . 'line_up', ['type' => 'accept', 'id' => $pivotIdLatestSector]) . "'>receber pedido</a>.";
                    Event::fire(new AlertSysEvent($users_id, $content));

                    // Envia email para o Cliente se o campo de mensagem do setor for preenchido
                    $sendMailOnProductionSteps = sendMailOnProductionSteps($model, $next_sector, $end_datetime);
                    if($sendMailOnProductionSteps === true) {
                        return response()->json([
                            'error'   => false,
                            'message' => "Você enviou o pedido com sucesso."."<br/>"."O Cliente foi notificado via email."
                        ]);
                    } elseif($sendMailOnProductionSteps === false) {
                        return response()->json([
                            'error'   => false,
                            'message' => "Você enviou o pedido com sucesso, mas, não foi possível notificar o Cliente via email."
                        ]);
                    }
                } catch(Exception $ex) {
                    \Log::error("Erros - Alert Sys: lineUp() > type == 'next' :: " . $ex->getMessage());
                }

                return response()->json([
                            'error'   => false,
                            'message' => "Você enviou o pedido com sucesso."
                ]);
            } else {
                return response()->json([
                            'error'   => true,
                            'message' => "Pemissão negada, somente usuários do mesmo setor podem enviar o pedido."
                ]);
            }
        } else {
            return response()->json([
                        'error'   => true,
                        'message' => "Ação Esperada não definida."
            ]);
        }
    }

    /*
     * Regras para enviar ao próximo setor
     * 
     * @var $order Order
     * @var $current_sector string
     * @var $next_sector string
     * 
     * @return Boolean
     */

    private function rulesToSendNextSector($order, $current_sector, $next_sector)
    {
        $flag           = false;
        $ordersProducts = $order->ordersProducts;
        $storage        = Storage::disk('local');

        // atendente para designer
        if (count($ordersProducts) == 0 && $current_sector == Sector::ATTENDANCE && $next_sector == Sector::DESIGNER) {
            return "Você deve preencher os dados do Cliente e cadastrar o Orçamento.";
        }
        // designer para atendente
        if ($current_sector == Sector::DESIGNER && $next_sector == Sector::ATTENDANCE) {
            foreach ($ordersProducts as $value) {
                $image_art                = $value->pivot->image_art;
                $presents_partnerships_id = $value->pivot->presents_partnerships_id;
                \Log::debug("Próximo Setor [rulesToSendNextSector()]: Designer > Atendimento: " . ($image_art . "|" . $presents_partnerships_id . '|' . empty($image_art) . '|' . $storage->exists($image_art)));

                if ($presents_partnerships_id == 0 && empty($image_art)) {
                    $flag = "Você deve upar as artes antes de enviar para o próximo setor.";
                    break;
                }
            }
        }
        // atentende para produção
        if ($current_sector == Sector::ATTENDANCE && $next_sector == Sector::PRODUCTION) {
            // Tamanhos
            foreach ($ordersProducts as $value) {
                $order_product = OrderProduct::with(['sizes'])->find($value->pivot->id);
                $sizes         = $order_product->sizes;

                if (empty($sizes) || count($sizes) == 0) {
                    $flag = "Você precisa definir todos os tamanhos para prosseguir.";
                    break;
                }

                $product_quantity = $order_product->quantity;
                $sum_pivot        = 0;
                foreach ($sizes as $size) {
                    $sum_pivot += $size->pivot->quantity;
                }

                if ($product_quantity != $sum_pivot) {
                    $flag = "Você precisa definir todos os tamanhos para prosseguir.";
                    break;
                }
            }

            if ($flag === false) {
                // Pagamentos
                $paymentsInstallments = $order->paymentsInstallments;
                if (count($paymentsInstallments) == 0) {
                    return "Você precisa definir a forma de pagamento para prosseguir.";
                }

                $at_least_one_payment = false;
                foreach ($paymentsInstallments as $value) {
                    $status       = trim($value->status);
                    $payment_form = trim($value->payment_form);

                    if ($status == "PG" && $payment_form != "CHQ") {
                        $at_least_one_payment = true;
                    }
                }

                if ($at_least_one_payment === false) {
                    return "Pelo menos uma parcela deve estar paga para prosseguir.";
                }
            }
        }
        // produção para financeiro
        if ($current_sector == Sector::PRODUCTION && $next_sector == Sector::FINANCIAL) {
            // pagamento completo
            $paymentsInstallments = $order->paymentsInstallments;
            if (count($paymentsInstallments) == 0) {
                return "Você precisa definir a forma de pagamento para prosseguir.";
            }

            $all_paid = true;
            foreach ($paymentsInstallments as $value) {
                $status = trim($value->status);
                if ($status != "PG") {
                    $all_paid = false;
                }
            }

            if ($all_paid === false) {
                return "Todas as parcelas deve estar pagas para prosseguir.";
            }
            // pré envio feito
            $pre_shipment_name = $order->pre_shipment_name;
            $pre_shipment_date = $order->pre_shipment_date;
            $pre_shipment_hour = $order->pre_shipment_hour;
            $pre_shipment_form = $order->pre_shipment_form;
            if (empty($pre_shipment_name) || empty($pre_shipment_date) || empty($pre_shipment_hour) || empty($pre_shipment_form))
            {
                return "Todos os dados de PRE ENVIO devem ser preenchidos para prosseguir.";
            }
        }

        return $flag;
    }

    /**
     * Lista dos Pedidos
     * @return Response
     */
    private function actionsForSelect($waitActions)
    {
        $auth_user = auth()->user();
        $alias = $auth_user->sector->alias;
        $sectors_actions = $this->_sector_action->formSelect2()->toArray();

        $select2 = array_map(function($v) use($waitActions, $alias) {
            $search = $waitActions->search(function ($item, $key) use($v) {
                return $key == $v['id'];
            });
            $disabled = ($search > 0 || trim($alias) == 'admin') ? [] : ['disabled' => 'disabled'];

            return $v + $disabled;
        }, array_values($sectors_actions));

        return $select2;
    }

    /**
     * Lista dos Pedidos
     * @return Response
     */
    public function datatable(Request $request)
    {
        $filters = $request->all();

        //$user_data = auth()->user();
        //$waitActions = $user_data->sector->waitActions->pluck('name', 'id');
        $select2 = $this->_sector_action->formSelect2()->toArray();
        //$select2 = $this->actionsForSelect($waitActions);

        return Datatables::of($this->_order->datatable($filters))
                        ->addColumn('infoOrder', function ($object) {
                            $urlEdit = "";
                            if (!empty($object->have_join)) {
                                $explode = explode('-', $object->have_join);
                                $object_id = $explode[0];
                                $object_orders_id = $explode[1];
                                $urlEdit = route('cerebelo.orders.join_edit', $object_id) . '?orders_id=' . $object_orders_id;
                            }
                            $client_name = (isset($object->client->company_name)) ? (empty($object->client->company_name) ? $object->client->fantasy_name : $object->client->company_name) : "";
                            $status_mark = ($object->urgent == 'Y') ? '<span title="Urgente" data-popup="tooltip" data-original-title="Urgente" class="status-mark border-danger position-left"></span>' : '<span class="status-mark border-blue position-left"></span>';
                            $have_join = empty($object->have_join) ? '' : '<a onclick="popUpWin(\'' . $urlEdit . '\', \'900\', \'1000\');return false;" title="" data-popup="tooltip" data-original-title="Visualizar Juntados"><span class="status-mark border-violet-800 position-left"></span></a>';
                            return '<div class="media-left">' .
                                    '  <div><a class="text-default text-semibold" href="' . route('cerebelo.' . self::FOLDER . 'edit', $object->id) . '">' . $client_name . '</a></div>' .
                                    '  <div class="text-size-small">' . $have_join . '<a class="text-default text-semibold" href="' . route('cerebelo.' . self::FOLDER . 'edit', $object->id) . '">' . $status_mark . $object->order_number . '</a></div>' .
                                    '</div>';
                        })
                        ->addColumn('orderInfo', function ($object) {
                            $urlEdit = "";
                            if (!empty($object->have_join)) {
                                $explode = explode('-', $object->have_join);
                                $object_id = $explode[0];
                                $object_orders_id = $explode[1];
                                $urlEdit = route('cerebelo.orders.join_edit', $object_id) . '?orders_id=' . $object_orders_id;
                            }
                            $client_name = (isset($object->client->company_name)) ? (empty($object->client->company_name) ? $object->client->fantasy_name : $object->client->company_name) : "";
                            $status_mark = ($object->urgent == 'Y') ? '<span title="Urgente" data-popup="tooltip" data-original-title="Urgente" class="status-mark border-danger position-left"></span>' : '<span class="status-mark border-blue position-left"></span>';
                            $have_join = empty($object->have_join) ? '' : '<a onclick="popUpWin(\'' . $urlEdit . '\', \'900\', \'1000\');return false;" title="" data-popup="tooltip" data-original-title="Visualizar Juntados"><span class="status-mark border-violet-800 position-left"></span></a>';
                            return '<div class="media-left">' .
                                    '  <div><a class="text-default text-semibold">' . $client_name . '</a></div>' .
                                    '  <div class="text-size-small">' . $have_join . '<a class="text-default text-semibold">' . $status_mark . $object->order_number . '</a></div>' .
                                    '</div>';
                        })
                        ->addColumn('btnProduction', function ($object) {
                            return waitActionProduction($object);
                        })
                        ->addColumn('btnCheck', function ($object) {
                            return \Form::checkbox('', $object->id, false, ['class' => 'check_order', 'id' => $object->id]);
                        })
                        ->addColumn('eventDate', function ($object) {
                            $color = empty($object->event_date) ? '' : ' bg-indigo-300 ';
                            return \Form::text('event_date', $object->event_date, ['data-popup' => 'tooltip', 'data-placement' => 'left', 'title' => 'Tecle ENTER para salvar', 'class' => 'form-control input-xs event_date date text-center' . $color, 'style' => 'width: 100px;', 'id' => $object->id]);
                        })
                        ->addColumn('status', function ($object) {
                            $latestSector = $object->status;
                            if (!empty($latestSector)) {
                                return '<span class="text-muted">' . $latestSector->name . '</span>';
                            } else {
                                return '<span class="text-muted">Sem orçamento</span>';
                            }
                        })
                        ->addColumn('delivery_status', function ($object) {
                            $delivery_date = $object->delivery_date;

                            if(!empty($delivery_date)) {
                                $delivery_date = Carbon::createFromFormat('d/m/Y', $delivery_date);
                                $now = Carbon::now();
                                if($now->gt($delivery_date) == true) {
                                    return false;
                                }

                                return true;
                            }

                            return true;
                        })
                        ->addColumn('wait_action', function ($object) {
                            return $this->waitAction($object);
                        })
                        ->addColumn('actions', function ($object) use($select2) {
                            return actionsOptionsSelect($object, $select2);
                        })
                        ->addColumn('str_actions', function ($object) {
                            return (isset($object->sectorAction->name) ? $object->sectorAction->name : "");
                        })
                        ->addColumn('attendant', function ($object) {
                            return (!isset($object->client->attendant->name)) ? "" : $object->client->attendant->name;
                        })
                        ->addColumn('designer', function ($object) {
                            return (!isset($object->client->designer->name)) ? "" : $object->client->designer->name;
                        })
                        ->addColumn('updated_at', function ($object) {
                            $updated_at = Carbon::createFromFormat('d/m/Y H:i:s', $object->updated_at);
                            return '<span>' . $object->updated_at . '</span>' .
                            '<div class="text-size-small"><span class="icon-watch2 position-left"></span>' . $updated_at->diffForHumans() . '</div>';
                        })
                        ->addColumn('input_checkbox', function ($object) use($filters) {
                            $isDashboard = isset($filters['dashboard']);
                            if($isDashboard === false)
                                return \Form::checkbox('order_id[]', $object->id, false, ['class' => 'check_order']);
                        })
                        ->addColumn('btnList', function ($object) {
                            $array = [
                                'art' => [
                                    'title'   => 'Ir para Arte',
                                    'href'    => route('cerebelo.' . self::FOLDER . 'edit', $object->id) . "#justified-badges-tab2",
                                    'class'   => 'text-default',
                                    'i_class' => 'icon-images2',
                                ],
                                'briefing' => [
                                    'title'   => 'Ir para Briefing',
                                    'href'    => route('cerebelo.' . self::FOLDER . 'edit', $object->id) . "#justified-badges-tab3",
                                    'class'   => 'text-default',
                                    'i_class' => 'icon-briefcase',
                                ],
                                'divider' => [],
                                'to_fiscal' => [
                                    'title'   => 'Enviar Para Fiscal',
                                    'onclick' => 'orderSendMailToFiscal(false, this);',
                                    'url'     => route('cerebelo.' . self::FOLDER . 'send_mail', $object->id),
                                    'class'   => 'text-muted',
                                    'i_class' => 'icon-cash',
                                ],
                                'editing' => [
                                    'title'   => 'Editar',
                                    'href'    => route('cerebelo.' . self::FOLDER . 'edit', $object->id),
                                    'class'   => 'text-primary',
                                    'i_class' => 'icon-pencil6',
                                ],
                                'print'   => [
                                    'title'   => 'Imprimir',
                                    'href1'   => route('cerebelo.' . self::FOLDER . 'edit', $object->id) . "?act=print#justified-badges-tab5",
                                    'href2'   => route('cerebelo.' . self::FOLDER . 'edit', $object->id) . "?act=print#justified-badges-tab6",
                                    'class'   => 'text-default',
                                    'onclick' => 'btnPrintOPOrBudget(this);',
                                    'i_class' => 'icon-printer2',
                                ],
                                'destroy' => [
                                    'title'   => 'Excluir',
                                    'route'   => route('cerebelo.' . self::FOLDER . 'destroy', $object->id),
                                    'class'   => 'text-danger',
                                    'i_class' => 'icon-trash',
                                ],
                            ];

                            return iconsList($array);
                        })
                        ->escapeColumns([])
                        ->make(true);
    }

    public function xmlFinancial(Request $request, $ids)
    {
        $model = $request->get('model', 'Order');

        $file_name = generatorXmlFinancial($ids, $model);

        return view(self::MODULE_VIEW . self::FOLDER . 'xml_financial', compact('file_name'))->render();
    }

    private function waitAction($object)
    {
        return waitAction($object);
    }

    private function nextSector($allLineUp, $idLatestSector)
    {
        $key = 0;
        foreach ($allLineUp as $value) {
            $id = $value["id"];

            if ($key === 0 && $idLatestSector == $id) {
                $key = 1;
            } elseif ($key === 1) {
                unset($allLineUp);
                $allLineUp[] = $value;
                break;
            }
        }

        return last($allLineUp);
    }

    /** Ajax
     * Buscar porcentage para o valor do produto e a tabela de preço
     * @return Response
     */
    public function percentage(Request $request)
    {
        $products_id    = $request->get('products_id');
        $price_table_id = $request->get('price_table_id');

        $product = $this->_product->find($products_id)->productsPrices()->where('price_table_id', $price_table_id)->first();

        return response()->json([
                    'error'   => false,
                    'message' => ((isset($product->pivot->value) && !empty($product->pivot->value)) ? $product->pivot->value : null)
        ]);
    }

    /** Ajax
     * Bloco de produto
     * @return Response
     */
    public function addBlockProduct(Request $request)
    {
        $clients_id             = $request->get('clients_id');
        $session_old_input      = $request->get('session_old_input', []);

        // Tratamento da clonagem de um card de produto
        if(is_string($session_old_input)) {
            $index = autoIndex();
            parse_str($session_old_input, $session_old_input);
            $products_index = current($session_old_input['products_index']);

            parse_str(str_replace($products_index, $index, http_build_query($session_old_input)), $session_old_input);
            unset($session_old_input['orders_products_id']); // Limpa o valor do parametro correspondente 'id' da tabela 'orders_products' (porque é um novo card de produto)
        }

        $price_table_id         = $request->get('price_table_id');
        $price_table_value      = $request->get('price_table_value');

        $categories = $this->clientCategories($clients_id);

        $products               = $this->_product->formSelect2($categories);
        $variations             = $this->_variation->formSelect2();
        $sizes                  = $this->_size->getAll();
        $sizes_select2          = $this->_size->formSelect();
        $_budget_tables_selected = $budget_tables_selected = collect(json_decode($request->get('budget_tables_selected', []), true));

        $presents_partnerships = $this->_present_partnership->formSelect();
        return view(self::MODULE_VIEW . self::FOLDER . '_product', compact('price_table_value', 'price_table_id', 'products', 'variations', 'sizes', 'sizes_select2', 'session_old_input', 'budget_tables_selected', 'presents_partnerships', '_budget_tables_selected'))->render();
    }

    public function clientCategories($clients_id = "") {
        $categories = [];
        if(!empty($clients_id)) {
            $categories = $this->_client->find($clients_id)->categories->pluck('id')->toArray();
        }

        return $categories;
    }

    /** Ajax
     * Remover comentário
     * @return Response
     */
    public function updatePrinter(Request $request, $id)
    {
        $printer = $request->get('printer');

        $order = $this->_order->find($id);
        $order->fill(['printer_option' => $printer]);

        if ($order->save()) {
            return response()->json([
                        'error'   => false,
                        'message' => trans('messages.success.update')
            ]);
        } else {
            return response()->json([
                        'error'   => true,
                        'message' => trans('messages.error.update')
            ]);
        }
    }

    /** Ajax
     * Remover comentário
     * @return Response
     */
    public function removeComment(Request $request)
    {
        $id = $request->get('id');

        $destroy = new $this->_comment();

        if ($destroy->destroy($id)) {
            return response()->json([
                        'error'   => false,
                        'message' => trans('messages.success.destroy')
            ]);
        } else {
            return response()->json([
                        'error'   => true,
                        'message' => trans('messages.error.destroy')
            ]);
        }
    }

    /** Ajax
     * Remover arquivo
     * @return Response
     */
    public function removeFile(Request $request)
    {
        $id = $request->get('id');

        $file_model = $this->_file_model->find($id);
        $filename   = $file_model->file;

        if (Storage::delete($filename)) {
            $file_model->delete();

            return $this->redirectBackAfterSuccess(trans('messages.success.destroy'));
        } else {
            return $this->redirectBackAfterError(trans('messages.error.destroy'));
        }
    }

    /** Ajax
     * Enviar o orçamento por email
     * @return Response
     */
    public function sendMail(Request $request, $id)
    {
        $download         = $request->get('download', false);
        //$html             = $request->get('html');
        $email_to         = $request->get('email_to');
        $email_type       = $request->get('email_type');
        $email_recipients = array_filter(explode(",", $request->get('email_recipients')));
        $email_body       = $request->get('email_body');
        $to_fiscal        = $request->get('to_fiscal', false);

        $model = $this->_order->with(['client', 'company.address', 'ordersProducts', 'deliveryAddress', 'billingAddress', 'billingClient'])->find($id);

        $sizes                  = $this->_size->getAll();
        $presents_partnerships  = $this->_present_partnership->formSelect();
        $budget_tables_selected = $this->removeDuplicateItems();

        $colors_sizes = $this->colorsSizes($sizes);

        $products_selected = $this->productSelected($model, $budget_tables_selected, $presents_partnerships, $colors_sizes);

        $model_client = $model->client;
        $html = view(self::MODULE_VIEW . self::FOLDER . '_print_for_client', compact('model', 'sizes', 'products_selected', 'model_client'))->render();

        $users_fiscal = null;
        if($to_fiscal !== false)
        {
            $users_fiscal = $this->_sector->with('users')->withAlias(Sector::FISCAL)->first();
            if(!isset($users_fiscal->users) || empty($users_fiscal->users))
            {
                return response()->json([
                    'error'   => true,
                    'message' => "Não foi encontrado nenhum usuário no setor 'Fiscal'."
                ]);
            }

            $html = str_replace('<tr id="payment_notes"></tr>', "<tr><td>Obsevações:</td><td>$model->payment_notes</td></tr>", $html);
        }

        $data = $model->toArray();
        $data['content'] = $html;

        $file_name = "orcamento_n_{$data['full_order_number']}.pdf";
        $file_path = storage_path("app/$file_name");

        $this->generatePdf($html, $file_path, $file_name);

        if ((bool) $download === false) {
            try {
                $client_data        = auth()->user();
                $data['email_body'] = $email_body;

                Mail::send(['html' => self::MODULE_VIEW . self::FOLDER . 'mail'], $data, function ($message) use ($users_fiscal, $data, $client_data, $file_path, $email_to, $email_type, $email_recipients, $to_fiscal) {
                    //$message->from(trim($client_data->email), trim($client_data->name));
                    if($to_fiscal === false)
                    {
                        if(empty($email_to))
                        {
                            $message->to(trim($data['client']['email']), trim($data['client']['name']));
                        }
                        else
                        {
                            $message->to(trim($email_to), trim($data['client']['name']));
                        }
                    }
                    else
                    {
                        if(isset($users_fiscal->users))
                        {
                            foreach ($users_fiscal->users as $user)
                            {
                                if(!empty(trim($user->email)))
                                {
                                    $message->to(trim($user->email), trim($user->name));
                                }
                            }
                        }
                    }
                    $message->to(trim($client_data->email), trim($client_data->name))
                            ->replyTo(trim($client_data->email), trim($client_data->name))
                            //->bcc('sotnas.lony@emiolo.com', 'Sotnas Leunam - eMiolo.com')
                            ->subject('Orçamento/Pedido nº ' . $data['full_order_number'])
                            ->attach($file_path, [
                                'as'   => "ERT_Orçamento_Nº_{$data['full_order_number']}.pdf",
                                'mime' => 'application/pdf',
                            ]);
                    if (count($email_recipients) > 0) {
                        foreach ($email_recipients as $value) {
                            if (!empty(trim($value))) {
                                if ($email_type == "cc") {
                                    $message->to(trim($value));
                                } elseif ($email_type == "cco") {
                                    $message->bcc(trim($value));
                                }
                            }
                        }
                    }
                });

                $this->deleteTmpFiles($file_name);

                if (count(Mail::failures()) == 0) {
                    return response()->json([
                                'error'   => false,
                                'message' => "E-mail enviado com successo."
                    ]);
                } else {
                    return response()->json([
                                'error'   => true,
                                'message' => "Não foi possível enviar o email."
                    ]);
                }
            } catch (\Exception $ex) {
                return response()->json([
                            'error'   => true,
                            'message' => $ex->getMessage()
                ]);
            }
        }
        else
        {
            $storage = Storage::disk('local');
            if ($storage->exists($file_name))
                return route('storage.files', ['filename' => $file_name, 'download' => true]);

            return;
        }
    }

    public function deleteTmpFiles($file_name) {
        try {
            $storage = Storage::disk('local');
            if ($storage->exists($file_name))
            {
                $storage->delete($file_name);
            }
        }
        catch(\Exception $ex)
        {

        }
    }

    /** Ajax
     * Enviar o orçamento por email
     * @return Response
     */
    public function notificationMenu($type)
    {
        try {
            $total = 0;

            if ($type == "pedidos") {
                $total = $this->_order->totalOrdersInSector();
            }

            return response()->json([
                        'error'   => false,
                        'message' => $total
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                        'error'   => true,
                        'message' => $ex->getMessage()
            ]);
        }
    }
}
