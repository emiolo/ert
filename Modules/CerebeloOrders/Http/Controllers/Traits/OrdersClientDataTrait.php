<?php

namespace Modules\CerebeloOrders\Http\Controllers\Traits;

use Event;
use File;
use Modules\CerebeloOrders\Http\Requests\CustomerRequest;
use Modules\CerebeloSettings\Events\AlertSysEvent;
use Storage;

trait OrdersClientDataTrait
{

    /**
     * Criar cliente - Aba 1
     * @return Response
     */
    public function traitStoreClientData(CustomerRequest $request)
    {
        $attributes = $request->all();

        if ($attributes['person_type'] == 'PF') {
            $attributes['fantasy_name'] = $attributes['name'];
        }

        if (isset($attributes['clients_id']) && !empty($attributes['clients_id'])) {
            $create = $this->_client->find($attributes['clients_id']);
        } else {
            $create = new $this->_client();
        }
        $create->fill($attributes);

        if ($create->save()) {
            $client_id = $create->id;
            $representative_id = $attributes['representative_id'];
            $attendant_id = $attributes['attendant_id'];
            $now_date = date('dmy');

            // Criar pedido ########################################## inicio
            if (isset($attributes['order_id']) && !empty($attributes['order_id'])) {
                $_order = $this->_order->find($attributes['order_id']);
            } else {
                $_order = new $this->_order();
            }
            $_order->fill([
                'clients_id' => $client_id,
            ]);
            $_order->save();
            $id = $_order->id;
            $order_number = preg_replace('/\D/', '', $_order->order_number);
            if (empty($order_number)) {
                $order_number = "$representative_id.$attendant_id.$client_id.$now_date.$id";
                $_order->update([
                    'order_number' => $order_number,
                ]);
            }
            $attributes['btnSendTo'] = "sendToAtendant";
            $this->saveSectors($id, $attributes, "Y");
            // Criar pedido ########################################## fim
            //
            // Salvar endereço ######################################## inicio

            $address_id = null;

            if (isset($attributes['cep'])) {
                foreach ($attributes['cep'] as $key => $value) {
                    $cep = $value;
                    if (!empty($cep)) {
                        $type = $attributes['type'][$key];
                        $table = $attributes['table'][$key];
                        $street = $attributes['street'][$key];
                        $number = $attributes['number'][$key];
                        $complement = $attributes['complement'][$key];
                        $neighborhood = $attributes['neighborhood'][$key];
                        $city = $attributes['city'][$key];
                        $state = $attributes['state'][$key];
                        $local = $attributes['local'][$key];

                        $addresses = array(
                            'cep' => $cep,
                            'type' => $type,
                            'table' => $table,
                            'street' => $street,
                            'number' => $number,
                            'complement' => $complement,
                            'neighborhood' => $neighborhood,
                            'city' => $city,
                            'state' => $state,
                            'local' => $local,
                        );

                        $address = new $this->_address();
                        $address->fill($addresses);
                        $result = $create->address()->save($address);
                        $address_id = $result->id;
                    }
                }
            }

            $_order->update([
                'delivery_address_id' => isset($attributes['address_id']) ? $attributes['address_id'] : $address_id,
                'billing_address_id' => isset($attributes['billing_address_id']) ? $attributes['billing_address_id'] : null,
                'clients_contacts_id' => isset($attributes['clients_contacts_id']) ? $attributes['clients_contacts_id'] : "",
            ]);

            // Salvar endereço ######################################## fim
            //
            // Salvar tabelas ######################################## inicio
            $create->priceTables()->sync((!isset($attributes['tables_id'])) ? [] : $attributes['tables_id']);
            // Salvar tabelas ######################################## fim
            //
            // Salvar observação/comentário ############################ inicio
            if (!empty($attributes['comment'])) {
                $user_id = auth()->user()->id;
                $create->comments()->attach($user_id, ['comment' => $attributes['comment']]);
            }
            // Salvar observação/comentário ############################ fim

            return $this->redirectAfterSuccess(trans('messages.success.create'), 'cerebelo.' . self::FOLDER . 'edit', $id);
        }

        return $this->redirectBackAfterError(trans('messages.error.create'));
    }

    /**
     * Atualizar dados do cliente - Aba 1
     *
     * @return Response
     */
    public function traitUpdateClientData(CustomerRequest $request, $id)
    {
        $attributes = $request->all();

        if ($attributes['person_type'] == 'PF') {
            $attributes['fantasy_name'] = $attributes['name'];
        }

        $update = $this->_client->find($attributes['clients_id']);

        if (empty($update)) {
            return $this->traitStoreClientData($request);
        }

        $update->fill($attributes);

        if ($update->save()) {
            $address_id = null;

            // Salvar endereço ######################################## inicio
            if (isset($attributes['cep'])) {
                foreach ($attributes['cep'] as $key => $value) {
                    $cep = $value;
                    if (!empty($cep)) {
                        $type = $attributes['type'][$key];
                        $table = $attributes['table'][$key];
                        $street = $attributes['street'][$key];
                        $number = $attributes['number'][$key];
                        $complement = $attributes['complement'][$key];
                        $neighborhood = $attributes['neighborhood'][$key];
                        $city = $attributes['city'][$key];
                        $state = $attributes['state'][$key];
                        $local = $attributes['local'][$key];

                        $addresses = array(
                            'cep' => $cep,
                            'type' => $type,
                            'table' => $table,
                            'street' => $street,
                            'number' => $number,
                            'complement' => $complement,
                            'neighborhood' => $neighborhood,
                            'city' => $city,
                            'state' => $state,
                            'local' => $local,
                        );

                        $address = new $this->_address();
                        $address->fill($addresses);
                        $result = $update->address()->save($address);
                        $address_id = $result->id;
                    }
                }
            }

            $_order = $this->_order->find($id);

            $old_client_id = $_order->clients_id;
            if ($update->id != $old_client_id) {
                $client_id = $update->id;
                $representative_id = $attributes['representative_id'];
                $attendant_id = $attributes['attendant_id'];
                $now_date = date('dmy');
                $order_number = "$representative_id.$attendant_id.$client_id.$now_date.$id";

                $_order->clients_id = $client_id;
                $_order->order_number = $order_number;
            }

            // Salvar endereço ######################################## fim

            $_order->delivery_address_id = isset($attributes['address_id']) ? $attributes['address_id'] : $address_id;
            $_order->billing_address_id = isset($attributes['billing_address_id']) ? $attributes['billing_address_id'] : null;
            $_order->clients_contacts_id = isset($attributes['clients_contacts_id']) ? $attributes['clients_contacts_id'] : "";
            $_order->save();

            //
            // Salvar tabelas ######################################## inicio
            $update->priceTables()->sync((!isset($attributes['tables_id'])) ? [] : $attributes['tables_id']);
            // Salvar tabelas ######################################## fim
            //
            // Salvar observação/comentário ############################ inicio
            if (!empty($attributes['comment'])) {
                $user_id = auth()->user()->id;
                $update->comments()->attach($user_id, ['comment' => $attributes['comment']]);
            }
            // Salvar observação/comentário ############################ fim

            $this->saveSectors($id, $attributes);

            return $this->redirectBackAfterSuccess(trans('messages.success.update'));
        }

        return $this->redirectBackAfterError(trans('messages.error.update'));
    }

    public function saveFiles($model, $files, $attributes)
    {
        $_path = $this->_order->filesPath();
        $users_id = auth()->user()->id;

        foreach ($files as $key => $value) {
            $file_name = md5(time() . $value->getFilename());
            $extension = $value->getClientOriginalExtension();
            $path = "$_path$file_name.$extension";
            if (Storage::put($path, File::get($value))) {
                $name = $attributes['file_name'][$key];
                $customer_text = $attributes['customer_text'][$key];

                $model->manyFiles()->create([
                    'users_id' => $users_id,
                    'name' => $name,
                    'description' => $customer_text,
                    'file' => $path,
                ]);
            }
        }
    }

    public function saveComments($model, $comments)
    {
        $users_id = auth()->user()->id;

        foreach ($comments as $value) {
            $model->comments()->create([
                'users_id' => $users_id,
                'message' => $value,
            ]);
        }
    }

    public function saveSectors($id, $attributes, $confirmed = "N")
    {
        $send = false;
        $model = $this->_order->with(['client'])->find($id);

        if (isset($attributes['btnSendTo'])) {
            if ($attributes['btnSendTo'] === "sendToDesign") {
                $users_id = $model->client->designer_id;
                $user = $this->_user->with('sector')->find($users_id);
                if (isset($user->sector->period)) {
                    $period = $user->sector->period;
                    $period_option = $user->sector->period_option;
                    $send = true;
                }
            } elseif ($attributes['btnSendTo'] === "sendToAtendant") {
                $users_id = $model->client->attendant_id;
                $user = $this->_user->with('sector')->find($users_id);
                if (isset($user->sector->period)) {
                    $period = $user->sector->period;
                    $period_option = $user->sector->period_option;
                    $send = true;
                }
            }
        } else {
            if (isset($attributes['management_analysis']) && !empty($attributes['management_analysis'])) {
                $users_id = $attributes['management_analysis'];
                $user = $this->_user->with('sector')->find($users_id);
                if (isset($user->sector->period)) {
                    $period = $user->sector->period;
                    $period_option = $user->sector->period_option;
                    $send = true;

                    // Criar Alerta
                    //$users_name = auth()->user()->name;
                    //$content = "$users_name enviou o pedido <span class='text-bold'>$model->order_number</span> para sua análise, clique <a href='" . route('cerebelo.orders.edit', $id) . '#justified-badges-tab2' . "'>aqui</a>.";
                    //Event::fire(new AlertSysEvent($users_id, $content));
                }
            }
        }

        if ($send === true) {
            $model->sectors()->attach([$users_id => calculatePeriodPerSector($period_option, $period, $confirmed)]);

            // Criar Alerta
            try {
                $users_name = auth()->user()->name;
                $client_name = $model->client->name;
                $material = empty($model->group) ? "" : ", material " . $model->group;

                $content = "$users_name enviou o pedido nº <span class='text-bold'>$model->order_number</span>$material do cliente <span class='text-bold'>$client_name</span> para você.";
                if (isset($attributes['obs']) && !empty($attributes['obs'])) {
                    $content .= "<br/>" . $attributes['obs'] . "<br/>";
                }

                $latestSector = $model->latestSector()->first();
                $pivotIdLatestSector = $latestSector->pivot->id;
                $content .= " <a q='" . $model->order_number . "' onclick='btnReceiveOrderFromAlert(this);return false;' href='" . route('cerebelo.' . self::FOLDER . 'line_up', ['type' => 'accept', 'id' => $pivotIdLatestSector]) . "'>receber pedido</a>.";
                Event::fire(new AlertSysEvent($users_id, $content));
            } catch (Exception $ex) {
                \Log::error("Erros - Alert Sys: saveSectors() > btnSendTo :: " . $ex->getMessage());
            }
        } else {

        }
    }

}
