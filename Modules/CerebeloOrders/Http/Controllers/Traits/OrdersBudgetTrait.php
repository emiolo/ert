<?php

namespace Modules\CerebeloOrders\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Modules\CerebeloOrders\Http\Requests\BudgetRequest;
use Validator;

trait OrdersBudgetTrait
{

    public function traitUpdateBudget(BudgetRequest $request, $id)
    {
        $attributes = $request->all();

        $update = $this->_order->find($id);

        $transport_freight = (isset($attributes['transport_freight'])) ? $attributes['transport_freight'] : false;
        if ($transport_freight) {
            $attributes['transport_type'] = $transport_freight;
            $attributes['transport_deadline'] = $attributes['transport_deadline_' . $transport_freight];
            $transport_price_ = $attributes['transport_price_' . $transport_freight];
            if ($transport_price_ <= 0) {
                $attributes['transport_type'] = null;
            }
        } else {
            $attributes['transport_type'] = null;
        }

        if (!isset($attributes['use_credit_value'])) {
            $attributes['credit_value'] = 0;
        }

        if (!isset($attributes['to_charge'])) {
            $attributes['to_charge'] = 'N';

            if (empty($attributes['payments_form_id'])) {
                $attributes['payments_form_id'] = null;
            }

        } else {
            $attributes['to_charge'] = 'Y';
        }

        if (!isset($attributes['urgent'])) {
            $attributes['urgent'] = 'N';
        } else {
            $attributes['urgent'] = 'Y';
        }

        if (empty($attributes['discount_value'])) {
            $attributes['discount_value'] = 0;
        }

        if (!isset($attributes['billing_address_id']) || empty($attributes['billing_address_id'])) {
            $attributes['billing_address_id'] = null;
        }

        $attributes['delivery_boxes'] = [];
        if (isset($attributes['box_quantity']) || count($attributes['box_quantity']) > 0) {
            $arr_boxes = array_map(function ($k, $v) use ($attributes) {
                $box_weight = $attributes['box_weight'][$k];
                $box_dimension = $attributes['box_dimension'][$k];

                return ['quantity' => $v, 'weight' => $box_weight, 'dimension' => $box_dimension];
            }, array_keys($attributes['box_quantity']), array_values($attributes['box_quantity']));

            $attributes['delivery_boxes'] = $arr_boxes;
        }

        $update->fill($attributes);

        if ($update->save()) {
            /*
             * Salvar produtos
             */
            $this->saveProducts($id, $attributes);

            $this->saveSectors($id, $attributes);

            /*
             * Salvar parcelas da forma de pagamento
             */
            $this->savePaymentsInstallments($update, $attributes);

            return $this->redirectAfterSuccess(trans('messages.success.update'), 'cerebelo.' . self::FOLDER . 'edit', $id);
        }

        return $this->redirectBackAfterError(trans('messages.error.update'));
    }

    public function saveProducts($order_id, $attributes)
    {
        $ordination = 0;
        foreach ($attributes['products_index'] as $key => $value) {
            $orders_products_id = (!isset($attributes['orders_products_id'][$key]) || empty($attributes['orders_products_id'][$key])) ? false : $attributes['orders_products_id'][$key];

            if (!$orders_products_id) {
                $order_product = new $this->_order_product();
            } else {
                $order_product = $this->_order_product->find($orders_products_id);
            }

            $products_id = $attributes['products_id'][$key];
            $quantity = $attributes['quantity'][$key];
            $have_sizes = (isset($attributes['have_sizes'][$key])) ? 'N' : 'Y';
            $quantity_piece = empty($attributes['quantity_piece'][$key]) ? 0 : $attributes['quantity_piece'][$key];
            $size_piece = empty($attributes['size_piece'][$key]) ? 0 : $attributes['size_piece'][$key];
            $name_piece = $attributes['name_piece'][$key];
            $unit_price = $attributes['unit_price'][$key];
            $sizes = $attributes['sizes'][$key];
            $variations_id = (!isset($attributes['variations_id'][$key])) ? [] : $attributes['variations_id'][$key];
            $extra_variation = $attributes['extra_variation'][$key];
            $price_table_id = empty($attributes['custom_price_table_id'][$key]) ? 0 : $attributes['custom_price_table_id'][$key];
            $price_table_value = empty($attributes['price_table_value'][$key]) ? 0 : $attributes['price_table_value'][$key];
            $presents_partnerships_id = empty($attributes['presents_partnerships_id'][$key]) ? 0 : $attributes['presents_partnerships_id'][$key];

            $order_product_attr = [
                'orders_id' => $order_id,
                'products_id' => $products_id,
                'quantity' => $quantity,
                'have_sizes' => $have_sizes,
                'quantity_piece' => $quantity_piece,
                'size_piece' => $size_piece,
                'name_piece' => $name_piece,
                'unit_price' => $unit_price,
                'extra_variation' => $extra_variation,
                'price_table_id' => $price_table_id,
                'price_table_value' => $price_table_value,
                'presents_partnerships_id' => $presents_partnerships_id,
                'ordination' => $ordination,
            ];

            $order_product->fill($order_product_attr);
            $order_product->save();

            // Salvar os tamanhos
            //$sizes_sync = [];
            foreach ($sizes as $key => $value) {
                $qty_val = (int) (empty($value) ? 0 : $value);
                //$sizes_sync[$key] = ['quantity' => $qty_val];

                $sz = $order_product->sizes()->where('sizes_id', $key)->first();
                if (empty($sz)) {
                    $order_product->sizes()->attach($key, ['quantity' => $qty_val]);
                } else {
                    $pivot = $sz->pivot;
                    $qty = $pivot->quantity;

                    if ($qty_val != $qty) {
                        $order_product->sizes()->detach($key);
                        $order_product->sizes()->attach($key, ['quantity' => $qty_val]);
                    }
                }
            }
            //$order_product->sizes()->sync($sizes_sync);

            /*
             * Verifica se as variações o produto foram alteradas
             */
            $order_product->checkIfVariationProductUpdated($order_product, $variations_id);

            // Salvar as variação
            $order_product->variations()->sync($variations_id);

            $ordination++;
        }
    }

    public function savePaymentsInstallments($model, $attributes)
    {
        $to_charge = (!isset($attributes['to_charge']) || $attributes['to_charge'] == "N") ? false : true;
        $plots = $attributes['plots'];
        //$new_count = count($plots);
        //$old_count = $this->_payment_installment->where('orders_id', $model->id)->count();

        if (count($plots) > 0 && $to_charge === true) {
            if (isset($attributes['plots_id']) && is_array($attributes['plots_id'])) {
                $this->_payment_installment->whereId($model->id)->whereNotIn('id', array_values($attributes['plots_id']))->delete();
            }

            $key = 1;
            foreach ($plots as $value) {
                $plots_id = (!isset($attributes['plots_id'][$key])) ? false : $attributes['plots_id'][$key];
                $plots_date = $attributes['plots_date'][$key];
                $plots_expiration_date = $attributes['plots_expiration_date'][$key];
                $plots_status = $attributes['plots_status'][$key];
                $plots_payment_form = $attributes['plots_payment_form'][$key];

                $_attributes = [
                    'portion' => $key,
                    'value' => $value,
                    'date' => $plots_date,
                    'expiration_date' => $plots_expiration_date,
                    'status' => $plots_status,
                    'payment_form' => $plots_payment_form,
                ];

                $payment_installment = $model->paymentsInstallments()->updateOrCreate(['orders_id' => $model->id, 'portion' => $key], $_attributes);

                $plots_file = (!isset($attributes['plots_file'][$key])) ? null : $attributes['plots_file'][$key];
                if (!empty($plots_file)) {
                    $storage = \Storage::disk('local');
                    $extension = $plots_file->getClientOriginalExtension();
                    $new_file_name = "orders_checking_copy/" . md5($plots_file->getFilename() . $key . date("dmYHis")) . '.' . $extension;

                    $image = \Image::make($plots_file->getRealPath());

                    if ($storage->put($new_file_name, (string) $image->encode())) {
                        $receipts = [];
                        $receipt = new \Modules\CerebeloOrders\Entities\PaymentInstallmentReceipt();
                        $receipt->file = $new_file_name;

                        array_push($receipts, $receipt);
                        $payment_installment->receipts()->saveMany($receipts);
                    }
                }

                $key++;
            }
        } else {
            $this->_payment_installment->whereId($model->id)->delete();
        }
    }

    public function traitConsultFreight(Request $request)
    {
        $freight_type = $request->get('freight_type');

        if ($freight_type == 'correios') {
            $products_weight = $request->get('products_weight');
            $types = $request->get('type');
            $cep_destino = $request->get('cep');
            $freights_grouping = [];

            $min_weight = $max_weight = $width = $length = $height = 0;
            $products_weight_calc = $products_weight;
            $correiosConsultFreight = [];

            $estimateBox = estimateBox($products_weight);

            foreach ($estimateBox as $value):
                $length = $value['length'];
                $width = $value['width'];
                $height = $value['height'];
                $weight = $value['weight'];

                $data = array(
                    'tipo' => $types,
                    'formato' => 'caixa',
                    'cep_destino' => $cep_destino,
                    //'cep_origem' => 'caixa',
                    'peso' => current($weight),
                    'comprimento' => $length,
                    'altura' => $height,
                    'largura' => $width,
                );
                $request = request()->create(null, null, $data);

                $correiosConsultFreight = correiosConsultFreight($request);

                if (isset($correiosConsultFreight['error']) && $correiosConsultFreight['error'] === true) {
                    return response()->json([
                        'error' => true,
                        'message' => $correiosConsultFreight['message'],
                    ]);
                }

                foreach ($correiosConsultFreight as $key => $value) {
                    if (isset($value['valor'])) {
                        $value['value'] = count($weight) * $value['valor'];
                        $value['deadline'] = $value['prazo'];
                        $value['min_weight'] = $min_weight;
                        $value['max_weight'] = $max_weight;

                        $freights_grouping[$value['tipo']][] = $value;
                    }
                }
            endforeach;

            return response()->json([
                'error' => false,
                'message' => $freights_grouping,
            ]);
        }

        return response()->json([
            'error' => true,
            'message' => "Consulta somente para Correios.",
        ]);
    }

    public function traitEstimateBox(Request $request, $join = false)
    {
        $load = $request->get('load', 'A');
        if ($load == 'M') {
            $order_id = $request->get('order_id');
            if (!empty($order_id)) {
                if($join === false) {
                    $delivery_boxes = \Modules\CerebeloOrders\Entities\Order::find($order_id)->delivery_boxes;
                } else {
                    $delivery_boxes = \Modules\CerebeloOrders\Entities\OrderJoin::find($order_id)->delivery_boxes;
                }
                if (!empty($delivery_boxes)) {
                    $freights_grouping = [];
                    foreach ($delivery_boxes as $key => $value):
                        $quantity = $value->quantity;
                        $weight = $value->weight;
                        $dimension = $value->dimension;

                        $delivery_box = \Modules\CerebeloOrders\Entities\DeliveryBox::find($dimension);

                        $freights_grouping[$key]['format'] = 'caixa';
                        $freights_grouping[$key]['tolerate'] = $delivery_box->max_weight;
                        $freights_grouping[$key]['quantity'] = [$quantity];
                        $freights_grouping[$key]['weight'] = [$weight];
                        $freights_grouping[$key]['length'] = $delivery_box->length;
                        $freights_grouping[$key]['height'] = $delivery_box->height;
                        $freights_grouping[$key]['width'] = $delivery_box->width;
                        $freights_grouping[$key]['dimension'] = $delivery_box->id;
                    endforeach;

                    return response()->json([
                        'error' => false,
                        'message' => $freights_grouping,
                    ]);
                }
            }
        }

        $products_weight = $request->get('products_weight');

        return response()->json([
            'error' => false,
            'message' => estimateBox($products_weight),
        ]);
    }

    public function traitPrintOpWithName($id)
    {
        $model = $this->_order->with(['client', 'ordersProducts' => function ($q) use ($id) {
            $q->where('orders_products.id', $id);
        }])->whereHas('ordersProducts', function ($q) use ($id) {
            $q->where('orders_products.id', $id);
        })->first();

        $prepress_users = $this->_user->formSelectPrepress();
        $op_names = $this->_order_product->with('opWithName')->find($id)->opWithName;
        $_sizes = $this->_size->getAll();
        $sizes = [];
        foreach ($_sizes as $value) {
            $sizes[$value->name] = $value->hex_color;
        }

        $html = view(self::MODULE_VIEW . self::FOLDER . '_print_op_with_name', compact('model', 'sizes', 'op_names', 'prepress_users'))->render();

        try
        {
            $mpdf = new $this->_mpdf(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                0, // font size - default 0
                '', // default font family
                5, // margin_left
                5, // margin right
                5, // margin top
                5, // margin bottom
                0, // margin header
                0, // margin footer
                'P' // L - landscape, P - portrait
            );

            $stylesheet = '<link href="' . asset('css/cerebelo/application.css') . '" media="all" rel="stylesheet" type="text/css"/>';
            $mpdf->WriteHTML($stylesheet);
            $stylesheet = '<link href="' . asset('modules/cerebeloorders/css/orders/_order_print_area.css') . '" media="all" rel="stylesheet" type="text/css"/>';
            $mpdf->WriteHTML($stylesheet);
            $stylesheet = '<link href="' . asset('modules/cerebeloorders/css/orders/_order_print_pdf.css') . '" media="all" rel="stylesheet" type="text/css"/>';
            $mpdf->WriteHTML($stylesheet);

            $_html = '<!DOCTYPE html>';
            $_html .= '<html>';
            $_html .= ' <head>';
            $_html .= '  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>';
            //$_html .= '  <title>Orçamentos - ' . $client_name . '</title>';
            $_html .= ' </head>';
            $_html .= ' <body>';
            $_html .= $html;
            $_html .= ' </body>';
            $_html .= '</html>';
            $mpdf->WriteHTML($_html, 2, false, true);

            $mpdf->Output();
        } catch (\Exception $ex) {
            return;
        }
    }

    public function traitDestroyOpWithName($id)
    {
        if ($this->_op_with_name->destroy($id)) {
            return response()->json([
                'error' => false,
                'message' => 'Resgistro excluído com sucesso.',
            ]);
        }

        return response()->json([
            'error' => true,
            'message' => 'Não possível escluir o registro.',
        ]);
    }

    public function traitSaveOpWithName($request, $id)
    {
        $attributes = $request->all();
        $model = [];

        if (!isset($attributes['name'])) {
            return response()->json([
                'error' => true,
                'message' => 'Nenhum dado foi extraído do arquivo. Favor, verifique o arquivo e tente novamente.',
            ]);
        }

        $validator = Validator::make($attributes, [
            'name.*' => 'required|max:100',
        ], [
            'name.*.required' => 'O campo Nome é obrigatório.',
            'name.*.max' => 'O campo Nome não deve ser maior que :max.',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => true, 'message' => $validator->errors()->first()]);
        }

        $order_product = $this->_order_product->with(['opWithName', 'sizes'])->find($id);
        $sum_quantity = [];

        foreach ($attributes['name'] as $key => $name) {
            $id_op_name = isset($attributes['id_op_name'][$key]) ? $attributes['id_op_name'][$key] : false;
            $quantity = $attributes['quantity'][$key];
            $size = $attributes['size'][$key];

            if ($quantity > 0 && !empty($quantity)) {
                $sum_quantity[$size][] = $quantity;

                if ($id_op_name === false) {
                    $op = new $this->_op_with_name();
                } else {
                    $op = $this->_op_with_name->find($id_op_name);
                }

                $op->fill([
                    'ordination' => $key,
                    'name' => $name,
                    'quantity' => $quantity,
                    'size' => $size,
                ]);

                $model[] = $op;
            }
        }

        if (empty($model) || count($model) == 0) {
            return response()->json([
                'error' => true,
                'message' => "Nenhum item do arquivo foi processado.",
            ]);
        }

        // Validar tamanhos do arquivo com tamanhos dos cards
        foreach ($order_product->sizes as $key => $value) {
            if (array_key_exists($value->name, $sum_quantity)) {
                if (array_sum($sum_quantity[$value->name]) != $value->pivot->quantity) {
                    /*return response()->json([
                'error'   => true,
                'message' => "O tamanho $value->name fornecido pelo arquivo não bate com o total do card."
                ]);*/
                }
            }
        }

        // Salvar
        $result = $order_product->opWithName()->saveMany($model);

        $order_product = $order_product->with(['opWithName'])->find($id);

        if ($result) {
            return response()->json([
                'error' => false,
                'message' => 'Ação finalizada com sucesso.',
                'html' => $this->indexOpWithNameSizes($order_product),
            ]);
        }

        return response()->json([
            'error' => true,
            'message' => 'Não foi possível finalizar a ação.',
        ]);
    }

    public function indexOpWithNameSizes($order_product)
    {
        $products_selected = [];
        $autoIndex = autoIndex();
        $sizes = $this->_size->getAll();
        $colors_sizes = $this->colorsSizes($sizes);

        foreach ($order_product->opWithName as $size) {
            $products_selected['op_name_tab2'][$autoIndex][$size->size][] = $size->name;
            $products_selected['op_quantity_tab2'][$autoIndex][$size->size][] = $size->quantity;
            $products_selected['op_size_tab2'][$autoIndex][$size->size][] = $size->size;
            $products_selected['op_color_tab2'][$autoIndex][$size->size] = $colors_sizes[$size->size];
        }

        $session_old_input = $products_selected;
        $input = $autoIndex;

        return view(self::MODULE_VIEW . self::FOLDER . '_product_op_name_size', compact('session_old_input', 'input'))->render();
    }

    public function saveOpWithName(Request $request, $id)
    {
        $file = $request->file('file');

        if (empty($file) === false) {
            return $this->traitLoadOpWithName($request, $id);
        }

        return $this->traitSaveOpWithName($request, $id);
    }

    public function traitLoadOpWithName(Request $request, $id)
    {
        $file = $request->file('file');

        // Carregar os dados do arquivo
        $excel = \Excel::load($file)->get()->toArray();
        $data = [];

        if (count($excel) > 0) {
            $array_keys = array_filter(array_keys($excel[0]));
            $array_values = array_filter(array_values($excel[0]));

            if (count($array_keys) !== 3 || count($array_values) !== 3) {
                return response()->json([
                    'error' => true,
                    'message' => 'Verifique se existe as colunas e se estão na sequência Nome, Quantidade e Tamanho no arquivo excel.',
                ]);
            }

            $_sizes = collect($this->_size->getAll()->toArray());

            foreach ($excel as $value) {
                $array_values = array_values($value);

                $name = trim($array_values[0]);
                $quantity = (int) trim($array_values[1]);
                $size = strtoupper(trim($array_values[2]));

                if ($name != 'nome' && $quantity != 'quantidade' && $size != 'tamanho' && $quantity > 0 && !empty($name) && !empty($size)) {
                    $current = current($_sizes->where('name', '=', $size)->toArray());
                    $size_id = isset($current['id']) ? $current['id'] : '';

                    $data[$size][] = array($name, $quantity, $size, $size_id);
                }
            }
        }

        return response()->json($data);
    }

}
