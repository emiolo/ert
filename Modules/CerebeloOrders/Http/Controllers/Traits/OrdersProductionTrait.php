<?php

namespace Modules\CerebeloOrders\Http\Controllers\Traits;

use Datatables;
use File;
use Form;
use Illuminate\Http\Request;
use Mail;
use Storage;

trait OrdersProductionTrait
{

    /**
     * Lista dos Pedidos em Produção
     * @var $products_selected
     * @return array
     */
    private function traitMountLayout(&$products_selected)
    {
        $layout_op_data = [];
        $products_id = $products_selected['products_id'];

        if (count($products_id) == 0) {
            return;
        }

        $all_sizes = $this->_size->getAll();

        $session_extra_variation = [];

        $i = 0;
        foreach ($products_id as $key => $value):
            $sizes_name = array_flip($products_selected['sizes_name'][$key]);
            $extra_variation = snake_case(str_slug(trim($products_selected['extra_variation'][$key]))); // Infantil | Modelo 1.1 | Modelo 2.1 | ...
            $presents_partnerships_id = $products_selected['presents_partnerships_id'][$key]; // 1=Brinde | 2=Parceria | 3=Sem Nome
            $present_partnership = snake_case(str_slug(trim($products_selected['present_partnership'][$key]))); // 1=Brinde | 2=Parceria | 3=Sem Nome

            // Dados de peças da ERT de cada card
            $quantity_piece = $products_selected['quantity_piece'][$key];
            $size_piece = $products_selected['size_piece'][$key];
            $name_piece = $products_selected['name_piece'][$key];

            if (isset($products_selected['op_name'][$key])) { // COM NOME +++++++++++++++++++++++
                $op_name = $products_selected['op_name'][$key];

                foreach ($op_name as $k => $v):
                    $size = $sizes_name[$k];
                    $op_quantity = $products_selected['op_quantity'][$key][$k];
                    $op_size = $products_selected['op_size'][$key][$k];
                    $op_color = $products_selected['op_color'][$key][$k];

                    $layout_op_data[$value][$extra_variation]['with_name'][$size]['index'][] = $key;
                    $layout_op_data[$value][$extra_variation]['with_name'][$size]['name'][] = $v;
                    $layout_op_data[$value][$extra_variation]['with_name'][$size]['quantity'][] = $op_quantity;
                    $layout_op_data[$value][$extra_variation]['with_name'][$size]['size'][] = $op_size;
                    $layout_op_data[$value][$extra_variation]['with_name'][$size]['color'][] = $op_color;
                endforeach;

                // Peça ERT
                if (in_array($size_piece, $sizes_name)) {
                    $array_flip = array_flip($sizes_name);
                    $size = $array_flip[$size_piece];

                    if (!empty($size)):
                        $layout_op_data[$value][$extra_variation]['with_name'][$size_piece]['index']['piece'] = $key;
                        $layout_op_data[$value][$extra_variation]['with_name'][$size_piece]['name']['piece'] = $name_piece;
                        $layout_op_data[$value][$extra_variation]['with_name'][$size_piece]['quantity']['piece'] = $quantity_piece;
                        $layout_op_data[$value][$extra_variation]['with_name'][$size_piece]['size']['piece'] = $size;
                        $layout_op_data[$value][$extra_variation]['with_name'][$size_piece]['color']['piece'] = 'color-' . strtolower($size);
                    endif;
                }

                ksort($layout_op_data[$value][$extra_variation]['with_name']);
            } else { // SEM NOME +++++++++++++++++++++++
                $quantity = $products_selected['quantity'][$key];
                $have_sizes = $products_selected['have_sizes'][$key];

                $group_key = (empty($presents_partnerships_id) || $presents_partnerships_id == 2) ? 0 : $presents_partnerships_id; // Vazio{""} ou Parceria{2} = agrupar [0], nova linha [>=1]
                $group_value = (empty($present_partnership)) ? 'parceria' : $present_partnership; // Vazio{""} ou Parceria{2} = agrupar ['Parceria'], nova linha ['Brinde, Sem Nome, ...']

                // Esse trecho é para vincular todo card 'INFANTIL' ao card anterior ou pode deixar como card avulso
                if (str_contains($extra_variation, 'infantil') && !in_array($extra_variation, $session_extra_variation) && last($layout_op_data) !== false) {
                    array_push($session_extra_variation, $extra_variation);
                    $last_key = key(last($layout_op_data));
                    if (empty($last_key)) {
                        $extra_variation = "";
                        $group_value = "infantil";
                    }
                }

                // Para colocar produto com a mesma variação (Modelo 1.0 ou Modelo 2.0 ...) do tipo "INFANTIL" agrupado com o produto principal (para manter arte única)
                $explode = array_map('trim', explode(',', trim($products_selected['extra_variation'][$key])));
                if (count($explode) > 1) {
                    $have_infantil = array_map(function ($v, $k) use ($explode, &$extra_variation, &$group_value) {
                        if (str_contains(strtolower($v), 'infantil')) {
                            $extra_variation = snake_case(str_slug(trim($explode[$k - 1]))); // Infantil | Modelo 1.1 | Modelo 2.1 | ...
                            $group_value = "infantil";

                            return true;
                        }

                        return false;
                    }, array_values($explode), array_keys($explode)); // Infantil | Modelo 1.1 | Modelo 2.1 | ...
                }

                if ($have_sizes == 'Y') { // Com tamanhos
                    foreach ($all_sizes as $size):
                        $size_quantity = $products_selected['sizes'][$key][$size->id];
                        $size_name = $products_selected['sizes_name'][$key][$size->id];

                        $layout_op_data[$value][$extra_variation]['unnamed'][$group_value][$size->id]['index'][] = $key;
                        $layout_op_data[$value][$extra_variation]['unnamed'][$group_value][$size->id]['quantity'][] = $size_quantity;
                        $layout_op_data[$value][$extra_variation]['unnamed'][$group_value][$size->id]['size'][] = $size_name;
                        $layout_op_data[$value][$extra_variation]['unnamed'][$group_value][$size->id]['color'][] = 'color-' . strtolower($size_name);
                    endforeach;

                    // Peça ERT
                    if (!empty($quantity_piece) && !empty($size_piece)) {
                        $size_name = $products_selected['sizes_name'][$key][$size_piece];

                        $layout_op_data[$value][$extra_variation]['unnamed'][$group_value][$size_piece]['index']['piece'] = $key;
                        $layout_op_data[$value][$extra_variation]['unnamed'][$group_value][$size_piece]['quantity']['piece'] = $quantity_piece;
                        $layout_op_data[$value][$extra_variation]['unnamed'][$group_value][$size_piece]['size']['piece'] = $size_name;
                        $layout_op_data[$value][$extra_variation]['unnamed'][$group_value][$size_piece]['color']['piece'] = 'color-' . strtolower($size_name);
                    }
                } else { // Sem tamanhos
                    $layout_op_data[$value][$extra_variation]['unnamed'][$group_value][1]['index'][] = $key;
                    $layout_op_data[$value][$extra_variation]['unnamed'][$group_value][1]['quantity'][] = $quantity;
                    $layout_op_data[$value][$extra_variation]['unnamed'][$group_value][1]['color'][] = 'color-pp';

                    // Peça ERT
                    if (!empty($quantity_piece) && !empty($size_piece)) {
                        $size_name = $products_selected['sizes_name'][$key][$size_piece];

                        $layout_op_data[$value][$extra_variation]['unnamed'][$group_value][$size_piece]['index']['piece'] = $key;
                        $layout_op_data[$value][$extra_variation]['unnamed'][$group_value][$size_piece]['quantity']['piece'] = $quantity_piece;
                    }
                }
            }

            $i++;
        endforeach;

        $products_selected['layout_op_data'] = $layout_op_data;

        if (auth()->user()->id == 1 && request()->has('emi-debug')) {
            dd($products_selected);
        }
    }

    public function traitDatatableClientFiles(Request $request, $id)
    {
        $orders_id = $request->get('orders_id');

        return Datatables::of($this->_file->datatable($id, $orders_id, true))
            ->addColumn('inputCheckbox', function ($object) {
                $storage = Storage::disk('local');
                $file = $object->file;
                if ($storage->exists($file)) {
                    $extension = File::extension(storage_path("app/$file"));
                    if (in_array(strtolower($extension), ['jpg', 'jpeg', 'png', 'bmp', 'webp'])) {
                        return Form::checkbox('client_files', $object->id, (isset($object->attachedFiles[0]->pivot->clients_files_id) ? true : false), ['class' => 'client-files', 'title' => 'Somente']);
                    }
                }
            })
            ->addColumn('file', function ($object) {
                return '<a href="' . route('storage.files', ['filename' => $object->file, 'download' => true]) . '">Clique aqui para baixar</a>';
            })
            ->addColumn('file_extension', function ($object) {
                $storage = Storage::disk('local');
                $file = $object->file;
                if ($storage->exists($file)) {
                    return File::extension(storage_path("app/$file"));
                } else {
                    return '-';
                }
            })
            ->addColumn('contentFile', function ($object) {
                return route('storage.files', ['filename' => $object->file]);
            })
            ->addColumn('contentDescription', function ($object) {
                return $object->title;
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function traitAttachFiles(Request $request, $id)
    {
        $model = $this->_order->find($id);
        $model->attachedFiles()->sync($request->get('ids', []));

        return response()->json([
            'error' => false,
        ]);
    }

    private function generatePdf($html, $file_path, $file_name, $output = 'F', $download = false, $delete_lp = false)
    {
        try {
            $errorlevel = error_reporting();
            $errorlevel = error_reporting($errorlevel & ~(E_NOTICE | E_WARNING));
            $mpdf = new $this->_mpdf(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                0, // font size - default 0
                '', // default font family
                5, // margin_left
                5, // margin right
                5, // margin top
                5, // margin bottom
                0, // margin header
                0, // margin footer
                'P' // L - landscape, P - portrait
            );

            $stylesheet = '<link href="' . asset('css/cerebelo/application.css') . '" media="all" rel="stylesheet" type="text/css"/>';
            $mpdf->WriteHTML($stylesheet);
            $stylesheet = '<link href="' . asset('modules/cerebeloorders/css/orders/_order_print_area.css') . '" media="all" rel="stylesheet" type="text/css"/>';
            $mpdf->WriteHTML($stylesheet);
            $stylesheet = '<link href="' . asset('modules/cerebeloorders/css/orders/_order_print_pdf.css') . '" media="all" rel="stylesheet" type="text/css"/>';
            $mpdf->WriteHTML($stylesheet);

            $_html = '<!DOCTYPE html>';
            $_html .= '<html>';
            $_html .= ' <head>';
            $_html .= '  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>';
            $_html .= ' </head>';
            $_html .= ' <body>';
            $_html .= $html;
            $_html .= ' </body>';
            $_html .= '</html>';
            $mpdf->WriteHTML($_html, 2, false, true);

            // Exclui a ultima página do PDF normalmente é em branco (adicionado pela class .pagebreak)
            if ($delete_lp === true) {
                $page_count = count($mpdf->pages);
                if ($page_count > 1) {
                    $mpdf->DeletePages($page_count);
                }
            }

            $mpdf->Output($file_path, $output);

            if ((bool) $download === true) {
                return response()->json(route('storage.files', ['filename' => $file_name]));
            }
        } catch (\Exception $ex) {
            return response()->json([
                'error' => true,
                'message' => $ex->getMessage(),
            ]);
        }
    }

    public function traitSendOrderToClient(Request $request, $id)
    {
        $model = $this->_order->with(['client', 'billingClient'])->find($id);

        $sizes = $this->_size->getAll();
        $presents_partnerships = $this->_present_partnership->formSelect();
        $budget_tables_selected = $this->removeDuplicateItems();

        $colors_sizes = $this->colorsSizes($sizes);

        $products_selected = $this->productSelected($model, $budget_tables_selected, $presents_partnerships, $colors_sizes);

        //$html = view(self::MODULE_VIEW . self::FOLDER . '_print_order_client_size', compact('model', 'sizes', 'products_selected'))->render();
        $html = view(self::MODULE_VIEW . self::FOLDER . 'tab_production_order.for_client.print', compact('model', 'sizes', 'products_selected'))->render();

        $email_to = $request->get('email_to');
        $email_type = $request->get('email_type');
        $email_recipients = array_filter(explode(",", $request->get('email_recipients')));
        $email_body = $request->get('email_body');

        $data = $model->toArray();

        $data['content'] = $html;

        $file_name = "tamanhos__orcamento_n_" . $data['order_number'] . ".pdf";
        $file_path = storage_path("app/$file_name");

        // Anexar Tamanhos e Artes
        $this->generatePdf($html, $file_path, $file_name, 'F', false, true);

        // Anexar Orçamento
        $html = $request->get('html');
        $file_name2 = "orcamento_n_" . $data['full_order_number'] . ".pdf";
        $file_path2 = storage_path("app/$file_name2");
        $this->generatePdf($html, $file_path2, $file_name2);

        try {
            $client_data = auth()->user();
            $data['email_body'] = $email_body;

            Mail::send(['html' => self::MODULE_VIEW . self::FOLDER . 'mail'], $data, function ($message) use ($data, $client_data, $file_path, $file_path2, $email_to, $email_type, $email_recipients) {
                //$message->from($client_data->email, $client_data->name);
                if (empty($email_to)) {
                    $message->to(trim($data['client']['email']), trim($data['client']['name']));
                } else {
                    $message->to(trim($email_to), trim($data['client']['name']));
                }
                $message->to(trim($client_data->email), trim($client_data->name))
                    ->replyTo(trim($client_data->email), trim($client_data->name))
                //->bcc('sotnas.lony@emiolo.com', 'Sotnas Leunam - eMiolo.com')
                    ->subject('Tamanhos - Orçamento/Pedido nº ' . $data['full_order_number'])
                    ->attach($file_path, [
                        'as' => "TAMANHOS__ERT_Orçamento_Nº_{$data['full_order_number']}.pdf",
                        'mime' => 'application/pdf',
                    ])
                    ->attach($file_path2, [
                        'as' => "ERT_Orçamento_Nº_{$data['full_order_number']}.pdf",
                        'mime' => 'application/pdf',
                    ]);
                if (count($email_recipients) > 0) {
                    foreach ($email_recipients as $value) {
                        if (!empty(trim($value))) {
                            if ($email_type == "cc") {
                                $message->to(trim($value));
                            } elseif ($email_type == "cco") {
                                $message->bcc(trim($value));
                            }
                        }
                    }
                }
            });

            $this->deleteTmpFiles($file_name);
            $this->deleteTmpFiles($file_name2);

            if (count(Mail::failures()) == 0) {
                return response()->json([
                    'error' => false,
                    'message' => "E-mail enviado com successo.",
                ]);
            } else {
                return response()->json([
                    'error' => true,
                    'message' => "Não foi possível enviar o email.",
                ]);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'error' => true,
                'message' => $ex->getMessage(),
            ]);
        }
    }
}
