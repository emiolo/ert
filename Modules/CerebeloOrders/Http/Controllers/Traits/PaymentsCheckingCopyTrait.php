<?php

namespace Modules\CerebeloOrders\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Storage;
use File;
use Image;
use Validator;
use Modules\CerebeloOrders\Entities\PaymentInstallmentReceipt;

trait PaymentsCheckingCopyTrait
{
    /**
     * Salvar comprovante de pagamento
     * @return Response
     */
    public function traitSaveCheckingCopy(Request $request)
    {
      $id   = $request->get('id');
      $files = $request->file('image', []);

      if(empty($id)) {
        return response()->json(['error' => true, 'message' => 'Você deve salvar as parcelas antes de upar os comprovantes']);
      }

      foreach ($files as $key => $file) {
        $validator = Validator::make(['image' => $file], [
          'image' => 'mimes:jpeg,png,jpg,bmp',
        ]);
        if ($validator->fails())
        {
            return response()->json(['error' => true, 'message' => $validator->errors()->first()]);
        }
      }
      //echo "<pre>";die(var_dump($files));
      $storage = Storage::disk('local');

      $payment_installment = $this->_payment_installment->find($id);
      $receipts = [];

      if(empty($payment_installment)) {
        return response()->json(['error' => true, 'message' => 'Você deve salvar as parcelas antes de upar os comprovantes']);
      }

      foreach ($files as $key => $file) {
        $extension     = $file->getClientOriginalExtension();
        $new_file_name = "orders_checking_copy/" . md5($file->getFilename() . $key . date("dmYHis")) . '.' . $extension;

        $image = Image::make($file->getRealPath());

        //        $image->resize(null, 200, function ($constraint) {
          //            $constraint->aspectRatio();
          //            $constraint->upsize();
          //        });

        if ($storage->put($new_file_name, (string) $image->encode())) {
            $receipt = new PaymentInstallmentReceipt();
            $receipt->file = $new_file_name;

            array_push($receipts, $receipt);
        }
      }

      if(count($receipts) == 0) {
        return response()->json([
          'error'   => true,
          'message' => "Nenhum comprovante foi encontrado."
        ]);
      }

      if($payment_installment->receipts()->saveMany($receipts)) {
        return response()->json([
          'error'   => false,
          'message' => "Comprovante salvo com sucesso."
        ]);
      } else {
        return response()->json([
          'error'   => true,
          'message' => "Não foi possível salvar o comprovante."
        ]);
      }
    }

    /**
     * Editar comporvante de pagamento
     * @return Response
     */
    public function traitEditCheckingCopy($id)
    {
        $file_exists   = false;
        $model = PaymentInstallmentReceipt::allByInstallment($id);

        if (!empty($model)) {
            $file_exists = true;
        }

        return view(self::MODULE_VIEW . self::FOLDER . '_upload_checking_copy', compact('id', 'model', 'file_exists'))->render();
    }

    /**
     * Excluir comporvante de pagamento
     * @return Response
     */
    public function traitDestroyCheckingCopy($id)
    {
      if(PaymentInstallmentReceipt::destroy($id)) {
        return response()->json([
          'error'   => false,
          'message' => "Comprovante excluído com sucesso."
        ]);
      }

      return response()->json([
        'error'   => true,
        'message' => "Não foi possível excluído o comprovante."
      ]);
    }
}