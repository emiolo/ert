<?php

namespace Modules\CerebeloOrders\Http\Controllers\Traits;

use Datatables;
use Illuminate\Http\Request;
use Image;
use Modules\CerebeloOrders\Entities\OrderJoin;
use Modules\CerebeloOrders\Entities\OrderJoinPaymentInstallmentReceipt;
use Storage;
use Validator;

trait JoinOrdersOfClientTrait
{

    private function saveUnifiedOrders($client_id, $order_number, $ids)
    {
        $find = OrderJoin::findByClientAndOrdersId($client_id, $ids);

        if (empty($find)) {
            $user = auth()->user();

            $created = OrderJoin::create([
                'clients_id' => $client_id,
                'order_number' => $order_number,
                'orders_id' => trim($ids),
                'created_by' => $user->id,
            ]);

            return $created;
        }

        return $find;
    }

    public function traitEditJoinOrdersOfClient(Request $request, $id)
    {
        $orders_id = $request->get('orders_id');
        $explode_ids = explode(",", trim($orders_id));

        if (count($explode_ids) == 0) {
            return "Nenhum orçamento foi encontrado.";
        }

        $products_selected = [];

        $sizes = $this->_size->getAll();
        $colors_sizes = $this->colorsSizes($sizes);
        $presents_partnerships = $this->_present_partnership->formSelect();
        $budget_tables_selected = $this->removeDuplicateItems();
        $payments_forms = $this->_payments_form->formSelect2();
        $transports = $this->_transport->formSelect2();

        // Buscar os orçamentos/pedidos
        $orders = $this->_order->with(['client.addresses', 'client.lastUsedAddress', 'briefings', 'ordersProducts', 'auditing'])->whereIn('id', $explode_ids)->get();

        foreach ($orders as $key => $model) {
            $products_selected[] = $this->productSelected($model, $budget_tables_selected, $presents_partnerships, $colors_sizes);
        }

        if (count($products_selected) == 0) {
            return "Orçamento(s) sem produto.";
        }

        $model = $this->_order_join->with('paymentsInstallments')->find($id);

        $client = $orders->first()->client;
        $featured_address = [$client->lastUsedAddress[0]->delivery_address_id, $client->lastUsedAddress[0]->billing_address_id];
        $addresses = (isset($client->addresses) && !empty($client->addresses)) ? $client->addresses : [];

        $urlPdf = route('cerebelo.orders.join', $id) . '?orders_id=' . $orders_id;

        return view(self::MODULE_VIEW . self::FOLDER . 'budget._edit_join_orders', compact('urlPdf', 'featured_address', 'nums_order_unified', 'model_client', 'budget_tables_selected', 'model', 'transports', 'correios_types_freight', 'payments_forms', 'tables_selected', 'products_selected', 'presents_partnerships', 'addresses'))->render();
    }

    public function traitUpdateJoinOrdersOfClient(Request $request, $id)
    {
        $attributes = $request->all();

        $update = $this->_order_join->find($id);

        $transport_freight = (isset($attributes['transport_freight'])) ? $attributes['transport_freight'] : false;
        if ($transport_freight) {
            $attributes['transport_type'] = $transport_freight;
            $attributes['transport_deadline'] = $attributes['transport_deadline_' . $transport_freight];
            $transport_price_ = $attributes['transport_price_' . $transport_freight];
            if ($transport_price_ <= 0) {
                $attributes['transport_type'] = null;
            }
        } else {
            $attributes['transport_type'] = null;
        }

        if (!isset($attributes['to_charge'])) {
            $attributes['to_charge'] = 'N';

            if (empty($attributes['payments_form_id'])) {
                $attributes['payments_form_id'] = null;
            }
        } else {
            $attributes['to_charge'] = 'Y';
        }

        if (!isset($attributes['billing_address_id']) || empty($attributes['billing_address_id'])) {
            $attributes['billing_address_id'] = null;
        }

        $attributes['delivery_boxes'] = [];
        if (isset($attributes['box_quantity']) || count($attributes['box_quantity']) > 0) {
            $arr_boxes = array_map(function ($k, $v) use ($attributes) {
                $box_weight = $attributes['box_weight'][$k];
                $box_dimension = $attributes['box_dimension'][$k];

                return ['quantity' => $v, 'weight' => $box_weight, 'dimension' => $box_dimension];
            }, array_keys($attributes['box_quantity']), array_values($attributes['box_quantity']));

            $attributes['delivery_boxes'] = $arr_boxes;
        }

        $update->fill($attributes);

        if ($update->save()) {
            /*
             * Salvar parcelas da forma de pagamento
             */
            $this->saveJoinPaymentsInstallments($update, $attributes);

            return $this->redirectBackAfterSuccess(trans('messages.success.update'));
        }

        return $this->redirectBackAfterError(trans('messages.error.update'));
    }

    private function saveJoinPaymentsInstallments($model, $attributes)
    {
        $plots = $attributes['plots'];

        $this->_join_payment_instal->destroyAll($model->id);

        if (count($plots) > 0) {
            foreach ($plots as $key => $value) {
                $plots_id = (!isset($attributes['plots_id'][$key])) ? false : $attributes['plots_id'][$key];
                $plots_date = $attributes['plots_date'][$key];
                $plots_expiration_date = $attributes['plots_expiration_date'][$key];
                $plots_status = $attributes['plots_status'][$key];
                $plots_payment_form = $attributes['plots_payment_form'][$key];
                //$plots_file   = (!isset($attributes['plots_file'][$key])) ? NULL : $attributes['plots_file'][$key];

                $_attributes = [
                    'portion' => $key,
                    'value' => $value,
                    'date' => $plots_date,
                    'expiration_date' => $plots_expiration_date,
                    'status' => $plots_status,
                    'payment_form' => $plots_payment_form,
                    //'file'    => $plots_file,
                ];

                $model->paymentsInstallments()->updateOrCreate(['id' => $plots_id], $_attributes);

                /*if (!$plots_id)
            {
            $model->paymentsInstallments()->create($_attributes);
            }
            else
            {
            $payment_installment = $this->_join_payment_instal->find($plots_id);
            $payment_installment->fill($_attributes);
            $payment_installment->save();
            }*/
            }
        }
    }

    /**
     * Excluir comporvante de pagamento
     * @return Response
     */
    public function traitDestroyJoinCheckingCopy($id)
    {
        if (OrderJoinPaymentInstallmentReceipt::destroy($id)) {
            return response()->json([
                'error' => false,
                'message' => "Comprovante excluído com sucesso.",
            ]);
        }

        return response()->json([
            'error' => true,
            'message' => "Não foi possível excluído o comprovante.",
        ]);
    }

    public function traitEditJoinCheckingCopy($id)
    {
        $file_exists = false;
        $model = OrderJoinPaymentInstallmentReceipt::allByInstallment($id);

        if (!empty($model)) {
            $file_exists = true;
        }

        return view(self::MODULE_VIEW . self::FOLDER . 'budget._upload_join_checking_copy', compact('id', 'model', 'file_exists'))->render();
    }

    /**
     * Salvar comprovante de pagamento do Orçamento Unificado
     * @return Response
     */
    public function traitSaveJoinCheckingCopy(Request $request)
    {
        $id = $request->get('id');
        $files = $request->file('image', []);

        if (empty($id)) {
            return response()->json(['error' => true, 'message' => 'Você deve salvar as parcelas antes de upar os comprovantes']);
        }

        foreach ($files as $key => $file) {
            $validator = Validator::make(['image' => $file], [
                'image' => 'mimes:jpeg,png,jpg,bmp',
            ]);
            if ($validator->fails()) {
                return response()->json(['error' => true, 'message' => $validator->errors()->first()]);
            }
        }
        //echo "<pre>";die(var_dump($files));
        $storage = Storage::disk('local');

        $payment_installment = $this->_join_payment_instal->find($id);
        $receipts = [];

        if (empty($payment_installment)) {
            return response()->json(['error' => true, 'message' => 'Você deve salvar as parcelas antes de upar os comprovantes']);
        }

        foreach ($files as $key => $file) {
            $extension = $file->getClientOriginalExtension();
            $new_file_name = "orders_join_checking_copy/" . md5($file->getFilename() . $key . date("dmYHis")) . '.' . $extension;

            $image = Image::make($file->getRealPath());

            //        $image->resize(null, 200, function ($constraint) {
            //            $constraint->aspectRatio();
            //            $constraint->upsize();
            //        });

            if ($storage->put($new_file_name, (string) $image->encode())) {
                $receipt = new OrderJoinPaymentInstallmentReceipt();
                $receipt->file = $new_file_name;

                array_push($receipts, $receipt);
            }
        }

        if (count($receipts) == 0) {
            return response()->json([
                'error' => true,
                'message' => "Nenhum comprovante foi encontrado.",
            ]);
        }

        if ($payment_installment->receipts()->saveMany($receipts)) {
            return response()->json([
                'error' => false,
                'message' => "Comprovante salvo com sucesso.",
            ]);
        } else {
            return response()->json([
                'error' => true,
                'message' => "Não foi possível salvar o comprovante.",
            ]);
        }
    }

    public function traitDestroyJoinOrdersOfClient($id)
    {
        if (OrderJoin::destroy($id)) {
            return response()->json([
                'error' => false,
                'message' => trans('messages.success.destroy'),
            ]);
        }

        return response()->json([
            'error' => true,
            'message' => trans('messages.error.destroy'),
        ]);
    }

    public function traitDatatableOrdersOfClient(Request $request, $client_id)
    {
        $filters = $request->all();

        return Datatables::of($this->_order_join->datatable($client_id, $filters))
            ->addColumn('btnActions', function ($object) {
                $urlEdit = route('cerebelo.orders.join_edit', $object->id) . '?orders_id=' . $object->orders_id;
                $urlPdf = route('cerebelo.orders.join', $object->id) . '?orders_id=' . $object->orders_id;
                $urlDestroy = route('cerebelo.orders.join_destroy', $object->id);

                return '<div class="">
                                        <button title="Editar" data-popup="tooltip" data-original-title="Editar" type="button" onclick="popUpWin(\'' . $urlEdit . '\', \'900\', \'1000\');return false;" class="btn btn-default btn-xs"><i class="icon-compose text-primary"></i></button>
                                        <button title="Gerar PDF" data-popup="tooltip" data-original-title="Gerar PDF" type="button" onclick="popUpWin(\'' . $urlPdf . '\', \'900\', \'1000\');return false;" class="btn btn-default btn-xs"><i class="icon-file-pdf"></i></button>
                                        <button title="Excluir" data-popup="tooltip" data-original-title="Excluir" type="button" onclick="destroyUnified(\'' . $urlDestroy . '\');return false;" class="btn btn-default btn-xs"><i class="icon-trash text-danger"></i></button>
                                    </div>';
            })
            ->addColumn('input_checkbox', function ($object) {
                return \Form::checkbox('order_id[]', $object->id, false, ['class' => 'check_order']);
            })
            ->addColumn('orders_numbers', function ($object) {
                return str_replace(',', '<br/>', $object->order_id_to_num);
            })
            ->addColumn('created_by_user', function ($object) {
                return $object->user->name;
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function traitJoinOrdersOfClient($id, $unified_num = "")
    {
        $orders_id = request()->get('orders_id');
        $explode_ids = explode(",", trim($orders_id));

        if (count($explode_ids) == 0) {
            return;
        }

        $sizes = $this->_size->getAll();

        $products_selected = [];
        $colors_sizes = $this->colorsSizes($sizes);
        $presents_partnerships = $this->_present_partnership->formSelect();
        $budget_tables_selected = $this->removeDuplicateItems();

        // Buscar os orçamentos/pedidos
        $orders = $this->_order->with(['transport', 'client', 'briefings', 'ordersProducts', 'paymentsInstallments', 'auditing', 'deliveryAddress', 'billingAddress'])->whereIn('id', $explode_ids)->get();

        foreach ($orders as $key => $model) {
            $products_selected[] = $this->productSelected($model, $budget_tables_selected, $presents_partnerships, $colors_sizes);
        }

        if (!empty($id) && $id > 0) {
            $model = $this->_order_join->with('paymentsInstallments', 'client', 'deliveryAddress')->find($id);
            $model->group = $orders[0]->group;
        } else {
            $model = $orders[0];
        }

        // Salvar o pedido unificado se não existir
        $saveUnifiedOrders = $this->saveUnifiedOrders($model->clients_id, $unified_num, $orders_id);

        // Alterando alguns dados do Objeto OrderJoin()
        $model->created_at = toSql($saveUnifiedOrders->created_at);
        $model->order_number = $saveUnifiedOrders->order_number;
        $nums_order_unified = $saveUnifiedOrders->order_id_to_num;

        $model_client = $model->client;

        $html = view(self::MODULE_VIEW . self::FOLDER . '_join_orders', compact('nums_order_unified', 'model_client', 'budget_tables_selected', 'model', 'transports', 'correios_types_freight', 'payments_forms', 'tables_selected', 'products_selected', 'presents_partnerships'))->render();

        $client_name = $model_client->name;

        try {
            $errorlevel = error_reporting();
            $errorlevel = error_reporting($errorlevel & ~(E_NOTICE | E_WARNING));
            $mpdf = new $this->_mpdf(
                'utf-8', // mode - default ''
                'A4', // format - A4, for example, default ''
                0, // font size - default 0
                '', // default font family
                5, // margin_left
                5, // margin right
                5, // margin top
                5, // margin bottom
                0, // margin header
                0, // margin footer
                'P' // L - landscape, P - portrait
            );

            $stylesheet = '<link href="' . asset('css/cerebelo/application.css') . '" media="all" rel="stylesheet" type="text/css"/>';
            $mpdf->WriteHTML($stylesheet);
            $stylesheet = '<link href="' . asset('modules/cerebeloorders/css/orders/_order_print_area.css') . '" media="all" rel="stylesheet" type="text/css"/>';
            $mpdf->WriteHTML($stylesheet);
            $stylesheet = '<link href="' . asset('modules/cerebeloorders/css/orders/_order_print_pdf.css') . '" media="all" rel="stylesheet" type="text/css"/>';
            $mpdf->WriteHTML($stylesheet);

            $_html = '<!DOCTYPE html>';
            $_html .= '<html>';
            $_html .= ' <head>';
            $_html .= '  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>';
            //$_html .= '  <title>Orçamentos - ' . $client_name . '</title>';
            $_html .= ' </head>';
            $_html .= ' <body>';
            $_html .= $html;
            $_html .= ' </body>';
            $_html .= '</html>';
            $mpdf->WriteHTML($_html, 2, false, true);

            $mpdf->Output();
        } catch (\Exception $ex) {
            dd($ex);
            //$errorlevel = error_reporting($errorlevel & ~(E_NOTICE|E_WARNING)); // colocar no arquivo vendor\mpdf\mpdf\mpdf.php
        }
    }
}
