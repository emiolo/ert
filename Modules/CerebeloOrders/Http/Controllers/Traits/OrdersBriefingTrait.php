<?php

namespace Modules\CerebeloOrders\Http\Controllers\Traits;

use Illuminate\Http\Request;

trait OrdersBriefingTrait
{

    public function traitUpdateBriefing(Request $request, $id)
    {
        $attributes = $request->all();

        $update = $this->_order->find($id);

        $answers = $attributes['answers'];

        if ($this->saveAnswers($update, $answers))
        {
            $this->saveSectors($id, $attributes);

            if (isset($attributes['variations_position']))
            {
                foreach ($attributes['variations_position'] as $key => $value)
                {
                    \DB::table('orders_products_variations')->where('id', $key)->update(['position' => $value]);
                }
            }

            return $this->redirectBackAfterSuccess(trans('messages.success.update'));
        }

        return $this->redirectBackAfterError(trans('messages.error.update'));
    }

    public function saveAnswers($model, $answers)
    {
        $attributes_answers = [];

        foreach ($answers as $key => $value)
        {
            if (is_array($value))
            {
                if (count($value) == 1)
                {
                    $attributes_answers[$value[0]] = ['value' => ''];
                }
            }
            else
            {
                $attributes_answers[$key] = ['value' => $value];
            }
        }

        if ($model->briefings()->sync($attributes_answers))
        {
            return true;
        }

        return false;
    }

}
