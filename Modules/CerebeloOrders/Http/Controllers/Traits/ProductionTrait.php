<?php

namespace Modules\CerebeloOrders\Http\Controllers\Traits;

use Datatables;
use Illuminate\Http\Request;

trait ProductionTrait
{

    /**
     * Lista dos Pedidos em Produção
     * @return Response
     */
    private function traitIndexProduction(Request $request)
    {
        return view(self::MODULE_VIEW . self::FOLDER . 'production');
    }

    /**
     * Página dos Pedidos em Produção
     * @return Response
     */
    private function traitDatatableProduction(Request $request)
    {
        $filters = [];

        return Datatables::of($this->_order->datatable($filters))
            ->addColumn('btnAction', function ($object) {
                return '<button type="button" class="btn btn-danger btn-float btn-float-lg"><i class="icon-air"></i></button>';
            })
            ->addColumn('infoOrder', function ($object) {
                $urlEdit = "";
                if (!empty($object->have_join)) {
                    $explode = explode('-', $object->have_join);
                    $object_id = $explode[0];
                    $object_orders_id = $explode[1];
                    $urlEdit = route('cerebelo.orders.join_edit', $object_id) . '?orders_id=' . $object_orders_id;
                }
                $client_name = (isset($object->client->company_name)) ? (empty($object->client->company_name) ? $object->client->fantasy_name : $object->client->company_name) : "";
                $status_mark = ($object->urgent == 'Y') ? '<span title="Urgente" data-popup="tooltip" data-original-title="Urgente" class="status-mark border-danger position-left"></span>' : '<span class="status-mark border-blue position-left"></span>';
                $have_join = empty($object->have_join) ? '' : '<a onclick="popUpWin(\'' . $urlEdit . '\', \'900\', \'1000\');return false;" title="" data-popup="tooltip" data-original-title="Visualizar Juntados"><span class="status-mark border-violet-800 position-left"></span></a>';
                return '<div class="media-left">' .
                '  <div><a class="text-default text-semibold" href="' . route('cerebelo.' . self::FOLDER . 'edit', $object->id) . '">' . $client_name . '</a></div>' .
                '  <div class="text-size-small">' . $have_join . '<a class="text-default text-semibold" href="' . route('cerebelo.' . self::FOLDER . 'edit', $object->id) . '">' . $status_mark . $object->order_number . '</a></div>' .
                    '</div>';
            })
            ->escapeColumns([])
            ->make(true);
    }

}
