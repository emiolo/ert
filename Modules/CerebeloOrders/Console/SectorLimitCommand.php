<?php

namespace Modules\CerebeloOrders\Console;

use Illuminate\Console\Command;
use Log;
use Modules\CerebeloOrders\Entities\Order;
use Modules\CerebeloSettings\Entities\AlertSys;

class SectorLimitCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:expired-sector';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verificar os pedidos que extrapolaram a data em cada Setor e notificar os responsáveis.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        try {
            $order = Order::expiredInSectors();

            if (count($order) == 0 || empty($order)) {
                $this->comment("Nenhum pedido que extrapolou o prazo por setor foi encontrado.");
                return;
            }

            $admin_user_id = 28; // FABRICIO VIANA (fabricio@ertuniformes.com.br)
            $sector_admin = Sector::withAlias(Sector::ADMIN)->first();
            if (!empty($sector_admin->responsible_id)) {
                $admin_user_id = $sector_admin->responsible_id;
            }

            foreach ($order as $key => $value) {
                $id = $value->id;
                $order_number = $value->order_number;
                $name = $value->name;
                $sector_name = $value->sector_name;
                $end_datetime = $value->end_datetime;

                $content = "O pedido nº <span class='text-bold'>$order_number</span> esta em atraso no setor $sector_name ($name) data limite $end_datetime, <a href='" . route('cerebelo.orders.edit', $id) . "#justified-badges-tab2'>ver pedido</a>.";

                $attributes = ['users_id' => $admin_user_id, 'content' => $content, 'read' => 'N'];

                if (AlertSys::checkDataExisting($admin_user_id . md5($content)) == 0) {
                    AlertSys::create($attributes);
                }
            }

            $this->comment("Os pedidos que extrapolaram o prazo por setor foram enviados para os Administradores.");
        } catch (\Exception $ex) {
            Log::error("Erro ao criar um Alerta do Sistema: " . $ex->getMessage() . " :: " . $ex->getFile());
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            //['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            //['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
