<?php

namespace Modules\CerebeloOrders\Console;

use Event;
use Illuminate\Console\Command;
use Log;
use Modules\CerebeloOrders\Entities\Order;
use Modules\CerebeloSettings\Entities\AlertSys;
use Modules\CerebeloSettings\Entities\Sector;
use Modules\CerebeloSettings\Events\AlertSysEvent;

class OrderAcceptLimitCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:order-accept-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notifica ao administrador os pedidos que ultrapassaram as 12 hrs limite para aceitar um pedido em um determinado setor.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        try {
            $order = Order::orderAcceptExpired();

            if (count($order) == 0 || empty($order)) {
                $this->comment("Nenhum pedido que extrapolou o limite de aceite no setor foi encontrado.");
                return;
            }

            $admin_user_id = 28; // FABRICIO VIANA (fabricio@ertuniformes.com.br)
            $sector_admin = Sector::withAlias(Sector::ADMIN)->first();
            if (!empty($sector_admin->responsible_id)) {
                $admin_user_id = $sector_admin->responsible_id;
            }

            foreach ($order as $key => $value) {
                $id = $value->id;
                $order_number = $value->order_number;
                $name = $value->name;
                $sector_name = $value->sector_name;
                $responsible_id = $value->responsible_id;

                $content = "<span class='label bg-grey-400'>ATRASO</span> O pedido nº <span class='text-bold'>$order_number</span> ainda não foi aceitado pelo responsável <span class='text-bold'>$name</span> do setor $sector_name, <a href='" . route('cerebelo.orders.edit', $id) . "#justified-badges-tab2'>ver pedido</a>.";
                if (empty($responsible_id)) {
                    $content .= "<br/>";
                    $content .= "<em>O responsável do setor não pode ser notificado porque ainda não foi definido no cadastro de Setores.</em>";
                }

                if (AlertSys::checkDataExisting($admin_user_id . md5($content)) == 0) {
                    Event::fire(new AlertSysEvent($admin_user_id, $content));
                }

                if (!empty($responsible_id)) {
                    if (AlertSys::checkDataExisting($responsible_id . md5($content)) == 0) {
                        Event::fire(new AlertSysEvent($responsible_id, $content));
                    }
                }
            }

            $this->comment("Os pedidos que extrapolaram o prazo por setor foram enviados para os Administradores.");
        } catch (\Exception $ex) {
            Log::error("Erro ao criar um Alerta do Sistema: " . $ex);
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            //['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            //['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
