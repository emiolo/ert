var route = document.getElementById("route-datatable").getAttribute("route-datatable");

var oTable = $('#datatable').DataTable({
    "pageLength": 10,
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "columnDefs": [{"targets": [], "orderable": false}],
    "ajax": {
        "url": route
    },
    "order": [[2, "asc"]],
    "columns": [
        {data: 'btnList', name: 'btnList', orderable: false, searchable: false},
        {data: 'id', name: 'id'},
        {data: 'name', name: 'name'},
        {data: 'type', name: 'type'},
        {data: 'link', name: 'link'},
        {data: 'phone', name: 'phone'},
        {data: 'created_at', name: 'created_at'},
    ]
});