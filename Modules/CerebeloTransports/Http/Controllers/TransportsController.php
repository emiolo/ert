<?php

namespace Modules\CerebeloTransports\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\CerebeloTransports\Http\Controllers\CerebeloController as Controller;
use Modules\CerebeloTransports\Http\Requests\TransportRequest;
use Datatables;
use Modules\CerebeloTransports\Entities\Transport;
use JsValidator;

class TransportsController extends Controller {

    const FOLDER = "transports.";

    protected $_transport;

    public function __construct(Transport $transport) {
        $this->_transport = $transport;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        return view(self::MODULE_VIEW . self::FOLDER . 'index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        $validator = JsValidator::formRequest(new TransportRequest, '#formCreateTransport');

        return view(self::MODULE_VIEW . self::FOLDER . 'create', compact('validator'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(TransportRequest $request) {
        $attributes = $request->all();
        $create = $this->_transport->create($attributes);
        if ($create) {
            return $this->redirectBackAfterSuccess(trans('messages.success.create'));
        }

        return $this->redirectBackAfterError(trans('messages.error.create'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return view(self::MODULE_VIEW . self::FOLDER . 'show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id) {
        $validator = JsValidator::formRequest(new TransportRequest, '#formEditTransport');
        $model = $this->_transport->find($id);

        return view(self::MODULE_VIEW . self::FOLDER . 'edit', compact('model', 'validator'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(TransportRequest $request, $id) {
        $attributes = $request->all();
        $update = $this->_transport->find($id);
        $update->fill($attributes);
        if ($update->save()) {
            return $this->redirectAfterSuccess(trans('messages.success.create'), 'cerebelo.' . self::FOLDER . 'index');
        }

        return $this->redirectBackAfterError(trans('messages.error.create'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id) {
        if ($this->_transport->destroy($id)) {
            return $this->redirectBackAfterSuccess(trans('messages.success.destroy'));
        }

        return $this->redirectBackAfterError(trans('messages.error.destroy'));
    }

    /**
     * Lista dos Setores
     * @return Response
     */
    public function datatable() {
        return Datatables::of($this->_transport->datatable())
                        ->addColumn('btnList', function ($object) {
                            $array = [
                                'edit' => [
                                    'route' => route('cerebelo.' . self::FOLDER . 'edit', $object->id),
                                ],
                                'destroy' => [
                                    'route' => route('cerebelo.' . self::FOLDER . 'destroy', $object->id),
                                ],
                            ];
                            return iconsList($array);
                        })
                        ->escapeColumns([])
                        ->make(true);
    }

}
