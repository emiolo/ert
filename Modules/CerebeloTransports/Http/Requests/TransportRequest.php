<?php

namespace Modules\CerebeloTransports\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransportRequest extends FormRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'type' => 'required|max:50',
            'name' => 'required|max:150|unique:transports,name' . ((isset($this->id) && !empty($this->id)) ? ',' . $this->id : ''),
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

}
