<?php

Route::group(['middleware' => ['web', 'cerebelo.authenticated'], 'prefix' => 'cerebelo', 'namespace' => 'Modules\CerebeloTransports\Http\Controllers'], function() {

    Route::group(['middleware' => ['cerebelo.permission']], function() {

        Route::group(['prefix' => 'transports'], function() {
            Route::get('/', 'TransportsController@index')->name('cerebelo.transports.index');
            Route::get('/listing', 'TransportsController@datatable')->name('cerebelo.transports.datatable');
            Route::get('/create', 'TransportsController@create')->name('cerebelo.transports.create');
            Route::post('/', 'TransportsController@store')->name('cerebelo.transports.store');
            Route::get('/show/{id}', 'TransportsController@show')->name('cerebelo.transports.show');
            Route::get('/edit/{id}', 'TransportsController@edit')->name('cerebelo.transports.edit');
            Route::match(['put', 'patch'], '/update/{id}', 'TransportsController@update')->name('cerebelo.transports.update');
            Route::delete('/delete/{id}', 'TransportsController@destroy')->name('cerebelo.transports.destroy');
        });
    });
});
