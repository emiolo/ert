<?php

return [
    'link' => 'Link',
    'comments' => 'Comentário',
    'transport' => 'Transportadora',
    'transports_type' => [
        'correios' => 'Correios',
        'transportadora' => 'Transportadora',
        'motoboy' => 'Motoboy/Entrega Pessoal',
    ]
];
