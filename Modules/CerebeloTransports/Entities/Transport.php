<?php

namespace Modules\CerebeloTransports\Entities;

use Modules\CerebeloSettings\Entities\BaseModel as Model;
use DB;

class Transport extends Model {

    protected $table = 'transports';
    protected $fillable = [
        'type',
        'name',
        'link',
        'phone',
        'comments',
    ];
    protected $appends = [];

    /*
     * Relationships
     */

    /*
     * Attributes
     */


    /*
     * Scopes
     */
    
    public function scopeSelectNameAndType($query) {
        return $query->select('id', DB::raw('CONCAT(transports.name, " (", transports.type, ")") AS name'));
    }


    /*
     * Others
     */
    
    public function formSelect() {
        $return = $this->selectNameAndType()->pluck('name', 'id')->prepend('Selecione', '');
        return $return;
    }
    
    public function formSelect2() {
        $return = $this->select('id', DB::raw('CONCAT(transports.name, " (", transports.type, ")") AS name'), 'type', 'link', 'phone', 'comments')->get()->toArray();
        return $return;
    }

    public function datatable() {
        return $this->select($this->table . '.*');
    }

}
