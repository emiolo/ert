<?php

namespace Modules\CerebeloTablesValues\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\CerebeloTablesValues\Http\Controllers\CerebeloController as Controller;
use Datatables;
use Modules\CerebeloProducts\Entities\Product;
use Modules\CerebeloTable\Entities\PriceTable;

class TablesValuesController extends Controller
{

    const FOLDER = self::MODULE_VIEW . "tablesvalues.";

    protected $_product;
    protected $_price_table;

    public function __construct(Product $product, PriceTable $_price_table)
    {
        $this->_product     = $product;
        $this->_price_table = $_price_table;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $column_headers = $this->_price_table->formSelect2();

        return view(self::FOLDER . 'index', compact('column_headers'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $product_id   = $request->get('product_id');
        $product_name = $request->get('product_name');
        $table_id     = $request->get('table_id');
        $table_name   = $request->get('table_name');
        $new_value    = empty($request->get('new_value')) ? 0 : $request->get('new_value');
        $old_value    = $request->get('old_value');

        $update = $this->_product->find($product_id);

        if ($update->productsPrices()->sync([$table_id => ['value' => moneyUnFormat($new_value)]], false))
        {
            return response()->json([
                        'error'   => false,
                        'message' => "Valor do produto '$product_name' e tabela '$table_name' foi atualizado com sucesso."
            ]);
        }
        else
        {
            return response()->json([
                        'error'   => true,
                        'message' => "Houve um erro ao atualizar o valor do produto '$product_name' da tabela '$table_name'."
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function datatable($product = "")
    {
        $column_headers = $this->_price_table->formSelect2();

        $datatable = Datatables::of($this->_product->datatable($product));
        $datatable->addColumn('productRowHeader', function ($object) {
            return $object->name;
        });
        foreach ($column_headers as $key => $value)
        {
            $datatable->addColumn("inputCell$key", function ($object) use($key, $value) {
                $productsPrices = $object->productsPrices()->where('price_table_id', $key)->first();
                $product_id     = $object->id;
                $product_name   = $object->name;
                $table_id       = $key;
                $table_name     = $value;

                $v = "";
                if (isset($productsPrices->pivot))
                {
                    $v = moneyFormat($productsPrices->pivot->value);
                }

                return \Form::text('', $v, ['data-popup' => 'tooltip', 'data-placement' => 'left', 'title' => 'Tecle ENTER para salvar', 'class' => "form-control input-xs edit-value-cell-new decimal", 'product_name' => $product_name, 'table_name' => $table_name, 'product_id' => $product_id, 'table_id' => $table_id, 'maxlength' => '6', 'placeholder' => 'R$']) .
                        \Form::hidden('', $v, ['id' => "edit-value-cell-old-$product_id-$table_id"]);
            });
        }
        return $datatable->escapeColumns([])->make(true);
    }

}
