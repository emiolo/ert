<?php

Route::group(['middleware' => ['web', 'cerebelo.authenticated'], 'prefix' => 'cerebelo', 'namespace' => 'Modules\CerebeloTablesValues\Http\Controllers'], function() {
    Route::group(['middleware' => ['cerebelo.permission']], function() {

        Route::group(['prefix' => 'tables-values'], function() {
            Route::get('/', 'TablesValuesController@index')->name('cerebelo.tablesvalues.index');
            Route::get('/listing/{product?}', 'TablesValuesController@datatable')->name('cerebelo.tablesvalues.datatable');
            //Route::get('/create', 'TablesValuesController@create')->name('cerebelo.tablesvalues.create');
            //Route::post('/', 'TablesValuesController@store')->name('cerebelo.tablesvalues.store');
            //Route::get('/show/{id}', 'TablesValuesController@show')->name('cerebelo.tablesvalues.show');
            //Route::get('/edit/{id}', 'TablesValuesController@edit')->name('cerebelo.tablesvalues.edit');
            Route::match(['put', 'patch'], '/update', 'TablesValuesController@update')->name('cerebelo.tablesvalues.update');
            //Route::delete('/delete/{id}', 'TablesValuesController@destroy')->name('cerebelo.tablesvalues.destroy');
        });
    });
});
