var route = document.getElementById("main-routes").getAttribute("route-datatable");
var routeUpdate = document.getElementById("main-routes").getAttribute("route-update");

var jsonColumnHeaders = document.getElementById("main-routes").getAttribute("json-column-headers");
var $json_parse = JSON.parse(jsonColumnHeaders);
var $columns = [{ data: 'productRowHeader', name: 'name' }];

if (Object.keys($json_parse).length > 0) {
    var $json = sortProperties($json_parse);
    $.each($json, function (k, v) {
        $columns.push({ data: 'inputCell' + v[0], name: 'inputCell' + v[0], orderable: false, searchable: false });
    });
}

var oTable = $('#datatable').DataTable({
    "pageLength": 10,
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "columnDefs": [{ "targets": [], "orderable": false }],
    "fixedColumns": {
        leftColumns: 1
    },
    "ajax": {
        "url": route
    },
    "order": [[0, "asc"]],
    "columns": $columns,
    "scrollCollapse": true,
    //"scrollY": '480px',
    "scrollXInner": "100%",
    "scrollX": true,
});

function sortProperties(obj) {
    // convert object into array
    var sortable = [];
    for (var key in obj)
        if (obj.hasOwnProperty(key))
            sortable.push([key, obj[key]]); // each item is an array in format [key, value]

    // sort items by value
    sortable.sort(function (a, b) {
        var x = a[1].toLowerCase(),
            y = b[1].toLowerCase();
        return x < y ? -1 : x > y ? 1 : 0;
    });
    return sortable; // array in format [ [ key1, val1 ], [ key2, val2 ], ... ]
}

$(document).ajaxStop(function () {
    $(".edit-value-cell-new").off('keypress').on('keypress', function (e) {
        if (e.which === 13) { // Salvar ao pressionar ENTER
            var $this = $(this);
            var product_id = $this.attr('product_id');
            var product_name = $this.attr('product_name');
            var table_id = $this.attr('table_id');
            var table_name = $this.attr('table_name');
            var new_value = $this.val();
            var old_value = $("#edit-value-cell-old-" + product_id + "-" + table_id).val();

            $.ajax({
                url: routeUpdate,
                type: 'PUT',
                dataType: 'json',
                data: {
                    product_id: product_id,
                    product_name: product_name,
                    table_id: table_id,
                    table_name: table_name,
                    new_value: new_value,
                    old_value: old_value
                },
                success: function (response) {
                    if (response.error === true) {
                        new PNotify({
                            title: 'Atenção',
                            text: response.message,
                            addclass: 'bg-warning alert-styled-right'
                        });
                    } else {
                        new PNotify({
                            title: 'Sucesso',
                            text: response.message,
                            addclass: 'bg-success alert-styled-right'
                        });
                    }
                },
                error: function (response) {

                }
            });
        }
    });
});