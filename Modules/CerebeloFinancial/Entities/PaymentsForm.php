<?php

namespace Modules\CerebeloFinancial\Entities;

use Modules\CerebeloSettings\Entities\BaseModel as Model;

class PaymentsForm extends Model
{

    protected $table = 'payments_forms';
    protected $fillable = [
        'name',
        'max_plots',
        'as',
    ];

    /*
     * Relationships
     */

    /*
     * Scopes
     */

    /*
     * Others
     */

    public function formSelect2()
    {
        $return = $this->select('id', 'name', 'max_plots', 'as')->get()->toArray();
        return $return;
    }

}
