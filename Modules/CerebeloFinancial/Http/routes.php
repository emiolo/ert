<?php

Route::group(['middleware' => ['web', 'cerebelo.authenticated'], 'prefix' => 'cerebelo', 'namespace' => 'Modules\CerebeloFinancial\Http\Controllers'], function() {

    Route::group(['middleware' => ['cerebelo.permission']], function() {

        Route::group(['prefix' => 'cashier'], function() {
            Route::get('/', 'CashierController@index')->name('cerebelo.cashier.index');
            Route::get('/listing', 'CashierController@datatable')->name('cerebelo.cashier.datatable');
            Route::get('/create', 'CashierController@create')->name('cerebelo.cashier.create');
            Route::post('/', 'CashierController@store')->name('cerebelo.cashier.store');
            Route::get('/show/{id}', 'CashierController@show')->name('cerebelo.cashier.show');
            Route::get('/edit/{id}', 'CashierController@edit')->name('cerebelo.cashier.edit');
            Route::match(['put', 'patch'], '/update/{id}', 'CashierController@update')->name('cerebelo.cashier.update');
            Route::delete('/delete/{id}', 'CashierController@destroy')->name('cerebelo.cashier.destroy');
        });

        Route::group(['prefix' => 'billet-settings'], function() {
            Route::get('/', 'BilletSettingsController@index')->name('cerebelo.billet_settings.index');
            Route::get('/listing', 'BilletSettingsController@datatable')->name('cerebelo.billet_settings.datatable');
            Route::get('/create', 'BilletSettingsController@create')->name('cerebelo.billet_settings.create');
            Route::post('/', 'BilletSettingsController@store')->name('cerebelo.billet_settings.store');
            Route::get('/show/{id}', 'BilletSettingsController@show')->name('cerebelo.billet_settings.show');
            Route::get('/edit/{id}', 'BilletSettingsController@edit')->name('cerebelo.billet_settings.edit');
            Route::match(['put', 'patch'], '/update/{id}', 'BilletSettingsController@update')->name('cerebelo.billet_settings.update');
            Route::delete('/delete/{id}', 'BilletSettingsController@destroy')->name('cerebelo.billet_settings.destroy');
        });

        Route::group(['prefix' => 'bills-to-pay'], function() {
            Route::get('/', 'BillsToPayController@index')->name('cerebelo.bills_to_pay.index');
            Route::get('/listing', 'BillsToPayController@datatable')->name('cerebelo.bills_to_pay.datatable');
            Route::get('/create', 'BillsToPayController@create')->name('cerebelo.bills_to_pay.create');
            Route::post('/', 'BillsToPayController@store')->name('cerebelo.bills_to_pay.store');
            Route::get('/show/{id}', 'BillsToPayController@show')->name('cerebelo.bills_to_pay.show');
            Route::get('/edit/{id}', 'BillsToPayController@edit')->name('cerebelo.bills_to_pay.edit');
            Route::match(['put', 'patch'], '/update/{id}', 'BillsToPayController@update')->name('cerebelo.bills_to_pay.update');
            Route::delete('/delete/{id}', 'BillsToPayController@destroy')->name('cerebelo.bills_to_pay.destroy');
        });

        Route::group(['prefix' => 'bills-to-receive'], function() {
            Route::get('/', 'BillsToReceiveController@index')->name('cerebelo.bills_to_receive.index');
            Route::get('/listing', 'BillsToReceiveController@datatable')->name('cerebelo.bills_to_receive.datatable');
            Route::get('/create', 'BillsToReceiveController@create')->name('cerebelo.bills_to_receive.create');
            Route::post('/', 'BillsToReceiveController@store')->name('cerebelo.bills_to_receive.store');
            Route::get('/show/{id}', 'BillsToReceiveController@show')->name('cerebelo.bills_to_receive.show');
            Route::get('/edit/{id}', 'BillsToReceiveController@edit')->name('cerebelo.bills_to_receive.edit');
            Route::match(['put', 'patch'], '/update/{id}', 'BillsToReceiveController@update')->name('cerebelo.bills_to_receive.update');
            Route::delete('/delete/{id}', 'BillsToReceiveController@destroy')->name('cerebelo.bills_to_receive.destroy');
        });

        Route::group(['prefix' => 'accounts'], function() {
            Route::get('/', 'AccountsController@index')->name('cerebelo.accounts.index');
            Route::get('/listing', 'AccountsController@datatable')->name('cerebelo.accounts.datatable');
            Route::get('/create', 'AccountsController@create')->name('cerebelo.accounts.create');
            Route::post('/', 'AccountsController@store')->name('cerebelo.accounts.store');
            Route::get('/show/{id}', 'AccountsController@show')->name('cerebelo.accounts.show');
            Route::get('/edit/{id}', 'AccountsController@edit')->name('cerebelo.accounts.edit');
            Route::match(['put', 'patch'], '/update/{id}', 'AccountsController@update')->name('cerebelo.accounts.update');
            Route::delete('/delete/{id}', 'AccountsController@destroy')->name('cerebelo.accounts.destroy');
        });

        Route::group(['prefix' => 'providers'], function() {
            Route::get('/', 'ProvidersController@index')->name('cerebelo.providers.index');
            Route::get('/listing', 'ProvidersController@datatable')->name('cerebelo.providers.datatable');
            Route::get('/create', 'ProvidersController@create')->name('cerebelo.providers.create');
            Route::post('/', 'ProvidersController@store')->name('cerebelo.providers.store');
            Route::get('/show/{id}', 'ProvidersController@show')->name('cerebelo.providers.show');
            Route::get('/edit/{id}', 'ProvidersController@edit')->name('cerebelo.providers.edit');
            Route::match(['put', 'patch'], '/update/{id}', 'ProvidersController@update')->name('cerebelo.providers.update');
            Route::delete('/delete/{id}', 'ProvidersController@destroy')->name('cerebelo.providers.destroy');
        });

        Route::group(['prefix' => 'officials'], function() {
            Route::get('/', 'OfficialsController@index')->name('cerebelo.officials.index');
            Route::get('/listing', 'OfficialsController@datatable')->name('cerebelo.officials.datatable');
            Route::get('/create', 'OfficialsController@create')->name('cerebelo.officials.create');
            Route::post('/', 'OfficialsController@store')->name('cerebelo.officials.store');
            Route::get('/show/{id}', 'OfficialsController@show')->name('cerebelo.officials.show');
            Route::get('/edit/{id}', 'OfficialsController@edit')->name('cerebelo.officials.edit');
            Route::match(['put', 'patch'], '/update/{id}', 'OfficialsController@update')->name('cerebelo.officials.update');
            Route::delete('/delete/{id}', 'OfficialsController@destroy')->name('cerebelo.officials.destroy');
        });

        Route::group(['prefix' => 'plans'], function() {
            Route::get('/', 'PlansController@index')->name('cerebelo.plans.index');
            Route::get('/listing', 'PlansController@datatable')->name('cerebelo.plans.datatable');
            Route::get('/create', 'PlansController@create')->name('cerebelo.plans.create');
            Route::post('/', 'PlansController@store')->name('cerebelo.plans.store');
            Route::get('/show/{id}', 'PlansController@show')->name('cerebelo.plans.show');
            Route::get('/edit/{id}', 'PlansController@edit')->name('cerebelo.plans.edit');
            Route::match(['put', 'patch'], '/update/{id}', 'PlansController@update')->name('cerebelo.plans.update');
            Route::delete('/delete/{id}', 'PlansController@destroy')->name('cerebelo.plans.destroy');
        });

        Route::group(['prefix' => 'remuneration-and-benefits'], function() {
            Route::get('/', 'PlansController@index')->name('cerebelo.remunerationandbenefits.index');
            Route::get('/listing', 'PlansController@datatable')->name('cerebelo.remunerationandbenefits.datatable');
            Route::get('/create', 'PlansController@create')->name('cerebelo.remunerationandbenefits.create');
            Route::post('/', 'PlansController@store')->name('cerebelo.remunerationandbenefits.store');
            Route::get('/show/{id}', 'PlansController@show')->name('cerebelo.remunerationandbenefits.show');
            Route::get('/edit/{id}', 'PlansController@edit')->name('cerebelo.remunerationandbenefits.edit');
            Route::match(['put', 'patch'], '/update/{id}', 'PlansController@update')->name('cerebelo.remunerationandbenefits.update');
            Route::delete('/delete/{id}', 'PlansController@destroy')->name('cerebelo.remunerationandbenefits.destroy');
        });
    });
});
