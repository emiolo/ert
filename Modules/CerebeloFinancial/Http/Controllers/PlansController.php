<?php

namespace Modules\CerebeloFinancial\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\CerebeloFinancial\Http\Controllers\CerebeloController as Controller;
use Modules\CerebeloOrders\Http\Requests\OrderRequest;
use Datatables;

class PlansController extends Controller {

    const FOLDER = "plans.";

    public function __construct() {
        
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        return view(self::MODULE_VIEW . self::FOLDER . 'index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view(self::MODULE_VIEW . self::FOLDER . 'create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(OrderRequest $request) {
        $attributes = $request->all();
        dd($attributes);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return view(self::MODULE_VIEW . self::FOLDER . 'show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id) {
        return view(self::MODULE_VIEW . self::FOLDER . 'edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(OrderRequest $request, $id) {
        $attributes = $request->all();
        dd($attributes);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id) {
        return $this->redirectBackAfterError(trans('messages.error.destroy'));
    }

    /**
     * Lista dos Setores
     * @return Response
     */
    public function datatable() {
        
    }

}
