<?php

namespace Modules\CerebeloCompanies\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cnpj'               => 'required|max:18|cnpj|unique:companies,cnpj' . ((isset($this->id) && !empty($this->id)) ? ',' . $this->id : ''),
            'state_registration' => 'required|max:17',
            'corporate_name'     => 'required|max:150',
            'fantasy_name'       => 'required|max:150',
            'phone'              => 'required|max:15|phone_with_ddd',
            'email'              => 'required|email',
            'logo'               => ((isset($this->id) && !empty($this->id)) ? '' : 'required|') . 'image|mimes:jpeg,jpg,png',
            'type.*'             => 'required',
            'cep.*'              => 'required:max:9',
            'street.*'           => 'required:max:250',
            'number.*'           => 'required:max:50',
            'neighborhood.*'     => 'required:max:150',
            'city.*'             => 'required:max:150',
            'state.*'            => 'required:max:2',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'type.*.required'         => 'O campo Tipo é obrigatório',
            'cep.*.required'          => 'O campo CEP é obrigatório.',
            'street.*.required'       => 'O campo Logradouro é obrigatório',
            'number.*.required'       => 'O campo Número é obrigatório',
            'neighborhood.*.required' => 'O campo Bairro é obrigatório',
            'city.*.required'         => 'O campo Cidade é obrigatório',
            'state.*.required'        => 'O campo Estado é obrigatório',
        ];
    }

}
