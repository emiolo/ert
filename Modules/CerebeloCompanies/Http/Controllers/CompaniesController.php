<?php

namespace Modules\CerebeloCompanies\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\CerebeloCompanies\Http\Controllers\CerebeloController as Controller;
use Datatables;
use Modules\CerebeloCompanies\Http\Requests\CompanyRequest;
use Modules\CerebeloCompanies\Entities\Company;
use Storage;
use Image;
use Modules\CerebeloSettings\Entities\Address;

class CompaniesController extends Controller
{

    const FOLDER = "companies.";

    protected $_company;
    protected $_address;

    public function __construct(Company $company, Address $address)
    {
        $this->_company = $company;
        $this->_address = $address;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view(self::MODULE_VIEW . self::FOLDER . 'index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view(self::MODULE_VIEW . self::FOLDER . 'create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CompanyRequest $request)
    {
        $attributes = $request->all();
        $logo       = $request->file('logo');

        if (!empty($logo))
        {
            $storage = Storage::disk('local');

            $extension     = $logo->getClientOriginalExtension();
            $new_file_name = "companies/" . md5($logo->getFilename()) . '.' . $extension;

            // open file a image resource
            $image = Image::make($logo->getRealPath());
            /* $width  = $attributes['dataWidth'];
              $height = $attributes['dataHeight'];
              $x      = $attributes['dataX'];
              $y      = $attributes['dataY']; */
            // crop image | (int $width, int $height, [int $x, int $y])
            /* $image->crop($width, $height, $x, $y);
              $image->resize(null, 200, function ($constraint) {
              $constraint->aspectRatio();
              $constraint->upsize();
              }); */

            if ($storage->put($new_file_name, (string) $image->encode()))
            {
                $attributes['logo'] = $new_file_name;
            }
            else
            {
                return $this->redirectBackAfterError(trans('messages.error.create'));
            }
        }

        $create = $this->_company->create($attributes);

        if ($create)
        {
            // Salvar endereço
            if (isset($attributes['cep']))
            {
                foreach ($attributes['cep'] as $key => $value)
                {
                    $cep          = $value;
                    $type         = $attributes['type'][$key];
                    $table        = $attributes['table'][$key];
                    $street       = $attributes['street'][$key];
                    $number       = $attributes['number'][$key];
                    $complement   = $attributes['complement'][$key];
                    $neighborhood = $attributes['neighborhood'][$key];
                    $city         = $attributes['city'][$key];
                    $state        = $attributes['state'][$key];
                    $local        = $attributes['local'][$key];

                    $addresses = array(
                        'cep'          => $cep,
                        'type'         => $type,
                        'table'        => $table,
                        'street'       => $street,
                        'number'       => $number,
                        'complement'   => $complement,
                        'neighborhood' => $neighborhood,
                        'city'         => $city,
                        'state'        => $state,
                        'local'        => $local,
                    );

                    $address = new $this->_address();
                    $address->fill($addresses);
                    $create->address()->save($address);
                }
            }

            return $this->redirectBackAfterSuccess(trans('messages.success.create'));
        }

        return $this->redirectBackAfterError(trans('messages.error.create'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view(self::MODULE_VIEW . self::FOLDER . 'show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $model = $this->_company->with('address')->find($id);

        return view(self::MODULE_VIEW . self::FOLDER . 'edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(CompanyRequest $request, $id)
    {
        $attributes = $request->all();
        $logo       = $request->file('logo');

        $update = $this->_company->find($id);

        if (!empty($logo))
        {
            $old_photo = $update->logo;
            $storage   = Storage::disk('local');

            if (!empty($old_photo))
            {
                if ($storage->exists($old_photo))
                {
                    $storage->delete($old_photo);
                }
            }

            $extension     = $logo->getClientOriginalExtension();
            $new_file_name = "companies/" . md5($logo->getFilename()) . '.' . $extension;

            // open file a image resource
            $image = Image::make($logo->getRealPath());
            /* $width  = $attributes['dataWidth'];
              $height = $attributes['dataHeight'];
              $x      = $attributes['dataX'];
              $y      = $attributes['dataY']; */
            // crop image | (int $width, int $height, [int $x, int $y])
            /* $image->crop($width, $height, $x, $y);
              $image->resize(null, 200, function ($constraint) {
              $constraint->aspectRatio();
              $constraint->upsize();
              }); */

            if ($storage->put($new_file_name, (string) $image->encode()))
            {
                $attributes['logo'] = $new_file_name;
            }
            else
            {
                return $this->redirectBackAfterError(trans('messages.error.update'));
            }
        }

        $update->fill($attributes);

        if ($update->save())
        {
            // Salvar endereço
            if (isset($attributes['cep']))
            {
                foreach ($attributes['cep'] as $key => $value)
                {
                    $cep          = $value;
                    $type         = $attributes['type'][$key];
                    $table        = $attributes['table'][$key];
                    $street       = $attributes['street'][$key];
                    $number       = $attributes['number'][$key];
                    $complement   = $attributes['complement'][$key];
                    $neighborhood = $attributes['neighborhood'][$key];
                    $city         = $attributes['city'][$key];
                    $state        = $attributes['state'][$key];
                    $local        = $attributes['local'][$key];

                    $addresses = array(
                        'cep'          => $cep,
                        'type'         => $type,
                        'table'        => $table,
                        'street'       => $street,
                        'number'       => $number,
                        'complement'   => $complement,
                        'neighborhood' => $neighborhood,
                        'city'         => $city,
                        'state'        => $state,
                        'local'        => $local,
                    );

                    $address = new $this->_address();
                    $address->fill($addresses);
                    $update->address()->save($address);
                }
            }

            return $this->redirectAfterSuccess(trans('messages.success.create'), 'cerebelo.' . self::FOLDER . 'index');
        }

        return $this->redirectBackAfterError(trans('messages.error.create'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->_company->destroy($id))
        {
            return $this->redirectBackAfterSuccess(trans('messages.success.destroy'));
        }

        return $this->redirectBackAfterError(trans('messages.error.destroy'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function datatable()
    {
        return Datatables::of($this->_company->datatable())
                        ->addColumn('btnList', function ($object) {
                            $array = [
                                'edit'    => [
                                    'route' => route('cerebelo.' . self::FOLDER . 'edit', $object->id),
                                ],
                                'destroy' => [
                                    'route' => route('cerebelo.' . self::FOLDER . 'destroy', $object->id),
                                ],
                            ];
                            return iconsList($array);
                        })
                        ->escapeColumns([])
                        ->make(true);
    }

}
