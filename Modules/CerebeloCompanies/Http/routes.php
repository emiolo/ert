<?php

Route::group(['middleware' => ['web', 'cerebelo.authenticated'], 'prefix' => 'cerebelo', 'namespace' => 'Modules\CerebeloCompanies\Http\Controllers'], function() {
    Route::group(['middleware' => ['cerebelo.permission']], function() {

        Route::group(['prefix' => 'companies'], function() {
            Route::get('/', 'CompaniesController@index')->name('cerebelo.companies.index');
            Route::get('/listing', 'CompaniesController@datatable')->name('cerebelo.companies.datatable');
            Route::get('/create', 'CompaniesController@create')->name('cerebelo.companies.create');
            Route::post('/', 'CompaniesController@store')->name('cerebelo.companies.store');
            //Route::get('/show/{id}', 'CompaniesController@show')->name('cerebelo.companies.show');
            Route::get('/edit/{id}', 'CompaniesController@edit')->name('cerebelo.companies.edit');
            Route::match(['put', 'patch'], '/update/{id}', 'CompaniesController@update')->name('cerebelo.companies.update');
            Route::delete('/delete/{id}', 'CompaniesController@destroy')->name('cerebelo.companies.destroy');
        });
    });
});
