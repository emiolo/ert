var route = document.getElementById("route-companies").getAttribute("datatable");

var oTable = $('#datatable').DataTable({
    "pageLength": 10,
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "columnDefs": [{"targets": [], "orderable": false}],
    "ajax": {
        "url": route
    },
    "order": [[3, "asc"]],
    "columns": [
        {data: 'btnList', name: 'btnList', orderable: false, searchable: false},
        {data: 'id', name: 'id'},
        {data: 'cnpj', name: 'cnpj'},
        {data: 'fantasy_name', name: 'fantasy_name'},
        {data: 'phone', name: 'phone'},
    ]
});