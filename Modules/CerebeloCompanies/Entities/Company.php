<?php

namespace Modules\CerebeloCompanies\Entities;

use Illuminate\Database\Eloquent\Model;
use Storage;
use Modules\CerebeloSettings\Entities\Address;

class Company extends Model
{

    public $table       = 'companies';
    protected $fillable = [
        'cnpj',
        'state_registration',
        'corporate_name',
        'fantasy_name',
        'phone',
        'phone2',
        'email',
        'logo',
    ];
    public $timestamps  = false;
    protected $appends = ['name'];

    public static function boot()
    {
        parent::boot();
        static::deleting(function($model) {

        });
        static::deleted(function($model) {
            $old_file_name = $model->logo;
            $storage       = Storage::disk('local');

            if (!empty($old_file_name))
            {
                if ($storage->exists($old_file_name))
                {
                    $storage->delete($old_file_name);
                }
            }
        });
    }

    public function getNameAttribute()
    {
        return (empty($this->attributes['corporate_name']) ? $this->attributes['fantasy_name'] : $this->attributes['corporate_name']);
    }

    /*
     * Relationships
     */

    public function address()
    {
        return $this->hasOne(Address::class, 'table_id')->where('table', $this->table);
    }

    /*
     * Others
     */

    public static function formSelect2()
    {
        $return = self::pluck('fantasy_name', 'id');
        return $return;
    }

    public function datatable()
    {
        return $this->select("*");
    }

}
