<?php

Route::group(['middleware' => 'web', 'prefix' => 'cerebelosizes', 'namespace' => 'Modules\CerebeloSizes\Http\Controllers'], function()
{
    Route::get('/', 'CerebeloSizesController@index');
});
