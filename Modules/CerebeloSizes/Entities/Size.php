<?php

namespace Modules\CerebeloSizes\Entities;

use Modules\CerebeloSettings\Entities\BaseModel as Model;

class Size extends Model
{

    protected $table    = 'sizes';
    protected $fillable = [
        'name',
        'hex_color',
    ];

    /*
     * Others
     */

    public function getAll()
    {
        return $this->select('id', 'name', 'hex_color')->get();
    }

    public function formSelect()
    {
        $return = $this->pluck('name', 'id')->prepend('Selecione', '');
        return $return;
    }

}
