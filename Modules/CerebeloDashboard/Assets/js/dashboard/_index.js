var route = document.getElementById("route-dashboard").getAttribute("datatable");

var index = $("#ordersDashboard li.active a").parent('li').index();

var options = {
    "pageLength": 10,
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "columnDefs": [{"targets": [], "orderable": false}],
    "ajax": {
        "url": route + '?dashboard-tab=' + index
    },
    "order": [[5, "desc"]],
    createdRow: function (row, data, dataIndex) {
        //$(row).find('td:eq(3)').attr('class', 'btn-group-justified');
    },
    "columns": [
        {data: 'btnList', name: 'btnList', orderable: false, searchable: false},
        {data: 'infoOrder', name: 'client.company_name'},
        {data: 'delivery_date', name: 'delivery_date'},
        {data: 'status', name: 'latestSector.sector.name'},
        {data: 'wait_action', name: 'wait_action', orderable: false, searchable: false},
        {data: 'updated_at', name: 'updated_at'},
    ]
};
var oTable = $('#datatable').DataTable(options);

$("#ordersDashboard li a").on('click', function (e) {
    e.preventDefault();
    var index = $(this).parent('li').index();
    datatable(index);
});

function datatable(index)
{
    console.log('index: ', index);
    $('#datatable tbody').html("");

    if (index !== 1) {
        var url = route + '?dashboard-tab=' + index;
        oTable.ajax.url(url).load();
    }
}