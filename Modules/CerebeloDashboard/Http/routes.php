<?php

//Route::group(['middleware' => ['web'], 'prefix' => 'cerebelo', 'namespace' => 'Modules\CerebeloDashboard\Http\Controllers'], function() {
Route::group(['middleware' => ['web', 'cerebelo.authenticated'], 'prefix' => 'cerebelo', 'namespace' => 'Modules\CerebeloDashboard\Http\Controllers'], function() {

    Route::group(['middleware' => ['cerebelo.permission']], function() {

        Route::group(['prefix' => '/'], function() {
            Route::get('/', 'DashboardController@index')->name('cerebelo.dashboard.index');
            //Route::get('/', 'DashboardController@index')->name('cerebelo.index');
        });
    });
});
