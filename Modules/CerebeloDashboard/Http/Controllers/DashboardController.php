<?php

namespace Modules\CerebeloDashboard\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\CerebeloDashboard\Http\Controllers\CerebeloController as Controller;
use Modules\CerebeloOrders\Entities\Order;
use Modules\CerebeloOrders\Entities\OrderStatus;
use Modules\CerebeloSettings\Entities\Sector;
use Modules\CerebeloSettings\Entities\SectorAction;

class DashboardController extends Controller
{

    const FOLDER = "dashboard.";

    protected $_sector;
    protected $_order_status;
    protected $_sector_action;

    public function __construct()
    {
        $this->_sector = new Sector();
        $this->_order_status = new OrderStatus();
        $this->_sector_action = new SectorAction();
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $group = "";
        $auth = auth()->user();

        if (isset($auth->sector->group)) {
            $group = $auth->sector->group;
        }

        if ($group == 'producao') {
            return view(self::MODULE_VIEW . self::FOLDER . 'production');
        }

        $sectors = $this->_sector->formSelect();
        $orders_status = $this->_order_status->formSelect();
        $sectors_actions = $this->_sector_action->formSelect();
        $sectors_actions->prepend('Não Definido', 0);

        $order = new Order();
        $status_filter = $order->statusFilter('dashboard');

        return view(self::MODULE_VIEW . self::FOLDER . 'index', compact('sectors', 'orders_status', 'sectors_actions', 'status_filter'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view(self::MODULE_VIEW . self::FOLDER . 'create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view(self::MODULE_VIEW . self::FOLDER . 'show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view(self::MODULE_VIEW . self::FOLDER . 'edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {

    }

}
