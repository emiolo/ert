<?php

Route::group(['middleware' => ['web', 'cerebelo.authenticated'], 'prefix' => 'cerebelo', 'namespace' => 'Modules\CerebeloTable\Http\Controllers'], function() {
    Route::group(['middleware' => ['cerebelo.permission']], function() {

        Route::group(['prefix' => 'tables'], function() {
            Route::get('/', 'TablesController@index')->name('cerebelo.tables.index');
            Route::get('/listing', 'TablesController@datatable')->name('cerebelo.tables.datatable');
            Route::get('/create', 'TablesController@create')->name('cerebelo.tables.create');
            Route::post('/', 'TablesController@store')->name('cerebelo.tables.store');
            //Route::get('/show/{id}', 'TablesController@show')->name('cerebelo.tables.show');
            Route::get('/edit/{id}', 'TablesController@edit')->name('cerebelo.tables.edit');
            Route::match(['put', 'patch'], '/update/{id}', 'TablesController@update')->name('cerebelo.tables.update');
            Route::delete('/delete/{id}', 'TablesController@destroy')->name('cerebelo.tables.destroy');
        });
    });
});
