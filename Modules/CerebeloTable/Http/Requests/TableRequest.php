<?php

namespace Modules\CerebeloTable\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TableRequest extends FormRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required|max:100|unique:price_table,name' . ((isset($this->id) && !empty($this->id)) ? ',' . $this->id : ''),
            'percentage' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

}
