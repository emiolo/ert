<?php

namespace Modules\CerebeloTable\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\CerebeloTable\Http\Controllers\CerebeloController as Controller;
use Datatables;
use Modules\CerebeloTable\Http\Requests\TableRequest;
use Modules\CerebeloTable\Entities\PriceTable;
use Modules\CerebeloSettings\Entities\Sector;
use JsValidator;

class TablesController extends Controller {

    const FOLDER = "tables.";

    protected $_price_table;
    protected $_sector;

    public function __construct(PriceTable $_price_table, Sector $_sector) {
        $this->_price_table = $_price_table;
        $this->_sector = $_sector;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        return view(self::MODULE_VIEW . self::FOLDER . 'index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        $validator = JsValidator::formRequest(new TableRequest, '#formCreateTable');
        $sectors   = $this->_sector->formSelect();

        return view(self::MODULE_VIEW . self::FOLDER . 'create', compact('sectors', 'validator'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(TableRequest $request) {
        $attributes = $request->all();
        $create = $this->_price_table->create($attributes);
        if ($create) {
            // Salvar setores
            $create->sectors()->sync($attributes['sectors_id']);

            return $this->redirectBackAfterSuccess(trans('messages.success.create'));
        }

        return $this->redirectBackAfterError(trans('messages.error.create'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return view(self::MODULE_VIEW . self::FOLDER . 'show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id) {
        $validator = JsValidator::formRequest(new TableRequest, '#formEditTable');
        $model = $this->_price_table->find($id);

        $sectors = $this->_sector->formSelect();
        $sectors_selected = $model->formMultiSelectSectorsSelected();

        return view(self::MODULE_VIEW . self::FOLDER . 'edit', compact('model', 'sectors', 'sectors_selected', 'validator'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(TableRequest $request, $id) {
        $attributes = $request->all();

        $update = $this->_price_table->find($id);
        $update->fill($attributes);

        if ($update->save()) {
            // Salvar setores
            $update->sectors()->sync($attributes['sectors_id']);

            return $this->redirectAfterSuccess(trans('messages.success.create'), 'cerebelo.' . self::FOLDER . 'index');
        }

        return $this->redirectBackAfterError(trans('messages.error.create'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id) {
        if ($this->_price_table->destroy($id)) {
            return $this->redirectBackAfterSuccess(trans('messages.success.destroy'));
        }

        return $this->redirectBackAfterError(trans('messages.error.destroy'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function datatable() {
        return Datatables::of($this->_price_table->datatable())
                        ->addColumn('btnList', function ($object) {
                            $array = [
                                'edit' => [
                                    'route' => route('cerebelo.' . self::FOLDER . 'edit', $object->id),
                                ],
                                'destroy' => [
                                    'route' => route('cerebelo.' . self::FOLDER . 'destroy', $object->id),
                                ],
                            ];
                            return iconsList($array);
                        })
                        ->escapeColumns([])
                        ->make(true);
    }

}
