<?php

namespace Modules\CerebeloTable\Entities;

use Modules\CerebeloSettings\Entities\BaseModel as Model;
use Modules\CerebeloSettings\Entities\Sector;
use DB;

class PriceTable extends Model
{

    protected $table    = 'price_table';
    protected $fillable = [
        'name',
        'percentage',
    ];

    public static function boot()
    {
        parent::boot();
    }

    /*
     * Relationships
     */

    public function sectors()
    {
        return $this->belongsToMany(Sector::class, 'price_table__sectors', 'price_table_id', 'sectors_id');
    }

    /*
     * Scopes
     */

    public function scopeOrderById($query, $value = "asc")
    {
        return $query->orderBy('id', $value);
    }

    public function scopeOrderByName($query, $value = "asc")
    {
        return $query->orderBy('name', $value);
    }

    /*
     * Ohters
     */

    /*
     * Retorna todas as tabelas com permissão para cada setor
     * @return array
     */

    public static function formSelect()
    {
        $return = self::whereHas('sectors', function($query1) {
                    $sectors_id = self::auth();
                    //$query1->where('id', $sectors_id);
                })
                ->pluck('name', 'id');
        return $return;
    }

    public static function formSelect2()
    {
        $return = self::orderByName()->pluck('name', 'id');
        return $return;
    }

    public function formMultiSelectSectorsSelected()
    {
        return $this->sectors()->pluck('id')->toArray();
    }

    public function formSelectExtra()
    {
        $return = $this->select('id', 'name', 'percentage')->get()->toArray();
        return $return;
    }

    public function datatable()
    {
        return $this->select("*");
    }

}
