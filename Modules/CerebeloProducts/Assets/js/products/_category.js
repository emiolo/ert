$("#categories").select2({
  tags: true,
  width: "100%",
  placeholder: "Adicionar um ou mais categorias",
  containerCssClass: 'select-xs'
});

ProductCategories = {
  // O botão deve ser composto

  /*
   * Listar as categorias
   */
  index: function (e) {
    var categories = JSON.parse(e.getAttribute('categories'));
    var destroyUrl = e.getAttribute('url');

    var html = '';
    html += '<div class="modal-dialog modal-xs">';
    html += ' <div class="modal-content">';
    html += '   <div class="modal-header">';
    html += '     <button type="button" class="close" data-dismiss="modal">×</button>';
    html += '     <h5 class="modal-title">Categorias</h5>';
    html += '   </div>';
    html += '   <div class="modal-body">';
    html += '     <ul class="media-list">';
    for (category in categories) {
      var id = category;
      var name = categories[category];
      html += '     <li class="media">';
      html += '       <div class="media-left"><a rel="nofollow" data-method="delete" data-confirm="Tem certeza que deseja excluir essa Categoria?" data-msg="" href="' + destroyUrl + '/' + id + '?_token=' + $('meta[name="csrf-token"]').attr('content') + '"><i class="icon-trash text-danger"></i></a></div>';
      html += '       <div class="media-body">' + name + '</div>';
      html += '     </li>';
    }
    html += '     </ul>';
    html += '   </div>';
    html += ' </div>';
    html += '</div>';
    $("#general_modal").modal('show').html(html);
  },

  /*
   * Excluir a categoria
   */
  destroy: function (e) {

  }
}

var select_cat_olds = $("#categories").attr('select_cat_olds');
if (select_cat_olds != "" && select_cat_olds != null && typeof (select_cat_olds) != 'undefined') {
  select_cat_olds = JSON.parse(select_cat_olds);
  if (select_cat_olds.length > 0) {
    var selectedIds = [];
    $.each(select_cat_olds, function (key, value) {
      selectedIds.push(value);
    });
    if (selectedIds.length > 0) {
      $("#categories")
        .val(selectedIds)
        .select2({
          tags: true,
          width: "100%",
          placeholder: "Adicionar um ou mais categorias",
          containerCssClass: 'select-xs',
        });
    }
  }
}