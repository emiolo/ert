var route = document.getElementById("route-datatable").getAttribute("route-datatable");

var oTable = $('#datatable').DataTable({
    "pageLength": 10,
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "columnDefs": [{"targets": [], "orderable": false}],
    "ajax": {
        "url": route
    },
    "order": [[2, "asc"]],
    "columns": [
        {data: 'btnList', name: 'btnList', orderable: false, searchable: false},
        {data: 'id', name: 'id'},
        {data: 'name', name: 'name'},
        {data: '_weight', name: 'weight'},
        {data: '_price', name: 'price'},
        {data: 'created_at', name: 'created_at'},
    ]
});

$(document).ajaxStop(function () {
    $(".datatbles-edit-row").off('keypress').on('keypress', function (e) {
        if (e.which === 13) { // Salvar ao pressionar ENTER
            var $this = $(this);
            var name = $this.attr('name');
            var href = $this.attr('href');
            var new_value = $this.val();

            if (new_value != "") {
                var data = {};
                if (name == "price") {
                    data = {price: new_value};
                } else {
                    data = {weight: new_value};
                }

                $.ajax({
                    url: href,
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    success: function (response) {
                        if (response.error === true) {
                            new PNotify({
                                title: 'Atenção',
                                text: response.message,
                                addclass: 'bg-warning alert-styled-right'
                            });
                        } else {
                            new PNotify({
                                title: 'Sucesso',
                                text: response.message,
                                addclass: 'bg-success alert-styled-right'
                            });
                        }
                    },
                    error: function (response) {

                    }
                });
            } else {
                new PNotify({
                    title: 'Atenção',
                    text: 'O campo é obrigatório.',
                    addclass: 'bg-warning alert-styled-right'
                });
            }
        }
    });
});