// Format displayed data
function formatRepo(repo) {
    //console.log('formatRepo(): ', repo);
    if (repo.loading) return repo.name;

    var markup = "<div class='select2-result-repository clearfix'>" +
        //"<div class='select2-result-repository__avatar'><img src='' /></div>" +
        "<div class='select2-result-repository__meta'>" +
        "<div class='select2-result-repository__title'>" + repo.name + "</div>";

    if (repo.description) {
        //markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
    }

    markup += "<div class='select2-result-repository__statistics'>" +
        "<div class='select2-result-repository__forks'>CPF/CNPJ: " + repo.cpf_cnpj + "</div>" +
        "<div class='select2-result-repository__stargazers'>E-mail: " + repo.email + "</div>" +
        "<div class='select2-result-repository__watchers'>Pessoa: " + repo.person_type + "</div>" +
        "</div>" +
        "</div></div>";

    return markup;
}

// Format selection
function formatRepoSelection(repo) {
    //console.log('formatRepoSelection(): ', repo);
    return repo.name;
}

$("#categories").select2({
    tags: true,
    width: "100%",
    placeholder: "Adicionar um ou mais categorias",
    containerCssClass: 'select-xs'
});

$("#models").select2({
    tags: true,
    width: "100%",
    placeholder: "Adicionar um ou mais modelos",
    containerCssClass: 'select-xs'
});

$("#clients_id").select2({
    placeholder: "Buscar Cliente",
    minimumResultsForSearch: Infinity,
    width: "100%",
    dropdownAutoWidth: true,
    ajax: {
        url: $("#clients_id").attr('url'),
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                q: params.term, // search term
                page: params.page
            };
        },
        processResults: function (data, params) {
            //console.log('processResults: ', data, params);
            // parse the results into the format expected by Select2
            // since we are using custom formatting functions we do not need to
            // alter the remote JSON data, except to indicate that infinite
            // scrolling can be used
            params.page = params.page || 1;

            return {
                results: data.data,
                pagination: {
                    more: (params.page * 30) < data.total
                }
            };
        },
        cache: true
    },
    escapeMarkup: function (markup) {
        return markup;
    }, // let our custom formatter work
    minimumInputLength: 1,
    templateResult: formatRepo, // omitted for brevity, see the source of this page
    templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
});