<?php

namespace Modules\CerebeloProducts\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\CerebeloProducts\Http\Controllers\CerebeloController as Controller;
use Datatables;
use Modules\CerebeloProducts\Entities\Product;
use Modules\CerebeloProducts\Http\Requests\ProductRequest;
use Modules\CerebeloTable\Entities\PriceTable;
use Modules\CerebeloProducts\Entities\ProductCategory;
use Modules\CerebeloSizes\Entities\Size;
use JsValidator;
use Modules\CerebeloProducts\Http\Controllers\Traits\IsapaTrait;

class ProductsController extends Controller
{

    use IsapaTrait;

    const FOLDER = "products.";

    protected $_product;
    protected $_price_table;
    protected $_product_category;
    protected $_size;

    public function __construct()
    {
        $this->_product          = new Product();
        $this->_price_table      = new PriceTable();
        $this->_product_category = new ProductCategory();
        $this->_size             = new Size();
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view(self::MODULE_VIEW . self::FOLDER . 'index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $validator  = JsValidator::formRequest(new ProductRequest, '#formCreateProduct');
        $categories = $this->_product_category->formSelect();
        $sizes      = $this->_size->getAll();

        return view(self::MODULE_VIEW . self::FOLDER . 'create', compact('categories', 'sizes', 'validator'));
    }

    /**
     * Tratar os dados da Categoria caso seja novo/pré-existente
     * @param  array $categories
     * @return array
     */
    public function categories($categories)
    {
        $new_categories = [];
        $pre_categories = [];
        array_map(function($k, $v) use(&$new_categories, &$pre_categories) {
            if(intval($v) > 0) {
                array_push($pre_categories, $v);
            } else {
                array_push($new_categories, $v);
            }
        }, array_keys($categories), array_values($categories));

        // Salvar nova categoria
        if(count($new_categories) > 0) {
            foreach ($new_categories as $key => $value) {
                $model = $this->_product_category->updateOrCreate(['name' => $value], ['name' => $value, 'enable' => 'Y']);
                $pre_categories[] = $model->id;
            }
        }

        return $pre_categories;
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(ProductRequest $request)
    {
        $attributes = $request->all();

        $create     = $this->_product->create($attributes);

        if ($create) {
            $categories = $request->get('categories', []);
            $categories_id = $this->categories($categories);
            $create->categories()->sync($categories_id);

            return $this->redirectBackAfterSuccess(trans('messages.success.create'));
        }

        return $this->redirectBackAfterError(trans('messages.error.create'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view(self::MODULE_VIEW . self::FOLDER . 'show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $validator      = JsValidator::formRequest(new ProductRequest, '#formEditProduct');
        $model          = $this->_product->find($id);
        $column_headers = $this->_price_table->formSelect2();
        $categories     = $this->_product_category->formSelect();
        $sizes          = $this->_size->getAll();

        return view(self::MODULE_VIEW . self::FOLDER . 'edit', compact('model', 'column_headers', 'categories', 'sizes', 'validator'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(ProductRequest $request, $id)
    {
        $attributes = $request->all();

        $update = $this->_product->find($id);

        $update->fill($attributes);

        if ($update->save()) {
            $categories = $request->get('categories', []);
            $categories_id = $this->categories($categories);
            $update->categories()->sync($categories_id);

            return $this->redirectAfterSuccess(trans('messages.success.update'), 'cerebelo.' . self::FOLDER . 'index');
        }

        return $this->redirectBackAfterError(trans('messages.error.update'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function fastUpdate(Request $request, $id)
    {
        $attributes = $request->all();

        $update = $this->_product->find($id);
        $update->fill($attributes);

        if ($update->save()) {
            return response()->json([
                        'error'   => false,
                        'message' => trans('messages.success.update')
            ]);
        }

        return response()->json([
                    'error'   => true,
                    'message' => trans('messages.error.update')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->_product->destroy($id)) {
            return $this->redirectBackAfterSuccess(trans('messages.success.destroy'));
        }

        return $this->redirectBackAfterError(trans('messages.error.destroy'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function datatable()
    {
        return Datatables::of($this->_product->datatable())
                        ->addColumn('_weight', function ($object) {
                            return \Form::text('weight', $object->weight, ['href' => route('cerebelo.' . self::FOLDER . 'update', $object->id), 'data-popup' => 'tooltip', 'data-placement' => 'left', 'title' => 'Tecle ENTER para salvar', 'class' => "form-control input-xs datatbles-edit-row weight", 'placeholder' => 'Peso', 'id' => $object->id]);
                        })
                        ->addColumn('_price', function ($object) {
                            return \Form::text('price', $object->price, ['href' => route('cerebelo.' . self::FOLDER . 'update', $object->id), 'data-popup' => 'tooltip', 'data-placement' => 'left', 'title' => 'Tecle ENTER para salvar', 'class' => "form-control input-xs datatbles-edit-row decimal", 'maxlength' => '10', 'placeholder' => 'Valor', 'id' => $object->id]);
                        })
                        ->addColumn('btnList', function ($object) {
                            $array = [
                                'edit'    => [
                                    'route' => route('cerebelo.' . self::FOLDER . 'edit', $object->id),
                                ],
                                'destroy' => [
                                    'route' => route('cerebelo.' . self::FOLDER . 'destroy', $object->id),
                                ],
                            ];
                            return iconsList($array);
                        })
                        ->escapeColumns([])
                        ->make(true);
    }

    /**
     * Importar produtos
     * @return Response
     */
    public function getImport()
    {
        return view(self::MODULE_VIEW . 'import');
    }

    /**
     * Importar produtos
     * @return Response
     */
    public function postImport(Request $request)
    {
        $file      = $request->file('file');

        if (empty($file)) {
            return redirect()->back()->with('failure', "Somente arquivo com extensão .xls e .xlsx");
        }

        $extension = $file->getClientOriginalExtension();

        if (!in_array($extension, ['xls', 'xlsx'])) {
            return redirect()->back()->with('failure', "Somente arquivo com extensão .xls e .xlsx");
        }

        $real_path = $file->getRealPath();
        $complete  = false;
        $message   = "";

        \Excel::selectSheetsByIndex(0)->load($real_path, function ($reader) use (&$complete, &$message) {
            $results = $reader->all()->toArray();

            foreach ($results as $value) {
                $row = array_values($value);

                if (count($row) != 3) {
                    //$message = "Seu arquivo deve ter as seguintes colunas (Nome | Peso | Preço).";
                    //break;
                }

                if (!array_key_exists("nome", $value) || !array_key_exists("peso", $value) || !array_key_exists("preco", $value)) {
                    $message = "Seu arquivo deve ter as seguintes colunas (Nome | Peso | Preço).";
                    break;
                }
            }

            //$key = 1;
            if(empty($message)) {
                foreach ($results as $value) {
                    $name   = trim($value['nome']);
                    $weight = trim($value['peso']);
                    $price  = trim($value['preco']);

                    if (!empty($name) && !empty($weight) && !empty($price)) {
                        //$message = "As colunas Nome, Peso e Preço são obrigatórias. (na linha $key)";
                        //break;

                        $attributes = [
                            'name'   => $name,
                            'price'  => moneyFormat($price),
                            'weight' => weightFormat($weight),
                        ];

                        try {
                            $this->_product->updateOrCreate(['name' => $name], $attributes);
                            $complete = true;
                        } catch (Illuminate\Database\QueryException $ex) {
                            $message = $ex->getMessage();
                            break;
                        }
                    }

                    //$key++;
                }
            }
        });

        if ($complete && empty($message)) {
            return redirect()->back()->with('success', "Importação efetuado com sucesso.");
        }

        return redirect()->back()->with('failure', $message);
    }


    /**
     * Excluir uma categoria
     * @return Response
     */
    public function destroyCategory($id)
    {
        if ($this->_product_category->destroy($id)) {
            return $this->redirectBackAfterSuccess(trans('messages.success.destroy'));
        }

        return $this->redirectBackAfterError(trans('messages.error.destroy'));
    }
}
