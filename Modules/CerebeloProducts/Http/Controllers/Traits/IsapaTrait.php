<?php

namespace Modules\CerebeloProducts\Http\Controllers\Traits;

use Excel;
use File;
use Illuminate\Http\Request;
use JsValidator;
use Validator;

trait IsapaTrait
{

    /**
     * @return Response
     */
    public function getTraitIsapaImport()
    {
        $validator = JsValidator::make(
            [
                'file' => 'required|file|mimes:xls,xlsx,csv',
                'clients_id' => 'required',
                'price_table_id' => 'required',
                'categories' => 'required',
                'models' => 'required',
            ],
            [
                'clients_id.required' => 'O campo Cliente é obrigatório.',
                'categories.required' => 'O campo Cetegorias é obrigatório.',
                'models.required' => 'O campo Modelos é obrigatório.',
            ],
            [],
            '#formIsapaImport'
        );

        $categories = $this->_product_category->formSelect();

        $tables = $this->_price_table->formSelect();
        $tables->prepend('Selecione Tabela', '');

        $models = \Modules\CerebeloProducts\Entities\IsapaModel::pluck('name', 'id');

        return view(self::MODULE_VIEW . self::FOLDER . 'isapa', compact('validator', 'tables', 'categories', 'models'));
    }

    /**
     * @return Response
     */
    private function manageModels($models)
    {
        foreach ($models as $key => $value):
            if (is_numeric($value)) {
                $model = \Modules\CerebeloProducts\Entities\IsapaModel::find($value);
                $models[$key] = $model->name;
            } else {
                $model = \Modules\CerebeloProducts\Entities\IsapaModel::updateOrCreate(['name' => $value], ['name' => $value]);
                $models[$key] = $model->name;
            }
        endforeach;

        return $models;
    }

    /**
     * @return Response
     */
    public function postTraitIsapaImport(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|file|mimes:xls,xlsx,csv',
            'clients_id' => 'required',
            'price_table_id' => 'required',
            'categories' => 'required',
            'models' => 'required',
        ],
            [
                'clients_id.required' => 'O campo Cliente é obrigatório.',
                'categories.required' => 'O campo Categorias é obrigatório.',
                'models.required' => 'O campo Modelos é obrigatório.',
            ]);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $file = $request->file('file');

        $excel = Excel::load($file)->get();

        // Validar cabeçalho
        $heading = $excel->getHeading();
        if (count(array_diff(['cod_fornecedor', 'descricao', 'quantidade', 'preco_unit'], array_values(array_filter($heading)))) > 0):
            return $this->redirectBackAfterError("O cabeçalho 'Cod Fornecedor|Descrição|Quantidade|Preço Unit' não foi encontrado no arquivo, favor faça a correção para prosseguir com a importação.");
        endif;

        // Filtrar linhas vazias
        $excel = $excel->toArray();
        $excel = array_values(array_filter($excel, function ($value) {
            return !empty($value['cod_fornecedor']) && !empty($value['descricao']) && !empty($value['quantidade']) && !empty($value['preco_unit']);
        }));

        // Validar dados do arquivo:
        $key = 2;
        foreach ($excel as $value):
            $validator = Validator::make($value, [
                'cod_fornecedor' => 'required',
                'descricao' => 'required|max:250',
                'quantidade' => 'required|numeric',
                'preco_unit' => 'required|numeric',
            ]);
            if ($validator->fails()) {
                $validator->errors()->add('line', "Erro aproximadamente na linha '$key' do arquivo.");

                return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            $key++;
        endforeach;

        // Extrair e tratar os dados importados
        $models = $this->manageModels(array_values(array_unique(array_map('trim', array_map('strtolower', $request->get('models', []))))));

        $products_name = [];
        Excel::load($file, function ($reader) use (&$products_name, $models) {
            $results = $reader->get();
            $sizes = [' P', ' M', ' GGG', ' GG', ' G'];
            $excludes = ['TAM' => '', 'UNICO' => '', 'ÚNICO' => ''];

            $key = 1;
            foreach ($results as $data):
                $provider_cod = (int) trim($data['cod_fornecedor']);
                $description = trim(preg_replace('/\s\s+/', ' ', str_replace('.', '', $data['descricao'])));
                $words = explode(' ', $description);
                $quantity = (int) trim($data['quantidade']);
                $unit_price = number_format((float) trim($data['preco_unit']), 2, '.', '');

                if (empty($description)):
                    $key++;
                    continue;
                endif;

                // (identificar o tamanho na descrição) : Retorna o valor encontrado na interseção dos 2 arrays
                $size = trim(current(array_intersect(array_map('strtoupper', $words), array_map('trim', $sizes))));

                $substr = trim(strtr($description, $excludes));
                $substr = trim(preg_replace("/\b$size\b/", "", $substr)); // substituir palavra inteira

                foreach ($models as $model) {
                    $str_contains = str_contains(strtolower($substr), $model);

                    if ($str_contains === true) {
                        $products_name[$model][$substr][$unit_price][] = [
                            'provider_cod' => $provider_cod,
                            'size' => ($size == 'GGG') ? '3G' : $size,
                            'quantity' => $quantity,
                            'unit_price' => $unit_price,
                        ];
                    }
                }

                $key++;
            endforeach;
        });

        if (count($products_name) == 0) {
            return $this->redirectBackAfterError("Não foi possível continuar a leitura dos dados, favor verificar o arquivo para prosseguir com a importação.");
        }

        // Validar produtos com mais de um valor unitário
        $duplicated_prod_price = false;
        array_map(function ($key, $value) use (&$duplicated_prod_price) {
            foreach ($value as $k => $val) {
                if (count($val) > 1) {
                    $duplicated_prod_price = $k;
                }
            }
        }, array_keys($products_name), array_values($products_name));
        if ($duplicated_prod_price !== false) {
            return $this->redirectBackAfterError("Produto '$duplicated_prod_price' com preço unitário diferente, favor corrigir no arquivo para prosseguir com a importação.");
        }

        $auth = auth()->user();

        $OriginalName = $file->getClientOriginalName();
        $request = $request->except('file');

        $request['original_name'] = $OriginalName;

        $this->dispatch(new \Modules\CerebeloProducts\Jobs\ImportExcelIsapa($products_name, $request, $auth));

        return $this->redirectBackAfterSuccess("A importação esta sendo processada em segundo plano, assim que for finalizada você será notificado(a) via e-mail ($auth->email).");
    }

}
