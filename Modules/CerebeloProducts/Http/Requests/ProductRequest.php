<?php

namespace Modules\CerebeloProducts\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $unique = '|unique:products,name,NULL,id,deleted_at,NULL';

        if(isset($this->id) && !empty($this->id)) {
            $unique = '|unique:products,name,' . $this->id . ',id,deleted_at,NULL';
        }

        return [
            'name' => 'required|max:250' . $unique, //'|unique:products,name' . ((isset($this->id) && !empty($this->id)) ? ',' . $this->id : ''),
            'price' => 'required',
            'weight' => 'required',
            'categories' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'weight.required' => 'O campo Peso deve ser preenchido.',
        ];
    }

}
