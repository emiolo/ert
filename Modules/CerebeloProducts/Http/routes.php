<?php

Route::group(['middleware' => ['web', 'cerebelo.authenticated'], 'prefix' => 'cerebelo', 'namespace' => 'Modules\CerebeloProducts\Http\Controllers'], function () {

    Route::group(['middleware' => ['cerebelo.permission']], function () {

        Route::group(['prefix' => 'products'], function () {
            Route::get('/', 'ProductsController@index')->name('cerebelo.products.index');
            Route::get('/listing', 'ProductsController@datatable')->name('cerebelo.products.datatable');
            Route::get('/create', 'ProductsController@create')->name('cerebelo.products.create');
            Route::post('/', 'ProductsController@store')->name('cerebelo.products.store');
            Route::get('/show/{id}', 'ProductsController@show')->name('cerebelo.products.show');
            Route::get('/edit/{id}', 'ProductsController@edit')->name('cerebelo.products.edit');
            Route::post('/update/{id}', 'ProductsController@fastUpdate')->name('cerebelo.products.update');
            Route::match(['put', 'patch'], '/update/{id}', 'ProductsController@update')->name('cerebelo.products.update');
            Route::delete('/delete/{id}', 'ProductsController@destroy')->name('cerebelo.products.destroy');

            Route::get('/import', 'ProductsController@getImport')->name('cerebelo.products.import');
            Route::post('/import', 'ProductsController@postImport')->name('cerebelo.products.import');

            Route::get('/isapa', 'ProductsController@getTraitIsapaImport')->name('cerebelo.products.import_isapa');
            Route::post('/isapa', 'ProductsController@postTraitIsapaImport')->name('cerebelo.products.import_isapa');

            Route::delete('/categories/{id?}', 'ProductsController@destroyCategory')->name('cerebelo.products.categories');
        });
    });

    Route::get('/prod-cli-cat', function () {
        // vincular todos os produtos na categoria "Produto ERT"
        Modules\CerebeloProducts\Entities\Product::get()->each(function($value) {
            $value->categories()->sync([1]);
        });
        // vincular todos os clientes na categoria "Produto ERT"
        Modules\CerebeloClient\Entities\Client::get()->each(function($value) {
            $value->categories()->sync([1]);
        });
    });
});
