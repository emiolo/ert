<?php

namespace Modules\CerebeloProducts\Providers;

use Illuminate\Support\ServiceProvider;

class CerebeloProductsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        foreach (glob(app_path() . '/../Modules/CerebeloProducts/Support/*.php') as $filename)
        {
            require_once($filename);
        }
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('cerebeloproducts.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'cerebeloproducts'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/cerebeloproducts');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/cerebeloproducts';
        }, \Config::get('view.paths')), [$sourcePath]), 'cerebeloproducts');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/cerebeloproducts');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'cerebeloproducts');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'cerebeloproducts');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
