<?php

/** Sotnas
 * Tratar os dados da Categoria caso seja novo/pré-existente
 * @param  array $categories
 * @return array
 */
if (!function_exists('categoriesUpdateOrCreate'))
{

    function categoriesUpdateOrCreate($categories)
    {
        $new_categories = [];
        $pre_categories = [];
        array_map(function($k, $v) use(&$new_categories, &$pre_categories) {
            if(intval($v) > 0) {
                array_push($pre_categories, $v);
            } else {
                array_push($new_categories, $v);
            }
        }, array_keys($categories), array_values($categories));

        // Salvar nova categoria
        if(count($new_categories) > 0) {
            foreach ($new_categories as $key => $value) {
                $model = \Modules\CerebeloProducts\Entities\ProductCategory::updateOrCreate(['name' => $value], ['name' => $value, 'enable' => 'Y']);
                $pre_categories[] = $model->id;
            }
        }

        return $pre_categories;
    }

}
