<?php

namespace Modules\CerebeloProducts\Entities;

use Illuminate\Database\Eloquent\Model;

class IsapaModel extends Model
{

    protected $table = 'isapa_models';
    protected $fillable = [
        'name',
    ];
    public $timestamps = false;

}
