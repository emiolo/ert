<?php

namespace Modules\CerebeloProducts\Entities;

use Modules\CerebeloSettings\Entities\BaseModel as Model;
use Modules\CerebeloOrders\Entities\Order;
use Modules\CerebeloTable\Entities\PriceTable;

class Product extends Model
{

    public $table       = 'products';
    protected $fillable = [
        'name',
        'price',
        'weight',
        'sizes_code',
    ];

    public static function boot()
    {
        parent::boot();
        static::deleting(function($model) {

        });
    }

    /*
     * Relationships
     */

    public function productsPrices()
    {
        return $this->belongsToMany(PriceTable::class, 'products__price_table', 'products_id', 'price_table_id')->withPivot('value');
    }

    public function ordersProducts()
    {
        return $this->belongsToMany(Order::class, 'orders_products', 'products_id', 'orders_id')->withPivot('quantity');
    }

    public function categories()
    {
        return $this->belongsToMany(ProductCategory::class, 'products_has_products_categories', 'products_id', 'products_categories_id');
    }

    /*
     * Attributes
     */

    public function getSizesCodeAttribute()
    {
        try
        {
            if(!empty($this->attributes['sizes_code']))
                return json_decode($this->attributes['sizes_code'], true);
        }
        catch (\Exception $e)
        {

        }
    }

    public function setSizesCodeAttribute($value)
    {
        try
        {
            $this->attributes['sizes_code'] = json_encode($value);
        }
        catch (\Exception $e)
        {

        }
    }

    public function getPriceAttribute()
    {
        try
        {
            return moneyFormat($this->attributes['price']);
        }
        catch (\Exception $e)
        {
            return 0;
        }
    }

    public function setPriceAttribute($value)
    {
        try
        {
            $this->attributes['price'] = moneyUnFormat($value);
        }
        catch (\Exception $e)
        {
            return 0;
        }
    }

    public function getWeightAttribute()
    {
        try
        {
            return str_replace(".", ",", str_pad($this->attributes['weight'], 5, "0"));
        }
        catch (\Exception $e)
        {
            return 0;
        }
    }

    public function setWeightAttribute($value)
    {
        try
        {
            $this->attributes['weight'] = str_replace(",", ".", $value);
        }
        catch (\Exception $e)
        {
            return 0;
        }
    }

    /*
     * Scopes
     */

    /*
     * Retorna todos os produtos
     * @return array
     */

    public function formSelect()
    {
        $return = $this->pluck('name', 'id')->prepend('Selecione produto', '');
        return $return;
    }

    public function formSelect2($categories = [])
    {
        $return = $this->select('id', 'name', 'price', 'weight');
        if(!empty($categories) && count($categories) > 0) {
            $return->whereHas('categories', function($q) use($categories) {
                $q->whereIn('products_has_products_categories.products_categories_id', $categories);
            });
        }
        $return = $return->get()->toArray();

        return $return;
    }

    /*
     * Ohters
     */

    public function datatable($product = "")
    {
        return $this->with("productsPrices")
                        ->where(function($q) use($product) {
                            if (!empty($product))
                            {
                                $q->where('id', $product);
                            }
                        })->select("products.*");
    }

}
