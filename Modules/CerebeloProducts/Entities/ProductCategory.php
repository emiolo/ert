<?php

namespace Modules\CerebeloProducts\Entities;

use Modules\CerebeloSettings\Entities\BaseModel as Model;
use Modules\CerebeloClient\Entities\Client;

class ProductCategory extends Model
{

    public $table       = 'products_categories';
    protected $fillable = [
        'name',
        'enable',
    ];

    public static function boot()
    {
        parent::boot();
        static::deleting(function($model) {

        });
    }

    /*
     * Relationships
     */

    public function products()
    {
        return $this->belongsToMany(Product::class, 'products_has_products_categories', 'products_id', 'products_categories_id');
    }

    public function clients()
    {
        return $this->belongsToMany(Client::class, 'clients_has_products_categories', 'clients_id', 'products_categories_id');
    }

    /*
     * Scopes
     */

    /*
     * Retorna todos os produtos
     * @return array
     */

    public function formSelect()
    {
        $return = $this->pluck('name', 'id');
        return $return;
    }

    /*
     * Ohters
     */

}
