<?php

namespace Modules\CerebeloProducts\Jobs;

use Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;
use Mail;
use Modules\CerebeloClient\Entities\Client;
use Modules\CerebeloOrders\Entities\Order;
use Modules\CerebeloOrders\Entities\OrderProduct;
use Modules\CerebeloProducts\Entities\Product;
use Modules\CerebeloSettings\Entities\User;
use Modules\CerebeloSizes\Entities\Size;

class ImportExcelIsapa implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Queueable;

    private $products_name;
    private $request;
    private $auth;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($products_name, $request, $auth)
    {
        $this->products_name = $products_name;
        $this->request = $request;
        $this->auth = $auth;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $start_queue = date('d/m/Y H:i:s');
        $products_name = $this->products_name;
        $request = $this->request;
        $auth = $this->auth;

        Auth::loginUsingId($auth->id);

        $original_name = $request['original_name'];
        $clients_id = $request['clients_id'];

        $client = Client::find($clients_id);
        // Vincular a categoria do produto no cliente
        $categories = isset($request['categories']) ? $request['categories'] : [];
        $categories_id = categoriesUpdateOrCreate($categories);
        $client->categories()->sync($categories_id);

        $all_sizes = collect(Size::select('id', 'name')->get()->toArray());

        // Salvar os dados
        $array_chunks = array_chunk($products_name, 1, true);
        foreach ($array_chunks as $model => $array_chunk) {
            $now_date = date('dmy');
            $representative_id = $client->representative->id;
            $attendant_id = $client->attendant->id;
            $price_table_id = $request['price_table_id'];

            $order = new Order();
            $order->fill([
                'clients_id' => $client->id,
                'order_number' => null,
                'price_table_id' => $price_table_id,
            ]);
            $order->save();

            // Salvar no setor Atendimento
            $users_id = $order->client->attendant_id;
            $user = User::with('sector')->find($users_id);
            if (isset($user->sector->period)) {
                $period_option = $user->sector->period_option;
                $period = $user->sector->period;
                $order->sectors()->attach([$users_id => calculatePeriodPerSector($period_option, $period)]);
            }

            $order_id = $order->id;

            $order_number = preg_replace('/\D/', '', $order->order_number);
            if (empty($order_number)) {
                $order_number = "$representative_id.$attendant_id.$client->id.$now_date.$order_id";
                $order->update(['order_number' => $order_number]);
            }

            $order_number = $order->order_number;

            $ordination = 0;
            array_map(function ($items) use ($request, $client, &$order_id, &$ordination, $all_sizes, $auth, &$order_number) {
                foreach ($items as $product_name => $grouped):
                    array_map(function ($item) use ($product_name, $request, $order_id, &$ordination, $all_sizes, $auth) {
                        $current = current($item);
                        $sizes_code = ['PP' => '', 'P' => '', 'M' => '', 'G' => '', 'GG' => '', '3G' => '', '4G' => '', '5G' => ''];
                        $qtt_sizes_code = $sizes_code;

                        $unit_price = moneyFormat(array_get($current, 'unit_price'));
                        $weight = 0.190;

                        $quantity = 0;
                        foreach ($item as $value):
                            array_set($sizes_code, $value['size'], $value['provider_cod']);
                            array_set($qtt_sizes_code, $value['size'], $value['quantity']);

                            $quantity += $value['quantity'];
                        endforeach;

                        // ##################################### Salvar produto (cadastro de produtos)
                        $product = Product::firstOrCreate(['name' => $product_name], [
                            'price' => $unit_price,
                            'weight' => $weight,
                            'sizes_code' => $sizes_code,
                        ]);
                        $categories = isset($request['categories']) ? $request['categories'] : [];
                        $categories_id = categoriesUpdateOrCreate($categories);
                        $product->categories()->sync($categories_id);

                        $product_id = $product->id;

                        $price_table_id = $request['price_table_id'];

                        // ##################################### Salvar pedido/orçamento (card do produto)
                        $product_card = new OrderProduct();
                        $product_card->fill([
                            'orders_id' => $order_id,
                            'products_id' => $product_id,
                            'quantity' => $quantity,
                            'have_sizes' => 'Y',
                            'quantity_piece' => null,
                            'size_piece' => null,
                            'name_piece' => null,
                            'unit_price' => $unit_price,
                            'extra_variation' => null,
                            'price_table_id' => $price_table_id,
                            'price_table_value' => 0,
                            'presents_partnerships_id' => null,
                            'ordination' => $ordination,
                        ]);
                        $product_card->save();

                        // Salvar os tamanhos
                        $sizes_sync = [];
                        foreach ($qtt_sizes_code as $key => $value) {
                            $id = $all_sizes->where('name', $key)->first();
                            if (isset($id['id'])) {
                                $sizes_sync[$id['id']] = ['quantity' => empty($value) ? 0 : $value];
                            }

                        }
                        $product_card->sizes()->sync($sizes_sync);

                        if (collect($sizes_sync)->sum('quantity') == 0):
                            $product_card->update(['have_sizes' => 'N']);
                        endif;

                        $ordination++;
                    }, array_values($grouped));
                endforeach;
            }, $array_chunk);

            if (!empty($order_id) && !empty($order_number)):
                $body = [
                    'items' => [
                        '' => "Segue o relatório da importação efetuada:",
                        'Importação: ' => "$start_queue",
                        'Status: ' => "Concluído.",
                        'Arquivo: ' => $original_name,
                        'Total registros: ' => count($array_chunk),
                        'Modelo: ' => $model,
                        'Atenção: ' => "Clique <b><a href='" . route('cerebelo.orders.edit', $order_id) . "'>aqui $order_number</a></b> para finalizar o pedido/orçamento.",
                    ],
                ];
                Log::info("Importação ISAPA: ($auth->email|$auth->name) ", $body);
                Mail::send('cerebeloproducts::emails.import', $body, function ($message) use ($auth) {
                    $message->from('nao-responder@ertuniformes.com.br', 'ERT Uniformes - Cron')
                        ->to($auth->email, $auth->name)
                        ->subject('Importação da Planilha Isapa');
                });
            endif;
        }

    }
}
