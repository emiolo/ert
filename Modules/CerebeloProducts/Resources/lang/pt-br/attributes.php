<?php

return [
    'weight'     => 'Peso',
    'categories' => 'Categorias',
    'import_only' => 'Marcar caso queira importar somente os produtos e não criar um pedido.',
];
