<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <link rel="icon" href="{{ asset('images/cerebelo/favicon.png') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Importação de Produtos</title>
    <!-- Global stylesheets-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/cerebelo/application.css') }}?{{ filemtime(public_path('css/cerebelo/application.css')) }}" rel="stylesheet" type="text/css">
  </head>
  <body>
    <!-- /main navbar-->
    <!-- Page container-->
    <div class="page-container">
      <!-- Page content-->
      <div class="page-content">
        <!-- Main content-->
        <div class="content-wrapper">
          <!-- Content-area-->
          <div class="content">
            <div class="row">
              <div class="col-lg-12">@if(session()->has('success'))
                <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                  <button class="close" data-dismiss="alert" type="button"><span>x<span class="sr-only">{{ trans('buttons.close') }}</span></span></button>{{ session('success') }}
                </div>@endif
                @if(session()->has('failure'))
                <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                  <button class="close" data-dismiss="alert" type="button"><span>x<span class="sr-only">{{ trans('buttons.close') }}</span></span></button>{{ session('failure') }}
                </div>@endif
                @if (count($errors) > 0)
                <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                  <button class="close" data-dismiss="alert" type="button"><span>x<span class="sr-only">{{ trans('buttons.close') }}</span></span></button>
                  <!--| Verifique o formulário-->
                  <ul>@foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>@endforeach
                  </ul>
                </div>
                <!--.alert.alert-danger-->@endif
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">{{ Form::open(array('url' => route('cerebelo.products.import'), 'method' => 'POST', 'files' => true)) }}
                <div class="panel panel-flat">
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group {{ $errors->has('file') ? ' has-error' : '' }}">
                          <label>
                             {{ trans('attributes.file') }}</label>{{ Form::file('file', ['class' => "form-control file-styled"]) }}
                             <span class="help-block text-danger">Somente arquivo com extensão .xls e .xlsx são permitidas.</span>
                             <span class="help-block text-danger">Seu arquivo deve ter as seguintes colunas (Nome | Peso | Preço).</span>
                          @if ($errors->has('file'))<span class="help-block text-danger"><i class="icon-cancel-circle2 position-left"></i> {{ $errors->first('file') }}</span>@endif
                        </div>
                      </div>
                    </div>
                    <div class="form-group text-right">{{ Form::button('<i class="icon-upload7 position-left"></i> Enviar', ['class' => 'btn btn-primary', 'type' => 'submit']) }}</div>
                  </div>
                </div>{{ Form::close() }}
              </div>
            </div>
            <!-- Footer-->
            <div class="footer text-muted">
              &copy; {{ date('Y') }}.
              @if(isset($client_data->site))<a href="{{ isset($client_data->site) ? $client_data->site : "#" }}" target="_blank">{{ $client_data->name }}</a>@else<a>Nome do Cliente</a>@endif
               por <a href="http://emiolo.com" target="_blank">eMiolo.com</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Core JS files-->
    <script type="text/javascript" src="{{ asset('js/cerebelo/application.js') }}?{{ filemtime(public_path('js/cerebelo/application.js')) }}"></script>
    <script type="text/javascript" src="{{ asset('js/cerebelo/application2.js') }}?{{ filemtime(public_path('js/cerebelo/application2.js')) }}"></script>
    <script type="text/javascript" src="{{ asset('js/cerebelo/application3.js') }}?{{ filemtime(public_path('js/cerebelo/application3.js')) }}"></script>
    <!-- /core JS files-->
  </body>
</html>