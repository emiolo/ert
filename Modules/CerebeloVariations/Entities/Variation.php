<?php

namespace Modules\CerebeloVariations\Entities;

use Modules\CerebeloSettings\Entities\BaseModel as Model;
use DB;

class Variation extends Model {

    public $table = 'variations';
    protected $fillable = [
        'name',
        'value',
    ];

    public static function boot() {
        parent::boot();
        static::deleting(function($model) {

        });
    }

    /*
     * Relationships
     */

    /*
     * Attributes
     */

    public function getValueAttribute() {
        try {
            return moneyFormat($this->attributes['value']);
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function setValueAttribute($value) {
        try {
            $this->attributes['value'] = moneyUnFormat($value);
        } catch (\Exception $e) {
            return 0;
        }
    }

    /*
     * Scopes
     */

    /*
     * Retorna todos os produtos
     * @return array
     */

    public function formSelect() {
        $return = $this->pluck('name', 'id');
        return $return;
    }

    public function formSelect2() {
        $return = $this->select(DB::raw('id, name, value AS price'))->get()->toArray();
        return $return;
    }

    /*
     * Ohters
     */

    public function datatable() {
        return $this->select("*");
    }

}
