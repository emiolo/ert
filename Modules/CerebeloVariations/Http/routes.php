<?php

Route::group(['middleware' => ['web', 'cerebelo.authenticated'], 'prefix' => 'cerebelo', 'namespace' => 'Modules\CerebeloVariations\Http\Controllers'], function() {

    Route::group(['middleware' => ['cerebelo.permission']], function() {

        Route::group(['prefix' => 'variations'], function() {
            Route::get('/', 'VariationsController@index')->name('cerebelo.variations.index');
            Route::get('/listing', 'VariationsController@datatable')->name('cerebelo.variations.datatable');
            Route::get('/create', 'VariationsController@create')->name('cerebelo.variations.create');
            Route::post('/', 'VariationsController@store')->name('cerebelo.variations.store');
            Route::get('/show/{id}', 'VariationsController@show')->name('cerebelo.variations.show');
            Route::get('/edit/{id}', 'VariationsController@edit')->name('cerebelo.variations.edit');
            Route::match(['put', 'patch'], '/update/{id}', 'VariationsController@update')->name('cerebelo.variations.update');
            Route::delete('/delete/{id}', 'VariationsController@destroy')->name('cerebelo.variations.destroy');
        });
    });
});
