<?php

namespace Modules\CerebeloVariations\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\CerebeloVariations\Http\Controllers\CerebeloController as Controller;
use Datatables;
use Modules\CerebeloVariations\Entities\Variation;
use Modules\CerebeloVariations\Http\Requests\VariationRequest;
use JsValidator;

class VariationsController extends Controller {

    const FOLDER = "variations.";

    protected $_variation;

    public function __construct(Variation $_variation) {
        $this->_variation = $_variation;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        return view(self::MODULE_VIEW . self::FOLDER . 'index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        $validator = JsValidator::formRequest(new VariationRequest, '#formCreateVariation');

        return view(self::MODULE_VIEW . self::FOLDER . 'create', compact('validator'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(VariationRequest $request) {
        $attributes = $request->all();
        $create = $this->_variation->create($attributes);
        if ($create) {
            return $this->redirectBackAfterSuccess(trans('messages.success.create'));
        }

        return $this->redirectBackAfterError(trans('messages.error.create'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return view(self::MODULE_VIEW . self::FOLDER . 'show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id) {
        $validator = JsValidator::formRequest(new VariationRequest, '#formEditVariation');
        $model = $this->_variation->find($id);

        return view(self::MODULE_VIEW . self::FOLDER . 'edit', compact('model', 'validator'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(VariationRequest $request, $id) {
        $attributes = $request->all();

        $update = $this->_variation->find($id);
        $update->fill($attributes);

        if ($update->save()) {
            return $this->redirectAfterSuccess(trans('messages.success.create'), 'cerebelo.' . self::FOLDER . 'index');
        }

        return $this->redirectBackAfterError(trans('messages.error.create'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id) {
        if ($this->_variation->destroy($id)) {
            return $this->redirectBackAfterSuccess(trans('messages.success.destroy'));
        }

        return $this->redirectBackAfterError(trans('messages.error.destroy'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function datatable() {
        return Datatables::of($this->_variation->datatable())
                        ->addColumn('btnList', function ($object) {
                            $array = [
                                'edit' => [
                                    'route' => route('cerebelo.' . self::FOLDER . 'edit', $object->id),
                                ],
                                'destroy' => [
                                    'route' => route('cerebelo.' . self::FOLDER . 'destroy', $object->id),
                                ],
                            ];
                            return iconsList($array);
                        })
                        ->escapeColumns([])
                        ->make(true);
    }

}
