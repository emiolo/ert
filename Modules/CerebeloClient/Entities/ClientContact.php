<?php

namespace Modules\CerebeloClient\Entities;

use Modules\CerebeloSettings\Entities\BaseModel as Model;
use Modules\CerebeloSettings\Entities\Address;

class ClientContact extends Model
{

    public $table       = 'clients_contacts';
    protected $fillable = [
        'clients_id',
        'person_type',
        'cpf_cnpj',
        'company_name',
        'fantasy_name',
        'state_registration',
        'rg',
        'phone',
        'email',
        'cep',
        'street',
        'number',
        'complement',
        'neighborhood',
        'city',
        'state',
    ];
    protected $appends = ['name'];

    /*
     * Attributes
     */
    /*
     * Gets/Sets
     */

    public function getNameAttribute()
    {
        return (empty($this->attributes['company_name']) ? $this->attributes['fantasy_name'] : $this->attributes['company_name']);
    }

    /*
     * Relationships
     */

    public function address()
    {
        return $this->hasOne(Address::class, 'table_id')->table($this->table);
    }

    /*
     * Scopes
     */

    /*
     * Ohters
     */
}
