<?php

namespace Modules\CerebeloClient\Entities;

use Modules\CerebeloSettings\Entities\BaseModel as Model;

class ClientComment extends Model
{

    public $table       = 'clients_comments';
    protected $fillable = [
        'clients_id',
        'users_id',
        'comment',
    ];

    /*
     * Attributes
     */

    /*
     * Relationships
     */

    /*
     * Scopes
     */

    /*
     * Ohters
     */
}
