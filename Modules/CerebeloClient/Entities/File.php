<?php

namespace Modules\CerebeloClient\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\CerebeloClient\Entities\Client;
use Modules\CerebeloOrders\Entities\Order;
use Carbon\Carbon;
use Storage;

class File extends Model
{

    protected $table    = 'clients_files';
    protected $fillable = [
        'clients_id',
        'title',
        'file',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function($model) {
            
        });
        static::updating(function($model) {
            
        });
        static::deleting(function($model) {
            
        });
        static::deleted(function($model) {
            $storage = Storage::disk('local');
            $file    = $model->file;
            if ($storage->exists($file))
            {
                $storage->delete($file);
            }
        });
    }

    /*
     * Attributes
     */

    public function getCreatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d/m/Y H:i:s');
    }

    /*
     * Scope
     */

    public function scopeWhereClient($query, $value)
    {
        return $query->where('clients_id', $value);
    }

    /*
     * Relationships
     */

    public function client()
    {
        return $this->belongsTo(Client::class, 'clients_id');
    }

    public function attachedFiles()
    {
        return $this->belongsToMany(Order::class, 'orders_attached_files', 'clients_files_id', 'orders_id');
    }

    /*
     * Others
     */

    public function datatable($id, $orders_id = null, $attached = false)
    {
        if($attached === false)
            return $this->whereClient($id)->select("*");

        return $this->with(['attachedFiles' => function ($q) use($orders_id) {
            $q->where('orders_id', $orders_id);
        }])->whereClient($id)->select("*");
    }

}
