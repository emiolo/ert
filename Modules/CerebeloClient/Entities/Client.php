<?php

namespace Modules\CerebeloClient\Entities;

use Modules\CerebeloSettings\Entities\BaseModel as Model;
use Modules\CerebeloSettings\Entities\Address;
use Modules\CerebeloTable\Entities\PriceTable;
use Modules\CerebeloRepresentatives\Entities\Representative;
use Modules\CerebeloSettings\Entities\User;
use Modules\CerebeloSettings\Entities\Sector;
use Modules\CerebeloOrders\Entities\Order;
use DB;
use Modules\CerebeloClient\Entities\File;
use Modules\CerebeloProducts\Entities\ProductCategory;

class Client extends Model
{

    public $table       = 'clients';
    protected $fillable = [
        'international',
        'person_type',
        'attendant_id',
        'representative_id',
        'designer_id',
        'cpf_cnpj',
        'state_registration',
        'company_name',
        'fantasy_name',
        'rg',
        'phone',
        'phone2',
        'email',
    ];
    protected $appends = ['name', 'last_credit_value'];

    public static function boot()
    {
        parent::boot();
    }

    public function transformAudit(array $data): array
    {
        $getOriginal = $this->getOriginal();
        $getAttributes = $this->getAttributes();

        // representative_id
        $data = transform_audit_for_humans(['data' => $data, 'model' => new Representative, 'compare' => 'representative_id', 'get_original' => $getOriginal, 'get_attributes' => $getAttributes, 'attribute' => 'name']);

        // attendant_id
        $data = transform_audit_for_humans(['data' => $data, 'model' => new User, 'compare' => 'attendant_id', 'get_original' => $getOriginal, 'get_attributes' => $getAttributes, 'attribute' => 'name']);

        // designer_id
        $data = transform_audit_for_humans(['data' => $data, 'model' => new User, 'compare' => 'designer_id', 'get_original' => $getOriginal, 'get_attributes' => $getAttributes, 'attribute' => 'name']);

        return $data;
    }

    /*
     * Attributes
     */
    /*
     * Gets/Sets
     */

    public function getLastCreditValueAttribute()
    {
        return DB::table('orders_payments_installments')
                ->join('orders', 'orders.id', '=', 'orders_payments_installments.orders_id')
                ->where('orders.clients_id', '=', $this->attributes['id'])
                ->where('orders_payments_installments.status', '=', 'CD')
                ->sum('orders_payments_installments.value')
        ;
    }

    public function getNameAttribute()
    {
        return (empty($this->attributes['company_name']) ? $this->attributes['fantasy_name'] : $this->attributes['company_name']);
    }

    /*
     * Relationships
     */

    public function representative()
    {
        return $this->belongsTo(Representative::class, 'representative_id');
    }

    public function attendant()
    {
        return $this->belongsTo(User::class, 'attendant_id');
    }

    public function designer()
    {
        return $this->belongsTo(User::class, 'designer_id');
    }

    public function address()
    {
        return $this->hasOne(Address::class, 'table_id')->table($this->table);
    }

    public function lastUsedAddress()
    {
        return $this->hasMany(Order::class, 'clients_id')->orderBy('id', 'desc')->take(1);
    }

    public function addresses()
    {
        return $this->hasMany(Address::class, 'table_id')->table($this->table);
    }

    public function priceTables()
    {
        return $this->belongsToMany(PriceTable::class, 'clients__price_table', 'clients_id', 'price_table_id');
    }

    public function comments()
    {
        return $this->belongsToMany(User::class, 'clients_comments', 'clients_id', 'users_id')->whereNull('clients_comments.deleted_at')->withPivot('id', 'comment', 'created_at');
    }

    public function files()
    {
        return $this->hasMany(File::class, 'clients_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'clients_id');
    }

    public function categories()
    {
        return $this->belongsToMany(ProductCategory::class, 'clients_has_products_categories', 'clients_id', 'products_categories_id');
    }

    public function contacts()
    {
        return $this->hasMany(ClientContact::class, 'clients_id');
    }
    /*
     * Scopes
     */

    public function scopeFindCpfCnpj($query, $cpf_cnpj)
    {
        $query->orWhere('cpf_cnpj', $cpf_cnpj)->orWhere('id', $cpf_cnpj);
    }

    /*
     * Ohters
     */

    public function searchByQuery($q, $page = 30)
    {
        return $this
                    ->orWhere('cpf_cnpj', 'like', '%' . $q . '%')
                    ->orWhere('state_registration', 'like', '%' . $q . '%')
                    ->orWhere('company_name', 'like', '%' . $q . '%')
                    ->orWhere('fantasy_name', 'like', '%' . $q . '%')
                    ->orWhere('rg', 'like', '%' . $q . '%')
                    ->orWhere('email', 'like', '%' . $q . '%')
                    ->paginate($page)
                    ->toArray();
    }

    public function firstCpfCnpj($cpf_cnpj)
    {
        return $this->with(['lastUsedAddress', 'address' => function ($q) {
                        $q->type('C');
        }, 'addresses', 'contacts', 'priceTables' => function ($q) {
            $q->select('name', 'id');
        }])->findCpfCnpj($cpf_cnpj)->first();
    }

    public function commentsForSectors($sector_id)
    {
        $result = $this->whereHas('comments', function ($query) use ($sector_id) {
                    $query->where('users.sectors_id', $sector_id)
                        ->where('clients_comments.clients_id', $this->attributes['id']);
        })
                ->with(['comments' => function ($query) use ($sector_id) {
                        $query->where('users.sectors_id', $sector_id)
                        ->where('clients_comments.clients_id', $this->attributes['id'])
                        ->orderBy('clients_comments.created_at', 'DESC')
                        ->orderBy('clients_comments.id', 'DESC')
                        ->select('clients_comments.comment', 'clients_comments.created_at', 'users.name');
                }])
                ->first();

        return $result;
    }

    /*
     * Retorna todos os setores com quantidade de comentários por cliente
     * @return array
     */

    public function allSectorsWithCommentsQuantity($client_id = 0)
    {
        $select = ['sectors.id', 'sectors.name'];
        if ($client_id != 0) {
            $subQuery = "SELECT COUNT(clients_comments.id) "
                    . "FROM users "
                    . "JOIN clients_comments ON clients_comments.users_id = users.id "
                    . "WHERE users.sectors_id = sectors.id AND clients_comments.clients_id = $client_id AND clients_comments.deleted_at IS NULL";
            array_push($select, DB::raw("($subQuery) AS qnt"));
        }
        $return = Sector::withoutAlias()
                ->get($select);

        return $return;
    }

    public function formMultiSelectTablesSelected()
    {
        return $this->priceTables()->pluck('id')->toArray();
    }

    public function loadMultiSelectTablesSelected()
    {
        return $this->priceTables()->pluck('name', 'id')->toArray();
    }

    public function datatable()
    {
        return $this->with(['representative', 'attendant', 'designer'])->select("*", DB::raw('IF(TRIM(clients.company_name) = "", TRIM(clients.fantasy_name), TRIM(clients.company_name)) as name'));
    }
}
