var cpf_cnpj = $("input[name='cpf_cnpj']");
var cpf_cnpj_val = cpf_cnpj.val().replace(/\D/g, '').length;

var person_type = $("select[name='person_type']");

var $international = $('select[name="international"]');

if(person_type.val() == "") {
    person_type.val((cpf_cnpj_val === 0 || cpf_cnpj_val > 11) ? 'PJ' : 'PF');
}

personType(person_type.val());

person_type.on('change', function (e) {
    var v = this.value;
    personType(v);
});

$international.on('change', function (e) {
    personType(person_type.val());
});

inputsFocusin();

function inputsFocusin() {
    var inputs_element = $("input[name='contact_phone'],input[name='phone'],input[name='phone2'],input[name^='cep['],input[name^='contact_cep[']");
    inputs_element.off('focusin').on('focusin', function (e) {
        var international = $international.val();
        var $this = $(this);
        var name = $this.attr('name').replace(/[^a-z]/g,'');

        if(name.indexOf('phone') > -1) {
            $this.off('keyup').on('keyup', function () {
                var v = $(this).val();
                v = v.replace(/\D/g, "");
                v = v.replace(/^(\d{2})(\d)/g, "($1) $2");
                v = v.replace(/(\d{1})(\d{1,4})$/, "$1-$2");
                $(this).val(v);
            });
        } else if(name.indexOf('cep') > -1 && name.indexOf('contactcep') < 0) {
            $('.cep').mask('99999-999');
            $this.off('blur').on('blur', function () {
                loadCep($(this));
            });
        } else if(name.indexOf('contact_cep') > -1) {
            $('.cep').mask('99999-999');
            $this.off('blur').on('blur', function () {
                loadContactsCep($(this));
            });
        }

        if(international == 'Y') {
            if(name.indexOf('phone') > -1) {
                $this.off('keyup');
            } else if(name.indexOf('cep') > -1) {
                $this.off('blur');
                $this.unmask();
                $('.cep').unmask();
            }
        }
    });
}


function personType(v) {
    var $international   = $('select[name="international"]');
    var contact_class_pj = $(".contact_person_type_pj");
    var contact_class_pf = $(".contact_person_type_pf");
    var class_pj = $(".person_type_pj");
    var class_pf = $(".person_type_pf");
    var international = $international.val();
    var cpf_cnpj = $("input[name='cpf_cnpj']");

    if (v == "PJ")
    {
        cpf_cnpj.attr('maxlength', '18');
    }
    else
    {
        cpf_cnpj.attr('maxlength', '14');
    }

    var ctx_cpf_cnpj = $("#ctx_cpf_cnpj");

    if(international == 'N') {
        if (v == "PJ")
        {
            ctx_cpf_cnpj.html('CNPJ <i class="text-danger">*</i>\n\
            <span class="label text-left"><a target="_blank" href="https://www.receita.fazenda.gov.br/pessoajuridica/cnpj/cnpjreva/cnpjreva_solicitacao2.asp">Receita Federal</a></span>');
            $("input[name='cpf_cnpj']").mask('00.000.000/0000-00');
            class_pj.show(0);
            class_pf.hide(0);
            contact_class_pj.show(0);
            contact_class_pf.hide(0);
        }
        else
        {
            ctx_cpf_cnpj.html('CPF <i class="text-danger">*</i>\n\
            <span class="label text-left"><a target="_blank" href="https://www.receita.fazenda.gov.br/pessoajuridica/cnpj/cnpjreva/cnpjreva_solicitacao2.asp">Receita Federal</a></span>');
            $("input[name='cpf_cnpj']").mask('000.000.000-000');
            class_pf.show(0);
            class_pj.hide(0);
            contact_class_pf.show(0);
            contact_class_pj.hide(0);
        }
    } else {
        ctx_cpf_cnpj.html('Nº Documento <i class="text-danger">*</i>');
        $("input[name='cpf_cnpj']").unmask();

        if (v == "PJ")
        {
            class_pj.show(0);
            class_pf.hide(0);
            contact_class_pj.show(0);
            contact_class_pf.hide(0);
        }
        else
        {
            class_pf.show(0);
            class_pj.hide(0);
            contact_class_pf.show(0);
            contact_class_pj.hide(0);
        }
    }
}

function personType22(v) {
    var contact_class_pj = $(".contact_person_type_pj");
    var contact_class_pf = $(".contact_person_type_pf");
    var class_pj = $(".person_type_pj");
    var class_pf = $(".person_type_pf");
    var international = $international.val();
    var cpf_cnpj = $("input[name='cpf_cnpj']");

    if(international == 'N') {
        var cpf_cnpj_val = cpf_cnpj.val().replace(/\D/g, '').length;

        if (v == "PJ")
        {
            contact_class_pj.show(0);
            contact_class_pj.show(0);
            class_pf.hide(0);
            class_pj.show(0);

            cpf_cnpj.closest('.form-group').find('label').html('CNPJ <i class="text-danger">*</i>');
            cpf_cnpj.attr('maxlength', '18');
            cpf_cnpj.attr('placeholder', 'CNPJ');
            //cpf_cnpj.mask("99.999.999/9999-99");
        } else
        {
            contact_class_pf.show(0);
            contact_class_pj.hide(0);
            class_pf.show(0);
            class_pj.hide(0);

            cpf_cnpj.closest('.form-group').find('label').html('CPF <i class="text-danger">*</i>');
            cpf_cnpj.attr('maxlength', '14');
            cpf_cnpj.attr('placeholder', 'CPF');
            //cpf_cnpj.mask("999.999.999-99");
        }

        $('i.text-danger', contact_class_pj).html("*");
        $('i.text-danger', contact_class_pf).html("*");
        $('i.text-danger', class_pj).html("*");
        $('i.text-danger', class_pf).html("*");

        if (cpf_cnpj_val < 11) {
            cpf_cnpj.val('');
        }

        var CpfCnpjMaskBehavior = function (val) {
			return val.replace(/\D/g, '').length <= 11 ? '000.000.000-009' : '00.000.000/0000-00';
		},
        cpfCnpjpOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(CpfCnpjMaskBehavior.apply({}, arguments), options);
            }
        };
        $("input[name='cpf_cnpj']").mask(CpfCnpjMaskBehavior, cpfCnpjpOptions);
    } else {
        if (v == "PJ")
        {
            contact_class_pj.show(0);
            contact_class_pf.hide(0);
            class_pj.show(0);
            class_pf.hide(0);
        } else
        {
            contact_class_pf.show(0);
            contact_class_pj.hide(0);
            class_pf.show(0);
            class_pj.hide(0);
        }

        $('i.text-danger', contact_class_pj).html("");
        $('i.text-danger', contact_class_pf).html("");
        $('i.text-danger', class_pj).html("");
        $('i.text-danger', class_pf).html("");

        cpf_cnpj.closest('.form-group').find('label').html('Nº Documento <i class="text-danger">*</i>');
        cpf_cnpj.attr('maxlength', '18');
        cpf_cnpj.attr('placeholder', 'Nº Documento');
        cpf_cnpj.unmask();
    }
}