var $all_addresses = $("#all-addresses");
var $all_contacts = $("#all-contacts");

var hash = window.location.hash;
if (hash) {
    // remover todos de active
    $('a[href^="#client-tab"]').parent('li').removeClass('active');
    $("div[id^='client-tab']").removeClass('active');

    $('a[href="' + hash + '"]').parent('li').addClass('active');
    $(hash).addClass('active');
}

$cpf_cnpj = $("input[name='cpf_cnpj']");
$cpf_cnpj.on('blur', function() {
    var val = this.value;
    var cpf_cnpj_val = val.replace(/\D/g, '').length;
    if(cpf_cnpj_val != "" && cpf_cnpj_val >= 11)
    {
        var url = $(this).attr('href');
        $.blockUI({message: 'Verificando CPF/CNPJ...'});
        $.ajax({
            dataType: "json",
            type: "GET",
            url: url,
            data: {
                cpf_cnpj: val
            }
        }).done(function (response) {
            $.unblockUI();
            if (response.error) {
                swal({
                    title: 'Atenção',
                    text: 'CPF/CNPJ já foi cadastrado, deseja visualizar o registro?',
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "Não, tentar outro!",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim, quero!",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: false,
                }, function (isConfirm) {
                    if (isConfirm)
                    {
                        window.location = response.message;
                    }
                    else
                    {
                        swal.close();
                    }
                });
            }
            else
            {
                //swal("Livre!", "CPF/CNPJ liberado para cadastro.", "success");
            }
        }).error(function (response) {
            $.unblockUI();
            new PNotify({
                title: 'Atenção',
                text: 'Erro interno.',
                type: 'error',
                icon: 'fa fa-ban'
            });
        });
    }
});


$select2_fields = $("select[name^='categories'], select[name^='tables_id']");

$select2_fields.each(function() {
    var $this = $(this);
    var selectedIds = [];
    var select_parse = JSON.parse($this.attr('select_tables'));
    if (select_parse) {
        $.each(select_parse, function (key, value) {
            selectedIds.push(value);
        });
    }
    $this.select2({
        minimumResultsForSearch: Infinity
    }).val(selectedIds);
});

$("button#addAddressCard").on('click', function (e) {
    e.preventDefault();
    addAddressCard();
});

$("button#addContactCard").on('click', function () {
    addContactCard($(this));
});

$("ul.nav-tabs-highlight li a").on('click', function () {
    var $this = $(this);
    var href = $this.attr('href');
    window.location.hash = href;
});

$("input[name^='contact_cpf_cnpj[']").off('keypress').on('keypress', function () {
    contactPersonType($(this));
});

if($("select[name^='contact_person_type[']").length > 0) {
    $("select[name^='contact_person_type[']").each(function () {
        contactPersonType($(this), true);
    });
}

/*
 * OBSERVAÇÕES DO CLIENTE
 */
getComments(false);

$("#tabbableSectors ul.nav li").on('click', function (e) {
    e.preventDefault();
    getComments($(this));
});

editComment();

function editComment() {
    $(".edit-comment").off('dblclick').on('dblclick', function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        if (href != "" && typeof (href) != "undefined") {
            var modal_remote = $("#modal_remote");
            $.ajax({
                dataType: "html",
                type: "GET",
                url: href,
            }).done(function (response) {
                modal_remote.modal('show').html(response);

                $("#btnDestroyComment").on('click', function (e) {
                    e.preventDefault();
                    var href = $(this).attr('href');
                    $.ajax({
                        dataType: "json",
                        type: "DELETE",
                        url: href,
                    }).done(function (response) {
                        if (!response.error) {
                            modal_remote.modal('hide').html("");
                            getComments($(this));
                        } else {
                            new PNotify({
                                title: 'Atenção',
                                text: response.message,
                                addclass: 'bg-warning alert-styled-right'
                            });
                        }
                    }).error(function (response) {

                    });
                });
                var options = {
                    beforeSubmit: function (formData, jqForm, options) {

                    },
                    success: function (responseText, statusText, xhr, $form) {
                        if (!responseText.error) {
                            modal_remote.modal('hide').html("");
                            getComments(false);
                        } else {
                            new PNotify({
                                title: 'Atenção',
                                text: responseText.message,
                                addclass: 'bg-warning alert-styled-right'
                            });
                        }
                    }
                };
                $('#formClientComment').submit(function () {
                    $(this).ajaxSubmit(options);
                    return false;
                });
            }).error(function (response) {

            });
        }
    }).css({'cursor': 'pointer'});
}

function saveObservation() {
    if (document.getElementById("customers-edit")) {
        var url = document.getElementById("customers-edit").getAttribute("client-comments-route-store");
        var observation = $("textarea[name=observation]");

        var block_area = $('.showComments');
        block_area.block({message: 'salvando comentário...'});
        $.ajax({
            dataType: "json",
            type: "POST",
            url: url,
            data: {
                observation: observation.val()
            }
        }).done(function (response) {
            block_area.unblock();
            if (!response.error) {
                observation.val('');
                getComments(false);
            } else {
                new PNotify({
                    title: 'Atenção',
                    text: response.message,
                    type: 'error',
                    icon: 'fa fa-ban'
                });
            }
        }).error(function (response) {
            block_area.unblock();
            new PNotify({
                title: 'Atenção',
                text: 'Erro interno.',
                type: 'error',
                icon: 'fa fa-ban'
            });
        });
    }
}

function getComments(obj) {
    if (document.getElementById("customers-edit")) {
        var url = document.getElementById("customers-edit").getAttribute("client-comments-route-index");
        var sector_id = '';

        if (obj) {
            sector_id = obj.attr('sector_id');
        } else {
            var active = $("#tabbableSectors ul.nav li.active");
            sector_id = active.attr('sector_id');
        }

        if (sector_id !== '') {
            var block_area = $('.showComments');
            block_area.block({message: 'buscando comentários...'});
            $.ajax({
                dataType: "html",
                type: "GET",
                url: url,
                data: {
                    sector_id: sector_id
                }
            }).done(function (response) {
                block_area.unblock();
                block_area.html(response);

                editComment();
            }).error(function (response) {
                block_area.unblock();
                new PNotify({
                    title: 'Atenção',
                    text: 'Erro interno.',
                    type: 'error',
                    icon: 'fa fa-ban'
                });
            });
        }
    }
}

/* INÍCIO ########################################
 * ENDEREÇOS
 */
function addAddressCard() {
    var $html = "";
    var $input_cep = $("input[name^='cep[']");
    var $select_type = document.querySelector("select[name='address_types']");
    var $select_type_clone = $($select_type.cloneNode(true));
    var $last_card_address = $input_cep.index($input_cep.eq(-1)) + 1;

    $select_type_clone.removeClass('select2-hidden-accessible');
    $select_type_clone.attr('name', 'type[' + $last_card_address + ']');
    $select_type_clone.addClass('select');
    $select_type_clone.removeAttr('tabindex').removeAttr('aria-hidden').removeAttr('disabled');
    var $type_html = $select_type_clone[0].outerHTML;

    $html += '<div class="panel panel-flat border-left-xlg border-left-info">';
    $html += '  <div class="panel-heading">';
    $html += '    <div class="heading-elements">';
    $html += '        <ul class="icons-list">';
    $html += '            <li><button type="button" class="btn btn-warning btn-xs" onclick="removeAddressCard(this);"><i class="icon-trash position-left"></i> Remover</button></li>';
    //$html += '            <li><a onclick="removeAddressCard(this);"><i class="icon-trash"></i></a></li>';
    $html += '        </ul>';
    $html += '    </div>';
    $html += '  </div>';
    $html += '  <div class="panel-body">';
    $html += '    <div class="row">';
    $html += '      <div class="col-md-4">';
    $html += '        <div class="form-group ">';
    $html += '          <label>Tipo</label>';
    $html += '          ' + $type_html;
    $html += '        </div>';
    $html += '      </div>';
    $html += '      <div class="col-md-4">';
    $html += '        <div class="form-group ">';
    $html += '          <label>CEP</label>';
    $html += '          <input class="form-control cep" placeholder="CEP" maxlength="9" name="cep[' + $last_card_address + ']" type="text" value="">';
    $html += '          <input name="address_id[' + $last_card_address + ']" type="hidden" value="">';
    $html += '          <input name="table[' + $last_card_address + ']" type="hidden" value="clients">';
    $html += '        </div>';
    $html += '      </div>';
    $html += '      <div class="col-md-6">';
    $html += '        <div class="form-group ">';
    $html += '          <label>Logradouro</label>';
    $html += '          <input class="form-control" placeholder="Logradouro" maxlength="250" name="street[' + $last_card_address + ']" type="text" value="">';
    $html += '        </div>';
    $html += '      </div>';
    $html += '      <div class="col-md-2">';
    $html += '        <div class="form-group ">';
    $html += '          <label>Número</label>';
    $html += '          <input class="form-control" placeholder="Número" maxlength="50" name="number[' + $last_card_address + ']" type="text" value="">';
    $html += '        </div>';
    $html += '      </div>';
    $html += '      <div class="col-md-4">';
    $html += '        <div class="form-group ">';
    $html += '          <label>Complemento</label>';
    $html += '          <input class="form-control" placeholder="Complemento" maxlength="50" name="complement[' + $last_card_address + ']" type="text" value="">';
    $html += '        </div>';
    $html += '      </div>';
    $html += '      <div class="col-md-4">';
    $html += '        <div class="form-group ">';
    $html += '          <label>Bairro</label>';
    $html += '          <input class="form-control" placeholder="Bairro" maxlength="150" name="neighborhood[' + $last_card_address + ']" type="text" value="">';
    $html += '        </div>';
    $html += '      </div>';
    $html += '      <div class="col-md-4">';
    $html += '        <div class="form-group ">';
    $html += '          <label>Cidade</label>';
    $html += '          <input class="form-control" placeholder="Cidade" maxlength="150" name="city[' + $last_card_address + ']" type="text" value="">';
    $html += '        </div>';
    $html += '      </div>';
    $html += '      <div class="col-md-4">';
    $html += '        <div class="form-group ">';
    $html += '          <label>Estado</label>';
    $html += '          <input class="form-control" placeholder="Estado" maxlength="100" name="state[' + $last_card_address + ']" type="text" value="">';
    $html += '        </div>';
    $html += '      </div>';
    $html += '      <div class="col-md-4">';
    $html += '        <div class="form-group ">';
    $html += '          <label>País</label>';
    $html += '          <input class="form-control" placeholder="País" maxlength="100" name="country[' + $last_card_address + ']" type="text" value="">';
    $html += '          <span class="help-block">(ex: Brasil, BR, Espanha, ES, Portugal, PT)</span>';
    $html += '        </div>';
    $html += '      </div>';
    $html += '      <div class="col-md-4">';
    $html += '        <div class="form-group ">';
    $html += '          <label>Local</label>';
    $html += '          <input class="form-control" placeholder="Local" maxlength="150" name="local[' + $last_card_address + ']" type="text" value="">';
    $html += '          <span class="help-block">(ex: Minha casa, Trabalho, Casa da avó)</span>';
    $html += '        </div>';
    $html += '      </div>';
    $html += '    </div>';
    $html += '  </div>';
    $html += '</div>';

    $all_addresses.append($html);

    inputsFocusin();
    loadCepFunction();
    inputsFocusin();
}

function removeAddressCard($this)
{
    $panel = $this.closest('.panel');

    $address_id = parseInt($panel.querySelector("input[name^='address_id']").value);
    if ($address_id > 0)
    {
        swal({
            title: 'Atenção',
            text: 'Deseja realmente excluir esse endereço?',
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "Não",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim, excluir!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function (isConfirm) {
            if (isConfirm)
            {
                var url = document.getElementById("customers-routes").getAttribute("remove-address");
                $.ajax({
                    url: url,
                    data: {
                        address_id: $address_id
                    },
                    type: 'DELETE',
                    dataType: 'json',
                    success: function (data, textStatus, jqXHR) {
                        if (!data.error) {
                            $panel.remove();
                            swal("Excluído!", data.message, "success");
                        } else {
                            swal("Erro", data.message, "error");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        swal.close();
                    }
                });
            }
        });
    } else
    {
        $panel.remove();
    }
}
/* FIM ########################################
 * ENDEREÇOS
 */

/* INÍCIO ########################################
 * CONTATOS/FATURAMENTO
 */
loadContactsFunction();
function addContactCard(e) {
    var href = e.attr('href');
    var $input_cep = $("input[name^='contact_cep[']");
    var $last_card_contact = $input_cep.index($input_cep.eq(-1)) + 1;

    $.ajax({
        dataType: "html",
        type: "GET",
        url: href + '?last_card_contact=' + $last_card_contact
    }).done(function (response) {
        $all_contacts.append(response);

        $("select[name^='contact_person_type[']").off('change').on('change', function () {
            contactPersonType($(this));
        });

        inputsFocusin();
        loadContactsFunction();
    });
}

function loadContactsFunction() {
    $('.select').select2({
        minimumResultsForSearch: Infinity,
        width: "100%",
        dropdownAutoWidth: true
    });
    $("input[name^='contact_cep']").mask('99999-999');
    $("input[name^='contact_cep']").on('blur', function (e) {
        loadContactsCep($(this));
    });
}

function loadContactsCep(e) {
    var $this = e;
    var val = $this.val();
    var panel = $this.closest('.panel');

    var cep = $this;
    var street = $("input[name^='contact_street[']", panel);
    var neighborhood = $("input[name^='contact_neighborhood[']", panel);
    var city = $("input[name^='contact_city[']", panel);
    var state = $("input[name^='contact_state[']", panel);
    var country = $("input[name^='contact_country[']", panel);
    //console.log('loadContactsCep: ', val, document.getElementById("correios-cep"));
    if (val !== '' && document.getElementById("correios-cep")) {
        var correiosCep = document.getElementById("correios-cep").getAttribute("correios-cep");
        $.blockUI({message: '<h4>buscando cep...</h4>'});
        $.ajax({
            dataType: "json",
            type: "GET",
            url: correiosCep + '/' + val
        }).done(function (response) {
            //console.log('loadContactsCep: ', response);
            if (response.city != "") {
                street.val(response.street);
                neighborhood.val(response.neighborhood);
                city.val(response.city);
                state.val(response.state);
                country.val('Brasil');
            } else {
                cep.val("");
                street.val("");
                neighborhood.val("");
                city.val("");
                state.val("");
                country.val("");
            }
        });
    } else {
        street.val("");
        neighborhood.val("");
        city.val("");
        state.val("");
        country.val("");
    }
}

function removeContactCard($this)
{
    $panel = $this.closest('.panel');

    $contact_id = parseInt($panel.querySelector("input[name^='contact_id[']").value);
    if ($contact_id > 0)
    {
        var url = $($this).attr('href');
        swal({
            title: 'Atenção',
            text: 'Deseja realmente excluir esse contato?',
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "Não",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim, excluir!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function (isConfirm) {
            if (isConfirm)
            {
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    dataType: 'json',
                    success: function (data, textStatus, jqXHR) {
                        if (!data.error) {
                            $panel.remove();
                            swal("Excluído!", data.message, "success");
                        } else {
                            swal("Erro", data.message, "error");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        swal.close();
                    }
                });
            }
        });
    } else
    {
        $panel.remove();
    }
}

function contactPersonType(e, onload) {
    var $this = e;
    var panel = $this.closest('.panel');

    var cpf_cnpj = $("input[name^='contact_cpf_cnpj[']", panel);
    var val      = cpf_cnpj.val();
    var name     = cpf_cnpj.attr('name');
    var index    = parseInt(name.match(/[0-9]+/));

    var class_pj = $(".contact_person_type_pj", panel);
    var class_pf = $(".contact_person_type_pf", panel);

    var person_type = $("select[name='contact_person_type[" + index + "]']");
    var v           = person_type.val();

    if (v == "PJ")
    {
        cpf_cnpj.attr('maxlength', '18');
    }
    else
    {
        cpf_cnpj.attr('maxlength', '14');
    }

    person_type.off('change').on('change', function (e) {
        contactPersonType($(this));
    });

    var $international       = $('select[name="international"]');
    var international        = $international.val();
    var ctx_contact_cpf_cnpj = $("#ctx_contact_cpf_cnpj_" + index);

    if(international == 'N') {
        if (v == "PJ")
        {
            ctx_contact_cpf_cnpj.html('CNPJ <i class="text-danger">*</i>\n\
            <span class="label text-left"><a target="_blank" href="https://www.receita.fazenda.gov.br/pessoajuridica/cnpj/cnpjreva/cnpjreva_solicitacao2.asp">Receita Federal</a></span>');
            $("input[name='contact_cpf_cnpj[" + index + "]']").mask('00.000.000/0000-00');
            class_pj.show(0);
            class_pf.hide(0);
        }
        else
        {
            ctx_contact_cpf_cnpj.html('CPF <i class="text-danger">*</i>\n\
            <span class="label text-left"><a target="_blank" href="https://www.receita.fazenda.gov.br/pessoajuridica/cnpj/cnpjreva/cnpjreva_solicitacao2.asp">Receita Federal</a></span>');
            $("input[name='contact_cpf_cnpj[" + index + "]']").mask('000.000.000-000');
            class_pf.show(0);
            class_pj.hide(0);
        }
    } else {
        ctx_contact_cpf_cnpj.html('Nº Documento <i class="text-danger">*</i>');
        $("input[name='contact_cpf_cnpj[" + index + "]']").unmask();

        if (v == "PJ")
        {
            class_pj.show(0);
            class_pf.hide(0);
        }
        else
        {
            class_pf.show(0);
            class_pj.hide(0);
        }
    }
}
/* FIM ########################################
 * CONTATOS/FATURAMENTO
 */