var route = document.getElementById("route-datatable").getAttribute("route-datatable");
var $modal_remote = $("#modal_remote");

var oTable2;

var oTable = $('#datatable').DataTable({
    "pageLength": 10,
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "columnDefs": [
        { "targets": [], "orderable": false },
        { "visible": false, "targets": [7, 8] }
    ],
    "ajax": {
        "url": route
    },
    "order": [[2, "asc"]],
    "createdRow": function (row, data, dataIndex) {
        if(data.last_credit_value > 0) {
            $(row).find('td:eq(0)')
                                .addClass('border-left-xlg border-left-info')
                                .attr('title', 'Valor do Crédito: R$ ' + $.number(data.last_credit_value, 2))
            ;
        } else {
            $(row).find('td:eq(0)')
                                .removeClass('border-left-xlg border-left-info')
                                .removeAttr('title')
            ;
        }
    },
    "columns": [
        { data: 'btnList', name: 'btnList', orderable: false, searchable: false },
        { data: 'id', name: 'id' },
        { data: 'name', name: 'name', searchable: false },
        { data: 'attendant.name', name: 'attendant.name', defaultContent: '-' },
        { data: 'representative.fantasy_name', name: 'representative.fantasy_name', defaultContent: '-' },
        { data: 'designer.name', name: 'designer.name', defaultContent: '-' },
        { data: 'cpf_cnpj', name: 'cpf_cnpj' },
        { data: 'fantasy_name', name: 'fantasy_name' },
        { data: 'company_name', name: 'company_name' },
        { data: 'phone', name: 'phone' },
        { data: 'email', name: 'email' },
        { data: 'created_at', name: 'created_at' },
    ]
});

$(document).ajaxStop(function () {

    $("a.btn-modal-join-orders").off('click').on('click', function (e) {
        e.preventDefault();
        var $this = $(this);
        var href = $this.attr('href');

        $.ajax({
            url: href,
            type: 'GET',
            dataType: 'html',
            success: function (response) {
                $modal_remote.modal('show').html(response);
                datatableOrders();
                datatableOPsUnified();
            },
            error: function (response) {
                console.log(response);
            },
        });
    });

});

function datatableOrders() {
    var oTable;
    var route = $("#datatable", $modal_remote).attr("datatable");

    var funcsTab1LoadDatatable = function () {
        $("input[name='all_order_id']", $(".modal-body", $modal_remote)).off('click').on('click', function () {
            var isCheck = $(this).is(':checked');
            $("input[name^='order_id']", $(".modal-body", $modal_remote)).prop('checked', isCheck);
        });
        $('button[type="button"]', $(".modal-body", $modal_remote)).off('click').on('click', function () {
            var ids = $("input[name^='order_id']:checked", $(".modal-body", $modal_remote)).map(
                function () { return this.value; }
            ).get().join(",");

            var count_ids = ids.split(',');

            if (ids != "" && ids != null && count_ids.length > 1) {
                var join_number = $('input[name="join_number"]', $modal_remote).val();
                var url_orders_join = $("#datatable", $modal_remote).attr("url_orders_join");
                popUpWin(url_orders_join + '/0/' + join_number + '?orders_id=' + ids, "900", "1000");
            } else {
                new PNotify({
                    title: 'Atenção',
                    text: 'Você deve marcar dois (2) ou mais OPs para serem unificados.',
                    addclass: 'bg-warning alert-styled-right'
                });
            }
        });
    };
    var tab1LoadDatatable = function () {
        var options = {
            "pageLength": 10,
            "processing": true,
            "serverSide": true,
            //"stateSave": true,
            "columnDefs": [
                { "visible": false, "targets": [3, 4, 5] }
            ],
            "ajax": {
                "url": route,
                "type": 'GET'
            },
            "order": [[9, "desc"]],
            "columns": [
                { data: 'input_checkbox', name: 'input_checkbox', orderable: false, searchable: false },
                { data: 'infoOrder', name: 'client.fantasy_name', searchable: true },
                { data: 'group', name: 'group' },
                { data: 'order_number', name: 'order_number', searchable: true },
                { data: 'client.company_name', name: 'client.company_name', searchable: true },
                { data: 'client.cpf_cnpj', name: 'client.cpf_cnpj', searchable: true },
                { data: 'str_actions', name: 'str_actions', orderable: false, searchable: false },
                { data: 'attendant', name: 'attendant', orderable: false, searchable: false },
                { data: 'delivery_date', name: 'delivery_date' },
                { data: 'updated_at', name: 'updated_at', searchable: false },
            ]
        };
        oTable = $('#datatable', $modal_remote).DataTable(options);
        funcsTab1LoadDatatable();
    }
    tab1LoadDatatable();
    $("ul.nav-tabs-highlight li:eq(0)", $modal_remote).off('click').on('click', function () {
        oTable.ajax.url(route).load();
        funcsTab1LoadDatatable();
    });
}

function datatableOPsUnified() {
    var route = $("#datatable-unified", $modal_remote).attr("datatable");

    $("ul.nav-tabs-highlight li:eq(1)", $modal_remote).off('click').on('click', function () {
        tab2LoadDatatable();
        oTable2.ajax.url(route).load();
    });
}

function tab2LoadDatatable() {
    var route = $("#datatable-unified", $modal_remote).attr("datatable");
    var options = {
        "pageLength": 10,
        "processing": true,
        "serverSide": true,
        //"stateSave": true,
        "columnDefs": [

        ],
        "ajax": {
            "url": route,
            "type": 'GET'
        },
        "order": [[4, "desc"]],
        "columns": [
            { data: 'btnActions', name: 'btnActions', orderable: false, searchable: false },
            { data: 'input_checkbox', name: 'input_checkbox', orderable: false, searchable: false },
            { data: 'order_number', name: 'order_number' },
            { data: 'orders_numbers', name: 'orders_id' },
            { data: 'created_at', name: 'created_at' },
            { data: 'created_by_user', name: 'created_by', orderable: false, searchable: false },
        ]
    };
    oTable2 = $('#datatable-unified', $modal_remote).DataTable(options);

    $("#check_order").on('click', function (e) {
        var $this = $(this);
        $(".check_order").prop('checked', $this.is(':checked'));
    });
}

function btnXMLFinancial(e) {
    var $this = $(e);
    var href = $this.attr('href');

    var ids = $(".check_order").map(function () {
        var $this = $(this);
        if ($this.is(':checked')) {
            return $this.val();
        }
    }).get().join(',');

    if (ids == null || ids == '') {
        new PNotify({
            title: 'Atenção',
            text: 'Nenhum item foi selecionado',
            addclass: 'bg-warning alert-styled-right'
        });
    } else {
        $.ajax({
            url: href + '/' + ids + '?model=OrderJoin',
            type: 'GET',
            dataType: 'html',
            beforeSend: function () {
                $.blockUI({ message: '<p><h4>aguarde...</h4></p> <p><h4>gerando XML</h4></p>' });
            },
            success: function (response) {
                $.unblockUI();
                $modal_remote.modal('show').html(response);
            },
            error: function (response) {
                $.unblockUI();
                console.log(response);
            },
        });
    }
}

function destroyUnified(url) {
    swal({
        title: 'Atenção',
        text: 'Deseja realmente excluir a OP?',
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Não",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, excluir!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                url: url,
                type: 'DELETE',
                dataType: 'json',
                success: function (response) {
                    if (!response.error) {
                        var route = $("#datatable-unified", $modal_remote).attr("datatable");
                        swal("Excluído!", response.message, "success");
                        tab2LoadDatatable();
                        oTable2.ajax.url(route).load();
                    } else {
                        swal("Erro", response.message, "error");
                    }
                },
                error: function (response) {
                    console.log(response);
                    swal.close();
                },
            });
        }
    });
}