var route = document.getElementById("route-customers-files").getAttribute("datatable-files");

var oTable = $('#datatable').DataTable({
    "pageLength": 10,
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "columnDefs": [{"targets": [], "orderable": false}],
    "ajax": {
        "url": route
    },
    "order": [[2, "asc"]],
    "columns": [
        {data: 'btnList', name: 'btnList', orderable: false, searchable: false},
        {data: 'id', name: 'id'},
        {data: 'title', name: 'title'},
        {data: 'file', name: 'file', orderable: false, searchable: false},
        {data: 'created_at', name: 'created_at'},
    ]
});