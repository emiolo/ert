<?php

namespace Modules\CerebeloClient\Http\Requests\Traits;

trait AddressTrait
{

    public function addressRules()
    {
        $attributes = $this->all();

        if (isset($attributes['cep']) && count($attributes['cep']) > 0):
            $this->items_address = count($attributes['cep']);
        endif;

        $contacts = [];
        for ($key = 0; $key < $this->items_address; $key++):
            $contacts["type.$key"] = 'required';
            $contacts["cep.$key"] = "required|max:9";
            $contacts["street.$key"] = 'required|max:250';
            $contacts["number.$key"] = 'required|max:50';
            $contacts["neighborhood.$key"] = 'required|max:150';
            $contacts["city.$key"] = "required|max:150";
        endfor;

        return $contacts;
    }

    public function addressMessages()
    {
        $attributes = $this->all();

        if (isset($attributes['cep']) && count($attributes['cep']) > 0):
            $this->items_address = count($attributes['cep']);
        endif;

        $contacts = [];
        for ($key = 0; $key < $this->items_address; $key++):
            $contacts['cep.' . $key . '.required'] = 'O campo CEP é obrigatório.';
            $contacts['street.' . $key . '.required'] = 'O campo Logradouro é obrigatório.';
            $contacts['number.' . $key . '.required'] = 'O campo Número é obrigatório.';
            $contacts['neighborhood.' . $key . '.required'] = 'O campo Bairro é obrigatório.';
            $contacts['city.' . $key . '.required'] = 'O campo Cidade é obrigatório.';
            $contacts['state.' . $key . '.required'] = 'O campo Estado é obrigatório.';
        endfor;

        return $contacts;
    }
}
