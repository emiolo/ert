<?php

namespace Modules\CerebeloClient\Http\Requests\Traits;

trait ContactsTrait
{

    public function contactRules()
    {
        $attributes = $this->all();
        if (isset($attributes['contact_person_type']) && count($attributes['contact_person_type']) > 0):
            $this->items_contact = count($attributes['contact_person_type']);
        endif;

        $contacts = [];
        for ($key = 0; $key < $this->items_contact; $key++):
            $contacts["contact_person_type.$key"] = 'max:2';
            $contacts["contact_name.$key"] = "required_if:contact_person_type.$key,PF|max:150";
            $contacts["contact_phone.$key"] = 'max:15|phone_with_ddd_if:international,Y';
            $contacts["contact_email.$key"] = 'email|max:150';
            $contacts["contact_cpf_cnpj.$key"] = 'required|max:18|cpf_cnpj_if:international,Y';
            $contacts["contact_state_registration.$key"] = "max:20";
            $contacts["contact_company_name.$key"] = "required_if:contact_person_type.$key,PJ|max:150";
            $contacts["contact_fantasy_name.$key"] = "required_if:contact_person_type.$key,PJ|max:150";
            $contacts["contact_cep.$key"] = 'max:9';
            $contacts["contact_street.$key"] = 'max:250';
            $contacts["contact_number.$key"] = 'max:50';
            $contacts["contact_neighborhood.$key"] = 'max:150';
            $contacts["contact_city.$key"] = 'max:150';
            $contacts["contact_state.$key"] = 'max:2';
        endfor;

        return $contacts;
    }

    public function contactMessages()
    {
        $attributes = $this->all();

        if (isset($attributes['contact_person_type']) && count($attributes['contact_person_type']) > 0):
            $this->items_contact = count($attributes['contact_person_type']);
        endif;

        $contacts = [];
        for ($key = 0; $key < $this->items_contact; $key++):
            $contacts['contact_name.' . $key . '.required_if'] = 'O campo Nome em Contatos/Faturamento é obrigatório quando Tipo de Pessoa é Pessoa Física.';
            $contacts['contact_name.' . $key . '.max'] = 'O campo Nome em Contatos/Faturamento não deve ter mais que 150 caracteres.';
            $contacts['contact_phone.' . $key . '.max'] = 'O campo Telefone em Contatos/Faturamento não deve ter mais que 15 caracteres.';
            $contacts['contact_phone.' . $key . '.phone_with_ddd_if'] = 'O campo Telefone em Contatos/Faturamento deve ser preenchido corretamente se o Cliente não for internacional.';
            $contacts['contact_email.' . $key . '.email'] = 'O campo Email em Contatos/Faturamento deve ser um endereço de e-mail válido.';
            $contacts['contact_email.' . $key . '.max'] = 'O campo Email em Contatos/Faturamento não deve ter mais que 150 caracteres.';
            $contacts['contact_cpf_cnpj.' . $key . '.max'] = 'O campo CPF/CNPJ em Contatos/Faturamento não deve ter mais que 18 caracteres.';
            $contacts['contact_cpf_cnpj.' . $key . '.required'] = 'O campo CPF/CNPJ/Nº Documento em Contatos/Faturamento é obrigatório.';
            $contacts['contact_cpf_cnpj.' . $key . '.cpf_cnpj_if'] = 'O campo CPF/CNPJ em Contatos/Faturamento deve ser preenchido corretamente se o Cliente não for internacional.';
            $contacts['contact_company_name.' . $key . '.required_if'] = 'O campo Razão Social em Contatos/Faturamento é obrigatório quando Tipo de Pessoa é Pessoa Jurídica.';
            $contacts['contact_company_name.' . $key . '.max'] = 'O campo Razão Social em Contatos/Faturamento não deve ter mais que 150 caracteres.';
            $contacts['contact_fantasy_name.' . $key . '.required_if'] = 'O campo Nome Fantasia em Contatos/Faturamento é obrigatório quando Tipo de Pessoa é Pessoa Jurídica.';
            $contacts['contact_fantasy_name.' . $key . '.max'] = 'O campo Nome Fantasia em Contatos/Faturamento não deve ter mais que 150 caracteres.';
            $contacts['contact_cep.' . $key . '.max'] = 'O campo CEP em Contatos/Faturamento não deve ter mais que 9 caracteres.';
            $contacts['contact_street.' . $key . '.max'] = 'O campo Complemento em Contatos/Faturamento não deve ter mais que 250 caracteres.';
            $contacts['contact_number.' . $key . '.max'] = 'O campo Número em Contatos/Faturamento não deve ter mais que 50 caracteres.';
            $contacts['contact_neighborhood.' . $key . '.max'] = 'O campo Bairro em Contatos/Faturamento não deve ter mais que 150 caracteres.';
            $contacts['contact_city.' . $key . '.max'] = 'O campo Cidade em Contatos/Faturamento não deve ter mais que 150 caracteres.';
            $contacts['contact_state.' . $key . '.max'] = 'O campo Estado em Contatos/Faturamento não deve ter mais que 2 caracteres.';
        endfor;

        return $contacts;
    }
}
