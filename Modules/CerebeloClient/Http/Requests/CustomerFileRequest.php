<?php

namespace Modules\CerebeloClient\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerFileRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'file'  => 'required|mimes:jpeg,jpg,png,pdf,doc,docx,xls,xlsx',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

}
