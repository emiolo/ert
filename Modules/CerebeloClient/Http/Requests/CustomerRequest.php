<?php

namespace Modules\CerebeloClient\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\CerebeloClient\Http\Requests\Traits\AddressTrait;
use Modules\CerebeloClient\Http\Requests\Traits\ContactsTrait;

class CustomerRequest extends FormRequest
{

    public $items_address;
    public $items_contact;

    use AddressTrait
    , ContactsTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = '|unique:clients,cpf_cnpj,NULL,id,deleted_at,NULL';

        if (isset($this->id) && !empty($this->id)) {
            $unique = '|unique:clients,cpf_cnpj,' . $this->id . ',id,deleted_at,NULL';
        }

        $rules = [
            'attendant_id' => 'required',
            'representative_id' => 'required',
            'designer_id' => 'required',
            'person_type' => 'required',
            'cpf_cnpj' => 'required|max:18|cpf_cnpj_if:international,Y' . $unique,
            'state_registration' => 'max:20',
            'company_name' => 'required_if:person_type,PJ|max:150',
            'fantasy_name' => 'required_if:person_type,PJ|max:150',
            'name' => 'required_if:person_type,PF|max:150',
            'phone' => 'required|max:15|phone_with_ddd_if:international,Y',
            'email' => 'required|email|max:150',
            'categories' => 'required',
        ];

        $result = array_merge($rules, $this->contactRules());
        $result = array_merge($result, $this->addressRules());

        return $result;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        $rules = [
            'categories.*.required' => 'O campo Categorias é obrigatório.',
            'cpf_cnpj.required' => 'O campo CPF/CNPJ/Nº Documento é obrigatório.',
            'cpf_cnpj.cpf_cnpj_if' => 'O campo CPF/CNPJ deve ser preenchido corretamente se o Cliente não for internacional.',
            'phone.phone_with_ddd_if' => 'O campo Telefone deve ser preenchido corretamente se o Cliente não for internacional.',
        ];

        $result = array_merge($rules, $this->contactMessages());
        $result = array_merge($result, $this->addressMessages());

        return $result;
    }

}
