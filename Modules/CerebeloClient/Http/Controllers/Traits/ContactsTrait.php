<?php

namespace Modules\CerebeloClient\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Form;
use Modules\CerebeloClient\Entities\ClientContact;

trait ContactsTrait
{

    private function traitCreateContact(Request $request) {
        $key = $request->get('last_card_contact');

        return view(self::MODULE_VIEW . self::FOLDER . '_panel_contact', compact('key'))->render();
    }

    public function destroyContact($id) {
        $destroy = ClientContact::destroy($id);
        if($destroy)
        {
            return response()->json(['error' => false, 'message' => trans('messages.success.destroy')]);
        }

        return response()->json(['error' => true, 'message' => trans('messages.error.destroy')]);
    }

    private function traitSaveContact(Request $request, $model) {
        $attributes = $request->all();
        $contact_id = $request->get('contact_id', []);
        $contacts = [];

        foreach ($contact_id as $key => $value) {
            if(empty($value)) {
                $contact = new ClientContact();
            } else {
                $contact = ClientContact::find($value);
            }

            if ($attributes['contact_person_type'][$key] == 'PF') {
                $attributes['contact_fantasy_name'][$key] = $attributes['contact_name'][$key];
            }

            $contact->fill([
                'person_type'        => $attributes['contact_person_type'][$key],
                'cpf_cnpj'           => $attributes['contact_cpf_cnpj'][$key],
                'company_name'       => $attributes['contact_company_name'][$key],
                'fantasy_name'       => $attributes['contact_fantasy_name'][$key],
                'state_registration' => $attributes['contact_state_registration'][$key],
                'rg'                 => $attributes['contact_rg'][$key],
                'phone'              => $attributes['contact_phone'][$key],
                'email'              => $attributes['contact_email'][$key],
            ]);

            if(!empty($contact))
                $contact = $model->contacts()->save($contact);

            // Salvar endereço
            if (!empty($contact)) {
                $cep          = $attributes['contact_cep'][$key];
                $address_id   = !isset($attributes['contact_address_id'][$key]) ? NULL : $attributes['contact_address_id'][$key];
                $type         = 'C';
                $table        = 'clients_contacts';
                $street       = $attributes['contact_street'][$key];
                $number       = $attributes['contact_number'][$key];
                $complement   = $attributes['contact_complement'][$key];
                $neighborhood = $attributes['contact_neighborhood'][$key];
                $city         = $attributes['contact_city'][$key];
                $state        = $attributes['contact_state'][$key];
                $country      = $attributes['contact_country'][$key];
                $local        = $attributes['contact_local'][$key];

                $addresses = array(
                    'cep'          => $cep,
                    'type'         => $type,
                    'table'        => $table,
                    'street'       => $street,
                    'number'       => $number,
                    'complement'   => $complement,
                    'neighborhood' => $neighborhood,
                    'city'         => $city,
                    'state'        => $state,
                    'local'        => $local,
                    'country'      => $country,
                );

                if (empty($address_id)) {
                    $address = new $this->_address();
                } else {
                    $address = $this->_address->find($address_id);
                }

                $address->fill($addresses);
                $contact->address()->save($address);
            }
        }
    }
}
