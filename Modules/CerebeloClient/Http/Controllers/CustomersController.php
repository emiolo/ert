<?php

namespace Modules\CerebeloClient\Http\Controllers;

use Datatables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use JsValidator;
use Modules\CerebeloClient\Entities\Client;
use Modules\CerebeloClient\Http\Controllers\CerebeloController as Controller;
use Modules\CerebeloClient\Http\Controllers\Traits\ContactsTrait;
use Modules\CerebeloClient\Http\Requests\CustomerRequest;
use Modules\CerebeloOrders\Entities\Order;
use Modules\CerebeloProducts\Entities\ProductCategory;
use Modules\CerebeloRepresentatives\Entities\Representative;
use Modules\CerebeloSettings\Entities\Address;
use Modules\CerebeloSettings\Entities\Sector;
use Modules\CerebeloSettings\Entities\User;
use Modules\CerebeloTable\Entities\PriceTable;
use Validator;

class CustomersController extends Controller
{

    use ContactsTrait;

    const FOLDER = "customers.";

    protected $_client;
    protected $_user;
    protected $_price_table;
    protected $_representative;
    protected $_address;
    protected $_comment;
    protected $_sector;
    protected $_order;
    protected $_product_category;

    public function __construct()
    {
        $this->_client = new Client();
        $this->_user = new User();
        $this->_price_table = new PriceTable();
        $this->_representative = new Representative();
        $this->_address = new Address();
        $this->_sector = new Sector();
        $this->_order = new Order();
        $this->_product_category = new ProductCategory();
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view(self::MODULE_VIEW . self::FOLDER . 'index');
    }

    /**
     * Tratar os dados da Categoria caso seja novo/pré-existente
     * @param  array $categories
     * @return array
     */
    public function categories($categories)
    {
        $new_categories = [];
        $pre_categories = [];
        array_map(function ($k, $v) use (&$new_categories, &$pre_categories) {
            if (intval($v) > 0) {
                array_push($pre_categories, $v);
            } else {
                array_push($new_categories, $v);
            }
        }, array_keys($categories), array_values($categories));

        // Salvar nova categoria
        if (count($new_categories) > 0) {
            foreach ($new_categories as $key => $value) {
                $model = $this->_product_category->updateOrCreate(['name' => $value], ['name' => $value, 'enable' => 'Y']);
                $pre_categories[] = $model->id;
            }
        }

        return $pre_categories;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $CustomerRequest = new CustomerRequest();
        $CustomerRequest->items_address = count(old('cep'));
        $CustomerRequest->items_contact = count(old('contact_person_type'));
        $validator = JsValidator::formRequest($CustomerRequest, '#formCreate');

        $categories = $this->_product_category->formSelect();
        $attendant_users = $this->_user->formSelectAttendant();
        $designer_users = $this->_user->formSelectDesigner();
        $representatives = $this->_representative->formSelect();
        $tables = $this->_price_table->formSelect();
        $sectors = $this->_client->allSectorsWithCommentsQuantity();
        $tables_selected = [];

        $script_attendant = "";
        $auth = auth()->user();
        $sector_alias = $auth->sector->alias;
        if ($sector_alias == Sector::ATTENDANCE) {
            $user_id = $auth->id;
            $script_attendant = '<script type="text/javascript">$("select[name=attendant_id]").val(' . $user_id . ');</script>';
        }

        return view(self::MODULE_VIEW . self::FOLDER . 'create', compact('script_attendant', 'validator', 'categories', 'attendant_users', 'designer_users', 'representatives', 'tables', 'sectors', 'tables_selected'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CustomerRequest $request)
    {
        $attributes = $request->all();

        if ($attributes['person_type'] == 'PF') {
            $attributes['fantasy_name'] = $attributes['name'];
        }

        $create = $this->_client->create($attributes);

        if ($create) {
            // Salvar Contatos
            $this->traitSaveContact($request, $create);

            $categories = $request->get('categories', []);
            $categories_id = $this->categories($categories);
            $create->categories()->sync($categories_id);

            // Salvar endereço
            if (isset($attributes['cep'])) {
                foreach ($attributes['cep'] as $key => $value) {
                    $cep = $value;
                    if (!empty($cep)) {
                        $address_id = $attributes['address_id'][$key];
                        $type = $attributes['type'][$key];
                        $table = $attributes['table'][$key];
                        $street = $attributes['street'][$key];
                        $number = $attributes['number'][$key];
                        $complement = $attributes['complement'][$key];
                        $neighborhood = $attributes['neighborhood'][$key];
                        $city = $attributes['city'][$key];
                        $state = $attributes['state'][$key];
                        $local = $attributes['local'][$key];

                        $addresses = array(
                            'cep' => $cep,
                            'type' => $type,
                            'table' => $table,
                            'street' => $street,
                            'number' => $number,
                            'complement' => $complement,
                            'neighborhood' => $neighborhood,
                            'city' => $city,
                            'state' => $state,
                            'local' => $local,
                        );

                        if (empty($address_id)) {
                            $address = new $this->_address();
                        } else {
                            $address = $this->_address->find($address_id);
                        }
                        $address->fill($addresses);
                        $create->address()->save($address);
                    }
                }
            }

            // Salvar tabelas
            $create->priceTables()->sync((!isset($attributes['tables_id']) ? [] : $attributes['tables_id']));

            $user_id = auth()->user()->id;
            // Salvar observação/comentário
            if (!empty($attributes['comment'])) {
                $create->comments()->attach($user_id, ['comment' => $attributes['comment']]);
            }

            $client_id = $create->id;
            $representative_id = $attributes['representative_id'];
            $attendant_id = $attributes['attendant_id'];
            $now_date = date('dmy');

            $order = $this->_order->create(['clients_id' => $client_id, 'order_number' => '']);
            $id = $order->id;
            $order_number = preg_replace('/\D/', '', $order->order_number);
            if (empty($order_number)) {
                $order_number = "$representative_id.$attendant_id.$client_id.$now_date.$id";
                $order->update([
                    'order_number' => $order_number,
                ]);

                $users_id = $order->client->attendant_id;
                $user = $this->_user->with('sector')->find($users_id);
                if (isset($user->sector->period)) {
                    $period = $user->sector->period;
                    $period_option = $user->sector->period_option;
                    $send = true;

                    if ($send === true) {
                        $order->sectors()->attach([$users_id => calculatePeriodPerSector($period_option, $period, 'Y')]);
                    }
                }
            }

            return redirect()->to(route('cerebelo.orders.edit', $id))->with('success', trans('messages.success.create'));
        }

        return $this->redirectBackAfterError(trans('messages.error.create'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view(self::MODULE_VIEW . self::FOLDER . 'show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $CustomerRequest = new CustomerRequest();
        $CustomerRequest->items_address = count(old('cep'));
        $CustomerRequest->items_contact = count(old('contact_person_type'));
        $validator = JsValidator::formRequest($CustomerRequest, '#formEdit');

        $categories = $this->_product_category->formSelect();
        $model = $this->_client->load('addresses', 'contacts.address')->find($id);

        $attendant_users = $this->_user->formSelectAttendant();
        $designer_users = $this->_user->formSelectDesigner();
        $representatives = $this->_representative->formSelect();
        $tables = $this->_price_table->formSelect();
        $tables_selected = $model->formMultiSelectTablesSelected();
        $sectors = $this->_client->allSectorsWithCommentsQuantity($id);

        $script_attendant = "";
        $auth = auth()->user();
        $sector_alias = $auth->sector->alias;
        if ($sector_alias == Sector::ATTENDANCE) {
            $user_id = $auth->id;
            if ($model->attendant_id == $user_id) {
                $script_attendant = '<script type="text/javascript">$("select[name=attendant_id]").val(' . $user_id . ');</script>';
            }
        }

        return view(self::MODULE_VIEW . self::FOLDER . 'edit', compact('script_attendant', 'validator', 'categories', 'model', 'attendant_users', 'designer_users', 'representatives', 'tables', 'tables_selected', 'sectors'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(CustomerRequest $request, $id)
    {
        $attributes = $request->all();

        $update = $this->_client->find($id);

        if ($attributes['person_type'] == 'PF') {
            $attributes['fantasy_name'] = $attributes['name'];
        }

        $update->fill($attributes);

        if ($update->save()) {
            // Salvar Contatos
            $this->traitSaveContact($request, $update);

            $categories = $request->get('categories', []);
            $categories_id = $this->categories($categories);
            $update->categories()->sync($categories_id);

            // Salvar endereço
            if (isset($attributes['cep'])) {
                foreach ($attributes['cep'] as $key => $value) {
                    $cep = $value;
                    if (!empty($cep)) {
                        $address_id = $attributes['address_id'][$key];
                        $type = $attributes['type'][$key];
                        $table = $attributes['table'][$key];
                        $street = $attributes['street'][$key];
                        $number = $attributes['number'][$key];
                        $complement = $attributes['complement'][$key];
                        $neighborhood = $attributes['neighborhood'][$key];
                        $city = $attributes['city'][$key];
                        $state = $attributes['state'][$key];
                        $local = $attributes['local'][$key];

                        $addresses = array(
                            'cep' => $cep,
                            'type' => $type,
                            'table' => $table,
                            'street' => $street,
                            'number' => $number,
                            'complement' => $complement,
                            'neighborhood' => $neighborhood,
                            'city' => $city,
                            'state' => $state,
                            'local' => $local,
                        );

                        if (empty($address_id)) {
                            $address = new $this->_address();
                        } else {
                            $address = $this->_address->find($address_id);
                        }
                        $address->fill($addresses);
                        $update->address()->save($address);
                    }
                }
            }

            // Salvar tabelas
            $update->priceTables()->sync((!isset($attributes['tables_id']) ? [] : $attributes['tables_id']));

            return $this->redirectAfterSuccess(trans('messages.success.create'), 'cerebelo.' . self::FOLDER . 'index');
        }

        return $this->redirectBackAfterError(trans('messages.error.create'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->_client->destroy($id)) {
            return $this->redirectBackAfterSuccess(trans('messages.success.destroy'));
        }

        return $this->redirectBackAfterError(trans('messages.error.destroy'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function datatable()
    {
        return Datatables::of($this->_client->datatable())
            ->addColumn('btnList', function ($object) {
                $array = [
                    'go_orders' => [
                        'title' => 'Pedidos',
                        'i_class' => 'icon-list3',
                        'class' => 'text-default',
                        'href' => route('cerebelo.orders.index') . "?q=" . $object->cpf_cnpj . '&c=' . $object->id,
                    ],
                    'files' => [
                        'title' => 'Anexar Arquivos',
                        'i_class' => 'icon-attachment',
                        'class' => 'text-default',
                        'href' => route('cerebelo.' . self::FOLDER . 'files_index', $object->id),
                    ],
                    'join_orders' => [
                        'title' => 'Juntar OPs',
                        'i_class' => 'icon-box',
                        'class' => 'text-muted btn-modal-join-orders',
                        'href' => route('cerebelo.' . self::FOLDER . 'join', $object->id),
                    ],
                    'edit' => [
                        'route' => route('cerebelo.' . self::FOLDER . 'edit', $object->id),
                    ],
                    'destroy' => [
                        'route' => route('cerebelo.' . self::FOLDER . 'destroy', $object->id),
                    ],
                ];
                return iconsList($array);
            })
            ->escapeColumns([])
            ->make(true);
    }

    /** Ajax
     * Listar pedido do Cliente
     * @return Response
     */
    public function joinOrders($client_id)
    {
        return view(self::MODULE_VIEW . self::FOLDER . '_orders', compact('client_id'))->render();
    }

    /** Ajax
     * Pesquisa/Carrega clientes por cpf/cnpj
     * @return Response
     */
    public function searchByQuery(Request $request)
    {
        $q = $request->get('q');
        $page = $request->get('page');

        $clients = $this->_client->searchByQuery($q);

        return response()->json($clients);
    }

    /** Ajax
     * Pesquisa/Carrega clientes por cpf/cnpj
     * @return Response
     */
    public function searchByCpfCnpj(Request $request)
    {
        $cpf_cnpj = $request->get('cpf_cnpj');

        $client = $this->_client->firstCpfCnpj($cpf_cnpj);

        return response()->json($client);
    }

    /** Ajax
     * Verifica se o cpf/cnpj existe
     * @return Response
     */
    public function checkClientByCpfCnpj(Request $request)
    {
        $cpf_cnpj = $request->get('cpf_cnpj');

        $client = $this->_client->findCpfCnpj($cpf_cnpj)->first();

        if (empty($client)) {
            return response()->json(['error' => false, 'message' => "CPF/CNPJ não cadastrado."]);
        }

        return response()->json(['error' => true, 'message' => route('cerebelo.customers.edit', $client->id)]);
    }

    /** Ajax
     * Exibe os comentários por sector
     * @return Response
     */
    public function comments(Request $request, $client_id)
    {
        $sector_id = $request->get('sector_id');

        $comments = $this->_client->find($client_id)->commentsForSectors($sector_id);

        return view(self::MODULE_VIEW . self::FOLDER . '_comments', compact('comments', 'sector_id'))->render();
    }

    /** Ajax
     * Salva a observação do usuário/setor
     * @return Response
     */
    public function storeComments(Request $request, $client_id)
    {
        $validator = Validator::make($request->all(), ['observation' => 'required']);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $msg = "";
            foreach ($messages->all(':message<br/>') as $value) {
                $msg .= $value;
            }
            return response()->json(['error' => true, 'message' => $msg]);
        }

        $client = $this->_client->find($client_id);
        $user_id = auth()->user()->id;
        $comment = $request->get('observation');

        $client->comments()->attach($user_id, ['comment' => $comment]);

        return response()->json(['error' => false, 'message' => trans('messages.success.create')]);
    }

    /** Ajax
     * Carregar as tabelas vinculadas/liberadas para o cliente
     * @return Response
     */
    public function loadTablesByClient(Request $request)
    {
        $client_id = $request->get('client_id');

        $model = $this->_client->find($client_id);

        $tables_selected = $model->loadMultiSelectTablesSelected();

        return response()->json($tables_selected);
    }

    /*
     * Início - Traits
     */

    /* Show the form for creating a new resource.
     *
     * @param  Request $request
     * @return Response
     */
    public function createContact(Request $request)
    {
        return $this->traitCreateContact($request);
    }

    /*
 * Fim - Traits
 */

}
