<?php

namespace Modules\CerebeloClient\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\CerebeloClient\Http\Controllers\CerebeloController as Controller;
use Validator;
use Modules\CerebeloClient\Entities\Client;
use Modules\CerebeloClient\Entities\ClientComment;

class CustomersCommentsController extends Controller
{

    const FOLDER = "customers.";

    protected $_client;
    protected $_client_comment;

    public function __construct(Client $_client, ClientComment $client_comment)
    {
        $this->_client         = $_client;
        $this->_client_comment = $client_comment;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request, $client_id)
    {
        $sector_id = $request->get('sector_id');

        $comments = $this->_client->find($client_id)->commentsForSectors($sector_id);

        return view(self::MODULE_VIEW . self::FOLDER . '_comments', compact('comments', 'sector_id'))->render();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request, $client_id)
    {
        $validator = Validator::make($request->all(), ['observation' => 'required']);
        if ($validator->fails())
        {
            $messages = $validator->messages();
            $msg      = "";
            foreach ($messages->all(':message<br/>') as $value)
            {
                $msg .= $value;
            }
            return response()->json(['error' => true, 'message' => $msg]);
        }

        $client  = $this->_client->find($client_id);
        $user_id = auth()->user()->id;
        $comment = $request->get('observation');

        $client->comments()->attach($user_id, ['comment' => $comment]);

        return response()->json(['error' => false, 'message' => trans('messages.success.create')]);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $model = $this->_client_comment->find($id);

        return view(self::MODULE_VIEW . self::FOLDER . '_edit_comment', compact('model'))->render();
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), ['comment' => 'required']);
        if ($validator->fails())
        {
            $messages = $validator->messages();
            $msg      = "";
            foreach ($messages->all(':message<br/>') as $value)
            {
                $msg .= $value;
            }
            return response()->json(['error' => true, 'message' => $msg]);
        }

        $comment  = $this->_client_comment->find($id);
        $users_id = $comment->users_id;

        $user_id = auth()->user()->id;

        if ($users_id != $user_id)
        {
            return response()->json(['error' => true, 'message' => "Somente o responsável pode editar esse comentário."]);
        }

        $comment->comment = $request->get('comment');

        if ($comment->save())
        {
            return response()->json(['error' => false, 'message' => trans('messages.success.update')]);
        }

        return response()->json(['error' => true, 'message' => trans('messages.error.update')]);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $comment  = $this->_client_comment->find($id);
        $users_id = $comment->users_id;
        $user_id  = auth()->user()->id;

        if ($users_id != $user_id)
        {
            return response()->json(['error' => true, 'message' => "Somente o responsável pode excluir esse comentário."]);
        }

        if ($comment->delete())
        {
            return response()->json(['error' => false, 'message' => trans('messages.success.destroy')]);
        }

        return response()->json(['error' => true, 'message' => trans('messages.error.destroy')]);
    }

}
