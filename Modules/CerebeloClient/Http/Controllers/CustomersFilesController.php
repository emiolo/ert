<?php

namespace Modules\CerebeloClient\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\CerebeloClient\Http\Controllers\CerebeloController as Controller;
use Datatables;
use Modules\CerebeloClient\Http\Requests\CustomerFileRequest;
use Modules\CerebeloClient\Entities\File as ClientFile;
use Modules\CerebeloClient\Entities\Client;
use Storage;

class CustomersFilesController extends Controller
{

    const FOLDER = "files.";

    protected $_file;
    protected $_client;

    public function __construct(ClientFile $file, Client $client)
    {
        $this->_file   = $file;
        $this->_client = $client;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($id)
    {
        $client = $this->_client->find($id);

        return view(self::MODULE_VIEW . self::FOLDER . 'index', compact('client'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CustomerFileRequest $request, $id)
    {
        $attributes = $request->all();
        $file       = $request->file('file');

        $storage = Storage::disk('local');

        $extension     = $file->getClientOriginalExtension();
        $new_file_name = "clients_files/" . md5($file->getFilename()) . '.' . $extension;

        if ($storage->put($new_file_name, file_get_contents($file->getRealPath())))
        {
            $client = $this->_client->find($id);

            $file = new $this->_file();

            $attributes['file'] = $new_file_name;

            $file->fill($attributes);
            $client->files()->save($file);

            return $this->redirectBackAfterSuccess(trans('messages.success.create'));
        }

        return $this->redirectBackAfterError(trans('messages.error.create'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $model = $this->_file->find($id);

        return view(self::MODULE_VIEW . self::FOLDER . 'edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(CustomerRequest $request, $id)
    {
        $attributes = $request->all();

        $update = $this->_file->find($id);

        $update->fill($attributes);

        if ($update->save())
        {
            return $this->redirectAfterSuccess(trans('messages.success.create'), 'cerebelo.' . self::FOLDER . 'index');
        }

        return $this->redirectBackAfterError(trans('messages.error.create'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->_file->destroy($id))
        {
            return $this->redirectBackAfterSuccess(trans('messages.success.destroy'));
        }

        return $this->redirectBackAfterError(trans('messages.error.destroy'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function datatable($id)
    {
        return Datatables::of($this->_file->datatable($id))
                        ->addColumn('file', function ($object) {
                            return '<a href="' . route('storage.files', ['filename' => $object->file, 'download' => true]) . '">Clique aqui para baixar</a>';
                        })
                        ->addColumn('btnList', function ($object) {
                            $array = [
                                'destroy' => [
                                    'route' => route('cerebelo.customers.files_destroy', $object->id),
                                ],
                            ];
                            return iconsList($array);
                        })
                        ->escapeColumns([])
                        ->make(true);
    }

}
