<?php

Route::group(['middleware' => ['web', 'cerebelo.authenticated'], 'prefix' => 'cerebelo', 'namespace' => 'Modules\CerebeloClient\Http\Controllers'], function() {

    Route::group(['middleware' => ['cerebelo.permission']], function() {

        Route::group(['prefix' => 'customers'], function() {
            Route::get('/', 'CustomersController@index')->name('cerebelo.customers.index');
            Route::get('/listing', 'CustomersController@datatable')->name('cerebelo.customers.datatable');
            Route::get('/create', 'CustomersController@create')->name('cerebelo.customers.create');
            Route::post('/', 'CustomersController@store')->name('cerebelo.customers.store');
            Route::get('/search-query', 'CustomersController@searchByQuery')->name('cerebelo.customers.search_by_query');
            Route::post('/search-by-cpfcnpj', 'CustomersController@searchByCpfCnpj')->name('cerebelo.customers.search_by_cpfcnpj');
            Route::get('/search-by-cpfcnpj', 'CustomersController@checkClientByCpfCnpj')->name('cerebelo.customers.search_by_cpfcnpj');
            Route::post('/load-tables-by-client', 'CustomersController@loadTablesByClient')->name('cerebelo.customers.load_tables_by_client');
            Route::get('/show/{id}', 'CustomersController@show')->name('cerebelo.customers.show');
            Route::get('/edit/{id}', 'CustomersController@edit')->name('cerebelo.customers.edit');
            Route::match(['put', 'patch'], '/update/{id}', 'CustomersController@update')->name('cerebelo.customers.update');
            Route::delete('/delete/{id}', 'CustomersController@destroy')->name('cerebelo.customers.destroy');

            /*
             * Comentários
             */
            Route::get('/comments/{client_id}', 'CustomersCommentsController@index')->name('cerebelo.customers.comments_index');
            Route::post('/comments/{client_id}', 'CustomersCommentsController@store')->name('cerebelo.customers.comments_store');
            Route::get('/comments/edit/{id}', 'CustomersCommentsController@edit')->name('cerebelo.customers.comments_edit');
            Route::match(['put', 'patch'], '/comments/update/{id}', 'CustomersCommentsController@update')->name('cerebelo.customers.comments_update');
            Route::delete('/comments/delete/{id}', 'CustomersCommentsController@destroy')->name('cerebelo.customers.comments_destroy');
            /*
             * Arquivos
             */
            Route::get('/files/{id}', 'CustomersFilesController@index')->name('cerebelo.customers.files_index');
            Route::get('/files/listing/{id}', 'CustomersFilesController@datatable')->name('cerebelo.customers.files_datatable');
            Route::post('/files/{id}', 'CustomersFilesController@store')->name('cerebelo.customers.files_store');
            Route::delete('/files/{id}', 'CustomersFilesController@destroy')->name('cerebelo.customers.files_destroy');
            /*
            * Pedidos
            */
            Route::get('/join-orders/{client_id}', 'CustomersController@joinOrders')->name('cerebelo.customers.join');
            /*
            * Contatos/Faturamento
            */
            Route::get('/contacts/create', 'CustomersController@createContact')->name('cerebelo.customers.contact');
            Route::delete('/contacts/{id}', 'CustomersController@destroyContact')->name('cerebelo.customers.contact_destroy');
        });
    });
    Route::post('/customers/import', function() {
        $file      = request()->file('file');
        $extension = $file->getClientOriginalExtension();

        if (!in_array($extension, ['xls']))
        {
            return redirect()->back()->with('failure', "Somente arquivo com extensão .xls");
        }

        $real_path = $file->getRealPath();
        $complete  = false;
        $message   = "";

        \Excel::selectSheetsByIndex(0)->load($real_path, function($reader) use(&$complete, &$message) {
            $results = $reader->all()->toArray();

            $key = 1;
            foreach ($results as $value)
            {
                $row = array_values($value);

                if (count($row) < 11)
                {
                    $message = "Seu arquivo não esta configurado corretamento conforme o exemplo disponível para download.";
                    break;
                }

                $company_name       = trim($row[0]); // Nome Fantasia tbm
                $cep                = str_replace(' ', '', trim($row[2]));
                $street             = trim($row[3]);
                $neighborhood       = trim($row[4]);
                $city               = trim($row[5]);
                $state              = trim($row[6]);
                $cpf_cnpj           = str_replace(' ', '', trim($row[7]));
                $state_registration = str_replace(' ', '', trim($row[8]));
                $phone              = str_replace('x', '', str_replace(' ', '', trim($row[9])));
                $phone2             = str_replace('x', '', str_replace(' ', '', trim($row[10])));
                $email              = trim($row[12]);

//                if (empty($company_name) || empty($cep) || empty($street) || empty($neighborhood) || empty($city) || empty($state) || empty($cpf_cnpj) || empty($phone) || empty($email))
//                {
//                    $message = "As colunas Razão Social, Dados de endereço, CPF/CNPJ, Telefone e E-mail são obrigatórias. (na linha $key)";
//                    break;
//                }

                $attributes = [
                    'attendant_id'       => 1,
                    'representative_id'  => 1,
                    'designer_id'        => 1,
                    'cpf_cnpj'           => $cpf_cnpj,
                    'state_registration' => $state_registration,
                    'company_name'       => $company_name,
                    'fantasy_name'       => $company_name,
                    'phone'              => $phone,
                    'phone2'             => $phone2,
                    'email'              => $email,
                ];

                try
                {
                    $entitie = Modules\CerebeloClient\Entities\Client::updateOrCreate(['cpf_cnpj' => $cpf_cnpj], $attributes);

                    $attributes = [
                        'table_id'     => $entitie->id,
                        'table'        => 'clients',
                        'type'         => 'C',
                        'local'        => '',
                        'cep'          => $cep,
                        'number'       => '',
                        'street'       => $street,
                        'neighborhood' => $neighborhood,
                        'city'         => $city,
                        'state'        => $state,
                    ];
                    $entitie->address()->create($attributes);

                    $complete = true;
                }
                catch (Illuminate\Database\QueryException $ex)
                {
                    $message = $ex->getMessage();
                    break;
                }

                $key++;
            }
        });

        if ($complete && empty($message))
        {
            return redirect()->back()->with('success', "Importação efetuado com sucesso.");
        }

        return redirect()->back()->with('failure', $message);
    })->name('cerebelo.customers.import');
    Route::get('/customers/import', function() {
        return view('cerebeloclient::import');
    })->name('cerebelo.customers.import');
});
