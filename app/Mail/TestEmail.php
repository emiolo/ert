<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TestEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /*
     * Usar:
    $data = ['content_body' => 'Pah pah!!'];
    \Mail::to('sotnas.lony@emiolo.com')->send(new \App\Mail\TestEmail($data));
    dd('E-mail Enviado.');
     */

    public function build()
    {
        $data = $this->data;
        $subject = 'eMiolo Teste :: ' . "Do Host: " . env('MAIL_HOST') . " - Enviado em: " . date("d/m/Y H:i:s");

        return $this->view('emails.test')
                    ->to("tmrenanjf@yahoo.com.br", "Renan Yahoo")
                    ->to("testeemiolo@hotmail.com", "Renan Hotmail")
                    //->cc("sotnas.lony@emiolo.com", "Sotnas eMiolo.com")
                    ->to("lony2006@hotmail.com", "Sotnas Lony")
                    ->bcc("sotnas.pina@outlook.com", "Sotnas Pina")
                    ->replyTo("suporte@emiolo.com", "Suporte eMiolo.com")
                    ->subject($subject)
                    ->with([
                        'content_body' => $data['content_body'] . "<br/>Do Host: " . env('MAIL_HOST') . "<br/>Enviado em: " . date("d/m/Y H:i:s")
                    ]);
    }
}
