<p align="center"><img width="80" src="./public/images/cerebelo/logo_cliente.png"></p>

<p align="center">

</p>

## ERT UNIFORMES | Uniformes Personalizados para Ciclismo

Desenvolvido com Laravel 5.3

## Pré-requisitos

- [Módulo de Configuração do Sistema](https://bitbucket.org/emiolo/modules-cerebelosettings).
- [Node](https://nodejs.org/en/).
- [Docker](https://www.docker.com/).

## Instalação

Clonar o projeto na sua máquina

```
    git clone git@bitbucket.org:developers_emiolo/ert.git
```

*Clonar o módulo principal dentro de `ert/Modules/`:*

```
    git clone git@bitbucket.org:emiolo/modules-cerebelosettings.git CerebeloSettings
```

na raiz do projeto:

criar o arquivo *.env* com o conteúdo abaixo e fazer as alterações para o ambiente
```
APP_ENV=local
APP_KEY=GERAR_UMA_KEY
APP_DEBUG=true
APP_LOG=daily
APP_LOG_LEVEL=debug
APP_URL=http://localhost

DB_CONNECTION=mysql
DB_HOST=10.0.0.249
DB_USERNAME=emiolo
DB_PASSWORD=SENHA_SERVIDOR_LOCAL
DB_PORT=3306
DB_DATABASE=ert

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailgun.org
MAIL_PORT=587
MAIL_USERNAME=postmaster@emiolo.com
MAIL_PASSWORD=SENHA_DA_CONTA
MAIL_ENCRYPTION=tls

PUSHER_APP_ID=
PUSHER_KEY=
PUSHER_SECRET=
```

instalar as dependências php
`composer update` ou `composer install`

instalar as bibliotecas do js
`npm install` e `bower install`

Para gerar as **views** os **javascripts** e os **css** dos módulos, rode os comandos abaixo

*views*, *javascripts*, *css*, *imagens*, *fontes* global do sistema (verificar as tarefas de cada múdulo no arquivo `gulpfile.js` na raiz do projeto)
```
    gulp cerebelo-geral
```

*javascript* e *css* internos dos múdulos

para todos os múdulos `php artisan module:publish`, para múdulos específicos `php artisan module:publish NOME_DO_MODULO`

## Docker

No arquivo *docker-compose.yaml* na raiz do projeto você configura os serviços/containers do docker.

Na pasta *./docker* da raiz do projeto você configura as dependências dos serviços/containers do docker.

Rodar `docker-compose up -d` para subir os containers

Para verificar os containers rodando `docker ps`

Para acessar o projeto *[http://localhost:8001](http://localhost:8001)*

Para matar os containers `docker-compose kill`

Para remover os containers `docker-compose rm`