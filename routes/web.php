<?php
/*
|--------------------------------------------------------------------------
| Financial
|--------------------------------------------------------------------------
*/


Route::group(['prefix' => 'dashboard-financial'], function () {
    Route::get('/', function () {
        return view('cerebelo/financial/dashboard/index');
    });
});


Route::group(['prefix' => 'contas_a_pagar'], function () {
    Route::get('/', function () {
        return view('cerebelo/financial/payments/index');
    });
    Route::get('/novo', function () {
        return view('cerebelo/financial/payments/create');
    });
    Route::get('/editar', function () {
        return view('cerebelo/financial/payments/edit');
    });
});

Route::group(['prefix' => 'contas_a_receber'], function () {
    Route::get('/', function () {
        return view('cerebelo/financial/incomings/index');
    });
    Route::get('/novo', function () {
        return view('cerebelo/financial/incomings/create');
    });
    Route::get('/editar', function () {
        return view('cerebelo/financial/incomings/edit');
    });
});

Route::group(['prefix' => 'caixa'], function () {
    Route::get('/', function () {
        return view('cerebelo/financial/cashier/index');
    });
});

Route::group(['prefix' => 'contas'], function () {
    Route::get('/', function () {
        return view('cerebelo/financial/account/index');
    });
    Route::get('/novo', function () {
        return view('cerebelo/financial/account/create');
    });
    Route::get('/editar', function () {
        return view('cerebelo/financial/account/edit');
    });
});

Route::group(['prefix' => 'empresas'], function () {
    Route::get('/', function () {
        return view('cerebelo/financial/companies/index');
    });
    Route::get('/novo', function () {
        return view('cerebelo/financial/companies/create');
    });
    Route::get('/editar', function () {
        return view('cerebelo/financial/companies/edit');
    });
});

Route::group(['prefix' => 'plano_de_contas'], function () {
    Route::get('/', function () {
        return view('cerebelo/financial/account_plan/index');
    });
});

Route::group(['prefix' => 'configurar_boleto'], function () {
    Route::get('/', function () {
        return view('cerebelo/financial/billet_settings/index');
    });
    Route::get('/novo', function () {
        return view('cerebelo/financial/billet_settings/create');
    });
    Route::get('/editar', function () {
        return view('cerebelo/financial/billet_settings/edit');
    });
});

Route::group(['prefix' => 'fornecedores'], function () {
    Route::get('/', function () {
        return view('cerebelo/financial/suppliers/index');
    });
    Route::get('/novo', function () {
        return view('cerebelo/financial/suppliers/create');
    });
    Route::get('/editar', function () {
        return view('cerebelo/financial/suppliers/edit');
    });
});

Route::group(['prefix' => 'funcionarios'], function () {
    Route::get('/', function () {
        return view('cerebelo/financial/employees/index');
    });
    Route::get('/novo', function () {
        return view('cerebelo/financial/employees/create');
    });
    Route::get('/editar', function () {
        return view('cerebelo/financial/employees/edit');
    });
});