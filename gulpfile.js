const elixir = require('laravel-elixir');

const gulp = require('gulp');
const pug = require('gulp-pug');
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

// ####################################################################################################
// ############################################  CEREBELO  ############################################
// ####################################################################################################
elixir(function (mix) {
    /*
     * Copiando as fontes para o /public
     */
    mix.copy('Modules/CerebeloSettings/Resources/assets/css/cerebelo/icons/fontawesome/fonts', 'public/css/cerebelo/fonts');
    mix.copy('Modules/CerebeloSettings/Resources/assets/css/cerebelo/icons/glyphicons', 'public/css/cerebelo/icons/glyphicons');
    mix.copy('Modules/CerebeloSettings/Resources/assets/css/cerebelo/icons/icomoon/fonts', 'public/css/cerebelo/fonts');
    mix.copy('Modules/CerebeloSettings/Resources/assets/css/cerebelo/font', 'public/css/cerebelo/fonts');
    /*
     * Copiando as imagens para o /public
     */
    mix.copy('Modules/CerebeloSettings/Resources/assets/images/cerebelo', 'public/images/cerebelo');

    /*
     * Copiar arquivos de tradução de Módulo de CerebeloSettings para a raiz
     */
    mix.copy('Modules/CerebeloSettings/Resources/lang', 'resources/lang');

    /*
     * Includes do CSS Geral
     */
    mix.styles([
        '../../../Modules/CerebeloSettings/Resources/assets/css/cerebelo/extras/animate.min.css',
        '../../../Modules/CerebeloSettings/Resources/assets/css/cerebelo/icons/icomoon/styles.css',
        '../../../Modules/CerebeloSettings/Resources/assets/css/cerebelo/icons/fontawesome/styles.min.css',
        '../../../Modules/CerebeloSettings/Resources/assets/css/cerebelo/bootstrap.css',
        '../../../Modules/CerebeloSettings/Resources/assets/css/cerebelo/core.css',
        '../../../Modules/CerebeloSettings/Resources/assets/css/cerebelo/components.css',
        '../../../Modules/CerebeloSettings/Resources/assets/css/cerebelo/colors.css',
        '../../../Modules/CerebeloSettings/Resources/assets/css/cerebelo/summernote.css',
        '../../../Modules/CerebeloSettings/Resources/assets/css/cerebelo/extras/fab.css',
        '../../../bower_components/datetimepicker/jquery.datetimepicker.css',
        '../../../Modules/CerebeloSettings/Resources/assets/css/cerebelo/extras/cerebelo.css',
        //'../../../Modules/CerebeloSettings/Resources/assets/css/cerebelo/layout.css',
        '../../../Modules/CerebeloSettings/Resources/assets/css/cerebelo/custom.css',
        '../../../Modules/CerebeloSettings/Assets/css/alerts-sys/index.css',
        '../../../bower_components/bootstrap-suggest/bootstrap-suggest.css',
    ], 'public/css/cerebelo/application.css');
    /*
     * Includes do CSS Específicos
     */


    /*
     * Includes do JS Geral 
     */
    mix
            .scripts([
                //<!-- Core JS files -->
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/loaders/pace.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/core/libraries/jquery.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/jquery-ui.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/core/libraries/bootstrap.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/loaders/blockui.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/blockUiSettings.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/jquery.number.min.js',
                //<!-- Theme JS files -->
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/notifications/bootbox.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/notifications/pnotify.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/notifications/sweet_alert.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/forms/tags/tagsinput.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/core/libraries/jquery_ui/interactions.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/forms/selects/select2.full.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/forms/inputs/formatter.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/forms/wizards/steps.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/forms/tags/tagsinput.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/forms/tags/tokenfield.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/media/fancybox.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/editors/summernote/summernote.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/forms/styling/uniform.min.js',
            ], 'public/js/cerebelo/application.js')
            .scripts([
                //<!-- Core JS files -->
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/forms/selects/bootstrap_multiselect.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/forms/styling/switchery.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/forms/styling/switch.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/ui/moment/moment.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/pickers/daterangepicker.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/pickers/datepicker.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/pickers/jquery.datetimepicker.full.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/pickers/pickadate/picker.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/pickers/pickadate/picker.date.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/ui/nicescroll.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/tables/datatables/datatables.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/tables/datatables/extensions/buttons.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/tables/datatables/extensions/row_reorder.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/tables/datatables/extensions/fixed_columns.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/tables/handsontable/handsontable.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/forms/inputs/duallistbox.min.js',
            ], 'public/js/cerebelo/application2.js')
            .scripts([
                //<!-- Core JS files -->
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/uploaders/fileinput.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/forms/inputs/typeahead/typeahead.bundle.min.js',
                '../../../public/vendor/jsvalidation/js/jsvalidation.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/media/cropper.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/loaders/progressbar.min.js',
                //'../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/ui/fab.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/ui/prism.min.js',
                // Start - Tree
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/core/libraries/jquery_ui/core.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/core/libraries/jquery_ui/effects.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/trees/fancytree_all.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/plugins/trees/fancytree_childcounter.js',
                // End - Tree
                '../../../bower_components/jquery-ujs/src/rails.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/countdown.min.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/jquery.mask.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/datatable-pt-br.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/summernote-pt-BR.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/printElement.js',
                //'../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/datepicker_pt_br.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/simple-excel.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/confirmationRailsJS.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/core/app.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/pages/layout_fixed_custom.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/jquery.form.min.js',
                '../../../bower_components/bootstrap-suggest/bootstrap-suggest.js',
                //'../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/jquery.a-tools-1.4.1.js',
                //'../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/jquery.asuggest.js',
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/list.min.js',
                //<!-- Extras -->
                '../../../Modules/CerebeloSettings/Resources/assets/js/cerebelo/cerebelo.js',
                '../../../Modules/CerebeloOrders/Assets/js/notification.js',
                '../../../Modules/CerebeloSettings/Assets/js/alerts-sys/index.js',
            ], 'public/js/cerebelo/application3.js')
            ;
    /*
     * Includes do JS Específicos
     */


    /*
     * PUG
     * Gerar arquivos .blade.php e add em módulos
     * Estrutura:
     *  basepath: pub
     *  modulepath: Modules/Resources/views/...
     */
    gulp.task('blade-cr', function () {// Módulo de CerebeloRepresentatives
        var src = 'Modules/CerebeloRepresentatives/Resources/pug/views/**/';
        var dist = 'Modules/CerebeloRepresentatives/Resources/views/';
        gulp.src(src + '*.pug')
                .pipe(plumber())
                .pipe(pug({pretty: true, extension: '.blade.php'}))
                .pipe(rename({
                    extname: ".blade.php"
                }))
                .pipe(gulp.dest(dist));
    });
    gulp.task('blade-cc', function () {// Módulo de CerebeloClient
        var src = 'Modules/CerebeloClient/Resources/pug/views/**/';
        var dist = 'Modules/CerebeloClient/Resources/views/';
        gulp.src(src + '*.pug')
                .pipe(plumber())
                .pipe(pug({pretty: true, extension: '.blade.php'}))
                .pipe(rename({
                    extname: ".blade.php"
                }))
                .pipe(gulp.dest(dist));
    });
    gulp.task('blade-cd', function () {// Módulo de CerebeloDashboard
        var src = 'Modules/CerebeloDashboard/Resources/pug/views/**/';
        var dist = 'Modules/CerebeloDashboard/Resources/views/';
        gulp.src(src + '*.pug')
                .pipe(plumber())
                .pipe(pug({pretty: true, extension: '.blade.php'}))
                .pipe(rename({
                    extname: ".blade.php"
                }))
                .pipe(gulp.dest(dist));
    });
    gulp.task('blade-cp', function () {// Módulo de CerebeloProducts
        var src = 'Modules/CerebeloProducts/Resources/pug/views/**/';
        var dist = 'Modules/CerebeloProducts/Resources/views/';
        gulp.src(src + '*.pug')
                .pipe(plumber())
                .pipe(pug({pretty: true, extension: '.blade.php'}))
                .pipe(rename({
                    extname: ".blade.php"
                }))
                .pipe(gulp.dest(dist));
    });
    gulp.task('blade-ct', function () {// Módulo de CerebeloTable
        var src = 'Modules/CerebeloTable/Resources/pug/views/**/';
        var dist = 'Modules/CerebeloTable/Resources/views/';
        gulp.src(src + '*.pug')
                .pipe(plumber())
                .pipe(pug({pretty: true, extension: '.blade.php'}))
                .pipe(rename({
                    extname: ".blade.php"
                }))
                .pipe(gulp.dest(dist));
    });
    gulp.task('blade-co', function () {// Módulo de CerebeloOrders
        var src = 'Modules/CerebeloOrders/Resources/pug/views/**/';
        var dist = 'Modules/CerebeloOrders/Resources/views/';
        gulp.src(src + '*.pug')
                .pipe(plumber())
                .pipe(pug({pretty: true, extension: '.blade.php'}))
                .pipe(rename({
                    extname: ".blade.php"
                }))
                .pipe(gulp.dest(dist));
    });
    gulp.task('blade-cs', function () {// Módulo de CerebeloSettings
        var src = 'Modules/CerebeloSettings/Resources/pug/views/**/';
        var dist = 'Modules/CerebeloSettings/Resources/views/';
        gulp.src(src + '*.pug')
                .pipe(plumber())
                .pipe(pug({pretty: true, extension: '.blade.php'}))
                .pipe(rename({
                    extname: ".blade.php"
                }))
                .pipe(gulp.dest(dist));
    });
    gulp.task('blade-cv', function () {// Módulo de CerebeloVariations
        var src = 'Modules/CerebeloVariations/Resources/pug/views/**/';
        var dist = 'Modules/CerebeloVariations/Resources/views/';
        gulp.src(src + '*.pug')
                .pipe(plumber())
                .pipe(pug({pretty: true, extension: '.blade.php'}))
                .pipe(rename({
                    extname: ".blade.php"
                }))
                .pipe(gulp.dest(dist));
    });
    gulp.task('blade-cf', function () {// Módulo Financeiro
        var src = 'Modules/CerebeloFinancial/Resources/pug/views/**/';
        var dist = 'Modules/CerebeloFinancial/Resources/views/';
        gulp.src(src + '*.pug')
                .pipe(plumber())
                .pipe(pug({pretty: true, extension: '.blade.php'}))
                .pipe(rename({
                    extname: ".blade.php"
                }))
                .pipe(gulp.dest(dist));
    });
    gulp.task('blade-ctp', function () {// Módulo de Transportadoras
        var src = 'Modules/CerebeloTransports/Resources/pug/views/**/';
        var dist = 'Modules/CerebeloTransports/Resources/views/';
        gulp.src(src + '*.pug')
                .pipe(plumber())
                .pipe(pug({pretty: true, extension: '.blade.php'}))
                .pipe(rename({
                    extname: ".blade.php"
                }))
                .pipe(gulp.dest(dist));
    });
    gulp.task('blade-ctv', function () {// Módulo de Valores das Tabelas
        var src = 'Modules/CerebeloTablesValues/Resources/pug/views/**/';
        var dist = 'Modules/CerebeloTablesValues/Resources/views/';
        gulp.src(src + '*.pug')
                .pipe(plumber())
                .pipe(pug({pretty: true, extension: '.blade.php'}))
                .pipe(rename({
                    extname: ".blade.php"
                }))
                .pipe(gulp.dest(dist));
    });
    gulp.task('blade-ccp', function () {// Módulo de Empresas (Nota Fiscal)
        var src = 'Modules/CerebeloCompanies/Resources/pug/views/**/';
        var dist = 'Modules/CerebeloCompanies/Resources/views/';
        gulp.src(src + '*.pug')
                .pipe(plumber())
                .pipe(pug({pretty: true, extension: '.blade.php'}))
                .pipe(rename({
                    extname: ".blade.php"
                }))
                .pipe(gulp.dest(dist));
    });
    gulp.task('blade-cos', function () {// Módulo de Status do Pedido
        var src = 'Modules/CerebeloOrdersStatus/Resources/pug/views/**/';
        var dist = 'Modules/CerebeloOrdersStatus/Resources/views/';
        gulp.src(src + '*.pug')
                .pipe(plumber())
                .pipe(pug({pretty: true, extension: '.blade.php'}))
                .pipe(rename({
                    extname: ".blade.php"
                }))
                .pipe(gulp.dest(dist));
    });
    gulp.task('blade-coq', function () {// Módulo de CerebeloOrderQueue
        var src = 'Modules/CerebeloOrderQueue/Resources/pug/views/**/';
        var dist = 'Modules/CerebeloOrderQueue/Resources/views/';
        gulp.src(src + '*.pug')
                .pipe(plumber())
                .pipe(pug({pretty: true, extension: '.blade.php'}))
                .pipe(rename({
                    extname: ".blade.php"
                }))
                .pipe(gulp.dest(dist));
    });
    gulp.task('trans-copy', function () {// Copiar arquivos de tradução de Módulo de CerebeloSettings para a raiz

    });
    gulp.task('cerebelo-error-pages', function () {// Páginas de erro
        var src = 'Modules/CerebeloSettings/Resources/pug/errors/';
        var dist = 'resources/views/errors/';
        gulp.src(src + '*.pug')
                .pipe(plumber())
                .pipe(pug({pretty: true, extension: '.blade.php'}))
                .pipe(rename({
                    extname: ".blade.php"
                }))
                .pipe(gulp.dest(dist));
    });

    /*
     * Rodar todo o conteudo do Cerebelo CSS/JS/IMAGENS/FONTES/BLADES...
     */
    gulp.task('cerebelo-modules', [
        'blade-cr',
        'blade-cc',
        'blade-cd',
        'blade-cp',
        'blade-ct',
        'blade-cs',
        'blade-co',
        'blade-cv',
        'blade-cf',
        'blade-ctp',
        'blade-ctv',
        'blade-ccp',
        'blade-cos',
        'blade-coq',
        'cerebelo-error-pages'
    ]);
    gulp.task('cerebelo-geral', [
        'default',
        'cerebelo-modules'
    ]);
});