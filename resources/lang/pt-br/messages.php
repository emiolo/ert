<?php

return [
    'success' => [
        'export_preparing' => 'Estamos a preparar a sua exportação. Nós lhe enviaremos um email quando ele estiver pronto.',
        'import_preparing' => 'Importação em progresso, após término você receberá um email.',
        'import' => 'Importação efetuada com sucesso.',
        'create' => 'Registro salvo com sucesso.',
        'update' => 'Registro atualizado com sucesso.',
        'destroy' => 'Registro removido com sucesso.',
        'duplicate' => 'Registro duplicado com sucesso.',
        'start_flow' => 'Fluxo iniciado com sucesso',
    ],
    'remove' => 'Deseja realmente excluir este registro?',
    'error' => [
        'create' => 'Não foi possível efetuar o cadastro.',
        'update' => 'Não foi possível alterar o registro.',
        'destroy' => 'Não foi possível excluir o registro.',
        'file_not_exist' => 'Arquivo não encontrado.',
        'export_preparing' => 'Não foi possível processar os dados para exportar.',
        'import_preparing' => 'Não foi possível processar os dados para importação.',
        'mailing' => 'Cadastro não realizado, não foi possível enviar o email.',
        'crud' => 'Possível erro técnico, tente novamente.',
        'empty' => 'Nenhum item foi processado.',
        'fields' => 'Campos do tipo Data e Hora não podem ser alterados para outros tipos.',
        'start_flow' => "O fluxo não pôde ser iniciado.",
    ],
    'active' => 'Deseja realmente ativar este registro?',
    'deactivate' => 'Deseja realmente desativar este registro?',
    'nonrecoverable' => 'Não será possível recuperar este registro depois de excluído!',
    'duplicate' => 'Deseja realmente duplicar o registro?',
    'credentials' => [
        'error' => 'Credenciais incorretas.'
    ],
    'recovery' => [
        'mail' => [
            'success' => 'Uma nova senha foi enviada para o email %s.',
            'error' => 'Não foi possível enviar a nova senha.',
        ],
        'empty' => 'E-mail não localizado.',
    ],
];
