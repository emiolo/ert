
<h6>Dados Pessoais</h6>
<fieldset>
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label class="col-lg-3 control-label">Foto:</label>
        <div class="col-lg-9 mb-15">
          <input class="file-styled" type="file"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Data de contratação:</label>
        <div class="col-lg-9">
          <div class="row">
            <div class="col-md-6">
              <input class="form-control mb-15" type="text" placeholder="contratação"/>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Nome:</label>
        <div class="col-lg-9">
          <div class="row">
            <div class="col-md-6">
              <input class="form-control mb-15" type="text" placeholder="Primeiro Nome"/>
            </div>
            <div class="col-md-6">
              <input class="form-control mb-15" type="text" placeholder="Sobrenome"/>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Sexo:</label>
        <div class="col-lg-9">
          <input class="form-control mb-15" type="text" placeholder="-"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Data de Nascimento:</label>
        <div class="col-lg-9">
          <input class="form-control mb-15" type="text" placeholder="dd/mm/aaaa"/>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label class="col-lg-3 control-label">CPF:</label>
        <div class="col-lg-9">
          <input class="form-control mb-15" type="text" placeholder="000.000.000-00"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">RG:</label>
        <div class="col-lg-9">
          <div class="row">
            <div class="col-md-6">
              <input class="form-control mb-15" type="text" placeholder="Número"/>
              <input class="form-control mb-15" type="text" placeholder="Órgão Expedidor"/>
            </div>
            <div class="col-md-6">
              <input class="form-control mb-15" type="text" placeholder="Data de Expedição"/>
              <div class="mb-15">
                <select class="select" data-placeholder="UF">
                  <option></option>
                  <option value="1">MG</option>
                  <option value="2">RG</option>
                  <option value="3">SP</option>
                  <option value="4">ES</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Estado Cívil:</label>
        <div class="col-lg-9">
          <input class="form-control" type="text" placeholder="-"/>
        </div>
      </div>
    </div>
  </div>
</fieldset>
<h6>Dados de Contato</h6>
<fieldset>
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label class="col-lg-3 control-label">Email:</label>
        <div class="col-lg-9">
          <input class="form-control mb-15" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Telefone 01:</label>
        <div class="col-lg-9">
          <input class="form-control mb-15" type="text" placeholder="+99-99-9999-9999"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Telefone 02:</label>
        <div class="col-lg-9">
          <input class="form-control mb-15" type="text" placeholder="+99-99-9999-9999"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Celular:</label>
        <div class="col-lg-9">
          <input class="form-control mb-15" type="text" placeholder="+99-99-9999-9999"/>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label class="col-lg-3 control-label">Endereço:</label>
        <div class="col-lg-9">
          <div class="row">
            <div class="col-md-6">
              <input class="form-control mb-15" type="text" placeholder="CEP"/>
            </div>
            <div class="col-md-12">
              <input class="form-control mb-15" type="text" placeholder="Endereço"/>
            </div>
            <div class="col-md-6">
              <input class="form-control mb-15" type="text" placeholder="Número"/>
              <input class="form-control" type="text" placeholder="Cidade"/>
            </div>
            <div class="col-md-6">
              <input class="form-control mb-15" type="text" placeholder="Complemento"/>
              <div class="mb-15">
                <select class="select" data-placeholder="Estado">
                  <option></option>
                  <option value="1">Canada</option>
                  <option value="2">USA</option>
                  <option value="3">Australia</option>
                  <option value="4">Germany</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</fieldset>
<h6>Dados Profissionais</h6>
<fieldset>
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label class="col-lg-3 control-label">Setor:</label>
        <div class="col-lg-9 mb-15">
          <select class="select" data-placeholder="UF">
            <option></option>
            <option value="1">MG</option>
            <option value="2">RG</option>
            <option value="3">SP</option>
            <option value="4">ES</option>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Cargo:</label>
        <div class="col-lg-9 mb-15">
          <select class="select" data-placeholder="UF">
            <option></option>
            <option value="1">MG</option>
            <option value="2">RG</option>
            <option value="3">SP</option>
            <option value="4">ES</option>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Função:</label>
        <div class="col-lg-9 mb-15">
          <select class="select" data-placeholder="UF">
            <option></option>
            <option value="1">MG</option>
            <option value="2">RG</option>
            <option value="3">SP</option>
            <option value="4">ES</option>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Nível:</label>
        <div class="col-lg-9 mb-15">
          <select class="select" data-placeholder="UF">
            <option></option>
            <option value="1">MG</option>
            <option value="2">RG</option>
            <option value="3">SP</option>
            <option value="4">ES</option>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Turno(s):</label>
        <div class="col-lg-9 mb-15">
          <select class="select" data-placeholder="UF">
            <option></option>
            <option value="1">MG</option>
            <option value="2">RG</option>
            <option value="3">SP</option>
            <option value="4">ES</option>
          </select>
        </div>
      </div>
    </div>
    <!--.col-md-12
    .panel-heading
        h6.text-semibold
            | Remuneração e Benefícios
    
    .col-md-6
        .form-group
            label.col-lg-3.control-label Remuneração:
            .col-lg-9
                input.form-control(type='text')
    .col-md-6
        .form-group
            label.text-semibold.col-md-12 Benefício(s)
            .col-md-4
                .checkbox
                    label
                        input(type='checkbox', checked='checked')
                        | Vale Alimentação
                .checkbox
                    label
                        input(type='checkbox')
                        | Plano de Saúde
                .checkbox
                    label
                        input(type='checkbox', checked='checked')
                        | Plano ...
            .col-md-4
                .checkbox
                    label
                        input(type='checkbox', checked='checked')
                        | Vale Alimentação
                .checkbox
                    label
                        input(type='checkbox')
                        | Plano de Saúde
                .checkbox
                    label
                        input(type='checkbox', checked='checked')
                        | Plano ...
    -->
  </div>
</fieldset>
<h6>Informações Adicionais</h6>
<fieldset>
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label class="col-lg-3 control-label">Possui dependentes?</label>
        <div class="col-lg-9">
          <label class="radio-inline">
            <input type="radio" name="radio-unstyled-inline-left" checked="checked"/>Sim
          </label>
          <label class="radio-inline">
            <input type="radio" name="radio-unstyled-inline-left"/>Não
          </label>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-12 control-label">Cadastrar Dependente:</label>
        <div class="col-lg-12">
          <div class="row">
            <div class="col-md-6">
              <input class="form-control mb-15" type="text" placeholder="Nome"/>
            </div>
            <div class="col-md-6">
              <div class="mb-15">
                <select class="select" data-placeholder="Parentesco">
                  <option></option>
                  <option value="1">Canada</option>
                  <option value="2">USA</option>
                  <option value="3">Australia</option>
                  <option value="4">Germany</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12 text-right">
        <button class="btn btn-primary" type="submit">Adicionar Dependente<i class="icon-arrow-right14 position-right"></i></button>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Observações</label>
        <textarea class="form-control" name="additional-info" rows="5" cols="5" placeholder="If you want to add any info, do it here."></textarea>
      </div>
    </div>
  </div>
</fieldset>