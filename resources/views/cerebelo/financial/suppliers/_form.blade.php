
<div class="row">
  <div class="col-md-6">
    <fieldset>
      <legend class="text-semibold"><i class="icon-truck position-left"></i> Dados do Fornecedor</legend>
      <div class="form-group">
        <label class="col-lg-3 control-label">Tipo de Fornecedor</label>
        <div class="col-lg-9">
          <label class="radio-inline">
            <input type="radio" name="radio-unstyled-inline-left" checked="checked"/>
            <Pessoa>Física</Pessoa>
          </label>
          <label class="radio-inline">
            <input type="radio" name="radio-unstyled-inline-left"/>
            <Pessoa>Jurídica</Pessoa>
          </label>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">CNAE - Ramo de Atividade:</label>
        <div class="col-lg-9">
          <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Razão Social:</label>
        <div class="col-lg-9">
          <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Nome Fantasia:</label>
        <div class="col-lg-9">
          <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6">
              <label class="control-label">CNPJ:</label>
              <input class="form-control" type="text" placeholder="Primeiro Nome"/>
            </div>
            <div class="col-md-6">
              <label class="control-label">Inscrição Estadual:</label>
              <input class="form-control" type="text" placeholder="Sobrenome"/>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Observações</label>
        <div class="col-lg-9">
          <textarea class="form-control" rows="5" cols="5" placeholder="Enter your message here"></textarea>
        </div>
      </div>
    </fieldset>
  </div>
  <div class="col-md-6">
    <fieldset>
      <legend class="text-semibold"><i class="icon-reading position-left"></i> Informações de Contato</legend>
      <div class="form-group">
        <label class="col-lg-3 control-label">Email:</label>
        <div class="col-lg-9">
          <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Telefone:</label>
        <div class="col-lg-9">
          <input class="form-control" type="text" placeholder="+99-99-9999-9999"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Celular:</label>
        <div class="col-lg-9">
          <input class="form-control" type="text" placeholder="+99-99-9999-9999"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Endereço:</label>
        <div class="col-lg-9">
          <div class="row">
            <div class="col-md-6">
              <input class="form-control mb-15" type="text" placeholder="CEP"/>
            </div>
            <div class="col-md-12">
              <input class="form-control mb-15" type="text" placeholder="Endereço"/>
            </div>
            <div class="col-md-6">
              <input class="form-control mb-15" type="text" placeholder="Número"/>
              <input class="form-control" type="text" placeholder="Cidade"/>
            </div>
            <div class="col-md-6">
              <input class="form-control mb-15" type="text" placeholder="Complemento"/>
              <div class="mb-15">
                <select class="select" data-placeholder="Estado">
                  <option></option>
                  <option value="1">Canada</option>
                  <option value="2">USA</option>
                  <option value="3">Australia</option>
                  <option value="4">Germany</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Site:</label>
        <div class="col-lg-9">
          <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
    </fieldset>
  </div>
</div>
<div class="text-right">
  <button class="btn btn-default fechar" type="submit">Cancelar<i class="icon-cross3 position-right"></i></button>
  <button class="btn btn-primary salvar" type="submit">Salvar<i class="icon-arrow-right14 position-right"></i></button>
</div>