
<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label class="col-lg-3 control-label">Nome da Conta:</label>
      <div class="col-lg-9">
        <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
      </div>
    </div>
    <div class="form-group">
      <label class="col-lg-3 control-label">Selecione o Banco</label>
      <div class="col-lg-9">
        <select class="select">
          <optgroup label="Mountain Time Zone">
            <option value="AZ">Arizona</option>
            <option value="CO">Colorado</option>
            <option value="ID">Idaho</option>
            <option value="WY">Wyoming</option>
          </optgroup>
          <optgroup label="Central Time Zone">
            <option value="AL">Alabama</option>
            <option value="AR">Arkansas</option>
            <option value="KS">Kansas</option>
            <option value="KY">Kentucky</option>
          </optgroup>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-lg-3 control-label">Agência:</label>
      <div class="col-lg-9">
        <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label class="col-lg-3 control-label">Conta:</label>
      <div class="col-lg-9">
        <div class="row">
          <div class="col-md-6">
            <input class="form-control mb-15" type="text" placeholder="Conta"/>
          </div>
          <div class="col-md-6">
            <div class="mb-15">
              <select class="select" data-placeholder="Tipo de Conta">
                <option></option>
                <option value="1">Canada</option>
                <option value="2">USA</option>
                <option value="3">Australia</option>
                <option value="4">Germany</option>
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-lg-3 control-label">Observações</label>
      <div class="col-lg-9">
        <textarea class="form-control" rows="5" cols="5" placeholder="Enter your message here"></textarea>
      </div>
    </div>
  </div>
</div>
<div class="text-right">
  <button class="btn btn-default fechar" type="submit">Cancelar<i class="icon-cross3 position-right"></i></button>
  <button class="btn btn-primary salvar" type="submit">Salvar<i class="icon-arrow-right14 position-right"></i></button>
</div>