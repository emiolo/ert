
<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label class="col-lg-3 control-label">Nome da Conta:</label>
      <div class="col-lg-9">
        <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
      </div>
    </div>
    <div class="form-group">
      <label class="col-lg-3 control-label">Plano de Contas</label>
      <div class="col-lg-9 select-plano-de-contas">
        <select class="select">
          <optgroup class="cat1" label="Despesas Fixas">
            <option class="cat2" value="AZ" disabled="disabled">Arizona</option>
            <option class="cat3" value="CO">Colorado</option>
            <option class="cat3" value="ID">Idaho</option>
            <option class="cat3" value="WY">Wyoming</option>
            <option class="cat2" value="AZ" disabled="disabled">Arizona</option>
            <option class="cat3" value="CO">Colorado</option>
            <option class="cat3" value="ID">Idaho</option>
            <option class="cat3" value="WY">Wyoming</option>
          </optgroup>
          <optgroup class="cat1" label="Despesas Extras">
            <option class="cat2" value="AZ" disabled="disabled">Arizona</option>
            <option class="cat3" value="CO">Colorado</option>
            <option class="cat3" value="ID">Idaho</option>
            <option class="cat3" value="WY">Wyoming</option>
          </optgroup>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-lg-3 control-label">Tipo (mudar nome, não é sugestivo)</label>
      <div class="col-lg-9">
        <select class="select">
          <option value="AZ">Fornecedor</option>
          <option value="CO">Funcionário</option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-lg-3 control-label">Fornecedor (ou Funcionário)</label>
      <div class="col-lg-9">
        <select class="select">
          <optgroup class="cat1" label="Professor">
            <option class="cat2" value="AZ" disabled="disabled">Arizona</option>
            <option value="CO">Colorado</option>
            <option value="ID">Idaho</option>
            <option value="WY">Wyoming</option>
          </optgroup>
          <optgroup label="Secertárias">
            <option value="AL">Alabama</option>
            <option value="AR">Arkansas</option>
            <option value="KS">Kansas</option>
            <option value="KY">Kentucky</option>
          </optgroup>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-lg-3 control-label">Tipo de Negociação</label>
      <div class="col-lg-9">
        <select class="select">
          <option value="AZ">Normal</option>
          <option value="CO">Permuta</option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-lg-3 control-label">Forma de lançamento</label>
      <div class="col-lg-9">
        <select class="select">
          <option>Lançamento simples</option>
          <option>Lançamento recorrente</option>
        </select>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <h4 class="no-margin">Simples</h4>
    <div class="form-group">
      <label class="col-lg-3 control-label">Parcelas</label>
      <div class="col-lg-9">
        <select class="select">
          <option>01</option>
          <option>02</option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-lg-3 control-label">Parcela 01:</label>
      <div class="col-lg-9">
        <div class="row">
          <div class="col-md-5">
            <input class="form-control mb-15" type="text" placeholder="Valor"/>
          </div>
          <div class="col-md-4">
            <input class="form-control mb-15" type="text" placeholder="Vencimento"/>
          </div>
          <div class="col-md-3">
            <select class="select">
              <option>Dinheiro</option>
              <option>Cheque</option>
              <option>Depósito</option>
              <option>Boleto</option>
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-lg-3 control-label">Alterações:</label>
      <div class="col-lg-9">
        <div class="row">
          <div class="col-md-4">
            <input class="form-control mb-15" type="text" placeholder="Juros"/>
          </div>
          <div class="col-md-4">
            <input class="form-control mb-15" type="text" placeholder="Acréscimos"/>
          </div>
          <div class="col-md-4">
            <input class="form-control mb-15" type="text" placeholder="Descontos"/>
          </div>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-lg-3 control-label">Observações</label>
      <div class="col-lg-9">
        <textarea class="form-control" rows="5" cols="5" placeholder="Enter your message here"></textarea>
      </div>
    </div>
    <h4 class="no-margin">Recorrente</h4>
    <div class="form-group">
      <label class="col-lg-3 control-label">Valor/Vencimento:</label>
      <div class="col-lg-9">
        <div class="row">
          <div class="col-md-7">
            <input class="form-control mb-15" type="text" placeholder="Valor"/>
          </div>
          <div class="col-md-5">
            <input class="form-control mb-15" type="text" placeholder="dd/mm/aaaa"/>
          </div>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-lg-3 control-label">Alterações:</label>
      <div class="col-lg-9">
        <div class="row">
          <div class="col-md-4">
            <input class="form-control mb-15" type="text" placeholder="Juros"/>
          </div>
          <div class="col-md-4">
            <input class="form-control mb-15" type="text" placeholder="Acréscimos"/>
          </div>
          <div class="col-md-4">
            <input class="form-control mb-15" type="text" placeholder="Descontos"/>
          </div>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-lg-3 control-label">Repetição:</label>
      <div class="col-lg-9">
        <div class="row">
          <div class="col-lg-3">
            <select class="select">
              <option>Dias da semana</option>
              <option>Semanal</option>
              <option>Mensal</option>
              <option>Anual</option>
            </select>
          </div>
          <div class="col-lg-9">
            <div class="checkbox">
              <label class="checkbox-inline">
                <input class="styled" type="checkbox" checked="checked"/>	D
              </label>
              <label class="checkbox-inline">
                <input class="styled" type="checkbox" checked="checked"/>	S
              </label>
              <label class="checkbox-inline">
                <input class="styled" type="checkbox" checked="checked"/>	T
              </label>
              <label class="checkbox-inline">
                <input class="styled" type="checkbox" checked="checked"/>	Q
              </label>
              <label class="checkbox-inline">
                <input class="styled" type="checkbox" checked="checked"/>	Q
              </label>
              <label class="checkbox-inline">
                <input class="styled" type="checkbox" checked="checked"/>	S
              </label>
              <label class="checkbox-inline">
                <input class="styled" type="checkbox" checked="checked"/>	S
              </label>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-lg-3 control-label">Repete a cada:</label>
      <div class="col-lg-9">
        <select class="select">
          <option>01</option>
          <option>02</option>
          <option>03</option>
          <option>04</option>
        </select><span class="help-block">Semana/Mês/Ano</span>
      </div>
    </div>
    <div class="form-group">
      <label class="col-lg-3 control-label">Detalhes:</label>
      <div class="col-lg-9">
        <div class="row">
          <div class="col-md-4">
            <input class="form-control mb-15" type="text" placeholder="Início em:"/>
          </div>
          <div class="col-md-4">
            <select class="select">
              <option>Nunca termina</option>
              <option>Termina na data:</option>
              <option>Terminar por ocorrência</option>
            </select>
          </div>
          <div class="col-md-4">
            <input class="form-control mb-15" type="text" placeholder="-"/>
          </div>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-lg-3 control-label">Observações</label>
      <div class="col-lg-9">
        <textarea class="form-control" rows="5" cols="5" placeholder="Enter your message here"></textarea>
      </div>
    </div>
  </div>
</div>
<div class="text-right">
  <button class="btn btn-default fechar" type="submit">Cancelar<i class="icon-cross3 position-right"></i></button><a class="btn btn-primary salvar" href="{{ url('contas_a_pagar') }}">Salvar<i class="icon-arrow-right14 position-right"></i></a>
  <!--button.btn.btn-primary.salvar(type='submit')
  | Salvar
  i.icon-arrow-right14.position-right
  -->
</div>