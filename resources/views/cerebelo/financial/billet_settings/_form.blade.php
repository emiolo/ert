
<h6>Cadastrar</h6>
<fieldset>
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label class="col-lg-3 control-label">Selecione a Conta Bancária:</label>
        <div class="col-lg-9 mb-15">
          <select class="select" data-placeholder="UF">
            <option></option>
            <option value="1">MG</option>
            <option value="2">RG</option>
            <option value="3">SP</option>
            <option value="4">ES</option>
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Nº do Convênio:</label>
        <div class="col-lg-9">
          <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label class="col-lg-3 control-label">Nº do Contrato:</label>
        <div class="col-lg-9 mb-15">
          <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Carteira:</label>
        <div class="col-lg-9 mb-15">
          <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Variação da Carteira:</label>
        <div class="col-lg-9 mb-15">
          <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
    </div>
  </div>
</fieldset>
<h6>Configurar</h6>
<fieldset>
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label class="col-lg-3 control-label">Logo:</label>
        <div class="col-lg-9 mb-15">
          <input class="form-control file-styled" type="file"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Identificação</label>
        <div class="col-lg-9 mb-15">
          <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">CNPJ</label>
        <div class="col-lg-9 mb-15">
          <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Cedente:</label>
        <div class="col-lg-9 mb-15">
          <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Taxa :</label>
        <div class="col-lg-9 mb-15">
          <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Endereço:</label>
        <div class="col-lg-9">
          <div class="row">
            <div class="col-md-6">
              <input class="form-control mb-15" type="text" placeholder="CEP"/>
            </div>
            <div class="col-md-12">
              <input class="form-control mb-15" type="text" placeholder="Endereço"/>
            </div>
            <div class="col-md-6">
              <input class="form-control mb-15" type="text" placeholder="Número"/>
              <input class="form-control" type="text" placeholder="Cidade"/>
            </div>
            <div class="col-md-6">
              <input class="form-control mb-15" type="text" placeholder="Complemento"/>
              <div class="mb-15">
                <select class="select" data-placeholder="Estado">
                  <option></option>
                  <option value="1">Canada</option>
                  <option value="2">USA</option>
                  <option value="3">Australia</option>
                  <option value="4">Germany</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label class="col-lg-3 control-label">Demonstrativo 1 :</label>
        <div class="col-lg-9 mb-15">
          <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Demonstrativo 2 :</label>
        <div class="col-lg-9 mb-15">
          <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Demonstrativo 3 :</label>
        <div class="col-lg-9 mb-15">
          <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Instruções 1 :</label>
        <div class="col-lg-9 mb-15">
          <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Instruções 2 :</label>
        <div class="col-lg-9 mb-15">
          <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Instruções 3 :</label>
        <div class="col-lg-9 mb-15">
          <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
      <div class="form-group">
        <label class="col-lg-3 control-label">Instruções 4 :</label>
        <div class="col-lg-9 mb-15">
          <input class="form-control" type="text" placeholder="eugene@kopyov.com"/>
        </div>
      </div>
    </div>
  </div>
</fieldset>